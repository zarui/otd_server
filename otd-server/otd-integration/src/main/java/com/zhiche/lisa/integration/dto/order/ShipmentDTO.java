package com.zhiche.lisa.integration.dto.order;

import com.zhiche.lisa.integration.anno.FiledAnno;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 装车单DTO
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
public class ShipmentDTO {

    private static final long serialVersionUID = 1L;

    /**
     * 装车单唯一标识
     */
    @FiledAnno("shipmentId")
    private String shipmentId;
    /**
     * 指令类型
     */
    @FiledAnno("shipmentType")
    private String shipmentType;
    /**
     * 数据处理方式(IU:新增,R:更新,RC:删除)
     */
    private String transactionCode;
    /**
     * 运输方式编码
     */
    private String transportModeId;
    /**
     * 分供方唯一键
     */
    private String serviceProviderId;
    /**
     * 分供方名称
     */
    @FiledAnno("serviceProviderName")
    private String serviceProviderName;
    /**
     * 司机唯一标识
     */
    private String driverId;
    /**
     * 司机姓名
     */
    @FiledAnno("driverName")
    private String driverName;
    /**
     * 司机手机号
     */
    @FiledAnno("driverMoble")
    private String driverMoble;
    /**
     * 车牌号/车厢号/船号
     */
    @FiledAnno("plateNo")
    private String plateNo;

    private String checkinId;
    /**
     * 挂车号
     */
    private String trailerNo;
    /**
     * 商品车数量
     */
    private Integer totalShipCount;
    /**
     * 要求入库时间
     */
    private Date expectInboundDate;
    /**
     * 要求回单时间
     */
    private Date expectReceiptDate;
    /**
     * 要求发运时间
     */
    private Date expcetShipDate;
    /**
     * 要求抵达时间
     */
    private Date expectArriveDate;
    /**
     * 起运地唯一标识
     */
    private String originLocationId;
    /**
     * 起运地名称
     */
    private String originLocationName;
    /**
     * 起运地省份
     */
    private String originLocationProvince;
    /**
     * 起运地城市
     */
    private String originLocationCity;
    /**
     * 起运地区县
     */
    private String originLocationCounty;
    /**
     * 起运地地址
     */
    private String originLocationAddress;
    /**
     * 目的地唯一标识
     */
    private String destLocationId;
    /**
     * 目的地名称
     */
    private String destLocationName;
    /**
     * 目的地省份
     */
    private String destLocationProvince;
    /**
     * 目的地城市
     */
    private String destLocationCity;
    /**
     * 目的地区县
     */
    private String destLocationCounty;
    /**
     * 目的地地址
     */
    private String destLocationAddress;
    /**
     * 指令状态
     */
    @FiledAnno("status")
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 修改时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 车队ID
     */
    private String fleetId;

    /**
     * 调度时间
     */
    private Date insertDate;

    /**
     * 空放类型
     * @return
     */
    private String emptyType;

    /**
     * 指令下发事件自增长标识id
     * @return
     */
    private Long senderTransmissionNo;

    public Long getSenderTransmissionNo() {
        return senderTransmissionNo;
    }

    public void setSenderTransmissionNo(Long senderTransmissionNo) {
        this.senderTransmissionNo = senderTransmissionNo;
    }

    public String getEmptyType() {
        return emptyType;
    }

    public void setEmptyType(String emptyType) {
        this.emptyType = emptyType;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    private List<ShipTaskDTO> shipTaskDTOList;

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTransportModeId() {
        return transportModeId;
    }

    public void setTransportModeId(String transportModeId) {
        this.transportModeId = transportModeId;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMoble() {
        return driverMoble;
    }

    public void setDriverMoble(String driverMoble) {
        this.driverMoble = driverMoble;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public Integer getTotalShipCount() {
        return totalShipCount;
    }

    public void setTotalShipCount(Integer totalShipCount) {
        this.totalShipCount = totalShipCount;
    }

    public Date getExpectInboundDate() {
        return expectInboundDate;
    }

    public void setExpectInboundDate(Date expectInboundDate) {
        this.expectInboundDate = expectInboundDate;
    }

    public Date getExpectReceiptDate() {
        return expectReceiptDate;
    }

    public void setExpectReceiptDate(Date expectReceiptDate) {
        this.expectReceiptDate = expectReceiptDate;
    }

    public Date getExpcetShipDate() {
        return expcetShipDate;
    }

    public void setExpcetShipDate(Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate() {
        return expectArriveDate;
    }

    public void setExpectArriveDate(Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public String getOriginLocationId() {
        return originLocationId;
    }

    public void setOriginLocationId(String originLocationId) {
        this.originLocationId = originLocationId;
    }

    public String getOriginLocationName() {
        return originLocationName;
    }

    public void setOriginLocationName(String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getOriginLocationProvince() {
        return originLocationProvince;
    }

    public void setOriginLocationProvince(String originLocationProvince) {
        this.originLocationProvince = originLocationProvince;
    }

    public String getOriginLocationCity() {
        return originLocationCity;
    }

    public void setOriginLocationCity(String originLocationCity) {
        this.originLocationCity = originLocationCity;
    }

    public String getOriginLocationCounty() {
        return originLocationCounty;
    }

    public void setOriginLocationCounty(String originLocationCounty) {
        this.originLocationCounty = originLocationCounty;
    }

    public String getOriginLocationAddress() {
        return originLocationAddress;
    }

    public void setOriginLocationAddress(String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getDestLocationId() {
        return destLocationId;
    }

    public void setDestLocationId(String destLocationId) {
        this.destLocationId = destLocationId;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationAddress() {
        return destLocationAddress;
    }

    public void setDestLocationAddress(String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public List<ShipTaskDTO> getShipTaskDTOList() {
        return shipTaskDTOList;
    }

    public void setShipTaskDTOList(List<ShipTaskDTO> shipTaskDTOList) {
        this.shipTaskDTOList = shipTaskDTOList;
    }

    public void addShipTaskDTO(ShipTaskDTO shipTaskDTO) {
        if (Objects.isNull(shipTaskDTOList)) {
            shipTaskDTOList = new ArrayList<>();
        }
        shipTaskDTOList.add(shipTaskDTO);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ShipmentDTO{" +
                "shipmentId='" + shipmentId + '\'' +
                ", shipmentType='" + shipmentType + '\'' +
                ", transactionCode='" + transactionCode + '\'' +
                ", transportModeId='" + transportModeId + '\'' +
                ", serviceProviderId='" + serviceProviderId + '\'' +
                ", serviceProviderName='" + serviceProviderName + '\'' +
                ", driverId='" + driverId + '\'' +
                ", driverName='" + driverName + '\'' +
                ", driverMoble='" + driverMoble + '\'' +
                ", plateNo='" + plateNo + '\'' +
                ", checkinId=" + checkinId + '\'' +
                ", trailerNo='" + trailerNo + '\'' +
                ", totalShipCount=" + totalShipCount +
                ", expectInboundDate=" + expectInboundDate +
                ", expectReceiptDate=" + expectReceiptDate +
                ", expcetShipDate=" + expcetShipDate +
                ", expectArriveDate=" + expectArriveDate +
                ", originLocationId='" + originLocationId + '\'' +
                ", originLocationName='" + originLocationName + '\'' +
                ", originLocationProvince='" + originLocationProvince + '\'' +
                ", originLocationCity='" + originLocationCity + '\'' +
                ", originLocationCounty='" + originLocationCounty + '\'' +
                ", originLocationAddress='" + originLocationAddress + '\'' +
                ", destLocationId='" + destLocationId + '\'' +
                ", destLocationName='" + destLocationName + '\'' +
                ", destLocationProvince='" + destLocationProvince + '\'' +
                ", destLocationCity='" + destLocationCity + '\'' +
                ", destLocationCounty='" + destLocationCounty + '\'' +
                ", destLocationAddress='" + destLocationAddress + '\'' +
                ", status='" + status + '\'' +
                ", remarks='" + remarks + '\'' +
                ", gmtCreate=" + gmtCreate +'\'' +
                ", gmtModified=" + gmtModified +'\'' +
                ", shipTaskDTOList=" + shipTaskDTOList +
                '}';
    }

    public String getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(String checkinId) {
        this.checkinId = checkinId;
    }
}
