package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.enums.TableStatusEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.UlcLimitionMidMapper;
import com.zhiche.lisa.integration.dao.model.CusLimitionMid;
import com.zhiche.lisa.integration.dao.model.UlcLimitionMid;
import com.zhiche.lisa.integration.service.UlcLimitionMidService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 内控时效规则头信息 服务实现类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@Service
public class UlcLimitionMidServiceImpl extends ServiceImpl<UlcLimitionMidMapper, UlcLimitionMid> implements UlcLimitionMidService {

    @Override
    public List<UlcLimitionMid> queryUlcLimition(Map<String, String> dto) {
        if (dto == null || dto.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String queryAll = dto.get("queryAll");
        String timeStamp = dto.get("timeStamp");
        EntityWrapper<UlcLimitionMid> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                ew.gt("a.gmt_modified", new Date(longTime))
                        .or()
                        .gt("b.gmt_modified", new Date(longTime));
            }
        }
        ew.orderBy("a.limitation_gid",false);
        return baseMapper.queryUlcLimition(ew);
    }
}
