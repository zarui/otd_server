package com.zhiche.lisa.integration.dto.junma;

import java.io.Serializable;
import java.util.Objects;

public class MesInboundParamDTO implements Serializable {
    private String vin;
    private String materialCode;
    private String materialName;
    private String colorCode;
    private String colorName;
    private String produceTime;
    private String offlineTime;
    private String engineNo;
    private String certification;
    private String gearBoxNo;
    private String vehicleCode;
    private String vehicleName;
    private String vehicleDesc;
    private String businessType;
    private String warehouseCode;
    private String freeuse1;
    private String freeuse2;
    private String freeuse3;


    public String getFreeuse1() {
        return freeuse1;
    }

    public void setFreeuse1(String freeuse1) {
        this.freeuse1 = freeuse1;
    }

    public String getFreeuse2() {
        return freeuse2;
    }

    public void setFreeuse2(String freeuse2) {
        this.freeuse2 = freeuse2;
    }

    public String getFreeuse3() {
        return freeuse3;
    }

    public void setFreeuse3(String freeuse3) {
        this.freeuse3 = freeuse3;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(String produceTime) {
        this.produceTime = produceTime;
    }

    public String getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(String offlineTime) {
        this.offlineTime = offlineTime;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public String getGearBoxNo() {
        return gearBoxNo;
    }

    public void setGearBoxNo(String gearBoxNo) {
        this.gearBoxNo = gearBoxNo;
    }

    public String getVehicleCode() {
        return vehicleCode;
    }

    public void setVehicleCode(String vehicleCode) {
        this.vehicleCode = vehicleCode;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleDesc() {
        return vehicleDesc;
    }

    public void setVehicleDesc(String vehicleDesc) {
        this.vehicleDesc = vehicleDesc;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    @Override
    public String toString() {
        return "MesInboundParamDTO{" +
                "vin='" + vin + '\'' +
                ", materialCode='" + materialCode + '\'' +
                ", materialName='" + materialName + '\'' +
                ", colorCode='" + colorCode + '\'' +
                ", colorName='" + colorName + '\'' +
                ", produceTime='" + produceTime + '\'' +
                ", offlineTime='" + offlineTime + '\'' +
                ", engineNo='" + engineNo + '\'' +
                ", certification='" + certification + '\'' +
                ", gearBoxNo='" + gearBoxNo + '\'' +
                ", vehicleCode='" + vehicleCode + '\'' +
                ", vehicleName='" + vehicleName + '\'' +
                ", vehicleDesc='" + vehicleDesc + '\'' +
                ", businessType='" + businessType + '\'' +
                ", warehouseCode='" + warehouseCode + '\'' +
                ", freeuse1='" + freeuse1 + '\'' +
                ", freeuse2='" + freeuse2 + '\'' +
                ", freeuse3='" + freeuse3 + '\'' +
                '}';
    }
}
