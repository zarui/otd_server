package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.CiamsMapper;
import com.zhiche.lisa.integration.dao.model.Ciams;
import com.zhiche.lisa.integration.dto.carrier.CiamsResultDto;
import com.zhiche.lisa.integration.dto.otd.CiamsWithOutIdDTO;
import com.zhiche.lisa.integration.service.ICiamsService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * OTM 业务主体表 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-09-04
 */
@Service
public class CiamsServiceImpl extends ServiceImpl<CiamsMapper, Ciams> implements ICiamsService {

    @Override
    public List<CiamsResultDto> queryCiamsNameList(Map<String, Object> condition) {
        Wrapper<CiamsResultDto> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()) {
            ew.eq("status", TableStatusEnum.STATUS_1.getCode());
            if (condition.containsKey("relationGid") && StringUtils.isNotBlank(condition.get("relationGid").toString())) {
                ew.like("relationGid", condition.get("relationGid").toString());
            }
            if (condition.containsKey("corpName") && StringUtils.isNotBlank(condition.get("corpName").toString())) {
                ew.eq("corporation_name", condition.get("corpName").toString());
            }
            if (condition.containsKey("timeAfter") && StringUtils.isNotBlank(condition.get("timeAfter").toString())) {
                Date timeAfter = new Date(Long.valueOf(condition.get("timeAfter").toString()));
                ew.andNew()
                        .where("(update_date IS NOT NULL and update_date >= {0})", timeAfter)
                        .or()
                        .where("(update_date IS NULL and insert_date >= {0})", timeAfter);
            }
            ew.groupBy("id");
        }
        return baseMapper.queryCiamsNameList(ew);

    }

    @Override
    public List<CiamsWithOutIdDTO> queryCiamOTD(Map<String, String> dto) {
        if (dto == null || dto.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String queryAll = dto.get("queryAll");
        String timeStamp = dto.get("timeStamp");
        EntityWrapper<CiamsWithOutIdDTO> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                ew.gt("gmt_modified", new Date(longTime));
            }
        }
        ew.orderBy("id", false);
        return baseMapper.selectListWithOutId(ew);
    }

}
