package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.VehicleClass;

/**
 * <p>
 * OTM车辆车系 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-08-21
 */
public interface VehicleClassMapper extends BaseMapper<VehicleClass> {

}
