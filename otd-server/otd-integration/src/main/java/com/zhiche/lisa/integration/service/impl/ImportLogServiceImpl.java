package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.qiniu.storage.model.DefaultPutRet;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.qiniu.util.QiniuUtils;
import com.zhiche.lisa.integration.dao.mapper.ImportLogMapper;
import com.zhiche.lisa.integration.dao.model.ImportLog;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import com.zhiche.lisa.integration.service.IImportLogHistoryService;
import com.zhiche.lisa.integration.service.IImportLogService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * 接口导入日志 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@Service
public class ImportLogServiceImpl extends ServiceImpl<ImportLogMapper, ImportLog> implements IImportLogService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IImportLogHistoryService importLogHistoryService;

    @Override
    public ImportLog saveImportLog(ImportLogHistory importLogHistory) {
        Wrapper<ImportLog> ew = new EntityWrapper<>();
        ew.eq("source_key", importLogHistory.getSourceKey());
        ew.eq("source_sys", importLogHistory.getSourceSys());
        ew.eq("type", importLogHistory.getType());
        ImportLog importLog = this.selectOne(ew);
        if (Objects.nonNull(importLog)) {
            Integer logId = importLog.getId();
            BeanUtils.copyProperties(importLogHistory, importLog);
            importLog.setId(logId);
            importLog.setGmtCreate(null);
            importLog.setGmtModified(null);
            importLogHistory.setLogId(logId);
            importLogHistoryService.insert(importLogHistory);
            this.updateById(importLog);
        } else {
            importLog = new ImportLog();
            BeanUtils.copyProperties(importLogHistory, importLog);
            this.insert(importLog);
            importLogHistory.setLogId(importLog.getId());
            importLogHistoryService.insert(importLogHistory);
        }
        return importLog;
    }


    @Override
    public void saveImportLogToQiniu(ImportLogHistory ilh,
                                     String dataContent) throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info("saveImportLogToQiniu.start ----> param:{}", dataContent);
        }
        if (StringUtils.isBlank(ilh.getSourceSys())) {
            throw new BaseException("saveLog sourceSys 不能为空");
        }
        if (StringUtils.isBlank(ilh.getSourceKey())) {
            throw new BaseException("saveLog sourceKey 不能为空");
        }
        if (StringUtils.isBlank(ilh.getType())) {
            throw new BaseException("saveLog type 不能为空");
        }
        Wrapper<ImportLog> ew = new EntityWrapper<>();
        ew.eq("source_sys", ilh.getSourceSys());
        ew.eq("source_key", ilh.getSourceKey());
        ew.eq("type", ilh.getType());
        ImportLog importLog = selectOne(ew);
        if (Objects.nonNull(importLog)) {
            Integer logId = importLog.getId();
            BeanUtils.copyProperties(ilh, importLog);
            ilh.setLogId(logId);
            importLog.setId(logId);
            importLog.setGmtCreate(null);
            importLog.setGmtModified(null);
            importLogHistoryService.insert(ilh);
            updateById(importLog);
        } else {
            importLog = new ImportLog();
            BeanUtils.copyProperties(ilh, importLog);
            insert(importLog);
            ilh.setLogId(importLog.getId());
            importLogHistoryService.insert(ilh);
        }

        String fileName = ilh.getSourceSys() + "_" + ilh.getType() + "_" +
                ilh.getSourceKey() + "_" + ilh.getLogId() + "_" + ilh.getId();
        DefaultPutRet putRet = QiniuUtils.uploadString(dataContent, fileName);
        if (putRet != null) {
            String qiniuKey = putRet.key;
            ilh.setDataStorageKey(qiniuKey);
            importLog.setDataStorageKey(qiniuKey);
        }
        importLogHistoryService.updateById(ilh);
        updateById(importLog);
    }
}
