package com.zhiche.lisa.integration.scheduled;

import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.service.IPushSubSystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: caiHua 补发指令
 * @Description:
 * @Date: Create in 11:23 2019/4/8
 */
@Component
public class ReissueShipmentToWMS {
    @Autowired
    IPushSubSystemService iPushSubSystemService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReissueShipmentToWMS.class);
    //每分钟执行一次
//    @Scheduled(cron="0 * * * * ?")
    //每天凌晨两点执行
//    @Scheduled(cron="0 0 2 * * ?")
    public void pushShipmentToWms () {
        long startTime = System.currentTimeMillis();
        LOGGER.info("integration补发指令至wms线程开始执行。。。。。。。。。");
        try {
            iPushSubSystemService.queryShipmentToWms();
        } catch (BaseException bx) {
            LOGGER.info(bx.getMessage());
        } catch (Exception e) {
            LOGGER.info("推送失败，系统异常！{}",e.getMessage());
        }
        LOGGER.info("integration补发指令至wms线程结束执行。。耗时{}。。。。。。。", (System.currentTimeMillis() - startTime));
    }

//    @Scheduled(cron="0 30 2 * * ?")
    public void pushShipmentToTms () {
        long startTime = System.currentTimeMillis();
        LOGGER.info("integration补发指令至 tms 线程开始执行。。。。。。。。。");
        try {
            iPushSubSystemService.pushShipmentToTms();
        } catch (BaseException bx) {
            LOGGER.info(bx.getMessage());
        } catch (Exception e) {
            LOGGER.info("推送失败，系统异常！{}",e.getMessage());
        }
        LOGGER.info("integration补发指令至 tms 线程结束执行。。耗时{}。。。。。。。", (System.currentTimeMillis() - startTime));
    }

}
