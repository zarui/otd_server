package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * OTM 业务主体表
 * </p>
 *
 * @author qichao
 * @since 2018-09-04
 */
@TableName("otm_ciams")
public class Ciams extends Model<Ciams> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * OTM 业务主体编码
     */
	@TableField("ciams_gid")
	private String ciamsGid;
    /**
     * OTM 业务主体名称
     */
	@TableField("ciams_name")
	private String ciamsName;
    /**
     * 状态 1正常
     */
	private String status;
	@TableField("insert_user")
	private String insertUser;
	@TableField("insert_date")
	private Date insertDate;
	@TableField("update_user")
	private String updateUser;
	@TableField("update_date")
	private Date updateDate;
	@TableField("gmt_create")
	private String gmtCreate;
	@TableField("gmt_modified")
	private Date gmtModified;

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}


	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCiamsGid() {
		return ciamsGid;
	}

	public void setCiamsGid(String ciamsGid) {
		this.ciamsGid = ciamsGid;
	}

	public String getCiamsName() {
		return ciamsName;
	}

	public void setCiamsName(String ciamsName) {
		this.ciamsName = ciamsName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Ciams{" +
			", id=" + id +
			", ciamsGid=" + ciamsGid +
			", ciamsName=" + ciamsName +
			", status=" + status +
			", insertUser=" + insertUser +
			", insertDate=" + insertDate +
			", updateUser=" + updateUser +
			", updateDate=" + updateDate +
			"}";
	}
}
