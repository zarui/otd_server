package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.zhiche.lisa.integration.dto.otd.OrderReleaseMidDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * OTM 指令中间表
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@TableName("v_shipment_mid")
public class ShipmentMid extends Model<ShipmentMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 指令gid
     */
    @TableId("shipment_gid")
    private String shipmentGid;
    /**
     * 指令xid
     */
    @TableField("shipment_xid")
    private String shipmentXid;
    /**
     * 指令状态
     */
    @TableField("user_defined1_icon_gid")
    private String userDefined1IconGid;
    /**
     * otm时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * otm时间
     */
    @TableField("insert_date")
    private Date insertDate;
    /**
     * 是否含 异常车辆
     */
    @TableField("is_send")
    private String isSend;
    /**
     * 空放类型
     */
    private String attribute19;
    /**
     * 原指令号
     */
    @TableField("tariff_organization_number")
    private String tariffOrganizationNumber;
    /**
     * 目的地详细地址
     */
    @TableField("dl_addr")
    private String dlAddr;
    /**
     * 目的地区县
     */
    @TableField("dl_country")
    private String dlCountry;
    /**
     * 目的地市
     */
    @TableField("dl_city")
    private String dlCity;
    /**
     * 目的地省
     */
    @TableField("dl_pro")
    private String dlPro;
    /**
     * 目的地gid
     */
    @TableField("dest_location_gid")
    private String destLocationGid;
    /**
     * 启运地详细地址
     */
    @TableField("sl_addr")
    private String slAddr;
    /**
     * 启运地区县
     */
    @TableField("sl_country")
    private String slCountry;
    /**
     * 启运地市
     */
    @TableField("sl_city")
    private String slCity;
    /**
     * 启运地省
     */
    @TableField("sl_pro")
    private String slPro;
    /**
     * 启运地gid
     */
    @TableField("source_location_gid")
    private String sourceLocationGid;
    /**
     * 司机发运时间
     */
    @TableField("attribute_date9")
    private Date attributeDate9;
    /**
     * 实际发运时间
     */
    @TableField("attribute_date1")
    private Date attributeDate1;
    /**
     * 装车点数量
     */
    @TableField("attribute_number2")
    private Integer attributeNumber2;
    /**
     * 空位数量
     */
    @TableField("attribute_number1")
    private Integer attributeNumber1;
    /**
     * 运输订单数量
     */
    @TableField("total_ship_unit_count")
    private Integer totalShipUnitCount;
    /**
     * 分段标识
     */
    private String attribute1;
    /**
     * 司机联系方式
     */
    private String attribute2;
    /**
     * 司机姓名
     */
    private String attribute7;
    /**
     * 司机id
     */
    @TableField("driver_gid")
    private String driverGid;
    /**
     * 车牌号
     */
    private String attribute12;
    /**
     * 运输方式
     */
    @TableField("transport_mode_gid")
    private String transportModeGid;
    /**
     * 车队
     */
    @TableField("servprov_gid")
    private String servprovGid;
    /**
     * 分供方
     */
    private String attribute10;
    /**
     * 数据修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 数据 修改时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;

    public String getShipmentGid() {
        return shipmentGid;
    }

    public void setShipmentGid(String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getShipmentXid() {
        return shipmentXid;
    }

    public void setShipmentXid(String shipmentXid) {
        this.shipmentXid = shipmentXid;
    }

    public String getUserDefined1IconGid() {
        return userDefined1IconGid;
    }

    public void setUserDefined1IconGid(String userDefined1IconGid) {
        this.userDefined1IconGid = userDefined1IconGid;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getIsSend() {
        return isSend;
    }

    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }

    public String getAttribute19() {
        return attribute19;
    }

    public void setAttribute19(String attribute19) {
        this.attribute19 = attribute19;
    }

    public String getTariffOrganizationNumber() {
        return tariffOrganizationNumber;
    }

    public void setTariffOrganizationNumber(String tariffOrganizationNumber) {
        this.tariffOrganizationNumber = tariffOrganizationNumber;
    }

    public String getDlAddr() {
        return dlAddr;
    }

    public void setDlAddr(String dlAddr) {
        this.dlAddr = dlAddr;
    }

    public String getDlCountry() {
        return dlCountry;
    }

    public void setDlCountry(String dlCountry) {
        this.dlCountry = dlCountry;
    }

    public String getDlCity() {
        return dlCity;
    }

    public void setDlCity(String dlCity) {
        this.dlCity = dlCity;
    }

    public String getDlPro() {
        return dlPro;
    }

    public void setDlPro(String dlPro) {
        this.dlPro = dlPro;
    }

    public String getDestLocationGid() {
        return destLocationGid;
    }

    public void setDestLocationGid(String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getSlAddr() {
        return slAddr;
    }

    public void setSlAddr(String slAddr) {
        this.slAddr = slAddr;
    }

    public String getSlCountry() {
        return slCountry;
    }

    public void setSlCountry(String slCountry) {
        this.slCountry = slCountry;
    }

    public String getSlCity() {
        return slCity;
    }

    public void setSlCity(String slCity) {
        this.slCity = slCity;
    }

    public String getSlPro() {
        return slPro;
    }

    public void setSlPro(String slPro) {
        this.slPro = slPro;
    }

    public String getSourceLocationGid() {
        return sourceLocationGid;
    }

    public void setSourceLocationGid(String sourceLocationGid) {
        this.sourceLocationGid = sourceLocationGid;
    }

    public Date getAttributeDate9() {
        return attributeDate9;
    }

    public void setAttributeDate9(Date attributeDate9) {
        this.attributeDate9 = attributeDate9;
    }

    public Date getAttributeDate1() {
        return attributeDate1;
    }

    public void setAttributeDate1(Date attributeDate1) {
        this.attributeDate1 = attributeDate1;
    }

    public Integer getAttributeNumber2() {
        return attributeNumber2;
    }

    public void setAttributeNumber2(Integer attributeNumber2) {
        this.attributeNumber2 = attributeNumber2;
    }

    public Integer getAttributeNumber1() {
        return attributeNumber1;
    }

    public void setAttributeNumber1(Integer attributeNumber1) {
        this.attributeNumber1 = attributeNumber1;
    }

    public Integer getTotalShipUnitCount() {
        return totalShipUnitCount;
    }

    public void setTotalShipUnitCount(Integer totalShipUnitCount) {
        this.totalShipUnitCount = totalShipUnitCount;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public String getDriverGid() {
        return driverGid;
    }

    public void setDriverGid(String driverGid) {
        this.driverGid = driverGid;
    }

    public String getAttribute12() {
        return attribute12;
    }

    public void setAttribute12(String attribute12) {
        this.attribute12 = attribute12;
    }

    public String getTransportModeGid() {
        return transportModeGid;
    }

    public void setTransportModeGid(String transportModeGid) {
        this.transportModeGid = transportModeGid;
    }

    public String getServprovGid() {
        return servprovGid;
    }

    public void setServprovGid(String servprovGid) {
        this.servprovGid = servprovGid;
    }

    public String getAttribute10() {
        return attribute10;
    }

    public void setAttribute10(String attribute10) {
        this.attribute10 = attribute10;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    protected Serializable pkVal() {
        return this.shipmentGid;
    }

    @Override
    public String toString() {
        return "ShipmentMid{" +
                ", shipmentGid=" + shipmentGid +
                ", shipmentXid=" + shipmentXid +
                ", userDefined1IconGid=" + userDefined1IconGid +
                ", updateDate=" + updateDate +
                ", insertDate=" + insertDate +
                ", isSend=" + isSend +
                ", attribute19=" + attribute19 +
                ", tariffOrganizationNumber=" + tariffOrganizationNumber +
                ", dlAddr=" + dlAddr +
                ", dlCountry=" + dlCountry +
                ", dlCity=" + dlCity +
                ", dlPro=" + dlPro +
                ", destLocationGid=" + destLocationGid +
                ", slAddr=" + slAddr +
                ", slCountry=" + slCountry +
                ", slCity=" + slCity +
                ", slPro=" + slPro +
                ", sourceLocationGid=" + sourceLocationGid +
                ", attributeDate9=" + attributeDate9 +
                ", attributeDate1=" + attributeDate1 +
                ", attributeNumber2=" + attributeNumber2 +
                ", attributeNumber1=" + attributeNumber1 +
                ", totalShipUnitCount=" + totalShipUnitCount +
                ", attribute1=" + attribute1 +
                ", attribute2=" + attribute2 +
                ", attribute7=" + attribute7 +
                ", driverGid=" + driverGid +
                ", attribute12=" + attribute12 +
                ", transportModeGid=" + transportModeGid +
                ", servprovGid=" + servprovGid +
                ", attribute10=" + attribute10 +
                ", gmtModified=" + gmtModified +
                ", gmtCreate=" + gmtCreate +
                "}";
    }
}
