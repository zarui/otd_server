package com.zhiche.lisa.integration.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * 单点登陆权限配置文件
 *
 * @auther lsj
 * @create 2018-06-13
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .anonymous()
                .and()
                .authorizeRequests()
                .antMatchers("/swagger-resources/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/*.html",
                        "/v2/api-docs",
                        "/",
                        "/*.html",
                        "favicon.ico",
                        "/**/*.html",
                        "/druid/**",
                        "/inner/**",
                        "/shipment/**",
                        "/event/**",
                        "/carrier/**",
                        "/corporation/**",
                        "/origination/**",
                        "/rateItem/**",
                        "/transportMode/**",
                        "/services/**",
                        "/user/**",
                        "/vehicleStandardMode/**",
                        "/vehicleClass/**",
                        "/ciams/**",
                        "/otm/**",
                        "/otdIntegration/**",
                        "/sparePart/queryReleaseData",
                        "/oilCardPay/**")
                .permitAll()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .anyRequest().authenticated();
    }
}
