package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysql.jdbc.StringUtils;
import com.zhiche.lisa.integration.dao.mapper.TransportModeMapper;
import com.zhiche.lisa.integration.dao.model.TransportMode;
import com.zhiche.lisa.integration.service.ITransportModeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * OTM运输方式(运输工具) 服务实现类
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
@Service
public class TransportModeServiceImpl extends ServiceImpl<TransportModeMapper, TransportMode> implements ITransportModeService {

    /**
     * 根据运输方式编码查询全部返回
     *
     * @param condition
     */
    @Override
    public List<TransportMode> queryTransportMode(Map<String, Object> condition) {
        Wrapper<TransportMode> ew = buildCondition(condition);
        return this.selectList(ew);
    }

    private Wrapper<TransportMode> buildCondition(Map<String, Object> condition) {
        Wrapper<TransportMode> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()){
            Object transportModeCode = condition.get("code");
            if (transportModeCode != null && !StringUtils.isNullOrEmpty(transportModeCode.toString())){
                ew.eq("code",transportModeCode.toString());
            }
        }
        return ew;
    }
}
