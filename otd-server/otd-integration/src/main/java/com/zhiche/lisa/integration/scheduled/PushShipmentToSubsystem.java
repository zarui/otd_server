package com.zhiche.lisa.integration.scheduled;

import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.service.IPushSubSystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: hxh 下发指令
 * @Description:
 * @Date: Create in 15:25 2019/9/20
 */
@Component
public class PushShipmentToSubsystem {
    @Autowired
    IPushSubSystemService iPushSubSystemService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PushShipmentToSubsystem.class);
    //每分钟执行一次
//    @Scheduled(cron="0 * * * * ?")
    public void pushShipment () {
        long startTime = System.currentTimeMillis();
        LOGGER.info("integration下发指令至wms线程开始执行。。。。。。。。。");
        try {
            iPushSubSystemService.pushNewShipment();
        } catch (BaseException bx) {
            LOGGER.info(bx.getMessage());
        } catch (Exception e) {
            LOGGER.info("推送失败，系统异常！{}",e.getMessage());
        }
        LOGGER.info("integration下发指令至wms线程结束执行。。耗时{}。。。。。。。", (System.currentTimeMillis() - startTime));
    }

}
