package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * OTM 客户数据表
 * </p>
 *
 * @author qichao
 * @since 2018-09-01
 */
public class CorporationDTO implements Serializable {

    private String corporationGid;
    private String corporationXid;
    private String corporationName;
    private String domainMaster;
    private String shippingAgentsActive;
    private String allowHouseCollect;
    private BigDecimal houseCollectAmt;
    private BigDecimal houseCollectAmtBase;
    private String pickupRoutingSequenceGid;
    private String dropoffRoutingSequenceGid;
    private String domainName;
    private Date gmtCreate;
    private Date gmtModified;

    public String getCorporationGid() {
        return corporationGid;
    }

    public void setCorporationGid(String corporationGid) {
        this.corporationGid = corporationGid;
    }

    public String getCorporationXid() {
        return corporationXid;
    }

    public void setCorporationXid(String corporationXid) {
        this.corporationXid = corporationXid;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public String getDomainMaster() {
        return domainMaster;
    }

    public void setDomainMaster(String domainMaster) {
        this.domainMaster = domainMaster;
    }

    public String getShippingAgentsActive() {
        return shippingAgentsActive;
    }

    public void setShippingAgentsActive(String shippingAgentsActive) {
        this.shippingAgentsActive = shippingAgentsActive;
    }

    public String getAllowHouseCollect() {
        return allowHouseCollect;
    }

    public void setAllowHouseCollect(String allowHouseCollect) {
        this.allowHouseCollect = allowHouseCollect;
    }

    public BigDecimal getHouseCollectAmt() {
        return houseCollectAmt;
    }

    public void setHouseCollectAmt(BigDecimal houseCollectAmt) {
        this.houseCollectAmt = houseCollectAmt;
    }

    public BigDecimal getHouseCollectAmtBase() {
        return houseCollectAmtBase;
    }

    public void setHouseCollectAmtBase(BigDecimal houseCollectAmtBase) {
        this.houseCollectAmtBase = houseCollectAmtBase;
    }

    public String getPickupRoutingSequenceGid() {
        return pickupRoutingSequenceGid;
    }

    public void setPickupRoutingSequenceGid(String pickupRoutingSequenceGid) {
        this.pickupRoutingSequenceGid = pickupRoutingSequenceGid;
    }

    public String getDropoffRoutingSequenceGid() {
        return dropoffRoutingSequenceGid;
    }

    public void setDropoffRoutingSequenceGid(String dropoffRoutingSequenceGid) {
        this.dropoffRoutingSequenceGid = dropoffRoutingSequenceGid;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}
