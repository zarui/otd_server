package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.RateItem;

/**
 * <p>
 * OTM费用名称 Mapper 接口
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
public interface RateItemMapper extends BaseMapper<RateItem> {

}
