package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.ImportLog;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;

/**
 * <p>
 * 接口导入日志 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface IImportLogService extends IService<ImportLog> {


    /**
     * 保存导入日志
     *
     */
    ImportLog saveImportLog(ImportLogHistory importLogHistory);

    /**
     * 保存导入日志  - 存入七牛
     */
    void saveImportLogToQiniu(ImportLogHistory ilh,
                              String dataContent) throws Exception;
}
