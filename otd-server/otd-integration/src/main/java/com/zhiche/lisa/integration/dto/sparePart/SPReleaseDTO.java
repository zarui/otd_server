package com.zhiche.lisa.integration.dto.sparePart;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 10:00 2019/7/19
 */
public class SPReleaseDTO implements Serializable {

    /**
     * 车型代码 order_model
     */
    private String vechileGid;

    /**
     * 车型 attribute10
     */
    private String vechileType;

    /**
     * 描述（一定要完整） cus_vechile_des
     */
    private String cusVechileDes;

    /**
     * 订单号 017开头 order_release_name
     */
    private String orderNo;

    /**
     * 车架号 attribute7
     */
    private String vin;

    /**
     * 客户名称 attribute2
     */
    private String customeName;


    /**
     * 下单时间 attribute_date1
     */
    private String orderDate;

    /**
     * 入库状态
     * 10 未入库；20 已入库
     */
    private String inboundStatus;

    /**
     * 入库时间 attribute_date8
     */
    private String inboundDate;

    public String getVechileGid () {
        return vechileGid;
    }

    public void setVechileGid (String vechileGid) {
        this.vechileGid = vechileGid;
    }

    public String getVechileType () {
        return vechileType;
    }

    public void setVechileType (String vechileType) {
        this.vechileType = vechileType;
    }

    public String getCusVechileDes () {
        return cusVechileDes;
    }

    public void setCusVechileDes (String cusVechileDes) {
        this.cusVechileDes = cusVechileDes;
    }

    public String getOrderNo () {
        return orderNo;
    }

    public void setOrderNo (String orderNo) {
        this.orderNo = orderNo;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getCustomeName () {
        return customeName;
    }

    public void setCustomeName (String customeName) {
        this.customeName = customeName;
    }


    public String getInboundStatus () {
        return inboundStatus;
    }

    public void setInboundStatus (String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    public String getOrderDate () {
        return orderDate;
    }

    public void setOrderDate (String orderDate) {
        this.orderDate = orderDate;
    }

    public String getInboundDate () {
        return inboundDate;
    }

    public void setInboundDate (String inboundDate) {
        this.inboundDate = inboundDate;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("SPReleaseDTO{");
        sb.append("vechileGid='").append(vechileGid).append('\'');
        sb.append(", vechileType='").append(vechileType).append('\'');
        sb.append(", cusVechileDes='").append(cusVechileDes).append('\'');
        sb.append(", orderNo='").append(orderNo).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", customeName='").append(customeName).append('\'');
        sb.append(", orderDate='").append(orderDate).append('\'');
        sb.append(", inboundStatus='").append(inboundStatus).append('\'');
        sb.append(", inboundDate=").append(inboundDate);
        sb.append('}');
        return sb.toString();
    }
}
