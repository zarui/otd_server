package com.zhiche.lisa.integration.dto.carrier;

import com.zhiche.lisa.integration.anno.ClassAnno;
import com.zhiche.lisa.integration.anno.FiledAnno;

import java.util.Date;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
@ClassAnno("classpath:otmxml/driver.xml")
public class DriverDTO {

    /**
     * 数据处理方式(I:新增,U:更新,D:删除,IU:新增或更新)
     */
    @FiledAnno("${transactionCode}")
    private String transactionCode;
    /**
     * 回调地址
     */
    @FiledAnno("${callBackUrl}")
    private String callBackUrl;

    /**
     * id
     */
    @FiledAnno("${driverId}")
    private Long id;
    /**
     * 真实姓名
     */
    @FiledAnno("${name}")
    private String name;
    /**
     * 身份证
     */
    @FiledAnno("${idCard}")
    private String idCard;
    /**
     * 手机号
     */
    @FiledAnno("${mobile}")
    private String mobile;
    /**
     * 出生日期
     */
    private Date dob;
    /**
     * 驾龄
     */
    private Integer yod;
    /**
     * 性别；0-女；1-男
     */
    private Integer sex;
    /**
     * 服务类型（多个用分号隔开）
     */
    @FiledAnno("${serviceType}")
    private String serviceType;
    /**
     * 微信的open_id
     */
    private String openId;
    /**
     * 登录账号
     */
    private Long accountId;
    /**
     * 所属承运商
     */
    @FiledAnno("${lspId}")
    private Long lspId;
    /**
     * 所属车队
     */
    private String fleetId;
    /**
     * 驾照类型（多个用分号隔开）
     */
    @FiledAnno("${drivingLicenseType}")
    private String drivingLicenseType;
    /**
     * 业务状态:0-停用;1-启用
     */
    private Integer status;
    /**
     * 分供方状态(启用:Y，其他:N，回传OTM使用)
     */
    @FiledAnno("${IsActive}")
    private String isActive;
    /**
     * 认证状态:0-未认证;1-已认证
     */
    private Integer authStatus;
    /**
     * 注册人
     */
    private String creator;
    /**
     * 注册时间
     */
    private Date gmtCreate;
    /**
     * 数据来源
     */
    private String createFrom;

    private Date gmtUpdate;
    /**
     * 业务覆盖城市。多个用分号分割
     */
    private String businessSite;

    //人送送车证号
    @FiledAnno("${personalLicense}")
    private String personalLicense;

    public String getPersonalLicense() {
        return personalLicense;
    }

    public void setPersonalLicense(String personalLicense) {
        this.personalLicense = personalLicense;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Integer getYod() {
        return yod;
    }

    public void setYod(Integer yod) {
        this.yod = yod;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getLspId() {
        return lspId;
    }

    public void setLspId(Long lspId) {
        this.lspId = lspId;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getDrivingLicenseType() {
        return drivingLicenseType;
    }

    public void setDrivingLicenseType(String drivingLicenseType) {
        this.drivingLicenseType = drivingLicenseType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(Integer authStatus) {
        this.authStatus = authStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getCreateFrom() {
        return createFrom;
    }

    public void setCreateFrom(String createFrom) {
        this.createFrom = createFrom;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getBusinessSite() {
        return businessSite;
    }

    public void setBusinessSite(String businessSite) {
        this.businessSite = businessSite;
    }

    @Override
    public String toString() {
        return "DriverDTO{" +
                "transactionCode='" + transactionCode + '\'' +
                ", callBackUrl='" + callBackUrl + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", idCard='" + idCard + '\'' +
                ", mobile='" + mobile + '\'' +
                ", dob=" + dob +
                ", yod=" + yod +
                ", sex=" + sex +
                ", serviceType='" + serviceType + '\'' +
                ", openId='" + openId + '\'' +
                ", accountId=" + accountId +
                ", lspId=" + lspId +
                ", fleetId=" + fleetId +
                ", drivingLicenseType='" + drivingLicenseType + '\'' +
                ", status=" + status +
                ", authStatus=" + authStatus +
                ", creator='" + creator + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", createFrom='" + createFrom + '\'' +
                ", gmtUpdate=" + gmtUpdate +
                ", businessSite='" + businessSite + '\'' +
                '}';
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
