package com.zhiche.lisa.integration.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.BaseUser;
import com.zhiche.lisa.integration.dto.base.UserDTO;
import com.zhiche.lisa.integration.service.IBaseUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(value = "user-API", description = "用户基础数据接口")
public class UserController {

    @Autowired
    private IBaseUserService baseUserService;

    //根据时间戳 获取用户信息
    @PostMapping("/listUser")
    @ApiOperation(value = "根据条件获取用户信息(未分页)", notes = "根据条件获取查询用户信息(未分页)")
    @ApiResponses({@ApiResponse(code = 0, message = "成功"), @ApiResponse(code = -1, message = "失败")})
    public RestfulResponse<List<UserDTO>> listUser(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<UserDTO>> result = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(),
                CommonEnum.SUCCESS.getText(), null);
        try {
            List<UserDTO> data = baseUserService.queryAllUser(condition);
            result.setData(data);
        } catch (Exception e) {
            result.setCode(CommonEnum.ERROR.getCode());
            result.setMessage(CommonEnum.ERROR.getText());
        }
        return result;
    }
    @PostMapping(value = "/queryUser")
    @ApiOperation(value = "/queryUserApi",notes = "根据用户中文名称/用户编码分页查询",response = RestfulResponse.class)
    public RestfulResponse<Page<BaseUser>> queryUserPage (@RequestBody Page<BaseUser> page){
        Logger logger = LoggerFactory.getLogger(getClass());
        logger.info("controller:/queryAllUser/queryUser data: {}", page);
        RestfulResponse<Page<BaseUser>> result = new RestfulResponse<>(0,"成功",null);
        try {
            Page<BaseUser>baseUserPage = baseUserService.queryUserPage(page);
            result.setData(baseUserPage);
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}
