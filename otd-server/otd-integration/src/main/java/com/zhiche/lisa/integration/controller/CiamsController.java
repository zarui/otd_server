package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.carrier.CiamsResultDto;
import com.zhiche.lisa.integration.service.ICiamsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by hxh on 2018/9/04.
 */
@RestController
@RequestMapping(value = "/ciams")
@Api(value = "/ciams", description = "业务主体", tags = {"业务主体"})
public class CiamsController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ICiamsService ciamsService;

    @PostMapping("/queryCiamsNameList")
    @ApiOperation(value = "通过客户gid查询业务主体全称", notes = "通过客户gid查询业务主体全称", response = RestfulResponse.class)
    public RestfulResponse<List<Map<String, Object>>> queryCiamsNameList(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/ciams/queryCiamsNameList data: {}", condition);
        RestfulResponse result = new RestfulResponse<>(0, "成功", null);
        try {
            List<CiamsResultDto> ciamsList = ciamsService.queryCiamsNameList(condition);
            result.setData(ciamsList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

}
