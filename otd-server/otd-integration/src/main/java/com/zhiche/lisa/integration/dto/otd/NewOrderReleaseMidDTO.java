package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.util.Date;


public class NewOrderReleaseMidDTO implements Serializable {
    /**
     * 路由GID
     */
    private String orderReleaseGid;
//    private String routeOrderRouteGid;
    /**
     * 订单ID
     */
    private String orderGid;
    //    private String routeOrderGid;
    /**
     * 指令号
     */
    private String shipmentId;
    /**
     * 运输方式Gid
     */
    private String transportModeGid;
    /**
     * 序号
     */
    private Integer routeSeq;
//    /**
//     * 路由xid
//     */
//    private String routeOrderRouteXid;
    /**
     * 分段标识
     */
    private String subMark;
    /**
     * 起运地类型
     */
    private String stPointType;
    /**
     * 目的地类型
     */
    private String enPointType;
    /**
     * 起运地Gid
     */
    private String sourceLocationGid;
    /**
     * 目的地Gid
     */
    private String destLocationGid;
    /**
     * 运输模式
     */
    private String transMode;
    /**
     * 运单状态GID
     */
    private String orderReleaseStatusGid;
    /**
     * 运单状态
     */
    private String orderReleaseStatus;
    /**
     * 分供方名称
     */
    private String masterCarrierName;
    /**
     * 车船号
     */
    private String transportCode;
    /**
     * 计划调度时间
     */
    private String planDispatchTime;
    /**
     * 实际调度时间
     */
    private String realDispatchTime;
    /**
     * 计划发运时间
     */
    private String planDeliverTime;
    /**
     * 实际发运时间
     */
    private String realDeliverTime;
    /**
     * 计划调运抵时间
     */
    private String planArrivedTime;
    /**
     * 实际运抵时间
     */
    private String realArrivedTime;
    /**
     * 计划入库时间
     */
    private String planStorageTime;
    /**
     * 实际入库时间
     */
    private String realStorageTime;
    /**
     * 计划回单时间
     */
    private String planReceiptTime;
    /**
     * 实际回单时间
     */
    private String realReceiptTime;
    /**
     * 数据创建时间
     */
    private String gmtCreate;
    /**
     * 数据修改时间
     */
    private String gmtModified;

    public String getOrderReleaseGid() {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid(String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getOrderGid() {
        return orderGid;
    }

    public void setOrderGid(String orderGid) {
        this.orderGid = orderGid;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getTransportModeGid() {
        return transportModeGid;
    }

    public void setTransportModeGid(String transportModeGid) {
        this.transportModeGid = transportModeGid;
    }

    public Integer getRouteSeq() {
        return routeSeq;
    }

    public void setRouteSeq(Integer routeSeq) {
        this.routeSeq = routeSeq;
    }

    public String getSubMark() {
        return subMark;
    }

    public void setSubMark(String subMark) {
        this.subMark = subMark;
    }

    public String getStPointType() {
        return stPointType;
    }

    public void setStPointType(String stPointType) {
        this.stPointType = stPointType;
    }

    public String getEnPointType() {
        return enPointType;
    }

    public void setEnPointType(String enPointType) {
        this.enPointType = enPointType;
    }

    public String getSourceLocationGid() {
        return sourceLocationGid;
    }

    public void setSourceLocationGid(String sourceLocationGid) {
        this.sourceLocationGid = sourceLocationGid;
    }

    public String getDestLocationGid() {
        return destLocationGid;
    }

    public void setDestLocationGid(String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getOrderReleaseStatusGid() {
        return orderReleaseStatusGid;
    }

    public void setOrderReleaseStatusGid(String orderReleaseStatusGid) {
        this.orderReleaseStatusGid = orderReleaseStatusGid;
    }

    public String getOrderReleaseStatus() {
        return orderReleaseStatus;
    }

    public void setOrderReleaseStatus(String orderReleaseStatus) {
        this.orderReleaseStatus = orderReleaseStatus;
    }

    public String getMasterCarrierName() {
        return masterCarrierName;
    }

    public void setMasterCarrierName(String masterCarrierName) {
        this.masterCarrierName = masterCarrierName;
    }

    public String getTransportCode() {
        return transportCode;
    }

    public void setTransportCode(String transportCode) {
        this.transportCode = transportCode;
    }

    public String getPlanDispatchTime() {
        return planDispatchTime;
    }

    public void setPlanDispatchTime(String planDispatchTime) {
        this.planDispatchTime = planDispatchTime;
    }

    public String getRealDispatchTime() {
        return realDispatchTime;
    }

    public void setRealDispatchTime(String realDispatchTime) {
        this.realDispatchTime = realDispatchTime;
    }

    public String getPlanDeliverTime() {
        return planDeliverTime;
    }

    public void setPlanDeliverTime(String planDeliverTime) {
        this.planDeliverTime = planDeliverTime;
    }

    public String getRealDeliverTime() {
        return realDeliverTime;
    }

    public void setRealDeliverTime(String realDeliverTime) {
        this.realDeliverTime = realDeliverTime;
    }

    public String getPlanArrivedTime() {
        return planArrivedTime;
    }

    public void setPlanArrivedTime(String planArrivedTime) {
        this.planArrivedTime = planArrivedTime;
    }

    public String getRealArrivedTime() {
        return realArrivedTime;
    }

    public void setRealArrivedTime(String realArrivedTime) {
        this.realArrivedTime = realArrivedTime;
    }

    public String getPlanStorageTime() {
        return planStorageTime;
    }

    public void setPlanStorageTime(String planStorageTime) {
        this.planStorageTime = planStorageTime;
    }

    public String getRealStorageTime() {
        return realStorageTime;
    }

    public void setRealStorageTime(String realStorageTime) {
        this.realStorageTime = realStorageTime;
    }

    public String getPlanReceiptTime() {
        return planReceiptTime;
    }

    public void setPlanReceiptTime(String planReceiptTime) {
        this.planReceiptTime = planReceiptTime;
    }

    public String getRealReceiptTime() {
        return realReceiptTime;
    }

    public void setRealReceiptTime(String realReceiptTime) {
        this.realReceiptTime = realReceiptTime;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }
}
