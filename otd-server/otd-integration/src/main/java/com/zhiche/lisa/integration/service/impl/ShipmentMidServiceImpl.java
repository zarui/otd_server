package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.enums.TableStatusEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.ShipmentMidMapper;
import com.zhiche.lisa.integration.dao.model.ShipmentMid;
import com.zhiche.lisa.integration.dao.model.UlcLimitionMid;
import com.zhiche.lisa.integration.dto.otd.NewOTDShipmentDTO;
import com.zhiche.lisa.integration.dto.otd.OTDShipmentDTO;
import com.zhiche.lisa.integration.dto.otd.OrderReleaseMidDTO;
import com.zhiche.lisa.integration.service.ShipmentMidService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 指令中间表 服务实现类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@Service
public class ShipmentMidServiceImpl extends ServiceImpl<ShipmentMidMapper, ShipmentMid> implements ShipmentMidService {

    @Override
    public Page<OTDShipmentDTO> queryShipment(Page<OTDShipmentDTO> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> con = page.getCondition();
        if (con == null || con.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        Object oAll = con.get("queryAll");
        Object oTime = con.get("timeStamp");
        String queryAll = "";
        String timeStamp = "";
        if (oAll != null) {
            queryAll = oAll.toString();
        }
        if (oTime != null) {
            timeStamp = oTime.toString();
        }
        EntityWrapper<OTDShipmentDTO> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                ew.gt("gmt_modified", new Date(longTime));
            }
        }
        ew.orderBy("shipment_gid", false);
        // 数据量大 分页
        List<OTDShipmentDTO> records = baseMapper.selectShipmentByPage(page, ew);
        page.setRecords(records);
        return page;
    }

    @Override
    public Page<NewOTDShipmentDTO> queryShipmentNew(Page<NewOTDShipmentDTO> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> con = page.getCondition();
        if (con == null || con.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        Object oAll = con.get("queryAll");
        Object oTime = con.get("timeStamp");
        String queryAll = "";
        String timeStamp = "";
        if (oAll != null) {
            queryAll = oAll.toString();
        }
        if (oTime != null) {
            timeStamp = oTime.toString();
        }
        EntityWrapper<NewOTDShipmentDTO> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String format = sdf.format(new Date(longTime));
                    ew.gt("gmt_modified", format);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
            }
        }
        ew.orderBy("shipment_gid", false);
        // 数据量大 分页
        List<NewOTDShipmentDTO> records = baseMapper.selectShipmentByPageNew(page, ew);
        page.setRecords(records);
        return page;
    }
}
