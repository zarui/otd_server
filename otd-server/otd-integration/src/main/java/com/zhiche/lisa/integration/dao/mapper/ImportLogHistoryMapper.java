package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 接口导入日志历史 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface ImportLogHistoryMapper extends BaseMapper<ImportLogHistory> {

    /**
     * 查询前一天推送给wms的数据
     * @param o
     * @param ew
     */
    List<ImportLogHistory> queryShipmentToWms (Page<ImportLogHistory> ew);

    List<ImportLogHistory> queryShipmentToTms (Page<ImportLogHistory> page);

    /**
     * 查询前三分钟未推送给wms和tms的数据
     * @param page
     * @return
     */
    List<ImportLogHistory> queryNoPushShipment (Page<ImportLogHistory> page);

    /**
     * 根据otm指令下发事件id更新下发各个子系统返回结果
     * @return
     */
    void updateImpResultByTranNo (@Param("targetSys") String targetSys, @Param("transmissionNo") Long transmissionNo, @Param("importStatus") String importStatus, @Param("importNote") String importNote );
}
