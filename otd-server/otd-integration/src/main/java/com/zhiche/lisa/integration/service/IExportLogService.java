package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.ExportLog;
import com.zhiche.lisa.integration.dao.model.ExportLogHistory;

/**
 * <p>
 * 接口导出日志 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface IExportLogService extends IService<ExportLog> {

    /**
     * 保存导出日志
     *
     * @param exportLogHistory
     * @return
     */
    ExportLog saveExportLog(ExportLogHistory exportLogHistory);

    /**
     * 保存导出日志 - 存入七牛
     */
    void saveExportLogToQiniu(ExportLogHistory elh,
                              String dataContent) throws Exception;
}
