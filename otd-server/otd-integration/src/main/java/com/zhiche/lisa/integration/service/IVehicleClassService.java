package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.VehicleClass;

import java.util.List;
import java.util.Map;

public interface IVehicleClassService extends IService<VehicleClass> {
    /**
     * 通过车系编码查询车系数据全部返回
     */
    List<VehicleClass> queryAllClass(Map<String, Object> classCode);
    /**
     * 通过车系名称模糊查询车系数据分页返回
     */
    Page<VehicleClass> queryClassPage(Page<VehicleClass> page);
}
