package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.ExportLog;

/**
 * <p>
 * 接口导出日志 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface ExportLogMapper extends BaseMapper<ExportLog> {

}
