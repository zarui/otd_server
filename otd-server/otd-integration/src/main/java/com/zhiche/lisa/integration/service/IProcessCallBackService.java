package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.ProcessCallBack;

/**
 * <p>
 * 接口回调处理 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface IProcessCallBackService extends IService<ProcessCallBack> {

}
