package com.zhiche.lisa.integration.dto.carrier;

/**
 * Created by hxh on 2018/9/05.
 */
public class CiamsResultDto {

    private String id;              //OTM标识id
    private String ciams;           //业务主体全称
    private String relationGid;     //所属客户GID
    private String corpName;     //所属客户GID

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCiams() {
        return ciams;
    }

    public void setCiams(String ciams) {
        this.ciams = ciams;
    }

    public String getRelationGid() {
        return relationGid;
    }

    public void setRelationGid(String relationGid) {
        this.relationGid = relationGid;
    }

    @Override
    public String toString() {
        return "CiamsResultDto{" +
                "id='" + id + '\'' +
                ", ciams='" + ciams + '\'' +
                ", relationGid='" + relationGid + '\'' +
                '}';
    }
}
