package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.enums.TableStatusEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.OrderMidMapper;
import com.zhiche.lisa.integration.dao.model.NewOrderMid;
import com.zhiche.lisa.integration.dao.model.OrderMid;
import com.zhiche.lisa.integration.dto.otd.RouteDelMid;
import com.zhiche.lisa.integration.service.OrderMidService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 订单信息中间表 服务实现类
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
@Service
public class OrderMidServiceImpl extends ServiceImpl<OrderMidMapper, OrderMid> implements OrderMidService {

    @Override
    public Page<OrderMid> queryOrder(Page<OrderMid> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> con = page.getCondition();
        if (con == null || con.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        Object oAll = con.get("queryAll");
        Object oTime = con.get("timeStamp");
        String queryAll = "";
        String timeStamp = "";
        if (oAll != null) {
            queryAll = oAll.toString();
        }
        if (oTime != null) {
            timeStamp = oTime.toString();
        }
        EntityWrapper<OrderMid> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                ew.gt("gmt_modified", new Date(longTime));
            }
        }
        ew.orderBy("order_gid", false);
        // 数据量大 分页
        List<OrderMid> records = baseMapper.selectOrderByPage(page, ew);
        page.setRecords(records);
        return page;
    }

    @Override
    public Page<NewOrderMid> queryOrderNew(Page<NewOrderMid> page) {
        if (page == null) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> con = page.getCondition();
        if (con == null || con.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        Object oAll = con.get("queryAll");
        Object oTime = con.get("timeStamp");
        String queryAll = "";
        String timeStamp = "";
        if (oAll != null) {
            queryAll = oAll.toString();
        }
        if (oTime != null) {
            timeStamp = oTime.toString();
        }
        EntityWrapper<NewOrderMid> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String format = sdf.format(new Date(longTime));
                    ew.gt("gmt_modified", format);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
            }
        }
        ew.orderBy("order_gid", false);
        // 数据量大 分页
        List<NewOrderMid> records = baseMapper.selectOrderByPageNew(page, ew);
        page.setRecords(records);
        return page;
    }

    @Override
    public Page<RouteDelMid> queryRouteDelMid (Page<RouteDelMid> page) {
        if (null == page) {
            throw new BaseException("page参数不能为空");
        }
        Map<String, Object> condition = page.getCondition();
        if (null == condition || condition.isEmpty()) {
            throw new BaseException("condition参数不能为空！");
        }
        if (Objects.isNull(condition.get("timeStamp")) && StringUtils.isEmpty((String) condition.get("timeStamp"))) {
            throw new BaseException("【timeStamp】查询参数不能为空");
        }
        EntityWrapper<RouteDelMid> ew = new EntityWrapper<>();

        String queryAll = (String)condition.get("queryAll");
        if (Objects.isNull(queryAll) ||!TableStatusEnum.STATUS_Y.getCode().equals(queryAll.toLowerCase())) {
            Long timeStamp = null;
            try {
                timeStamp = Long.valueOf(condition.get("timeStamp").toString());
            } catch (Exception e) {
                throw new BaseException("【timeStamp】时间戳格式不正确");
            }
            ew.ge("gmt_create", new Date(timeStamp));
        }
        ew.orderBy("gmt_create", false);
        List<RouteDelMid> routeDelMids = baseMapper.queryRouteDelMid(page, ew);
        return page.setRecords(routeDelMids);
    }

}
