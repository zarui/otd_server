package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 接口导入日志
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@TableName("itf_import_log")
public class ImportLog extends Model<ImportLog> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 来源系统(erp,otm)
     */
	@TableField("source_sys")
	private String sourceSys;
    /**
     * 来源唯一键
     */
	@TableField("source_key")
	private String sourceKey;
    /**
     * 类型(10:指令,20:份额)
     */
	private String type;
    /**
     * 数据存储键
     */
	@TableField("data_storage_key")
	private String dataStorageKey;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSourceSys() {
		return sourceSys;
	}

	public void setSourceSys(String sourceSys) {
		this.sourceSys = sourceSys;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDataStorageKey() {
		return dataStorageKey;
	}

	public void setDataStorageKey(String dataStorageKey) {
		this.dataStorageKey = dataStorageKey;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ImportLog{" +
			", id=" + id +
			", sourceSys=" + sourceSys +
			", sourceKey=" + sourceKey +
			", type=" + type +
			", dataStorageKey=" + dataStorageKey +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
