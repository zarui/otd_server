package com.zhiche.lisa.integration.dto.carrier;

public class SupplierErpDto {

    private Long id;
    private String name;
    /**
     * “1”:自有   “2” 外协  “3”:租赁
     */
    private String property;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SupplierErpDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", property='" + property + '\'' +
                '}';
    }
}
