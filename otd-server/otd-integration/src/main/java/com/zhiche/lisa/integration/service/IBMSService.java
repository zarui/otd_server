package com.zhiche.lisa.integration.service;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

public interface IBMSService {


    /**
     * bms查询标准价格和里程
     */
    JSONObject getPriceAndMiles(Map<String, String> condition);

    /**
     * 回写
     */
    HashMap<String, String> exportPrice(Map<String, String> condition);
}
