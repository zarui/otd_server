package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: G7获取在途信息
 * @Date: Create in 18:25 2019/6/10
 */
public class InTransitDTO implements Serializable {

    /**
     * 车牌号/车船税号/车架号
     */
    private String plateNo;

    /**
     * 在途定位时间
     */
    private Date positionDate;

    /**
     * 定位位置
     */
    private String positionLocation;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 数据来源
     */
    private String dataSource;

    public String getPlateNo () {
        return plateNo;
    }

    public void setPlateNo (String plateNo) {
        this.plateNo = plateNo;
    }

    public Date getPositionDate () {
        return positionDate;
    }

    public void setPositionDate (Date positionDate) {
        this.positionDate = positionDate;
    }

    public String getPositionLocation () {
        return positionLocation;
    }

    public void setPositionLocation (String positionLocation) {
        this.positionLocation = positionLocation;
    }

    public BigDecimal getLongitude () {
        return longitude;
    }

    public void setLongitude (BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude () {
        return latitude;
    }

    public void setLatitude (BigDecimal latitude) {
        this.latitude = latitude;
    }

    public String getDataSource () {
        return dataSource;
    }

    public void setDataSource (String dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("InTransitDTO{");
        sb.append("plateNo='").append(plateNo).append('\'');
        sb.append(", positionDate=").append(positionDate);
        sb.append(", positionLocation='").append(positionLocation).append('\'');
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append(", latitude='").append(latitude).append('\'');
        sb.append(", dataSource='").append(dataSource).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
