package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.model.NewOrderMid;
import com.zhiche.lisa.integration.dao.model.OrderMid;
import com.zhiche.lisa.integration.dto.otd.RouteDelMid;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单信息中间表 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
public interface OrderMidMapper extends BaseMapper<OrderMid> {

    List<OrderMid> selectOrderByPage(Page<OrderMid> page, @Param("ew") EntityWrapper<OrderMid> ew);

    List<NewOrderMid> selectOrderByPageNew(Page<NewOrderMid> page, @Param("ew") EntityWrapper<NewOrderMid> ew);

    List<RouteDelMid> queryRouteDelMid (Page<RouteDelMid> page, @Param("ew") EntityWrapper<RouteDelMid> ew);
}
