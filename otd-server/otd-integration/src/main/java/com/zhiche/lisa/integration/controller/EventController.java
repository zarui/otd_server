package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.ProcessCallBack;
import com.zhiche.lisa.integration.dto.order.OTMEvent;
import com.zhiche.lisa.integration.inteface.otm.OtmEventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zhaoguixin on 2018/7/22.
 */
@RestController
@RequestMapping(value = "/event")
@Api(value = "/event", description = "事件处理", tags = {"事件处理"})
public class EventController {

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OtmEventService otmEventService;

    @PostMapping(value = "/export")
    @ApiOperation(value = "OTM事件导出", notes = "OTM事件导出",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<String> eventExport(@RequestBody OTMEvent paramDTO) {
        RestfulResponse<String> result = new RestfulResponse<>(0, "成功", null);
        try {
            String requestId = otmEventService.exportEvent(paramDTO);
            result.setData(requestId);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/callBack")
    @ApiOperation(value = "OTM回调接口", notes = "OTM回调接口", response = RestfulResponse.class)
    public RestfulResponse callBack(@RequestBody String callBackXml) {
        RestfulResponse<ProcessCallBack> result = new RestfulResponse<>(0, "成功", null);
        try {
            ProcessCallBack callBack = otmEventService.callBack(callBackXml);
            result.setData(callBack);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }

    }

    @PostMapping(value = "/getProcessResult")
    @ApiOperation(value = "获取事件处理结果", notes = "获取事件处理结果(OTM异步处理结果)",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<ProcessCallBack> getProcessResult(@RequestParam("requestId") String requestId) {
        RestfulResponse<ProcessCallBack> result = new RestfulResponse<>(0, "成功", null);
        try {
            ProcessCallBack callBack = otmEventService.getProcessResult(requestId);
            result.setData(callBack);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }
}
