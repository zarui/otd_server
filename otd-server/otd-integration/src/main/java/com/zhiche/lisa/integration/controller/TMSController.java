package com.zhiche.lisa.integration.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import com.zhiche.lisa.integration.dto.tms.TmsCommonDTO;
import com.zhiche.lisa.integration.service.IImportLogHistoryService;
import com.zhiche.lisa.integration.service.TMSAPIService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/tmsAPI")
@Api(value = "tmsAPI", description = "TMS系统对接平台", tags = "TMS系统对接平台")
public class TMSController {

    @Autowired
    private TMSAPIService tmsapiService;
    @Autowired IImportLogHistoryService importLogHistoryService;

    @PostMapping(value = "/shipDeliver")
    @ApiOperation(value = "司机发运推送OTM", notes = "司机发运推送OTM",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Object> shipDeliver(@RequestBody TmsCommonDTO dto) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            tmsapiService.shipDeliver(dto);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping("/sendXMLShipToProv")
    public RestfulResponse<Object> sendXMLShipToProv(TmsCommonDTO dto) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            tmsapiService.sendXMLShipToProv(dto);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping(value = "/queryTmsImportLog")
    @ApiOperation(value = "OTM下发子系统日志", notes = "OTM下发子系统日志",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<ImportLogHistory>> queryTmsImportLog(@RequestBody Page<ImportLogHistory> page) {
        RestfulResponse<Page<ImportLogHistory>> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            response.setData(importLogHistoryService.queryTmsImportLogs(page));
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping(value = "/otmEmptyMonitor")
    @ApiOperation(value = "OTM下发空放指令监控接口", notes = "OTM下发空放指令监控接口",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<Map<String,Object>>> queryOtmEmptyMonitor(@RequestBody Page<ImportLogHistory> page) {
        RestfulResponse<Page<Map<String,Object>>> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            response.setData(importLogHistoryService.queryOtmEmptyMonitor(page));
        } catch (BaseException b) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), b.getMessage(), null);
        } catch (Exception e) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

}
