package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.TransportMode;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM运输方式(运输工具) 服务类
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
public interface ITransportModeService extends IService<TransportMode> {
    /**
     * 根据运输方式编码查询全部返回
     */
    List<TransportMode> queryTransportMode (Map<String,Object> condition);

}
