package com.zhiche.lisa.integration.dto.huiyunche;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrdercostDTO implements Serializable {
    //TMS系统订单编号
    private String orderCode;

    //TMS系统订单流水号
    private String orderlineId;

    //司机编号
    private String vcorderno;

    //身份证号
    private String vccard;

    //公里数
    private String kilometer;

    //劳务费
    private String serviceWage;

    //总油费
    private String oilCost;

    //单公里油价
    private String oilPrice;

    //第一桶油量
    private String firstOilCapacity;

    //第一桶油费用
    private String firstOilTotal;

    //应充卡油量
    private String cardOilCapacity;

    //应充卡油费
    private String cardOilTotal;

    //总油费
    private String confirmTotalCost;

    //市场油价
    private BigDecimal currentOilPrice;

    //计算时间
    private Date currentEffectDate;

    public String getConfirmTotalCost() {
        return confirmTotalCost;
    }

    public void setConfirmTotalCost(String confirmTotalCost) {
        this.confirmTotalCost = confirmTotalCost;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderlineId() {
        return orderlineId;
    }

    public void setOrderlineId(String orderlineId) {
        this.orderlineId = orderlineId;
    }

    public String getVcorderno() {
        return vcorderno;
    }

    public void setVcorderno(String vcorderno) {
        this.vcorderno = vcorderno;
    }

    public String getVccard() {
        return vccard;
    }

    public void setVccard(String vccard) {
        this.vccard = vccard;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getServiceWage() {
        return serviceWage;
    }

    public void setServiceWage(String serviceWage) {
        this.serviceWage = serviceWage;
    }

    public String getOilCost() {
        return oilCost;
    }

    public void setOilCost(String oilCost) {
        this.oilCost = oilCost;
    }

    public String getOilPrice() {
        return oilPrice;
    }

    public void setOilPrice(String oilPrice) {
        this.oilPrice = oilPrice;
    }

    public String getFirstOilCapacity() {
        return firstOilCapacity;
    }

    public void setFirstOilCapacity(String firstOilCapacity) {
        this.firstOilCapacity = firstOilCapacity;
    }

    public String getFirstOilTotal() {
        return firstOilTotal;
    }

    public void setFirstOilTotal(String firstOilTotal) {
        this.firstOilTotal = firstOilTotal;
    }

    public String getCardOilCapacity() {
        return cardOilCapacity;
    }

    public void setCardOilCapacity(String cardOilCapacity) {
        this.cardOilCapacity = cardOilCapacity;
    }

    public String getCardOilTotal() {
        return cardOilTotal;
    }

    public void setCardOilTotal(String cardOilTotal) {
        this.cardOilTotal = cardOilTotal;
    }

    public BigDecimal getCurrentOilPrice() {
        return currentOilPrice;
    }

    public void setCurrentOilPrice(BigDecimal currentOilPrice) {
        this.currentOilPrice = currentOilPrice;
    }

    public Date getCurrentEffectDate() {
        return currentEffectDate;
    }

    public void setCurrentEffectDate(Date currentEffectDate) {
        this.currentEffectDate = currentEffectDate;
    }

    @Override
    public String toString() {
        return "OrdercostDTO{" +
                "orderCode='" + orderCode + '\'' +
                ", orderlineId='" + orderlineId + '\'' +
                ", vcorderno='" + vcorderno + '\'' +
                ", vccard='" + vccard + '\'' +
                ", kilometer='" + kilometer + '\'' +
                ", serviceWage='" + serviceWage + '\'' +
                ", oilCost='" + oilCost + '\'' +
                ", oilPrice='" + oilPrice + '\'' +
                ", firstOilCapacity='" + firstOilCapacity + '\'' +
                ", firstOilTotal='" + firstOilTotal + '\'' +
                ", cardOilCapacity='" + cardOilCapacity + '\'' +
                ", cardOilTotal='" + cardOilTotal + '\'' +
                ", confirmTotalCost='" + confirmTotalCost + '\'' +
                ", currentOilPrice=" + currentOilPrice +
                ", currentEffectDate=" + currentEffectDate +
                '}';
    }
}
