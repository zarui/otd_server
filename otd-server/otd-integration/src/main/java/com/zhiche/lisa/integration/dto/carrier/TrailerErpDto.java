package com.zhiche.lisa.integration.dto.carrier;

import java.io.Serializable;

public class TrailerErpDto implements Serializable {

    private Long id;
    private String trailerPlate;
    private Long axisNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrailerPlate() {
        return trailerPlate;
    }

    public void setTrailerPlate(String trailerPlate) {
        this.trailerPlate = trailerPlate;
    }

    public Long getAxisNumber() {
        return axisNumber;
    }

    public void setAxisNumber(Long axisNumber) {
        this.axisNumber = axisNumber;
    }

    @Override
    public String toString() {
        return "TrailerErpDto{" +
                "id=" + id +
                ", trailerPlate='" + trailerPlate + '\'' +
                ", axisNumber=" + axisNumber +
                '}';
    }
}
