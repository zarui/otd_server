package com.zhiche.lisa.integration.dto.carrier;

import com.zhiche.lisa.integration.anno.ClassAnno;
import com.zhiche.lisa.integration.anno.FiledAnno;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
@ClassAnno("classpath:otmxml/lsp.xml")
public class LspInfoDTO {

    /**
     * 数据处理方式(I:新增,U:更新,D:删除,IU:新增或更新)
     */
    @FiledAnno("${transactionCode}")
    private String transactionCode;
    /**
     * 回调地址
     */
    @FiledAnno("${callBackUrl}")
    private String callBackUrl;
    /**
     * 承运商ID
     */
    @FiledAnno("${lspId}")
    private Long id;
    /**
     * 承运商编码
     */
    private String code;
    /**
     * 全称
     */
    @FiledAnno("${name}")
    private String name;
    /**
     * 办公地点
     */
    private String workingSite;
    /**
     * 注册地址
     */
    @FiledAnno("${registrySite}")
    private String registrySite;
    /**
     * 法人姓名
     */
    @FiledAnno("${legalRep}")
    private String legalRep;
    /**
     * 注册资本
     */
    private BigDecimal registryCapital;
    /**
     * 法人电话
     */
    @FiledAnno("${repMobile}")
    private String repMobile;
    /**
     * 注册时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtUpdate;
    /**
     * 分供方简称
     */
    @FiledAnno("${nameAbbr}")
    private String nameAbbr;
    /**
     * 备注
     */
    private String remark;
    /**
     * 服务类型（多个用分号隔开）
     */
    @FiledAnno("${serviceType}")
    private String serviceType;
    /**
     * 业务状态：0-停用;1-启用
     */
    private Integer status;
    /**
     * 分供方状态(启用:Y，其他:N，回传OTM使用)
     */
    @FiledAnno("${IsActive}")
    private String IsActive;
    /**
     * 认证状态：0-未认证;1-已认证
     */
    private Integer authStatus;

    /**
     * 创建人
     */
    private String creator;
    /**
     * 数据来源
     */
    private String createFrom;
    /**
     * 业务覆盖城市，取城市。多个用分号分割
     */
    private String businessSite;

    /**
     * 所属分供方
     */
    @FiledAnno("${fleetTypeId}")
    private String fleetTypeId;



    public String getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkingSite() {
        return workingSite;
    }

    public void setWorkingSite(String workingSite) {
        this.workingSite = workingSite;
    }

    public String getRegistrySite() {
        return registrySite;
    }

    public void setRegistrySite(String registrySite) {
        this.registrySite = registrySite;
    }

    public String getLegalRep() {
        return legalRep;
    }

    public void setLegalRep(String legalRep) {
        this.legalRep = legalRep;
    }

    public BigDecimal getRegistryCapital() {
        return registryCapital;
    }

    public void setRegistryCapital(BigDecimal registryCapital) {
        this.registryCapital = registryCapital;
    }

    public String getRepMobile() {
        return repMobile;
    }

    public void setRepMobile(String repMobile) {
        this.repMobile = repMobile;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public String getNameAbbr() {
        return nameAbbr;
    }

    public void setNameAbbr(String nameAbbr) {
        this.nameAbbr = nameAbbr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public Integer getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(Integer authStatus) {
        this.authStatus = authStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreateFrom() {
        return createFrom;
    }

    public void setCreateFrom(String createFrom) {
        this.createFrom = createFrom;
    }

    public String getBusinessSite() {
        return businessSite;
    }

    public void setBusinessSite(String businessSite) {
        this.businessSite = businessSite;
    }

    @Override
    public String toString() {
        return "LspInfoDTO{" +
                "transactionCode='" + transactionCode + '\'' +
                ", callBackUrl='" + callBackUrl + '\'' +
                ", id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", workingSite='" + workingSite + '\'' +
                ", registrySite='" + registrySite + '\'' +
                ", legalRep='" + legalRep + '\'' +
                ", registryCapital=" + registryCapital +
                ", repMobile='" + repMobile + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtUpdate=" + gmtUpdate +
                ", nameAbbr='" + nameAbbr + '\'' +
                ", remark='" + remark + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", status=" + status +
                ", IsActive='" + IsActive + '\'' +
                ", authStatus=" + authStatus +
                ", creator='" + creator + '\'' +
                ", createFrom='" + createFrom + '\'' +
                ", businessSite='" + businessSite + '\'' +
                '}';
    }
}
