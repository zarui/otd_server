package com.zhiche.lisa.integration.dao.mapper;

import com.zhiche.lisa.integration.dao.model.Origination;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * OTM起运地 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-15
 */
public interface OriginationMapper extends BaseMapper<Origination> {

    void updateSQLMode();

}
