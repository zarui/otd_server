package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.ProcessCallBack;

/**
 * <p>
 * 接口回调处理 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface ProcessCallBackMapper extends BaseMapper<ProcessCallBack> {

}
