package com.zhiche.lisa.integration.dto.tms;

import java.io.Serializable;

public class TmsCommonDTO implements Serializable {

    private String shipmentGid;
    private String shipPicKeys;
    private String departTime;
    private String xml;
    private String sourceKey;

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getShipmentGid() {
        return shipmentGid;
    }

    public void setShipmentGid(String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getShipPicKeys() {
        return shipPicKeys;
    }

    public void setShipPicKeys(String shipPicKeys) {
        this.shipPicKeys = shipPicKeys;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }
}
