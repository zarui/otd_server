package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.integration.dao.mapper.LimitionRuleMidMapper;
import com.zhiche.lisa.integration.dao.model.LimitionRuleMid;
import com.zhiche.lisa.integration.service.LimitionRuleMidService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * OTM 时效规则明细表视图 服务实现类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@Service
public class LimitionRuleMidServiceImpl extends ServiceImpl<LimitionRuleMidMapper, LimitionRuleMid> implements LimitionRuleMidService {

}
