package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * OTM用户信息
 * </p>
 *
 * @author qichao
 * @since 2018-08-24
 */
@TableName("otm_base_user")
@ApiModel(value = "用户基础数据对象", description = "用户基础数据对象")
public class BaseUser extends Model<BaseUser> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * otm唯一键
     */
    @TableField("user_gid")
    private String userGid;
    /**
     * 编码
     */
    @TableField("user_xid")
    @ApiModelProperty(value="用户编码",name="userXid",dataType = "String")
    private String userXid;
    /**
     * 名称
     */
    @TableField("user_name")
    @ApiModelProperty(value="用户名称",name="userName",dataType = "String")
    private String userName;
    /**
     * 中文名称
     */
    @TableField("common_name")
    @ApiModelProperty(value="中文名称",name="commonName",dataType = "String")
    private String commonName;
    /**
     * 邮箱
     */
    @TableField("email")
    @ApiModelProperty(value="用户邮箱",name="email",dataType = "String")
    private String email;
    /**
     * 域名
     */
    @TableField("domain_name")
    private String domainName;
    /**
     * ldap:ad域
     */
    @TableField("auth_type")
    private String authType;
    /**
     * 组织信息
     */
    @TableField("distinguished_name")
    private String distinguishedName;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    @ApiModelProperty(value="创建时间",name="gmtCreate",dataType = "Long")
    private Date gmtCreate;
    /**
     * 创建时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("status")
    @ApiModelProperty(value="状态 10:正常; 50:停用",name="status",dataType = "String")
    private String status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserGid() {
        return userGid;
    }

    public void setUserGid(String userGid) {
        this.userGid = userGid;
    }

    public String getUserXid() {
        return userXid;
    }

    public void setUserXid(String userXid) {
        this.userXid = userXid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getDistinguishedName() {
        return distinguishedName;
    }

    public void setDistinguishedName(String distinguishedName) {
        this.distinguishedName = distinguishedName;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BaseUser{" +
                "id=" + id +
                ", userGid='" + userGid + '\'' +
                ", userXid='" + userXid + '\'' +
                ", userName='" + userName + '\'' +
                ", commonName='" + commonName + '\'' +
                ", email='" + email + '\'' +
                ", domainName='" + domainName + '\'' +
                ", authType='" + authType + '\'' +
                ", distinguishedName='" + distinguishedName + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                ", status='" + status + '\'' +
                '}';
    }
}
