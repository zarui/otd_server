package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.integration.dao.mapper.ProcessCallBackMapper;
import com.zhiche.lisa.integration.dao.model.ProcessCallBack;
import com.zhiche.lisa.integration.service.IProcessCallBackService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 接口回调处理 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@Service
public class ProcessCallBackServiceImpl extends ServiceImpl<ProcessCallBackMapper, ProcessCallBack> implements IProcessCallBackService {

}
