package com.zhiche.lisa.integration.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import com.zhiche.lisa.integration.dto.order.ReleaseDTO;
import com.zhiche.lisa.integration.dto.order.ShipmentInbound;
import com.zhiche.lisa.integration.dto.order.ShipmentInfo;
import com.zhiche.lisa.integration.dto.tms.PodReasonDTO;
import com.zhiche.lisa.integration.service.OrderReleaseMidService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 14:02 2019/2/28
 */
@RestController
@RequestMapping("/otm")
public class OTMController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OTMController.class);

    @Autowired
    private OrderReleaseMidService orderShipment;

    @PostMapping(value = "/shipmentMonitor")
    @ApiOperation(value = "指令监控", notes = "指令监控",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<ShipmentInfo>> shipmentMonitor (@RequestBody Page<ShipmentInfo> page) {
        LOGGER.info("/shipmentMonitor OTM指令监控数据接口入参page：{}", page);
        RestfulResponse<Page<ShipmentInfo>> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            Page<ShipmentInfo> data = orderShipment.queryShipmentMonitor(page);
            LOGGER.info("/shipmentMonitor OTM指令监控数据接口返回数据为：{}", new ArrayList<>());
            response.setData(data);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }


    @PostMapping(value = "/queryShipmentInbound")
    @ApiOperation(value = "指令入库监控", notes = "指令入库监控",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Page<ShipmentInbound>> queryShipmentInbound (@RequestBody Page<ShipmentInbound> page) {
        LOGGER.info("/queryShipmentInbound OTM查询指令入库监控入参page：{}", page);
        RestfulResponse<Page<ShipmentInbound>> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            Page<ShipmentInbound> data = orderShipment.queryShipmentInbound(page);
            LOGGER.info("/queryShipmentInbound OTM查询指令入库监控返回数据为：{}", new ArrayList<>());
            response.setData(data);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }


    @PostMapping(value = "/otmUpdateReleaseInfo")
    @ApiOperation(value = "otm修改运单信息", notes = "otm修改运单信息",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<String> otmUpdateReleaseInfo (@RequestBody List<ReleaseDTO> param) {
        long startTime = System.currentTimeMillis();
        LOGGER.info("/otmUpdateReleaseInfo otm修改运单信息 param：{}", param);
        RestfulResponse<String> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        orderShipment.otmUpdateReleaseInfo(param);
        long endTime = System.currentTimeMillis();
        LOGGER.info("otm修改运单信息接口耗时 time : {}", endTime - startTime);
        return response;
    }

    @PostMapping(value = "/podReasonToOtm")
    @ApiOperation(value = "异常回单传送OTM", notes = "异常回单传送OTM",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<String> podReasonToOtm (@RequestBody PodReasonDTO podReasonDTO) {
        long startTime = System.currentTimeMillis();
        LOGGER.info("/podReasonToOtm 异常回单传送OTM param：{}", podReasonDTO);
        RestfulResponse<String> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try{
            response.setData(orderShipment.podReasonToOtm(podReasonDTO));
            long endTime = System.currentTimeMillis();
            LOGGER.info("podReasonToOtm异常回单传送OTM time : {}", endTime - startTime);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping(value = "/receiptStatus")
    @ApiOperation(value = "回单状态",notes = "回单状态",response = RestfulResponse.class)
    public RestfulResponse receiptStatus(@RequestBody List<Map<String,String>> maps){
        LOGGER.info("controller:/ShipOrderItem/receiptStatus data: {}", maps);
        RestfulResponse result = new RestfulResponse(0,"成功",null);
        try {
            for (Map<String, String> map : maps) {
                orderShipment.receiptStatus(map);
            }
        }catch (BaseException ex) {
            LOGGER.error("controller:/ShipOrderItem/receiptStatus ERROR... ", ex.getMessage());
            result = new RestfulResponse<>(CommonEnum.ERROR.getCode(), ex.getMessage(), null);
        } catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}
