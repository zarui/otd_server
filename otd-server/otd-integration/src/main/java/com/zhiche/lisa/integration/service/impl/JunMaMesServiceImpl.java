package com.zhiche.lisa.integration.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zhiche.lisa.core.enums.IntegrationURIEnum;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.core.utils.excel.ImportExcelUtil;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import com.zhiche.lisa.integration.dto.junma.MesInboundParamDTO;
import com.zhiche.lisa.integration.service.IImportLogService;
import com.zhiche.lisa.integration.service.IPushSubSystemService;
import com.zhiche.lisa.integration.service.JunMaMesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import java.util.Date;


@Service
@WebService(serviceName = "JunMaMesService",
        targetNamespace = "http://integration.lisa.zhiche.com",
        endpointInterface = "com.zhiche.lisa.integration.service.JunMaMesService")
public class JunMaMesServiceImpl implements JunMaMesService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IPushSubSystemService pushSubSystemService;
    @Autowired
    private IImportLogService importLogService;

    @Override
    public String updateInboundMES(MesInboundParamDTO[] paramDTOS) {

        ImportLogHistory ilh = new ImportLogHistory();
        ilh.setSourceSys(IntegrationURIEnum.SYS_JUNMA_MES.getAddress());
        ilh.setSourceKey(IntegrationURIEnum.SYS_JUNMA_MES.getAddress() +
                ImportExcelUtil.DateUtil.dateToStr(new Date(), ImportExcelUtil.DateUtil.YYYYMMDDHHMMSS));
        ilh.setType(IntegrationURIEnum.SYS_JUNMA_MES.getCode());
        ilh.setImportStartTime(new Date());
        String jsonString = JSONObject.toJSONString(paramDTOS);
        new Thread(() -> {
            try {
                importLogService.saveImportLogToQiniu(ilh, jsonString);
            } catch (Exception e) {
                logger.error("JunMaMesServiceImpl.updateInboundMES saveLog error:{}", e);
            }

        }).start();
        // 发送子系统 wms
        pushSubSystemService.pushToSubSystem(paramDTOS, IntegrationURIEnum.SYS_JUNMA_MES.getCode());
        RestfulResponse<Object> response = new RestfulResponse<>(0, "success", null);
        return JSONObject.toJSONString(response);
    }
}
