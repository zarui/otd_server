package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.CusLimitionMid;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 上游时效规则头信息 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface CusLimitionMidService extends IService<CusLimitionMid> {

    List<CusLimitionMid> queryCusLimition(Map<String, String> dto);
}
