package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.integration.dao.model.PushSubSystem;
import com.zhiche.lisa.integration.dto.carrier.QueryLspInfoDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 接口数据推送子系统 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-30
 */
public interface PushSubSystemMapper extends BaseMapper<PushSubSystem> {

    List<QueryLspInfoDTO> selectLSPinfo(@Param("ew") EntityWrapper<Object> ew);
}
