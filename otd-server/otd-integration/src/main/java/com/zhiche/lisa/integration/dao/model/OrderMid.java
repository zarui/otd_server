package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单信息中间表
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
@TableName("v_order_mid")
public class OrderMid extends Model<OrderMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统单号
     */
    @TableId("order_gid")
    private String orderGid;
    /**
     * order XID
     */
    @TableField("order_xid")
    private String orderXid;
    /**
     * 下单时间
     */
    @TableField("order_time")
    private Date orderTime;
    /**
     * 结算主运输模式
     */
    @TableField("settle_master_mode")
    private String settleMasterMode;
    /**
     * 主运输模式
     */
    @TableField("master_mode")
    private String masterMode;
    /**
     * xxx
     */
    private String project;
    /**
     * 业务主体
     */
    private String ciams;
    /**
     * 客户
     */
    private String customer;
    /**
     * 客户订单号
     */
    @TableField("cus_order_no")
    private String cusOrderNo;
    /**
     * 客户运单号
     */
    @TableField("cus_ship_no")
    private String cusShipNo;
    /**
     * 打单时间
     */
    @TableField("cus_print_time")
    private Date cusPrintTime;
    /**
     * 订单类型
     */
    @TableField("order_type")
    private String orderType;
    /**
     * 订单属性
     */
    @TableField("order_property")
    private String orderProperty;
    /**
     * 当前油价
     */
    @TableField("act_oil")
    private BigDecimal actOil;
    /**
     * 基础油价
     */
    @TableField("base_oil")
    private BigDecimal baseOil;
    /**
     * 回单状态
     */
    @TableField("pod_status")
    private String podStatus;
    /**
     * 结算应收里程
     */
    @TableField("settle_ar_miles")
    private BigDecimal settleArMiles;
    /**
     * 应收里程
     */
    @TableField("ar_miles")
    private BigDecimal arMiles;
    /**
     * 域
     */
    @TableField("domain_name")
    private String domainName;
    /**
     * 打印次数
     */
    @TableField("print_qty")
    private Integer printQty;
    /**
     * 打印时间
     */
    @TableField("print_time")
    private Date printTime;
    /**
     * 打印人
     */
    @TableField("print_man")
    private String printMan;
    /**
     * 收款方
     */
    @TableField("ar_party")
    private String arParty;
    /**
     * 付款方
     */
    @TableField("ap_party")
    private String apParty;
    /**
     * 备注
     */
    private String remark;
    /**
     * 单位
     */
    @TableField("vechile_unit")
    private String vechileUnit;
    /**
     * 商品车数量
     */
    @TableField("vechile_qty")
    private Integer vechileQty;
    /**
     * 是否改装
     */
    private Integer refit;
    /**
     * 改装后重量
     */
    @TableField("refit_wgt")
    private BigDecimal refitWgt;
    /**
     * 改装后高
     */
    @TableField("refit_height")
    private BigDecimal refitHeight;
    /**
     * 改装后宽
     */
    @TableField("refit_width")
    private BigDecimal refitWidth;
    /**
     * 改装后长
     */
    @TableField("refit_len")
    private BigDecimal refitLen;
    /**
     * 车系
     */
    private String vehicleClass;
    /**
     * 品牌
     */
    private String brand;
    /**
     * 路由模式
     */
    @TableField("route_mode")
    private String routeMode;
    /**
     * 扣费应收车型分组
     */
    @TableField("mg10_name")
    private String mg10Name;
    /**
     * 补贴应收车型分组
     */
    @TableField("mg9_name")
    private String mg9Name;
    /**
     * 油价联动应收车型分组
     */
    @TableField("mg8_name")
    private String mg8Name;
    /**
     * 固定费应收车型分组
     */
    @TableField("mg7_name")
    private String mg7Name;
    /**
     * 运输费应收车型分组
     */
    @TableField("mg6_name")
    private String mg6Name;
    /**
     * 结算应收车型Name
     */
    @TableField("settle_ar_model_name")
    private String settleArModelName;
    /**
     * 结算应收车型
     */
    @TableField("settle_ar_model")
    private String settleArModel;
    /**
     * 应收车型
     */
    @TableField("ar_model")
    private String arModel;
    /**
     * 标准车型
     */
    @TableField("std_vechile")
    private String stdVechile;
    /**
     * 车型描述
     */
    @TableField("cus_vechile_des")
    private String cusVechileDes;
    /**
     * 合格证车型代码
     */
    private String cn;
    /**
     * 发动机号
     */
    private String en;
    /**
     * VIN码
     */
    private String vin;
    /**
     * 车牌号
     */
    @TableField("master_number")
    private String masterNumber;
    /**
     * 分供方
     */
    @TableField("master_carrier")
    private String masterCarrier;
    /**
     * 主运输模式
     */
    @TableField("m_trans_mode_name")
    private String mTransModeName;
    /**
     * 主运输模式(带域名)
     */
    @TableField("m_transport_mode")
    private String mTransportMode;
    /**
     * 客户运输模式分类
     */
    @TableField("cus_trans_type")
    private String cusTransType;
    /**
     * 结算目的地地址
     */
    @TableField("settle_dest_addr")
    private String settleDestAddr;
    /**
     * 结算目的地区县
     */
    @TableField("settle_dest_distiricts")
    private String settleDestDistiricts;
    /**
     * 结算目的地城市
     */
    @TableField("settle_dest_city")
    private String settleDestCity;
    /**
     * 结算目的地省份
     */
    @TableField("settle_dest_prov")
    private String settleDestProv;
    /**
     * 结算目的地名称
     */
    @TableField("settle_dest_name")
    private String settleDestName;
    /**
     * 目的地联系电话
     */
    @TableField("dl_att8")
    private String dlAtt8;
    /**
     * 目的地联系人
     */
    @TableField("dl_att7")
    private String dlAtt7;
    /**
     * 目的地地址
     */
    @TableField("dl_desc")
    private String dlDesc;
    /**
     * 目的地区县
     */
    @TableField("dl_zone3")
    private String dlZone3;
    /**
     * 目的地城市
     */
    @TableField("dl_zone2")
    private String dlZone2;
    /**
     * 目的地省份
     */
    @TableField("dl_zone1")
    private String dlZone1;
    /**
     * 目的地区县(客户)
     */
    @TableField("dl_postc")
    private String dlPostc;
    /**
     * 目的地城市(客户)
     */
    @TableField("dl_city")
    private String dlCity;
    /**
     * 目的地省份(客户)
     */
    @TableField("dl_prov")
    private String dlProv;
    /**
     * 目的地名称
     */
    @TableField("dl_loc")
    private String dlLoc;
    /**
     * 目的地编码
     */
    @TableField("dest_location")
    private String destLocation;
    /**
     * 结算起运地地址
     */
    @TableField("settle_source_addr")
    private String settleSourceAddr;
    /**
     * 结算起运地区县
     */
    @TableField("settle_source_distiricts")
    private String settleSourceDistiricts;
    /**
     * 结算起运地城市
     */
    @TableField("settle_source_city")
    private String settleSourceCity;
    /**
     * 结算起运地省份
     */
    @TableField("settle_source_prov")
    private String settleSourceProv;
    /**
     * 结算起运地名称
     */
    @TableField("settle_source_name")
    private String settleSourceName;
    /**
     * 起运地地址
     */
    @TableField("sl_desc")
    private String slDesc;
    /**
     * 起运地区县
     */
    @TableField("sl_zone3")
    private String slZone3;
    /**
     * 起运地城市
     */
    @TableField("sl_zone2")
    private String slZone2;
    /**
     * 起运地省份
     */
    @TableField("sl_zone1")
    private String slZone1;
    /**
     * 起运地区县(客户)
     */
    @TableField("sl_posc")
    private String slPosc;
    /**
     * 起运地城市(客户)
     */
    @TableField("sl_city")
    private String slCity;
    /**
     * 起运地省份(客户)
     */
    @TableField("sl_prov")
    private String slProv;
    /**
     * 起运地名称
     */
    @TableField("sl_loc")
    private String slLoc;
    /**
     * 起运地编码
     */
    @TableField("source_location")
    private String sourceLocation;
    /**
     * 费用状态，结算系统结算后更新状态为“已结算”
     */
    @TableField("freight_status")
    private String freightStatus;
    /**
     * 工厂发运时间
     */
    @TableField("factory_delivery_time")
    private Date factoryDeliveryTime;
    /**
     * 要求抵达时间
     */
    private Date eta;
    /**
     * 要求发运时间
     */
    private Date etd;
    /**
     * 收付类型
     */
    @TableField("arap_type")
    private String arapType;
    /**
     * 一口价费用金额
     */
    @TableField("fixed_price")
    private BigDecimal fixedPrice;
    /**
     * 订单来源
     */
    @TableField("order_source")
    private String orderSource;
    /**
     * 当前节点
     */
    @TableField("current_point")
    private String currentPoint;
    /**
     * 订单状态
     */
    @TableField("order_status")
    private String orderStatus;
    /**
     * 急发
     */
    @TableField("urgency_order")
    private Integer urgencyOrder;
    /**
     * 实际交单时间
     */
    @TableField("a_submit_date")
    private Date aSubmitDate;
    /**
     * 实际回单时间
     */
    @TableField("a_pod_date")
    private Date aPodDate;
    /**
     * 更新时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * 订单插入时间
     */
    @TableField("insert_date")
    private Date insertDate;
    /**
     * 数据-插入时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 数据-更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    private String dlAtt9;

    public String getDlAtt9() {
        return dlAtt9;
    }

    public void setDlAtt9(String dlAtt9) {
        this.dlAtt9 = dlAtt9;
    }

    public String getOrderGid() {
        return orderGid;
    }

    public void setOrderGid(String orderGid) {
        this.orderGid = orderGid;
    }

    public String getOrderXid() {
        return orderXid;
    }

    public void setOrderXid(String orderXid) {
        this.orderXid = orderXid;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getSettleMasterMode() {
        return settleMasterMode;
    }

    public void setSettleMasterMode(String settleMasterMode) {
        this.settleMasterMode = settleMasterMode;
    }

    public String getMasterMode() {
        return masterMode;
    }

    public void setMasterMode(String masterMode) {
        this.masterMode = masterMode;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getCiams() {
        return ciams;
    }

    public void setCiams(String ciams) {
        this.ciams = ciams;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCusOrderNo() {
        return cusOrderNo;
    }

    public void setCusOrderNo(String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusShipNo() {
        return cusShipNo;
    }

    public void setCusShipNo(String cusShipNo) {
        this.cusShipNo = cusShipNo;
    }

    public Date getCusPrintTime() {
        return cusPrintTime;
    }

    public void setCusPrintTime(Date cusPrintTime) {
        this.cusPrintTime = cusPrintTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderProperty() {
        return orderProperty;
    }

    public void setOrderProperty(String orderProperty) {
        this.orderProperty = orderProperty;
    }

    public BigDecimal getActOil() {
        return actOil;
    }

    public void setActOil(BigDecimal actOil) {
        this.actOil = actOil;
    }

    public BigDecimal getBaseOil() {
        return baseOil;
    }

    public void setBaseOil(BigDecimal baseOil) {
        this.baseOil = baseOil;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public BigDecimal getSettleArMiles() {
        return settleArMiles;
    }

    public void setSettleArMiles(BigDecimal settleArMiles) {
        this.settleArMiles = settleArMiles;
    }

    public BigDecimal getArMiles() {
        return arMiles;
    }

    public void setArMiles(BigDecimal arMiles) {
        this.arMiles = arMiles;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Integer getPrintQty() {
        return printQty;
    }

    public void setPrintQty(Integer printQty) {
        this.printQty = printQty;
    }

    public Date getPrintTime() {
        return printTime;
    }

    public void setPrintTime(Date printTime) {
        this.printTime = printTime;
    }

    public String getPrintMan() {
        return printMan;
    }

    public void setPrintMan(String printMan) {
        this.printMan = printMan;
    }

    public String getArParty() {
        return arParty;
    }

    public void setArParty(String arParty) {
        this.arParty = arParty;
    }

    public String getApParty() {
        return apParty;
    }

    public void setApParty(String apParty) {
        this.apParty = apParty;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVechileUnit() {
        return vechileUnit;
    }

    public void setVechileUnit(String vechileUnit) {
        this.vechileUnit = vechileUnit;
    }

    public Integer getVechileQty() {
        return vechileQty;
    }

    public void setVechileQty(Integer vechileQty) {
        this.vechileQty = vechileQty;
    }

    public Integer getRefit() {
        return refit;
    }

    public void setRefit(Integer refit) {
        this.refit = refit;
    }

    public BigDecimal getRefitWgt() {
        return refitWgt;
    }

    public void setRefitWgt(BigDecimal refitWgt) {
        this.refitWgt = refitWgt;
    }

    public BigDecimal getRefitHeight() {
        return refitHeight;
    }

    public void setRefitHeight(BigDecimal refitHeight) {
        this.refitHeight = refitHeight;
    }

    public BigDecimal getRefitWidth() {
        return refitWidth;
    }

    public void setRefitWidth(BigDecimal refitWidth) {
        this.refitWidth = refitWidth;
    }

    public BigDecimal getRefitLen() {
        return refitLen;
    }

    public void setRefitLen(BigDecimal refitLen) {
        this.refitLen = refitLen;
    }

    public String getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRouteMode() {
        return routeMode;
    }

    public void setRouteMode(String routeMode) {
        this.routeMode = routeMode;
    }

    public String getMg10Name() {
        return mg10Name;
    }

    public void setMg10Name(String mg10Name) {
        this.mg10Name = mg10Name;
    }

    public String getMg9Name() {
        return mg9Name;
    }

    public void setMg9Name(String mg9Name) {
        this.mg9Name = mg9Name;
    }

    public String getMg8Name() {
        return mg8Name;
    }

    public void setMg8Name(String mg8Name) {
        this.mg8Name = mg8Name;
    }

    public String getMg7Name() {
        return mg7Name;
    }

    public void setMg7Name(String mg7Name) {
        this.mg7Name = mg7Name;
    }

    public String getMg6Name() {
        return mg6Name;
    }

    public void setMg6Name(String mg6Name) {
        this.mg6Name = mg6Name;
    }

    public String getSettleArModelName() {
        return settleArModelName;
    }

    public void setSettleArModelName(String settleArModelName) {
        this.settleArModelName = settleArModelName;
    }

    public String getSettleArModel() {
        return settleArModel;
    }

    public void setSettleArModel(String settleArModel) {
        this.settleArModel = settleArModel;
    }

    public String getArModel() {
        return arModel;
    }

    public void setArModel(String arModel) {
        this.arModel = arModel;
    }

    public String getStdVechile() {
        return stdVechile;
    }

    public void setStdVechile(String stdVechile) {
        this.stdVechile = stdVechile;
    }

    public String getCusVechileDes() {
        return cusVechileDes;
    }

    public void setCusVechileDes(String cusVechileDes) {
        this.cusVechileDes = cusVechileDes;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMasterNumber() {
        return masterNumber;
    }

    public void setMasterNumber(String masterNumber) {
        this.masterNumber = masterNumber;
    }

    public String getMasterCarrier() {
        return masterCarrier;
    }

    public void setMasterCarrier(String masterCarrier) {
        this.masterCarrier = masterCarrier;
    }

    public String getmTransModeName() {
        return mTransModeName;
    }

    public void setmTransModeName(String mTransModeName) {
        this.mTransModeName = mTransModeName;
    }

    public String getmTransportMode() {
        return mTransportMode;
    }

    public void setmTransportMode(String mTransportMode) {
        this.mTransportMode = mTransportMode;
    }

    public String getCusTransType() {
        return cusTransType;
    }

    public void setCusTransType(String cusTransType) {
        this.cusTransType = cusTransType;
    }

    public String getSettleDestAddr() {
        return settleDestAddr;
    }

    public void setSettleDestAddr(String settleDestAddr) {
        this.settleDestAddr = settleDestAddr;
    }

    public String getSettleDestDistiricts() {
        return settleDestDistiricts;
    }

    public void setSettleDestDistiricts(String settleDestDistiricts) {
        this.settleDestDistiricts = settleDestDistiricts;
    }

    public String getSettleDestCity() {
        return settleDestCity;
    }

    public void setSettleDestCity(String settleDestCity) {
        this.settleDestCity = settleDestCity;
    }

    public String getSettleDestProv() {
        return settleDestProv;
    }

    public void setSettleDestProv(String settleDestProv) {
        this.settleDestProv = settleDestProv;
    }

    public String getSettleDestName() {
        return settleDestName;
    }

    public void setSettleDestName(String settleDestName) {
        this.settleDestName = settleDestName;
    }

    public String getDlAtt8() {
        return dlAtt8;
    }

    public void setDlAtt8(String dlAtt8) {
        this.dlAtt8 = dlAtt8;
    }

    public String getDlAtt7() {
        return dlAtt7;
    }

    public void setDlAtt7(String dlAtt7) {
        this.dlAtt7 = dlAtt7;
    }

    public String getDlDesc() {
        return dlDesc;
    }

    public void setDlDesc(String dlDesc) {
        this.dlDesc = dlDesc;
    }

    public String getDlZone3() {
        return dlZone3;
    }

    public void setDlZone3(String dlZone3) {
        this.dlZone3 = dlZone3;
    }

    public String getDlZone2() {
        return dlZone2;
    }

    public void setDlZone2(String dlZone2) {
        this.dlZone2 = dlZone2;
    }

    public String getDlZone1() {
        return dlZone1;
    }

    public void setDlZone1(String dlZone1) {
        this.dlZone1 = dlZone1;
    }

    public String getDlPostc() {
        return dlPostc;
    }

    public void setDlPostc(String dlPostc) {
        this.dlPostc = dlPostc;
    }

    public String getDlCity() {
        return dlCity;
    }

    public void setDlCity(String dlCity) {
        this.dlCity = dlCity;
    }

    public String getDlProv() {
        return dlProv;
    }

    public void setDlProv(String dlProv) {
        this.dlProv = dlProv;
    }

    public String getDlLoc() {
        return dlLoc;
    }

    public void setDlLoc(String dlLoc) {
        this.dlLoc = dlLoc;
    }

    public String getDestLocation() {
        return destLocation;
    }

    public void setDestLocation(String destLocation) {
        this.destLocation = destLocation;
    }

    public String getSettleSourceAddr() {
        return settleSourceAddr;
    }

    public void setSettleSourceAddr(String settleSourceAddr) {
        this.settleSourceAddr = settleSourceAddr;
    }

    public String getSettleSourceDistiricts() {
        return settleSourceDistiricts;
    }

    public void setSettleSourceDistiricts(String settleSourceDistiricts) {
        this.settleSourceDistiricts = settleSourceDistiricts;
    }

    public String getSettleSourceCity() {
        return settleSourceCity;
    }

    public void setSettleSourceCity(String settleSourceCity) {
        this.settleSourceCity = settleSourceCity;
    }

    public String getSettleSourceProv() {
        return settleSourceProv;
    }

    public void setSettleSourceProv(String settleSourceProv) {
        this.settleSourceProv = settleSourceProv;
    }

    public String getSettleSourceName() {
        return settleSourceName;
    }

    public void setSettleSourceName(String settleSourceName) {
        this.settleSourceName = settleSourceName;
    }

    public String getSlDesc() {
        return slDesc;
    }

    public void setSlDesc(String slDesc) {
        this.slDesc = slDesc;
    }

    public String getSlZone3() {
        return slZone3;
    }

    public void setSlZone3(String slZone3) {
        this.slZone3 = slZone3;
    }

    public String getSlZone2() {
        return slZone2;
    }

    public void setSlZone2(String slZone2) {
        this.slZone2 = slZone2;
    }

    public String getSlZone1() {
        return slZone1;
    }

    public void setSlZone1(String slZone1) {
        this.slZone1 = slZone1;
    }

    public String getSlPosc() {
        return slPosc;
    }

    public void setSlPosc(String slPosc) {
        this.slPosc = slPosc;
    }

    public String getSlCity() {
        return slCity;
    }

    public void setSlCity(String slCity) {
        this.slCity = slCity;
    }

    public String getSlProv() {
        return slProv;
    }

    public void setSlProv(String slProv) {
        this.slProv = slProv;
    }

    public String getSlLoc() {
        return slLoc;
    }

    public void setSlLoc(String slLoc) {
        this.slLoc = slLoc;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

    public String getFreightStatus() {
        return freightStatus;
    }

    public void setFreightStatus(String freightStatus) {
        this.freightStatus = freightStatus;
    }

    public Date getFactoryDeliveryTime() {
        return factoryDeliveryTime;
    }

    public void setFactoryDeliveryTime(Date factoryDeliveryTime) {
        this.factoryDeliveryTime = factoryDeliveryTime;
    }

    public Date getEta() {
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public Date getEtd() {
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public String getArapType() {
        return arapType;
    }

    public void setArapType(String arapType) {
        this.arapType = arapType;
    }

    public BigDecimal getFixedPrice() {
        return fixedPrice;
    }

    public void setFixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(String currentPoint) {
        this.currentPoint = currentPoint;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getUrgencyOrder() {
        return urgencyOrder;
    }

    public void setUrgencyOrder(Integer urgencyOrder) {
        this.urgencyOrder = urgencyOrder;
    }

    public Date getaSubmitDate() {
        return aSubmitDate;
    }

    public void setaSubmitDate(Date aSubmitDate) {
        this.aSubmitDate = aSubmitDate;
    }

    public Date getaPodDate() {
        return aPodDate;
    }

    public void setaPodDate(Date aPodDate) {
        this.aPodDate = aPodDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderGid;
    }

}
