package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysql.jdbc.StringUtils;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.VehicleBrandMapper;
import com.zhiche.lisa.integration.dao.model.VehicleBrand;
import com.zhiche.lisa.integration.service.IVehicleBrandService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class VehicleBrandServiceImpl extends ServiceImpl<VehicleBrandMapper, VehicleBrand> implements IVehicleBrandService {


    @Override
    public List<VehicleBrand> queryListVehicleBrand(Map<String, Object> brandCode) {
        Wrapper<VehicleBrand> ew = buildCondition(brandCode);
        List<VehicleBrand> vehicleBrandList = this.selectList(ew);
        return vehicleBrandList;
    }

    /**
     * 通过品牌名称分页查询数据
     */
    @Override
    public Page<VehicleBrand> queryVehicleBrandPage(Page<VehicleBrand> page) {
        if (page == null) {
            throw new BaseException("分页参数不能为空");
        }
        Map<String, Object> cn = page.getCondition();
        Wrapper<VehicleBrand> ew = buildCondition(cn);
        List<VehicleBrand> brands = baseMapper.selectPage(page, ew);
        page.setRecords(brands);
        return page;


    }


    private Wrapper<VehicleBrand> buildCondition(Map<String, Object> condition) {
        Wrapper<VehicleBrand> ew = new EntityWrapper<>();

        if (!Objects.isNull(condition) && !condition.isEmpty()) {
            // 通过编码查询品牌数据
            Object brandCode = condition.get("code");
            if (brandCode != null && !StringUtils.isNullOrEmpty(brandCode.toString())) {
                ew.eq("code", condition.get("code").toString());
            }
            // 通过品牌名称模糊查询品牌数据
            Object brandName = condition.get("name");
            if (brandName != null && !StringUtils.isNullOrEmpty(brandName.toString())){
                ew.like("name",brandName.toString());
            }
        }
        return ew;
    }
}
