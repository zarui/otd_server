package com.zhiche.lisa.integration.dto.huiyunche;

import java.math.BigDecimal;
import java.util.Date;

public class UsedCarToOtm {

    private String order_time;

    private String order_type;

    private String order_property;

    private String order_source;
    /**
     * 备注
     */
    private String remark;
    /**
     * 型号
     */
    private String std_vechile;

    private String cus_vechile_des;
    /**
     * 车数量
     */
    private String vechile_qty;
    /**
     * 台
     */
    private String vechile_unit; 
    /**
     * 开票
     */
    private String invoice;
    /**
     * 订单号
     */
   private String order_xid;
    /**
     * 客户订单编号
     */
   private String cus_order_no;
    /**
     * 销售订单号
     */
   private String cus_ship_no;
    /**
     * 客户ID
     */
   private String customer;
    /**
     * 起运地名称
     */
   private String sl_location_name;
    /**
     * 发车省名称
     */
    private String sl_province;
    /**
     * 发车市名称
     */
    private String sl_city;
    /**
     * 发车县名称
     */
    private String sl_postal_code;
    /**
     * 发车详细地址
     */
    private String sl_description;

    /**
     * 目的地名称
     */
    private String dl_location_name;
    /**
     * 送达省名称
     */
    private String dl_province;
    /**
     * 送达市名称
     */
    private String dl_city;
    /**
     * 送达县名称
     */
    private String dl_postal_code;
    /**
     * 送达详细地址
     */
    private String dl_description;
    /**
     * 车型ID
     */
    private String Order_model;
    /**
     * 车架号
     */
    private String Vin;
    /**
     * 提车日期
     */
    private String Etd;
    /**
     * 送达日期
     */
    private String Eta;
    /**
     * 距离单位：千米
     */
    private BigDecimal ot__ar_miles;
    /**
     * 金额
     */
    private BigDecimal fixed_price;
    /**
     * 结算方式
     * 现金OR合同
     */
    private String  Cost_type;

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getOrder_property() {
        return order_property;
    }

    public void setOrder_property(String order_property) {
        this.order_property = order_property;
    }

    public String getOrder_source() {
        return order_source;
    }

    public void setOrder_source(String order_source) {
        this.order_source = order_source;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStd_vechile() {
        return std_vechile;
    }

    public void setStd_vechile(String std_vechile) {
        this.std_vechile = std_vechile;
    }

    public String getCus_vechile_des() {
        return cus_vechile_des;
    }

    public void setCus_vechile_des(String cus_vechile_des) {
        this.cus_vechile_des = cus_vechile_des;
    }

    public String getVechile_qty() {
        return vechile_qty;
    }

    public void setVechile_qty(String vechile_qty) {
        this.vechile_qty = vechile_qty;
    }

    public String getVechile_unit() {
        return vechile_unit;
    }

    public void setVechile_unit(String vechile_unit) {
        this.vechile_unit = vechile_unit;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getOrder_xid() {
        return order_xid;
    }

    public void setOrder_xid(String order_xid) {
        this.order_xid = order_xid;
    }

    public String getCus_order_no() {
        return cus_order_no;
    }

    public void setCus_order_no(String cus_order_no) {
        this.cus_order_no = cus_order_no;
    }

    public String getCus_ship_no() {
        return cus_ship_no;
    }

    public void setCus_ship_no(String cus_ship_no) {
        this.cus_ship_no = cus_ship_no;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getSl_location_name() {
        return sl_location_name;
    }

    public void setSl_location_name(String sl_location_name) {
        this.sl_location_name = sl_location_name;
    }

    public String getSl_province() {
        return sl_province;
    }

    public void setSl_province(String sl_province) {
        this.sl_province = sl_province;
    }

    public String getSl_city() {
        return sl_city;
    }

    public void setSl_city(String sl_city) {
        this.sl_city = sl_city;
    }

    public String getSl_postal_code() {
        return sl_postal_code;
    }

    public void setSl_postal_code(String sl_postal_code) {
        this.sl_postal_code = sl_postal_code;
    }

    public String getSl_description() {
        return sl_description;
    }

    public void setSl_description(String sl_description) {
        this.sl_description = sl_description;
    }

    public String getDl_location_name() {
        return dl_location_name;
    }

    public void setDl_location_name(String dl_location_name) {
        this.dl_location_name = dl_location_name;
    }

    public String getDl_province() {
        return dl_province;
    }

    public void setDl_province(String dl_province) {
        this.dl_province = dl_province;
    }

    public String getDl_city() {
        return dl_city;
    }

    public void setDl_city(String dl_city) {
        this.dl_city = dl_city;
    }

    public String getDl_postal_code() {
        return dl_postal_code;
    }

    public void setDl_postal_code(String dl_postal_code) {
        this.dl_postal_code = dl_postal_code;
    }

    public String getDl_description() {
        return dl_description;
    }

    public void setDl_description(String dl_description) {
        this.dl_description = dl_description;
    }

    public String getOrder_model() {
        return Order_model;
    }

    public void setOrder_model(String order_model) {
        Order_model = order_model;
    }

    public String getVin() {
        return Vin;
    }

    public void setVin(String vin) {
        Vin = vin;
    }

    public String getEtd() {
        return Etd;
    }

    public void setEtd(String etd) {
        Etd = etd;
    }

    public String getEta() {
        return Eta;
    }

    public void setEta(String eta) {
        Eta = eta;
    }

    public BigDecimal getOt__ar_miles() {
        return ot__ar_miles;
    }

    public void setOt__ar_miles(BigDecimal ot__ar_miles) {
        this.ot__ar_miles = ot__ar_miles;
    }

    public BigDecimal getFixed_price() {
        return fixed_price;
    }

    public void setFixed_price(BigDecimal fixed_price) {
        this.fixed_price = fixed_price;
    }

    public String getCost_type() {
        return Cost_type;
    }

    public void setCost_type(String cost_type) {
        Cost_type = cost_type;
    }

    @Override
    public String toString() {
        return "UsedCarToOtm{" +
                "order_time='" + order_time + '\'' +
                ", order_type='" + order_type + '\'' +
                ", order_property='" + order_property + '\'' +
                ", order_source='" + order_source + '\'' +
                ", remark='" + remark + '\'' +
                ", std_vechile='" + std_vechile + '\'' +
                ", cus_vechile_des='" + cus_vechile_des + '\'' +
                ", vechile_qty='" + vechile_qty + '\'' +
                ", vechile_unit='" + vechile_unit + '\'' +
                ", invoice='" + invoice + '\'' +
                ", order_xid='" + order_xid + '\'' +
                ", cus_order_no='" + cus_order_no + '\'' +
                ", cus_ship_no='" + cus_ship_no + '\'' +
                ", customer=" + customer +
                ", sl_location_name='" + sl_location_name + '\'' +
                ", sl_province='" + sl_province + '\'' +
                ", sl_city='" + sl_city + '\'' +
                ", sl_postal_code='" + sl_postal_code + '\'' +
                ", sl_description='" + sl_description + '\'' +
                ", dl_location_name='" + dl_location_name + '\'' +
                ", dl_province='" + dl_province + '\'' +
                ", dl_city='" + dl_city + '\'' +
                ", dl_postal_code='" + dl_postal_code + '\'' +
                ", dl_description='" + dl_description + '\'' +
                ", Order_model=" + Order_model +
                ", Vin='" + Vin + '\'' +
                ", Etd='" + Etd + '\'' +
                ", Eta='" + Eta + '\'' +
                ", ot__ar_miles=" + ot__ar_miles +
                ", fixed_price=" + fixed_price +
                ", Cost_type='" + Cost_type + '\'' +
                '}';
    }
}
