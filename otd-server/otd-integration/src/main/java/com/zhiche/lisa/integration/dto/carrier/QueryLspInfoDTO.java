package com.zhiche.lisa.integration.dto.carrier;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 承运商
 * </p>
 *
 * @author qichao
 * @since 2018-08-06
 */
public class QueryLspInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String code;
	/**
	 * 全称
	 */
	private String name;
	/**
	 * 办公地点
	 */
	private String workingSite;
	/**
	 * 注册地址
	 */
	private String registrySite;
	/**
	 * 法人姓名
	 */
	private String legalRep;
	/**
	 * 负责人
	 */
	private String principal;
	/**
	 * 注册资本
	 */
	private BigDecimal registryCapital;
	/**
	 * 承运商邮箱
	 */
	private String cropEmail;
	/**
	 * 合同邮寄地址
	 */
	private String contractAddress;
	/**
	 * 法人电话
	 */
	private String repMobile;
	/**
	 * 负责人电话
	 */
	private String principalMobile;
	/**
	 * 负责人邮箱
	 */
	private String principalEmail;
	/**
	 * 调度负责人
	 */
	private String dispatcher;
	/**
	 * 调度负责人电话
	 */
	private String dispatcherMobile;
	/**
	 * 调度负责人邮箱
	 */
	private String dispatcherEmail;
	/**
	 * 回单负责人
	 */
	private String receipter;
	/**
	 * 回单负责人电话
	 */
	private String receipterMobile;
	/**
	 * 结算负责人邮箱
	 */
	private String accountantEmail;
	/**
	 * 回单负责人邮箱
	 */
	private String receipterEmail;
	/**
	 * 结算负责人
	 */
	private String accountant;
	/**
	 * 结算负责人电话
	 */
	private String accountantMobile;

	private Long tenantId;
	/**
	 * 注册时间
	 */
	private Date gmtCreate;

	private Date gmtUpdate;
	/**
	 * 简称
	 */
	private String nameAbbr;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 服务类型（多个用分号隔开）
	 */
	private String serviceType;
	/**
	 * 业务状态：0-停用;1-启用
	 */
	private Integer status;
	/**
	 * 认证状态：0-未认证;1-已认证
	 */
	private Integer authStatus;
	/**
	 * 创建人
	 */
	private String creator;
	/**
	 * 数据来源
	 */
	private String createFrom;
	/**
	 * 业务覆盖城市，取城市。多个用分号分割
	 */
	private String businessSite;
	/**
	 * 删除标识，默认0，若删除，置为1
	 */
	private Integer isDelete;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkingSite() {
		return workingSite;
	}

	public void setWorkingSite(String workingSite) {
		this.workingSite = workingSite;
	}

	public String getRegistrySite() {
		return registrySite;
	}

	public void setRegistrySite(String registrySite) {
		this.registrySite = registrySite;
	}

	public String getLegalRep() {
		return legalRep;
	}

	public void setLegalRep(String legalRep) {
		this.legalRep = legalRep;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public BigDecimal getRegistryCapital() {
		return registryCapital;
	}

	public void setRegistryCapital(BigDecimal registryCapital) {
		this.registryCapital = registryCapital;
	}

	public String getCropEmail() {
		return cropEmail;
	}

	public void setCropEmail(String cropEmail) {
		this.cropEmail = cropEmail;
	}

	public String getContractAddress() {
		return contractAddress;
	}

	public void setContractAddress(String contractAddress) {
		this.contractAddress = contractAddress;
	}

	public String getRepMobile() {
		return repMobile;
	}

	public void setRepMobile(String repMobile) {
		this.repMobile = repMobile;
	}

	public String getPrincipalMobile() {
		return principalMobile;
	}

	public void setPrincipalMobile(String principalMobile) {
		this.principalMobile = principalMobile;
	}

	public String getPrincipalEmail() {
		return principalEmail;
	}

	public void setPrincipalEmail(String principalEmail) {
		this.principalEmail = principalEmail;
	}

	public String getDispatcher() {
		return dispatcher;
	}

	public void setDispatcher(String dispatcher) {
		this.dispatcher = dispatcher;
	}

	public String getDispatcherMobile() {
		return dispatcherMobile;
	}

	public void setDispatcherMobile(String dispatcherMobile) {
		this.dispatcherMobile = dispatcherMobile;
	}

	public String getDispatcherEmail() {
		return dispatcherEmail;
	}

	public void setDispatcherEmail(String dispatcherEmail) {
		this.dispatcherEmail = dispatcherEmail;
	}

	public String getReceipter() {
		return receipter;
	}

	public void setReceipter(String receipter) {
		this.receipter = receipter;
	}

	public String getReceipterMobile() {
		return receipterMobile;
	}

	public void setReceipterMobile(String receipterMobile) {
		this.receipterMobile = receipterMobile;
	}

	public String getAccountantEmail() {
		return accountantEmail;
	}

	public void setAccountantEmail(String accountantEmail) {
		this.accountantEmail = accountantEmail;
	}

	public String getReceipterEmail() {
		return receipterEmail;
	}

	public void setReceipterEmail(String receipterEmail) {
		this.receipterEmail = receipterEmail;
	}

	public String getAccountant() {
		return accountant;
	}

	public void setAccountant(String accountant) {
		this.accountant = accountant;
	}

	public String getAccountantMobile() {
		return accountantMobile;
	}

	public void setAccountantMobile(String accountantMobile) {
		this.accountantMobile = accountantMobile;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}

	public String getNameAbbr() {
		return nameAbbr;
	}

	public void setNameAbbr(String nameAbbr) {
		this.nameAbbr = nameAbbr;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(Integer authStatus) {
		this.authStatus = authStatus;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateFrom() {
		return createFrom;
	}

	public void setCreateFrom(String createFrom) {
		this.createFrom = createFrom;
	}

	public String getBusinessSite() {
		return businessSite;
	}

	public void setBusinessSite(String businessSite) {
		this.businessSite = businessSite;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "LspInfo{" +
				", id=" + id +
				", code=" + code +
				", name=" + name +
				", workingSite=" + workingSite +
				", registrySite=" + registrySite +
				", legalRep=" + legalRep +
				", principal=" + principal +
				", registryCapital=" + registryCapital +
				", cropEmail=" + cropEmail +
				", contractAddress=" + contractAddress +
				", repMobile=" + repMobile +
				", principalMobile=" + principalMobile +
				", principalEmail=" + principalEmail +
				", dispatcher=" + dispatcher +
				", dispatcherMobile=" + dispatcherMobile +
				", dispatcherEmail=" + dispatcherEmail +
				", receipter=" + receipter +
				", receipterMobile=" + receipterMobile +
				", accountantEmail=" + accountantEmail +
				", receipterEmail=" + receipterEmail +
				", accountant=" + accountant +
				", accountantMobile=" + accountantMobile +
				", tenantId=" + tenantId +
				", gmtCreate=" + gmtCreate +
				", gmtUpdate=" + gmtUpdate +
				", nameAbbr=" + nameAbbr +
				", remark=" + remark +
				", serviceType=" + serviceType +
				", status=" + status +
				", authStatus=" + authStatus +
				", creator=" + creator +
				", createFrom=" + createFrom +
				", businessSite=" + businessSite +
				", isDelete=" + isDelete +
				"}";
	}
}
