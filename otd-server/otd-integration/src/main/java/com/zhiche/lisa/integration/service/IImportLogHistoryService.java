package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;

import java.util.Map;

/**
 * <p>
 * 接口导入日志历史 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface IImportLogHistoryService extends IService<ImportLogHistory> {

    /**
     * 查询OTM下发子系统日志列表
     * @return Object
     */
    Page<ImportLogHistory> queryTmsImportLogs(Page<ImportLogHistory> page);

    /**
     * OTM下发空放指令监控接口
     * @param page
     * @return
     */
    Page<Map<String,Object>> queryOtmEmptyMonitor(Page<ImportLogHistory> page);
}
