package com.zhiche.lisa.integration.dto.order;

import com.zhiche.lisa.integration.anno.ClassAnno;
import com.zhiche.lisa.integration.anno.FiledAnno;

/**
 * Created by zhaoguixin on 2018/7/22.
 */
@ClassAnno("classpath:otmxml/event.xml")
public class OTMEvent {

    /**
     * 导出唯一键
     */
    private String exportKey;
    /**
     * 回调地址
     */
    @FiledAnno("${callBackUrl}")
    private String callBackUrl;
    /**
     * 事件类型
     * 已入库：BS_WMS_IN
     * 已发运：BS_OP_DELIVERY
     * 已运抵：BS_TRANS_ARRIVED
     * 已寻车：OR_FIND
     * 在途：BS_ENROUTED
     * 绑码：BINDING_CODE
     * 异常：ABNORMAL
     */
    @FiledAnno("${eventType}")
    private String eventType;
    /**
     * 事件发生时间
     */
    @FiledAnno("${occurDate}")
    private String occurDate;
    /**
     * 时间接收时间
     */
    @FiledAnno("${recdDate}")
    private String recdDate;
    /**
     * 发生事件的站点序号
     */
    @FiledAnno("${sort}")
    private int sort = 1;
    /**
     * 状态描述（已入库/已发运/已运抵）
     */
    @FiledAnno("${describe}")
    private String describe;
    /**
     * 运输订单号
     */
    @FiledAnno("${orderReleaseId}")
    private String orderReleaseId;
    /**
     * 指令号
     */
    @FiledAnno("${shipmentId}")
    private String shipmentId;
    /**
     * 二维码信息
     */
    @FiledAnno("${qrCode}")
    private String qrCode;

    /**
     * 车架号
     */
    @FiledAnno("${vin}")
    private String vin;

    @FiledAnno("${province}")
    private String province;

    @FiledAnno("${city}")
    private String city;

    @FiledAnno("${county}")
    private String county;

    @FiledAnno("${address}")
    private String address;

    @FiledAnno("${gpsLongitude}")
    private String gpsLongitude;

    @FiledAnno("${gpsLatitude}")
    private String gpsLatitude;

    @FiledAnno("${eventPicKeys}")
    private String shipToKeys;//司机运抵时拍摄照片


    private String plateNo;//车牌号
    private String trailerNo;//挂车号
    private String vehicleName;//标准车型
    private String originProvince;
    private String originCity;
    private String originCounty;
    private String originAddr;
    private String destProvince;
    private String destCity;
    private String destCounty;
    private String destAddr;
    private String driverGid;
    private String providerGid;
    private String shipTime;
    private String originCode;
    private String destCode;
    private String cusOrderNo;
    private String cusWaybill;

    public String getShipToKeys() {
        return shipToKeys;
    }

    public void setShipToKeys(String shipToKeys) {
        this.shipToKeys = shipToKeys;
    }

    public String getCusOrderNo() {
        return cusOrderNo;
    }

    public void setCusOrderNo(String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybill() {
        return cusWaybill;
    }

    public void setCusWaybill(String cusWaybill) {
        this.cusWaybill = cusWaybill;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getOriginProvince() {
        return originProvince;
    }

    public void setOriginProvince(String originProvince) {
        this.originProvince = originProvince;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getOriginCounty() {
        return originCounty;
    }

    public void setOriginCounty(String originCounty) {
        this.originCounty = originCounty;
    }

    public String getOriginAddr() {
        return originAddr;
    }

    public void setOriginAddr(String originAddr) {
        this.originAddr = originAddr;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getDestCounty() {
        return destCounty;
    }

    public void setDestCounty(String destCounty) {
        this.destCounty = destCounty;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public String getDriverGid() {
        return driverGid;
    }

    public void setDriverGid(String driverGid) {
        this.driverGid = driverGid;
    }

    public String getProviderGid() {
        return providerGid;
    }

    public void setProviderGid(String providerGid) {
        this.providerGid = providerGid;
    }

    public String getShipTime() {
        return shipTime;
    }

    public void setShipTime(String shipTime) {
        this.shipTime = shipTime;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public void setDestCode(String destCode) {
        this.destCode = destCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(String gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public String getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(String gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public String getExportKey() {
        return exportKey;
    }

    public void setExportKey(String exportKey) {
        this.exportKey = exportKey;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getOccurDate() {
        return occurDate;
    }

    public void setOccurDate(String occurDate) {
        this.occurDate = occurDate;
    }

    public String getRecdDate() {
        return recdDate;
    }

    public void setRecdDate(String recdDate) {
        this.recdDate = recdDate;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getOrderReleaseId() {
        return orderReleaseId;
    }

    public void setOrderReleaseId(String orderReleaseId) {
        this.orderReleaseId = orderReleaseId;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Override
    public String toString() {
        return "OTMEvent{" +
                "exportKey='" + exportKey + '\'' +
                ", callBackUrl='" + callBackUrl + '\'' +
                ", eventType='" + eventType + '\'' +
                ", occurDate='" + occurDate + '\'' +
                ", recdDate='" + recdDate + '\'' +
                ", sort=" + sort +
                ", describe='" + describe + '\'' +
                ", orderReleaseId='" + orderReleaseId + '\'' +
                ", shipmentId='" + shipmentId + '\'' +
                ", qrCode='" + qrCode + '\'' +
                ", vin='" + vin + '\'' +
                '}';
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
