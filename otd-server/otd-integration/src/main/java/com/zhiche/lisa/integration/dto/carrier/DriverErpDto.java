package com.zhiche.lisa.integration.dto.carrier;

import java.io.Serializable;

/**
 * Created by hxh on 2018/9/05.
 */
public class DriverErpDto implements Serializable {

    private Long id;
    private String driverName;
    private String driverMobile;
    private String driverPhone;
    private String lspId;
    private String lspName;
    private String fleetId;
    private String fleetName;
    private String licenseId;
    /**
     * “1”:自有   “2” 外协  “3”:租赁
     */
    private String property;

    private String idNo;

    private String carrierId;

    private String plate;


    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getLspId() {
        return lspId;
    }

    public void setLspId(String lspId) {
        this.lspId = lspId;
    }

    public String getLspName() {
        return lspName;
    }

    public void setLspName(String lspName) {
        this.lspName = lspName;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    @Override
    public String toString() {
        return "DriverErpDto{" +
                "id=" + id +
                ", driverName='" + driverName + '\'' +
                ", driverMobile='" + driverMobile + '\'' +
                ", lspId='" + lspId + '\'' +
                ", lspName='" + lspName + '\'' +
                ", fleetId='" + fleetId + '\'' +
                ", fleetName='" + fleetName + '\'' +
                ", licenseId='" + licenseId + '\'' +
                ", property='" + property + '\'' +
                '}';
    }
}
