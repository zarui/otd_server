package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.oilCardPay.FleetToOliCardDTO;
import com.zhiche.lisa.integration.inteface.erp.OilCardPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 运力平台对接油卡支付接口
 */
@RestController
@RequestMapping("/oilCardPay")
@Api(value = "oilCardPay", description = "对接油卡支付的接口")
public class OilCardPayController {
    @Autowired
    private OilCardPayService oilCardPayService;

    @PostMapping(value = "/fleetPushPay")
    @ApiOperation(value = "车队（运力平台推送车队到油卡支付）", notes = "车队（运力平台推送车队到油卡支付）",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse carrierPushErp(@RequestBody FleetToOliCardDTO fleetToOliCardDTO) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            oilCardPayService.fleetPushPay(fleetToOliCardDTO);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }
}
