package com.zhiche.lisa.integration.dto.oilCardPay;

public class FleetToOliCardDTO {
    /**
     * 车队编码
     */
    private String code;
    /**
     * 车队名称
     */
    private String name;
    /**
     * 车队管理员
     */
    private String administrator;
    /**
     * 管理员手机号
     */
    private String mobile;
    /**
     * 所属企业名称
     */
    private String corpName;

    /**
     * “1”:自有   “2” 外协  “3”:租赁
     */
    private String property;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdministrator() {
        return administrator;
    }

    public void setAdministrator(String administrator) {
        this.administrator = administrator;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public String toString() {
        return "FleetToOliCardDTO{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", administrator='" + administrator + '\'' +
                ", mobile='" + mobile + '\'' +
                ", corpName='" + corpName + '\'' +
                ", property='" + property + '\'' +
                '}';
    }
}
