package com.zhiche.lisa.integration.service;

import com.zhiche.lisa.integration.dto.sparePart.SPReleaseDTO;

import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 零部件系统接口查询
 * @Date: Create in 9:57 2019/7/19
 */
public interface ISPReleaseService {
    /**
     *  零部件查询运单信息
     */
    List<SPReleaseDTO> queryReleaseData (Map<String, String> param);
}
