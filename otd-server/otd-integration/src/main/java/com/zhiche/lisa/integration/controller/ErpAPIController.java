package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.carrier.CarrierErpDto;
import com.zhiche.lisa.integration.dto.carrier.DriverErpDto;
import com.zhiche.lisa.integration.dto.carrier.SupplierErpDto;
import com.zhiche.lisa.integration.dto.carrier.TrailerErpDto;
import com.zhiche.lisa.integration.inteface.erp.ErpShippingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 对接ERP 接口
 * <p>
 * 1.获取车头信息
 * 2.获取挂车信息
 * 3.获取司机信息
 * </p>
 */
@RestController
@RequestMapping("/erp-api")
@Api(value = "erp-api", description = "供ERP的接口", tags = {"提供给第三方的接口"})
public class ErpAPIController {

    @Autowired
    private ErpShippingService erpShippingService;

    @PostMapping(value = "/carrierPushErp")
    @ApiOperation(value = "牵引车（运力平台推送OTM，自有车队推送ERP）", notes = "牵引车（运力平台推送OTM，自有车队推送ERP）",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse carrierPushErp(@RequestBody CarrierErpDto carrierErpDto) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            erpShippingService.carrierPushErp(carrierErpDto);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping(value = "/trailerPushErp")
    @ApiOperation(value = "挂车数据同步", notes = "挂车数据同步",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse trailerPushErp(@RequestBody TrailerErpDto trailerErpDto) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            erpShippingService.trailerPushErp(trailerErpDto);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping(value = "/driverPushErp")
    @ApiOperation(value = "司机数据同步", notes = "司机数据同步",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse driverPushErp(@RequestBody DriverErpDto driverErpDto) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            erpShippingService.driverPushErp(driverErpDto);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }

    @PostMapping(value = "/supplierPushErp")
    @ApiOperation(value = "供方（车队）数据同步", notes = "供方（车队）数据同步",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse supplierPushErp(@RequestBody SupplierErpDto supplierErpDto) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            erpShippingService.supplierPushErp(supplierErpDto);
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }
}
