package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.BaseUser;
import com.zhiche.lisa.integration.dto.base.BaseUserDTO;
import com.zhiche.lisa.integration.dto.base.UserDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM用户信息 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-08-24
 */
public interface IBaseUserService extends IService<BaseUser> {

    /**
     * 根据条件查询用户信息
     */
    List<UserDTO> queryAllUser(Map<String, String> condition);
    /**
     * 根据根据用户中文名称/用户编码分页查询
     */
    Page<BaseUser> queryUserPage(Page<BaseUser> page);

    List<BaseUserDTO> queryUserOTD(Map<String, String> dto);
}
