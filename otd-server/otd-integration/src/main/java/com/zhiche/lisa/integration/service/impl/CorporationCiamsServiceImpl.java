package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.CorporationCiamsMapper;
import com.zhiche.lisa.integration.dao.model.CorporationCiams;
import com.zhiche.lisa.integration.dto.otd.CorpCiamDTO;
import com.zhiche.lisa.integration.service.CorporationCiamsService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 客户业务主体关联表 服务实现类
 * </p>
 *
 * @since 2018-12-06
 */
@Service
public class CorporationCiamsServiceImpl extends ServiceImpl<CorporationCiamsMapper, CorporationCiams> implements CorporationCiamsService {

    @Override
    public List<CorpCiamDTO> queryCorpCiamOTD(Map<String, String> dto) {
        if (dto == null || dto.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String queryAll = dto.get("queryAll");
        String timeStamp = dto.get("timeStamp");
        EntityWrapper<CorpCiamDTO> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                ew.gt("gmt_modified", new Date(longTime));
            }
        }
        ew.orderBy("id", false);
        return baseMapper.selectListOTD(ew);
    }
}
