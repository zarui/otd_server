package com.zhiche.lisa.integration.dto.huiyunche;

import java.io.Serializable;

public class HuiyuncheShipInfoDTO implements Serializable {

    /**
     * 数量
     */
    private String dcqty;
    /**
     * 2018-07-27 11;06;22, --TMS系统订单状态变更时间
     */
    private String dtassigndate;
    /**
     * null,  --要求发运时间
     */
    private String dtcomedate;
    /**
     * 2018-06-28 16;13;35, --TMS创建时间
     */
    private String dtcreatedate;
    /**
     * 2018-06-28 16;13;35, --工厂订单日期
     */
    private String dtorderdate;
    /**
     * 2018-06-28 17;29;24, --要求发运时间
     */
    private String dtshipdate;
    /**
     * 1, --客户ID（新增）
     */
    private String icustomerid;
    /**
     * 1874080,--流水号
     */
    private String ilineid;
    /**
     * 37,--车型分类ID（新增）
     */
    private String istyleid;
    /**
     * 0,  --急发
     */
    private String iurgent;
    /**
     * null, --客户备注说明
     */
    private String memo;
    /**
     * 10,  --操作：(10;正常，20; 挂起，30：取消)
     */
    private String opration;
    /**
     * 广州市白云大道北1367号（白云堡立交桥北100米）, --收货人地址
     */
    private String vcaddress;
    /**
     * 广州, --目的地市
     */
    private String vccityname;
    /**
     * 章有进/,  --收货人姓名
     */
    private String vccontact;
    /**
     * 广州市顺福铃汽车有限公司,  --经销商名称
     */
    private String vcdealername;
    /**
     * 1215113A,--经销商编号
     */
    private String vcdealerno;
    /**
     * 广州市顺福铃汽车有限公司, --经销商的货运地收货点名称
     */
    private String vcdestname;
    /**
     * A2510428,   --经销商的货运地收货点编号
     */
    private String vcdestno;
    /**
     * 13751806622, --收货人手机
     */
    private String vcmobile;
    /**
     * 0171537111,   --订单编号/指令号
     */
    private String vcorderno;
    /**
     * C, --订单类型
     */
    private String vcordertype;
    /**
     * 广东, --目的地省份
     */
    private String vcprovincename;
    /**
     * null,   --发运要求
     */
    private String vcrequire;
    /**
     * 全顺V348长轴v柴,  --车型名称（新增）
     */
    private String vcsstylename;
    /**
     * 南昌, --起运地
     */
    private String vcstartcity;
    /**
     * 江西,  --起运地省(中联库)
     */
    private String vcstartprovince;
    /**
     * 全顺V348长轴v柴,   --车型分类名称
     */
    private String vcstylename;
    /**
     * AYHEBVPGMJCAN0W002,  --车型编号
     */
    private String vcstyleno;
    /**
     * 020-86058966,  --收货人电话
     */
    private String vctel;
    /**
     * 人送,   --运输模式名称
     */
    private String vctransname;
    /**
     * 001, --运输模式编号
     */
    private String vctransno;
    /**
     * 辆,   --单位
     */
    private String vcunit;
    /**
     * LJXBMDJD8JT061071,   --车架号(底盘号)
     */
    private String vcvin;
    /**
     * null,    --提车地址(中联库)
     */
    private String vcwarehouseaddress;
    /**
     * 改装厂,    --司机提车仓库名称(中联库)
     */
    private String vcwarehousename;
    /**
     * V001,  --司机提车仓库编号(中联库)
     */
    private String vcwarehouseno;
    /**
     * 长15中普重欧V榉木不带窗框ABS-MCA改装车 --车型说明
     */
    private String vehicledesc;
    /**
     * 下单价格
     */
    private OrdercostDTO ordercosts;
    /**
     * 司机手机号
     */
    private String driverPhone;

    /**
     *  OR
     */
    private String vclineid;
    /**
     *  客户订单号
     */
    private String cusorderno;


    /**
     * 运输任务开始顺序（0开始，数字越大，顺序约靠后）
     */
    private Integer shipTaskOriginOrder;
    /**
     * 运输任务结束顺序（0开始，数字越大，顺序约靠后）
     */
    private Integer shipTaskDestOrder;

    public String getVclineid() {
        return vclineid;
    }

    public void setVclineid(String vclineid) {
        this.vclineid = vclineid;
    }

    public String getCusorderno() {
        return cusorderno;
    }

    public void setCusorderno(String cusorderno) {
        this.cusorderno = cusorderno;
    }

    public Integer getShipTaskOriginOrder() {
        return shipTaskOriginOrder;
    }

    public void setShipTaskOriginOrder(Integer shipTaskOriginOrder) {
        this.shipTaskOriginOrder = shipTaskOriginOrder;
    }

    public Integer getShipTaskDestOrder() {
        return shipTaskDestOrder;
    }

    public void setShipTaskDestOrder(Integer shipTaskDestOrder) {
        this.shipTaskDestOrder = shipTaskDestOrder;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public OrdercostDTO getOrdercosts() {
        return ordercosts;
    }

    public void setOrdercosts(OrdercostDTO ordercosts) {
        this.ordercosts = ordercosts;
    }

    public String getDcqty() {
        return dcqty;
    }

    public void setDcqty(String dcqty) {
        this.dcqty = dcqty;
    }

    public String getDtassigndate() {
        return dtassigndate;
    }

    public void setDtassigndate(String dtassigndate) {
        this.dtassigndate = dtassigndate;
    }

    public String getDtcomedate() {
        return dtcomedate;
    }

    public void setDtcomedate(String dtcomedate) {
        this.dtcomedate = dtcomedate;
    }

    public String getDtcreatedate() {
        return dtcreatedate;
    }

    public void setDtcreatedate(String dtcreatedate) {
        this.dtcreatedate = dtcreatedate;
    }

    public String getDtorderdate() {
        return dtorderdate;
    }

    public void setDtorderdate(String dtorderdate) {
        this.dtorderdate = dtorderdate;
    }

    public String getDtshipdate() {
        return dtshipdate;
    }

    public void setDtshipdate(String dtshipdate) {
        this.dtshipdate = dtshipdate;
    }

    public String getIcustomerid() {
        return icustomerid;
    }

    public void setIcustomerid(String icustomerid) {
        this.icustomerid = icustomerid;
    }

    public String getIlineid() {
        return ilineid;
    }

    public void setIlineid(String ilineid) {
        this.ilineid = ilineid;
    }

    public String getIstyleid() {
        return istyleid;
    }

    public void setIstyleid(String istyleid) {
        this.istyleid = istyleid;
    }

    public String getIurgent() {
        return iurgent;
    }

    public void setIurgent(String iurgent) {
        this.iurgent = iurgent;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getOpration() {
        return opration;
    }

    public void setOpration(String opration) {
        this.opration = opration;
    }

    public String getVcaddress() {
        return vcaddress;
    }

    public void setVcaddress(String vcaddress) {
        this.vcaddress = vcaddress;
    }

    public String getVccityname() {
        return vccityname;
    }

    public void setVccityname(String vccityname) {
        this.vccityname = vccityname;
    }

    public String getVccontact() {
        return vccontact;
    }

    public void setVccontact(String vccontact) {
        this.vccontact = vccontact;
    }

    public String getVcdealername() {
        return vcdealername;
    }

    public void setVcdealername(String vcdealername) {
        this.vcdealername = vcdealername;
    }

    public String getVcdealerno() {
        return vcdealerno;
    }

    public void setVcdealerno(String vcdealerno) {
        this.vcdealerno = vcdealerno;
    }

    public String getVcdestname() {
        return vcdestname;
    }

    public void setVcdestname(String vcdestname) {
        this.vcdestname = vcdestname;
    }

    public String getVcdestno() {
        return vcdestno;
    }

    public void setVcdestno(String vcdestno) {
        this.vcdestno = vcdestno;
    }

    public String getVcmobile() {
        return vcmobile;
    }

    public void setVcmobile(String vcmobile) {
        this.vcmobile = vcmobile;
    }

    public String getVcorderno() {
        return vcorderno;
    }

    public void setVcorderno(String vcorderno) {
        this.vcorderno = vcorderno;
    }

    public String getVcordertype() {
        return vcordertype;
    }

    public void setVcordertype(String vcordertype) {
        this.vcordertype = vcordertype;
    }

    public String getVcprovincename() {
        return vcprovincename;
    }

    public void setVcprovincename(String vcprovincename) {
        this.vcprovincename = vcprovincename;
    }

    public String getVcrequire() {
        return vcrequire;
    }

    public void setVcrequire(String vcrequire) {
        this.vcrequire = vcrequire;
    }

    public String getVcsstylename() {
        return vcsstylename;
    }

    public void setVcsstylename(String vcsstylename) {
        this.vcsstylename = vcsstylename;
    }

    public String getVcstartcity() {
        return vcstartcity;
    }

    public void setVcstartcity(String vcstartcity) {
        this.vcstartcity = vcstartcity;
    }

    public String getVcstartprovince() {
        return vcstartprovince;
    }

    public void setVcstartprovince(String vcstartprovince) {
        this.vcstartprovince = vcstartprovince;
    }

    public String getVcstylename() {
        return vcstylename;
    }

    public void setVcstylename(String vcstylename) {
        this.vcstylename = vcstylename;
    }

    public String getVcstyleno() {
        return vcstyleno;
    }

    public void setVcstyleno(String vcstyleno) {
        this.vcstyleno = vcstyleno;
    }

    public String getVctel() {
        return vctel;
    }

    public void setVctel(String vctel) {
        this.vctel = vctel;
    }

    public String getVctransname() {
        return vctransname;
    }

    public void setVctransname(String vctransname) {
        this.vctransname = vctransname;
    }

    public String getVctransno() {
        return vctransno;
    }

    public void setVctransno(String vctransno) {
        this.vctransno = vctransno;
    }

    public String getVcunit() {
        return vcunit;
    }

    public void setVcunit(String vcunit) {
        this.vcunit = vcunit;
    }

    public String getVcvin() {
        return vcvin;
    }

    public void setVcvin(String vcvin) {
        this.vcvin = vcvin;
    }

    public String getVcwarehouseaddress() {
        return vcwarehouseaddress;
    }

    public void setVcwarehouseaddress(String vcwarehouseaddress) {
        this.vcwarehouseaddress = vcwarehouseaddress;
    }

    public String getVcwarehousename() {
        return vcwarehousename;
    }

    public void setVcwarehousename(String vcwarehousename) {
        this.vcwarehousename = vcwarehousename;
    }

    public String getVcwarehouseno() {
        return vcwarehouseno;
    }

    public void setVcwarehouseno(String vcwarehouseno) {
        this.vcwarehouseno = vcwarehouseno;
    }

    public String getVehicledesc() {
        return vehicledesc;
    }

    public void setVehicledesc(String vehicledesc) {
        this.vehicledesc = vehicledesc;
    }
}
