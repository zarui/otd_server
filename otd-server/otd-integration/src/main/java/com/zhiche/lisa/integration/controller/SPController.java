package com.zhiche.lisa.integration.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.order.ShipmentInfo;
import com.zhiche.lisa.integration.dto.sparePart.SPReleaseDTO;
import com.zhiche.lisa.integration.service.ISPReleaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 零部件系统所需接口
 * @Date: Create in 9:52 2019/7/19
 */

@RestController
@RequestMapping(value = "/sparePart")
public class SPController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SPController.class);

    @Autowired
    private ISPReleaseService ispReleaseService;

    /**
     * 零部件查询运单信息
     */
    @PostMapping(value = "/queryReleaseData")
    @ApiOperation(value = "零部件查询运单信息", notes = "零部件查询运单信息", produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<List<SPReleaseDTO>> queryReleaseData (@RequestBody Map<String, String> param) {
        LOGGER.info("/queryReleaseInfo 零部件查询运单信息 page：{}", param);
        RestfulResponse<List<SPReleaseDTO>> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        List<SPReleaseDTO> data = ispReleaseService.queryReleaseData(param);
        LOGGER.info("/queryReleaseInfo 零部件查询运单信息结果为：{}", new ArrayList<>());
        response.setData(data);
        return response;
    }
}
