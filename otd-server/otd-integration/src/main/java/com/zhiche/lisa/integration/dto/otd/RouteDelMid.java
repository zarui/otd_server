package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description: 查询路由删除记录
 * @Date: Create in 11:47 2019/7/10
 */
public class RouteDelMid implements Serializable {

    /**
     * 路由GID
     */
    private String orderRouteGid;

    /**
     * 删除用户
     */
    private String deleteUser;

    /**
     * 删除时间
     */
    private Date gmtCreate;

    public String getOrderRouteGid () {
        return orderRouteGid;
    }

    public void setOrderRouteGid (String orderRouteGid) {
        this.orderRouteGid = orderRouteGid;
    }

    public String getDeleteUser () {
        return deleteUser;
    }

    public void setDeleteUser (String deleteUser) {
        this.deleteUser = deleteUser;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("RouteDelMid{");
        sb.append("orderRouteGid='").append(orderRouteGid).append('\'');
        sb.append(", deleteUser='").append(deleteUser).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append('}');
        return sb.toString();
    }
}
