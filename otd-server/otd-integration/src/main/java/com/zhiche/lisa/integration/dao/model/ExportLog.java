package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 接口导出日志
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@TableName("itf_export_log")
public class ExportLog extends Model<ExportLog> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 来源系统(erp,otm)
     */
	@TableField("targert_sys")
	private String targertSys;
    /**
     * 来源唯一键
     */
	@TableField("export_key")
	private String exportKey;
    /**
     * 类型(10:寻车,11:移车,12:提车,20:入库,21:出库,30:发运,31:在途,32:运抵,40:承运商,41:车辆,42:司机)
     */
	private String type;
    /**
     * 数据存储键
     */
	@TableField("data_storage_key")
	private String dataStorageKey;
    /**
     * 导出状态(1:成功,0:失败)
     */
	@TableField("export_status")
	private String exportStatus;
	/**
	 * 请求ID
	 */
	@TableField("request_id")
	private String requestId;
    /**
     * 导出备注
     */
	@TableField("export_remarks")
	private String exportRemarks;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTargertSys() {
		return targertSys;
	}

	public void setTargertSys(String targertSys) {
		this.targertSys = targertSys;
	}

	public String getExportKey() {
		return exportKey;
	}

	public void setExportKey(String exportKey) {
		this.exportKey = exportKey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDataStorageKey() {
		return dataStorageKey;
	}

	public void setDataStorageKey(String dataStorageKey) {
		this.dataStorageKey = dataStorageKey;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getExportRemarks() {
		return exportRemarks;
	}

	public void setExportRemarks(String exportRemarks) {
		this.exportRemarks = exportRemarks;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ExportLog{" +
			", id=" + id +
			", targertSys=" + targertSys +
			", exportKey=" + exportKey +
			", type=" + type +
			", dataStorageKey=" + dataStorageKey +
			", exportStatus=" + exportStatus +
			", exportRemarks=" + exportRemarks +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
}
