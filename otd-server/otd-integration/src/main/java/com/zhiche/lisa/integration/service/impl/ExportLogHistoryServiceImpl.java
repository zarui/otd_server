package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.ExportLogHistoryMapper;
import com.zhiche.lisa.integration.dao.model.ExportLogHistory;
import com.zhiche.lisa.integration.service.IExportLogHistoryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 接口导出日志历史 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@Service
public class ExportLogHistoryServiceImpl extends ServiceImpl<ExportLogHistoryMapper, ExportLogHistory> implements IExportLogHistoryService {

    @Override
    public Object getCheckinLogList(String checkinId) {

        if(null == checkinId || "".equals(checkinId)){
            throw  new BaseException("报班ID不能为空！");
        }

        Wrapper<ExportLogHistory> wrapper = new EntityWrapper<>();
        wrapper.eq("export_key",checkinId);
        wrapper.eq("type","53");
        List<ExportLogHistory> exportLogHistory = baseMapper.selectList(wrapper);

        return exportLogHistory;
    }
}
