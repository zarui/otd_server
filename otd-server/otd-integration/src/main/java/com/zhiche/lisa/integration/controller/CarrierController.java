package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.carrier.*;
import com.zhiche.lisa.integration.inteface.otm.OtmEventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
@RestController
@RequestMapping(value = "/carrier")
@Api(value = "/carrier", description = "运力接口",tags={"运力接口"})
public class CarrierController {

    @Autowired
    private OtmEventService otmEventService;

    @PostMapping(value = "/lspExport")
    @ApiOperation(value = "承运商导出", notes = "承运商导出",
            produces = MediaType.APPLICATION_JSON_VALUE,response = RestfulResponse.class)
    public RestfulResponse<String> lspExport(@RequestBody LspInfoDTO lspInfoDTO){
        RestfulResponse<String> result = new RestfulResponse<>(0,"成功",null);
        try {
            String requestId = otmEventService.exportLsp(lspInfoDTO);
            result.setData(requestId);
            return result;
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/vehicleExport")
    @ApiOperation(value = "车辆导出", notes = "车辆导出",
            produces = MediaType.APPLICATION_JSON_VALUE,response = RestfulResponse.class)
    public RestfulResponse<String> vehicleExport(@RequestBody VehicleDTO vehicleDTO){
        RestfulResponse<String> result = new RestfulResponse<>(0,"成功",null);
        try {
            String requestId = otmEventService.exportVehicle(vehicleDTO);
            result.setData(requestId);
            return result;
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/driverExport")
    @ApiOperation(value = "司机导出", notes = "司机导出",
            produces = MediaType.APPLICATION_JSON_VALUE,response = RestfulResponse.class)
    public RestfulResponse<String> driverExport(@RequestBody DriverDTO driverDTO){
        RestfulResponse<String> result = new RestfulResponse<>(0,"成功",null);
        try {
            String requestId = otmEventService.exportDriver(driverDTO);
            result.setData(requestId);
            return result;
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/checkinExport")
    @ApiOperation(value = "司机报班导出", notes = "司机报班导出",
            produces = MediaType.APPLICATION_JSON_VALUE,response = RestfulResponse.class)
    public RestfulResponse<Object> driverCheckinExport(@RequestBody DriverCheckinDTO driverCheckinDTO){
        try {
            return otmEventService.exportDriverCheckin(driverCheckinDTO);
        }catch (Exception ex){
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(),null);
        }
    }

    @PostMapping(value = "/revokeCheckin")
    @ApiOperation(value = "调度取消报班", notes = "调度取消报班",
            produces = MediaType.APPLICATION_JSON_VALUE,response = RestfulResponse.class)
    public RestfulResponse revokeCheckin(@RequestBody RevokeCheckinDTO revokeCheckinDTO){
        try {
            return otmEventService.revokeCheckin(revokeCheckinDTO);
        }catch (Exception ex){
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(),null);
        }
    }

}
