package com.zhiche.lisa.integration.controller;


import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.RateItem;
import com.zhiche.lisa.integration.service.IRateItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * OTM费用名称 前端控制器
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
@RestController
@RequestMapping("/rateItem")
@Api(value = "/rateItem",description = "OTM费用名称",tags = "OTM费用名称")
public class RateItemController {
    public Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IRateItemService rateItemService;
    @PostMapping(value = "/queryRateItemAll")
    @ApiOperation(value = "queryRateItemAll",notes = "根据费用名称编码查询费用名称数据",response = RestfulResponse.class)
    public RestfulResponse<List<RateItem>> queryRateItem (@RequestBody Map<String,Object> condition){
        logger.info("controller/rateItem/queryRateItem data: {}" ,condition);
        RestfulResponse result = new RestfulResponse(0,"成功",null);
        try {
            List<RateItem> rateItemList = rateItemService.queryRateItemAll(condition);
            result.setData(rateItemList);
        }catch (Exception e){
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}

