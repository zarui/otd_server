package com.zhiche.lisa.integration.dto.otd;


import java.io.Serializable;
import java.util.Date;

public class CorpCiamDTO implements Serializable {

    /**
     * 关联gid
     */
    private String ciamsRelationGid;
    /**
     * 关联xid
     */
    private String ciamsRelationXid;
    /**
     * 关联业务主体GID
     */
    private String ciamsGid;
    /**
     * 关联业务主体NAME
     */
    private String ciamsName;
    /**
     * 地址
     */
    private String location;
    /**
     * 客户GID
     */
    private String corporationGid;
    /**
     * 客户名称
     */
    private String corporationName;
    /**
     * 状态
     */
    private String status;
    /**
     * 创建时间
     */
    private Date insertDate;
    /**
     * 更新时间
     */
    private Date updateDate;
    /**
     * 域名
     */
    private String domainName;

    private Date gmtCreate;

    private Date gmtModified;

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }


    public String getCiamsRelationGid() {
        return ciamsRelationGid;
    }

    public void setCiamsRelationGid(String ciamsRelationGid) {
        this.ciamsRelationGid = ciamsRelationGid;
    }

    public String getCiamsRelationXid() {
        return ciamsRelationXid;
    }

    public void setCiamsRelationXid(String ciamsRelationXid) {
        this.ciamsRelationXid = ciamsRelationXid;
    }

    public String getCiamsName() {
        return ciamsName;
    }

    public void setCiamsName(String ciamsName) {
        this.ciamsName = ciamsName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCiamsGid() {
        return ciamsGid;
    }

    public void setCiamsGid(String ciamsGid) {
        this.ciamsGid = ciamsGid;
    }

    public String getCorporationGid() {
        return corporationGid;
    }

    public void setCorporationGid(String corporationGid) {
        this.corporationGid = corporationGid;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }
}
