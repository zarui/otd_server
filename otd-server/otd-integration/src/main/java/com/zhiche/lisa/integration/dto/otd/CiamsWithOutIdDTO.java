package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.util.Date;


public class CiamsWithOutIdDTO implements Serializable {

    /**
     * OTM 业务主体编码
     */
    private String ciamsGid;
    /**
     * OTM 业务主体名称
     */
    private String ciamsName;
    /**
     * 状态 1正常
     */
    private String status;

    private Date gmtCreate;

    private Date gmtModified;

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getCiamsGid() {
        return ciamsGid;
    }

    public void setCiamsGid(String ciamsGid) {
        this.ciamsGid = ciamsGid;
    }

    public String getCiamsName() {
        return ciamsName;
    }

    public void setCiamsName(String ciamsName) {
        this.ciamsName = ciamsName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
