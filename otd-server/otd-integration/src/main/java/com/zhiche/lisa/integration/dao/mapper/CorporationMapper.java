package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.integration.dao.model.Corporation;
import com.zhiche.lisa.integration.dto.otd.CorporationDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 客户数据表 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-09-01
 */
public interface CorporationMapper extends BaseMapper<Corporation> {

    List<Corporation> selectListByName(Map<String, Object> condition);

    List<CorporationDTO> selectListDto(@Param("ew") EntityWrapper<CorporationDTO> ew);
}
