package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.enums.IntegrationURIEnum;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.huiyunche.HuiyuncheShipInfoDTO;
import com.zhiche.lisa.integration.dto.order.ShipTaskDTO;
import com.zhiche.lisa.integration.dto.order.ShipmentDTO;
import com.zhiche.lisa.integration.inteface.erp.ErpShippingService;
import com.zhiche.lisa.integration.inteface.otm.OtmShipmentService;
import com.zhiche.lisa.integration.service.IImportLogHistoryService;
import com.zhiche.lisa.integration.service.IPushSubSystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zhaoguixin on 2018/6/12.
 */
@RestController
@RequestMapping(value = "/shipment")
@Api(value = "/shipment", description = "调度指令", tags = {"调度指令"})
public class ShipmentController {

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OtmShipmentService shipmentService;

    @Autowired
    private ErpShippingService erpShippingService;

    @Autowired
    private IPushSubSystemService pushSubSystemService;
    @Autowired
    private IImportLogHistoryService importLogHistoryService;

    @PostMapping(value = "/otmImport")
    @ApiOperation(value = "导入OTM调度指令", notes = "导入OTM调度指令(由OTM推送)", response = RestfulResponse.class)
    public RestfulResponse<Object> shipmentImport(@RequestBody String shipment) throws Exception {
        logger.info("controller:/shipment/import data: {}", shipment);
        RestfulResponse<Object> result = new RestfulResponse<>(0, "成功", null);
        try {

            ShipmentDTO shipmentDTO = shipmentService.otmShipmentImport(shipment);
            result.setData(shipmentDTO);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/getOtmShipment")
    @ApiOperation(value = "通过指令ID获取OTM调度指令", notes = "通过指令ID获取OTM调度指令(OTM已推送)", response = RestfulResponse.class)
    public RestfulResponse<ShipmentDTO> getOtmShipmentById(@RequestParam("shipmentId") String shipmentId) {
        logger.info("controller:/shipment/import data: {}", shipmentId);
        RestfulResponse<ShipmentDTO> result = new RestfulResponse<>(0, "成功", null);
        try {
            ShipmentDTO shipmentDTODto = shipmentService.getOtmShipmentById(shipmentId);
            result.setData(shipmentDTODto);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @GetMapping(value = "/getOtmShipmentDTL")
    @ApiOperation(value = "通过指令ID获取OTM调度指令明细", notes = "通过指令ID获取OTM调度指令明细(OTM已推送)", response = RestfulResponse.class)
    public RestfulResponse<List<ShipTaskDTO>> getOtmShipmentDTL(@RequestParam("shipmentId") String shipmentId, @RequestParam(required = false, value = "ORCode") String orCode) {
        logger.info("controller:/shipment/import data: {},{}", shipmentId, orCode);
        RestfulResponse<List<ShipTaskDTO>> result = new RestfulResponse<>(0, "成功", null);
        try {
            ShipmentDTO shipmentDTODto = shipmentService.getOtmShipmentById(shipmentId);
            if (shipmentDTODto != null) {
                if (orCode == null || "".equals(orCode)) {
                    result.setData(shipmentDTODto.getShipTaskDTOList());
                } else {
                    List<ShipTaskDTO> oldShipTaskDTOList = shipmentDTODto.getShipTaskDTOList();
                    List<ShipTaskDTO> shipTaskDTOList = new ArrayList<>();
                    for (ShipTaskDTO taskDTO :
                            oldShipTaskDTOList) {
                        if (taskDTO != null && orCode.equals(taskDTO.getTaskId())) {
                            shipTaskDTOList.add(taskDTO);
                        }
                        result.setData(shipTaskDTOList);
                    }
                }
            }
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/grabErpShipment")
    @ApiOperation(value = "通过车牌抓取ERP最新调度指令", notes = "通过车牌抓取ERP最新调度指令",
            response = RestfulResponse.class)
    public RestfulResponse<ShipmentDTO> grabErpShipment(String plate) {
        logger.info("controller:/shipment/grabErpShipment data: {}", plate);
        RestfulResponse<ShipmentDTO> result = new RestfulResponse<>(0, "成功", null);
        try {
            ShipmentDTO shipmentDTO = erpShippingService.grabErpShipment(plate);
            result.setData(shipmentDTO);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/otmImportEmptyOrder")
    @ApiOperation(value = "导入OTM空放指令", notes = "导入OTM空放指令(由OTM推送)", response = RestfulResponse.class)
    public RestfulResponse<Object> emptyOrderImport(@RequestBody String shipment) {
        logger.info("controller:/shipment/otmImportEmptyOrder data: {}", shipment);
        RestfulResponse<Object> result = new RestfulResponse<>(0, "成功", null);
        try {
            ShipmentDTO shipmentDTO = shipmentService.otmEmptyOrderImport(shipment);
            result.setData(shipmentDTO);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/getEmptyShipment")
    @ApiOperation(value = "根据指令获取空放指令", notes = "根据指令获取空放指令", response = RestfulResponse.class)
    public RestfulResponse<ShipmentDTO> getEmptyShipmentById(@RequestParam("shipmentId") String shipmentId) {
        logger.info("controller:/getEmptyShipment/import data: {}", shipmentId);
        RestfulResponse<ShipmentDTO> result = new RestfulResponse<>(0, "成功", null);
        try {
            ShipmentDTO shipmentDTODto = shipmentService.getEmptyShipmentById(shipmentId);
            result.setData(shipmentDTODto);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/getHuiyuncheShipment")
    @ApiOperation(value = "通过指令ID获取huiyunche调度指令", notes = "通过指令ID获取通过指令ID获取huiyunche调度指令调度指令(OTM已推送)", response = RestfulResponse.class)
    public RestfulResponse<HuiyuncheShipInfoDTO> getHuiyuncheShipment(@RequestParam("shipmentId") String shipmentId) {
        logger.info("controller:/shipment/getHuiyuncheShipment data: {}", shipmentId);
        RestfulResponse<HuiyuncheShipInfoDTO> result = new RestfulResponse<>(0, "成功", null);
        try {
            HuiyuncheShipInfoDTO huiyuncheShipInfoDTO = shipmentService.getHuiyuncheShipmentById(shipmentId);
            result.setData(huiyuncheShipInfoDTO);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/pushEmptyShipment")
    @ApiOperation(value = "重推空放指令", notes = "重推空放指令", response = RestfulResponse.class)
    public RestfulResponse<Object> pushEmptyShipment(@RequestParam("shipmentId") String shipmentId) {
        logger.info("controller:/shipment/pushEmptyShipment data: {}", shipmentId);
        RestfulResponse<Object> result = new RestfulResponse<>(0, "成功", null);
        try {
            String[] split = shipmentId.split(",");
            for (String s : split) {
                Date startDate = new Date();
                ShipmentDTO shipmentDTODto = shipmentService.getEmptyShipmentById(s.trim().replace("\n", ""));
                String shipmentXml = shipmentService.getEmptyStringXML(shipmentDTODto.getShipmentId());
                pushSubSystemService.pushToSubSystemNew(shipmentDTODto, IntegrationURIEnum.EMPTY_SHIPMENT_PUSH.getCode(), startDate, shipmentXml, s, shipmentDTODto.getSenderTransmissionNo());
            }
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/pushOrderShipmentTms")
    @ApiOperation(value = "重推重载指令到TMS", notes = "重推重载指令到TMS", response = RestfulResponse.class)
    public RestfulResponse<Object> pushOrderShipmentTms(@RequestParam("shipmentId") String shipmentId) {
        logger.info("controller:/shipment/pushOrderShipmentTms data: {}", shipmentId);
        RestfulResponse<Object> result = new RestfulResponse<>(0, "成功", null);
        try {
            String[] split = shipmentId.split(",");
            for (String sourceKey : split) {
                pushSubSystemService.pushOrderShipmentTms(sourceKey);
            }
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/pushOrderShipmentWms")
    @ApiOperation(value = "重推重载指令到WMS", notes = "重推重载指令到WMS", response = RestfulResponse.class)
    public RestfulResponse<Object> pushOrderShipmentWms(@RequestParam("shipmentId") String shipmentId) {
        logger.info("controller:/shipment/pushOrderShipmentWms data: {}", shipmentId);
        RestfulResponse<Object> result = new RestfulResponse<>(0, "成功", null);
        try {
            String[] split = shipmentId.split(",");
            for (String sourceKey : split) {
                pushSubSystemService.pushOrderShipmentWms(sourceKey);
            }
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }
}
