package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.ExportLogHistory;

/**
 * <p>
 * 接口导出日志历史 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface ExportLogHistoryMapper extends BaseMapper<ExportLogHistory> {

}
