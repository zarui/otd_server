package com.zhiche.lisa.integration.service;


import com.zhiche.lisa.integration.dto.junma.MesInboundParamDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface JunMaMesService {


    @WebMethod
    String updateInboundMES(@WebParam(name = "paramDTOS") MesInboundParamDTO[] paramDTOS);

}
