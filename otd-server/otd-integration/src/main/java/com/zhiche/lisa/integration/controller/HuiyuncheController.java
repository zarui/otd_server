package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dto.huiyunche.HuiyuncheCommonDTO;
import com.zhiche.lisa.integration.dto.huiyunche.HuiyuncheShipInfoDTO;
import com.zhiche.lisa.integration.service.IHuiyuncheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 慧运车  integration接口管理
 */
@RestController
@RequestMapping("/huiyuncheInterface")
public class HuiyuncheController {

    @Autowired
    private IHuiyuncheService huiyuncheService;

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 指令导入
     */
    @PostMapping("/importShipment")
    public RestfulResponse<Object> importShipment(@RequestBody HuiyuncheShipInfoDTO shipInfo) {
        RestfulResponse<Object> response = new RestfulResponse<>(0, "处理成功", null);
        try {
            huiyuncheService.importShipment(shipInfo);
        } catch (BaseException e) {
            logger.error("人送指令-->保存指令失败:{}", e);
            response.setCode(-1);
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("人送指令-->指令保存系统异常", e);
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * 人送指令退单发送otm/lspm
     */
    @PostMapping(value = "/cancelShip", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public RestfulResponse<Object> cancelShip(HuiyuncheCommonDTO dto) {
        RestfulResponse<Object> response = new RestfulResponse<>(0, "处理成功", null);
        try {
            huiyuncheService.cancelShip(dto);
        } catch (BaseException e) {
            logger.error("人送指令-->退单失败:{}", e);
            response.setCode(-1);
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("人送指令-->退单系统异常", e);
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * 人送发运确认
     */
    @PostMapping(value = "/shipConfirm", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public RestfulResponse<Object> shipConfirm(HuiyuncheCommonDTO dto) {
        RestfulResponse<Object> response = new RestfulResponse<>(0, "处理成功", null);
        try {
            huiyuncheService.shipConfirm(dto);
        } catch (BaseException e) {
            logger.error("人送指令-->发运确认失败:{}", e);
            response.setCode(-1);
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("人送指令-->发运确认系统异常", e);
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * 人送运抵
     */
    @PostMapping(value = "/shipTo", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public RestfulResponse<Object> shipTo(HuiyuncheCommonDTO dto) {
        RestfulResponse<Object> response = new RestfulResponse<>(0, "处理成功", null);
        try {
            huiyuncheService.shipTo(dto);
        } catch (BaseException e) {
            logger.error("人送指令-->运抵确认失败:{}", e);
            response.setCode(-1);
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.error("人送指令-->运抵系统异常", e);
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }


}
