package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.integration.dao.model.UlcLimitionMid;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * OTM 内控时效规则头信息 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface UlcLimitionMidMapper extends BaseMapper<UlcLimitionMid> {

    List<UlcLimitionMid> queryUlcLimition(@Param("ew") EntityWrapper<UlcLimitionMid> ew);
}
