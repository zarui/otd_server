package com.zhiche.lisa.integration.dto.order;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 14:52 2019/2/28
 */
public class ShipmentInfo implements Serializable {

    /**
     * 指令号
     */
    private String shipmwnt_xid;

    /**
     * 指令下发时间
     */
    private Date shipmentCreate;

    /**
     * 商品车数量
     */
    private Integer vehicleQty;

    public Integer getVehicleQty () {
        return vehicleQty;
    }

    public void setVehicleQty (Integer vehicleQty) {
        this.vehicleQty = vehicleQty;
    }


    public String getShipmwnt_xid () {
        return shipmwnt_xid;
    }

    public void setShipmwnt_xid (String shipmwnt_xid) {
        this.shipmwnt_xid = shipmwnt_xid;
    }

    public Date getShipmentCreate () {
        return shipmentCreate;
    }

    public void setShipmentCreate (Date shipmentCreate) {
        this.shipmentCreate = shipmentCreate;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("ShipmentInfo{");
        sb.append("shipmwnt_xid='").append(shipmwnt_xid).append('\'');
        sb.append(", shipmentCreate=").append(shipmentCreate);
        sb.append(", vehicleQty=").append(vehicleQty);
        sb.append('}');
        return sb.toString();
    }


}
