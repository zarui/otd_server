package com.zhiche.lisa.integration.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.VehicleStandardMode;
import com.zhiche.lisa.integration.service.IVehicleStandardModeService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM车辆标准车型 前端控制器
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-22
 */
@RestController
@RequestMapping("/vehicleStandardMode")
@Api(value = "/vehicleStandardMode", description = "OTM标准车型", tags = "OTM标准车型")
public class VehicleStandardModeController {
    public Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IVehicleStandardModeService vehicleStandardModeService;

    @PostMapping(value = "queryVehicleStandardModePage")
    @ApiOperation(value = "queryVehicleStandardModePageApi", notes = "根据品牌编码/车系编码/车型名称分页查询标准车型数据", response = RestfulResponse.class)
    public RestfulResponse<Page<VehicleStandardMode>> queryVehicleStandardModePage(@RequestBody Page page) {
        logger.info("controller:/VehicleStandardMode/queryClassPage data: {}", page);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            Page standardModePage = vehicleStandardModeService.queryStandardModePage(page);
            result.setData(standardModePage);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());

        }
        return result;
    }

    @PostMapping(value = "/queryVehicleName")
    @ApiOperation(value = "queryVehicleNameApi", notes = "根据时间点和车型名称模糊查询标准车型名称", response = RestfulResponse.class)
    public RestfulResponse<List<Map<String, Object>>> queryVehicleNameModePage(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/VehicleStandardMode/queryVehicleNameModePage data: {}", condition);
        RestfulResponse<List<Map<String, Object>>> result = new RestfulResponse<>(0, "成功", null);
        try {
            if (condition != null && condition.isEmpty()) {
                condition.put("status", TableStatusEnum.STATUS_1.getCode());
            }
            List<VehicleStandardMode> standardModes = vehicleStandardModeService.queryStandardModeList(condition);
            List<Map<String, Object>> resultList = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(standardModes)) {
                for (VehicleStandardMode vsm : standardModes) {
                    Map<String, Object> resultMap = new HashMap<>();
                    resultMap.put("id", vsm.getCode());
                    resultMap.put("vehicleName", vsm.getName());
                    resultList.add(resultMap);
                }
            }
            result.setData(resultList);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());

        }
        return result;
    }

    @PostMapping(value = "queryListVehicleStandardModeCode")
    @ApiOperation(value = "queryListVehicleStandardModeCodeApi", notes = "根据车型编码查询标准车型数据", response = RestfulResponse.class)
    public RestfulResponse<List<VehicleStandardMode>> queryListVehicleStandardModeCode(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/VehicleStandardMode/queryAllStandardMode data: {}", condition);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            List<VehicleStandardMode> standardModeList = vehicleStandardModeService.queryAllStandardMode(condition);
            result.setData(standardModeList);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 查询车型信息
     *
     * @return
     */
    @PostMapping(value = "/queryVehicleModelInfo")
    @ApiOperation(value = "queryVehicleModelInfoApi", notes = "查询车型信息", response = RestfulResponse.class)
    public RestfulResponse<Page<VehicleStandardMode>> queryVehicleModelInfo(@RequestBody Page<VehicleStandardMode> page) {
        logger.info("/vehicleStandardMode/queryVehicleModelInfo data: {}", page);
        RestfulResponse<Page<VehicleStandardMode>> result = new RestfulResponse(0, "success", null);
        try {
            Page<VehicleStandardMode> standardModeList = vehicleStandardModeService.queryVehicleModelInfo(page);
            result.setData(standardModeList);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 修改后的车型推送至OTM
     */
    @PostMapping(value = "/modifiedVehicleToOtm")
    @ApiOperation(value = "modifiedVehicleToOtm", notes = "修改后的车型推送至OTM", response = RestfulResponse.class)
    public RestfulResponse<Map<String, Object>> queryVehicleModelInfo(@RequestBody Map<String, String> params) {
        logger.info("/modifiedVehicleToOtm data: {}", params);
        RestfulResponse<Map<String, Object>> result = new RestfulResponse(0, "success", null);
        try {
            Map<String, Object> standardModeList = vehicleStandardModeService.modifiedVehicleToOtm(params);
            result.setData(standardModeList);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }
}

