package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.model.OrderReleaseMid;
import com.zhiche.lisa.integration.dto.order.ShipmentInfo;
import com.zhiche.lisa.integration.dto.otd.InTransitDTO;
import com.zhiche.lisa.integration.dto.otd.NewOrderReleaseMidDTO;
import com.zhiche.lisa.integration.dto.otd.OrderReleaseMidDTO;
import com.zhiche.lisa.integration.dto.sparePart.SPReleaseDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 运单信息中间表 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
public interface OrderReleaseMidMapper extends BaseMapper<OrderReleaseMid> {

    List<OrderReleaseMidDTO> selectRouteReleaseByPage (Page<OrderReleaseMidDTO> page, @Param("ew") EntityWrapper<OrderReleaseMidDTO> ew);

    List<ShipmentInfo> queryShipmentMonitor (Page<ShipmentInfo> page, @Param("ew") EntityWrapper<ShipmentInfo> ew);

    List<NewOrderReleaseMidDTO> selectRouteReleaseByPageNew (Page<NewOrderReleaseMidDTO> page, @Param("ew") EntityWrapper<NewOrderReleaseMidDTO> ew);

    List<InTransitDTO> queryInTransitInfo (Page<InTransitDTO> page, @Param("ew") EntityWrapper<InTransitDTO> ew);

    List<SPReleaseDTO> queryReleaseData ( @Param("ew") EntityWrapper<SPReleaseDTO> spew);
}
