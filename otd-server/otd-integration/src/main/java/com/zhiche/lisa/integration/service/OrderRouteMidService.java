package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.OrderRouteMid;

/**
 * <p>
 * OTM 订单实际路由中间表 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface OrderRouteMidService extends IService<OrderRouteMid> {

}
