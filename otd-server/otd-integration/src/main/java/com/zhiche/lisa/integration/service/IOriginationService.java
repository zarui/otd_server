package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.Origination;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM起运地 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-15
 */
public interface IOriginationService extends IService<Origination> {

    /**
     * 分页查询OTM起运地
     */
    Page<Origination> queryPageOrigination(Page page);

    /**
     * 查询全部OTM起运地
     */
    List<Origination> queryListOrigination(Map<String, Object> condition);

    /**
     * 查询起运城市
     */
    List<Origination> queryOriginationCity(String cityName);

    /**
     * 查询OMS起运地的
     */
    List<Origination> queryOriginationForOms(Map<String, Object> condition);

    List<Origination> queryOriginOTD(Map<String, String> dto);

    Origination queryOriginationCode(Map<String, String> condition);
}
