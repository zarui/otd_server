package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.CorporationMapper;
import com.zhiche.lisa.integration.dao.model.Corporation;
import com.zhiche.lisa.integration.dto.otd.CorporationDTO;
import com.zhiche.lisa.integration.service.ICorporationService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * OTM 客户数据表 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-09-01
 */
@Service
public class CorporationServiceImpl extends ServiceImpl<CorporationMapper, Corporation> implements ICorporationService {

    @Override
    public List<Corporation> queryCorporationByName(Map<String, Object> condition) {
        Map<String, Object> map = new HashMap<>();
        if (condition.containsKey("name") && StringUtils.isNotBlank(condition.get("name").toString())){
            map.put("name", "%" + condition.get("name").toString() + "%");
        }
        if (condition.containsKey("timeAfter") && StringUtils.isNotBlank(condition.get("timeAfter").toString())){
            map.put("timeAfter", new Date(Long.valueOf(condition.get("timeAfter").toString())));
        }

        return baseMapper.selectListByName(map);
    }

    @Override
    public List<CorporationDTO> queryCorpOTD(Map<String, String> dto) {
        if (dto == null || dto.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String queryAll = dto.get("queryAll");
        String timeStamp = dto.get("timeStamp");
        EntityWrapper<CorporationDTO> ew = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                ew.gt("gmt_modified", new Date(longTime));
            }
        }
        ew.orderBy("corporation_gid", false);
        return baseMapper.selectListDto(ew);
    }
}
