package com.zhiche.lisa.integration.dto.carrier;

import java.io.Serializable;

/**
 * Created by hxh on 2018/9/03.
 */
public class CarrierErpDto implements Serializable {

    private String id;            //运力平台标识
    private String plate;       //车牌号
    private String trailerId;     //对应挂车的运力平台标识
    /**
     * “1”:自有   “2” 外协  “3”:租赁
     */
    private String property;
    private String transMode;
    private String trailerProper;
    private String driverId;      //对应司机的运力平台标识
    private String driverName;
    private String driverPhone;
    private String fleetId;       //车队Id
    private String fleetName;
    private String lspId;
    private String lspName;

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getLspId() {
        return lspId;
    }

    public void setLspId(String lspId) {
        this.lspId = lspId;
    }

    public String getLspName() {
        return lspName;
    }

    public void setLspName(String lspName) {
        this.lspName = lspName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getTrailerProper() {
        return trailerProper;
    }

    public void setTrailerProper(String trailerProper) {
        this.trailerProper = trailerProper;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getFleetId() {
        return fleetId;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        return "CarrierErpDto{" +
                "id='" + id + '\'' +
                ", plate='" + plate + '\'' +
                ", trailerId='" + trailerId + '\'' +
                ", property='" + property + '\'' +
                ", transMode='" + transMode + '\'' +
                ", trailerProper='" + trailerProper + '\'' +
                ", driverId='" + driverId + '\'' +
                ", driverName='" + driverName + '\'' +
                ", driverPhone='" + driverPhone + '\'' +
                ", fleetId='" + fleetId + '\'' +
                ", fleetName='" + fleetName + '\'' +
                ", lspId='" + lspId + '\'' +
                ", lspName='" + lspName + '\'' +
                '}';
    }
}
