package com.zhiche.lisa.integration.service;

import com.zhiche.lisa.integration.dto.tms.TmsCommonDTO;

public interface TMSAPIService {

    void shipDeliver(TmsCommonDTO dto);

    void sendXMLShipToProv(TmsCommonDTO dto);
}
