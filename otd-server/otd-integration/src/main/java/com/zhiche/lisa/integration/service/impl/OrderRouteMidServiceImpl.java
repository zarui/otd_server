package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.integration.dao.mapper.OrderRouteMidMapper;
import com.zhiche.lisa.integration.dao.model.OrderRouteMid;
import com.zhiche.lisa.integration.service.OrderRouteMidService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * OTM 订单实际路由中间表 服务实现类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@Service
public class OrderRouteMidServiceImpl extends ServiceImpl<OrderRouteMidMapper, OrderRouteMid> implements OrderRouteMidService {

}
