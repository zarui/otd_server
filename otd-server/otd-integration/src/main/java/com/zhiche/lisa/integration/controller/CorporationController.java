package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.Corporation;
import com.zhiche.lisa.integration.service.ICorporationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM客户数据 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-09-03
 */
@RestController
@RequestMapping(value = "/corporation")
@Api(value = "/corporation", description = "OTM客户数据", tags = "OTM客户数据")
public class CorporationController {

    public Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ICorporationService corporationService;

    @PostMapping(value = "/queryCorporationName")
    @ApiOperation(value = "/queryCorporationNameApi", notes = "根据时间戳和客户名称模糊查询客户名称", response = RestfulResponse.class)
    public RestfulResponse<List<Map<String, Object>>> queryCorporationName(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/corporation/queryCorporationName data: {}", condition);
        RestfulResponse<List<Map<String, Object>>> result = new RestfulResponse<>(0, "成功", null);
        try {
            List<Corporation> corporations = corporationService.queryCorporationByName(condition);
            List<Map<String, Object>> resultList = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(corporations)) {
                for (Corporation corporation : corporations) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", corporation.getCorporationGid());
                    map.put("name", corporation.getCorporationName());
                    resultList.add(map);
                }
            }
            result.setData(resultList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

}
