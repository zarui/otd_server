package com.zhiche.lisa.integration.controller;


import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.TransportMode;
import com.zhiche.lisa.integration.service.ITransportModeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM运输方式(运输工具) 前端控制器
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
@RestController
@RequestMapping("/transportMode")
@Api(value = "transportModeApi", description = "OTM运输方式", tags = "OTM运输方式")
public class TransportModeController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ITransportModeService transportModeService;

    @PostMapping(value = "/queryTransportModeAll")
    @ApiOperation(value = "查询OTM全部运输方式", notes = "查询OTM全部运输方式", response = RestfulResponse.class)
    public RestfulResponse<List<TransportMode>> queryTransportModeAll(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/transportMode/queryTransportModeAll data:{}", condition);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            List<TransportMode> transportModeList = transportModeService.queryTransportMode(condition);
            result.setData(transportModeList);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}

