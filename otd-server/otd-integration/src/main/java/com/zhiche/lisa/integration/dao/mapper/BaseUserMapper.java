package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.integration.dao.model.BaseUser;
import com.zhiche.lisa.integration.dto.base.BaseUserDTO;
import com.zhiche.lisa.integration.dto.base.UserDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * OTM用户信息 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-08-24
 */
public interface BaseUserMapper extends BaseMapper<BaseUser> {

    List<UserDTO> listUsers(@Param("ew") EntityWrapper<UserDTO> ew);

    List<BaseUserDTO> selectListWIthDepart(@Param("ew") EntityWrapper<BaseUser>userEW);
}
