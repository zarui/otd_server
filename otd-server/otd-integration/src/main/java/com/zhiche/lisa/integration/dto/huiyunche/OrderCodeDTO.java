package com.zhiche.lisa.integration.dto.huiyunche;

public class OrderCodeDTO {
    private String orderCode ;

    private String code ;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    @Override
    public String toString() {
        return "OrderCodeDTO{" +
                "orderCode='" + orderCode + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
