package com.zhiche.lisa.integration.service;

import com.zhiche.lisa.integration.dto.huiyunche.HuiyuncheCommonDTO;
import com.zhiche.lisa.integration.dto.huiyunche.HuiyuncheShipInfoDTO;

public interface IHuiyuncheService {
    /**
     * 人送指令下发tms
     */
    void importShipment(HuiyuncheShipInfoDTO shipInfo);

    /**
     * 人送退单下发运力平台/otm
     */
    void cancelShip(HuiyuncheCommonDTO shipInfo);

    /**
     * 人送订单发运传送lspm
     */
    void shipConfirm(HuiyuncheCommonDTO dto);

    /**
     * 人送订单运抵Lspm
     */
    void shipTo(HuiyuncheCommonDTO dto);

    /**
     * 获取二手车运单号列表
     */
    void getOrderCodes();

    /**
     * 获取二手车运单号明细
     */
    void getOrderDetail(String orderCode);

    /**
     * 获取新项目二手车运单号列表
     */
    void getNewOrderCodes();

    /**
     * 获取新项目二手车运单号明细
     */
    void getNewOrderDetail(String orderCode);

}
