package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * OTM 时效规则明细表视图
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@TableName("v_limition_rule_mid")
public class LimitionRuleMid extends Model<LimitionRuleMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 规则ID
     */
    @TableId("LIMITATION_RULE_GID")
	private String limitationRuleGid;
    /**
     * 规则XID
     */
	@TableField("LIMITATION_RULE_XID")
	private String limitationRuleXid;
    /**
     * OTM 数据时间
     */
	@TableField("UPDATE_DATE")
	private Date updateDate;
    /**
     * OTM 数据时间
     */
	@TableField("INSERT_DATE")
	private Date insertDate;
    /**
     * 状态 1正常 0 禁用
     */
	@TableField("STATUS")
	private Integer status;
    /**
     * 公里数天数
     */
	@TableField("EXTERNAL_DAY")
	private String externalDay;
    /**
     *  1是 0否
     */
	@TableField("EXTERNAL")
	private Integer external;
    /**
     * 计算时间
     */
	@TableField("LIMITATION_TIME")
	private String limitationTime;
    /**
     * 计算因子字段
     */
	@TableField("LIMITATION_BASE_FIELD")
	private String limitationBaseField;
    /**
     * 计算字段名称
     */
	@TableField("LIMITATION_FIELD")
	private String limitationField;
    /**
     * 时效规则关联ID
     */
	@TableField("LIMITATION")
	private String limitation;
    /**
     * 数据修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;
    /**
     * 数据 修改时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;


	public String getLimitationRuleGid() {
		return limitationRuleGid;
	}

	public void setLimitationRuleGid(String limitationRuleGid) {
		this.limitationRuleGid = limitationRuleGid;
	}

	public String getLimitationRuleXid() {
		return limitationRuleXid;
	}

	public void setLimitationRuleXid(String limitationRuleXid) {
		this.limitationRuleXid = limitationRuleXid;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getExternalDay() {
		return externalDay;
	}

	public void setExternalDay(String externalDay) {
		this.externalDay = externalDay;
	}

	public Integer getExternal() {
		return external;
	}

	public void setExternal(Integer external) {
		this.external = external;
	}

	public String getLimitationTime() {
		return limitationTime;
	}

	public void setLimitationTime(String limitationTime) {
		this.limitationTime = limitationTime;
	}

	public String getLimitationBaseField() {
		return limitationBaseField;
	}

	public void setLimitationBaseField(String limitationBaseField) {
		this.limitationBaseField = limitationBaseField;
	}

	public String getLimitationField() {
		return limitationField;
	}

	public void setLimitationField(String limitationField) {
		this.limitationField = limitationField;
	}

	public String getLimitation() {
		return limitation;
	}

	public void setLimitation(String limitation) {
		this.limitation = limitation;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.limitationRuleGid;
	}

	@Override
	public String toString() {
		return "LimitionRuleMid{" +
			", limitationRuleGid=" + limitationRuleGid +
			", limitationRuleXid=" + limitationRuleXid +
			", updateDate=" + updateDate +
			", insertDate=" + insertDate +
			", status=" + status +
			", externalDay=" + externalDay +
			", external=" + external +
			", limitationTime=" + limitationTime +
			", limitationBaseField=" + limitationBaseField +
			", limitationField=" + limitationField +
			", limitation=" + limitation +
			", gmtModified=" + gmtModified +
			", gmtCreate=" + gmtCreate +
			"}";
	}
}
