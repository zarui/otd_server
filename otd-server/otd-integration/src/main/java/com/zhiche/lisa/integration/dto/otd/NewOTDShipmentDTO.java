package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.util.Date;

public class NewOTDShipmentDTO implements Serializable {
    private String shipmentId;
    private String shipmentType;
    /**
     * 指令状态（ils）-- 状态GID（OTD）
     */
    private String statusGid;
    /**
     * 状态名称
     */
    private String statusName;
    private Integer totalShipCount;
    /**
     *  运输方式
     */
    private String transMode;
    /**
     *  运输车辆车牌号/船号/车皮号
     */
    private String transportCode;
    /**
     *  指令下发时间（创建时间）
     */
    private String shipmentSendTime;
    /**
     * 司机id
     */
    private String driverId;
    /**
     * 司机名称
     */
    private String driverName;
    /**
     * 司机联系方式
     */
    private String driverContact;

    /**
     * 承运商id
     */
    private String serviceProviderId;
    /**
     * 承运商名称
     */
    private String serviceProviderName;
    /**
     * 起运地Gid
     */
    private String sourceLocationGid;
    /**
     * 起运地地址
     */
    private String sourceLocationAddress;
    /**
     * 目的地Gid
     */
    private String destLocationGid;
    /**
     * 目的地地址
     */
    private String destLocationAddress;
    /**
     * 数据创建时间
     */
    private Date gmtCreate;
    /**
     * 数据更新时间
     */
    private Date gmtModified;

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public Integer getTotalShipCount() {
        return totalShipCount;
    }

    public void setTotalShipCount(Integer totalShipCount) {
        this.totalShipCount = totalShipCount;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getDestLocationAddress() {
        return destLocationAddress;
    }

    public void setDestLocationAddress(String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getStatusGid() {
        return statusGid;
    }

    public void setStatusGid(String statusGid) {
        this.statusGid = statusGid;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }

    public String getTransportCode() {
        return transportCode;
    }

    public void setTransportCode(String transportCode) {
        this.transportCode = transportCode;
    }

    public String getShipmentSendTime() {
        return shipmentSendTime;
    }

    public void setShipmentSendTime(String shipmentSendTime) {
        this.shipmentSendTime = shipmentSendTime;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverContact() {
        return driverContact;
    }

    public void setDriverContact(String driverContact) {
        this.driverContact = driverContact;
    }

    public String getSourceLocationGid() {
        return sourceLocationGid;
    }

    public void setSourceLocationGid(String sourceLocationGid) {
        this.sourceLocationGid = sourceLocationGid;
    }

    public String getSourceLocationAddress() {
        return sourceLocationAddress;
    }

    public void setSourceLocationAddress(String sourceLocationAddress) {
        this.sourceLocationAddress = sourceLocationAddress;
    }

    public String getDestLocationGid() {
        return destLocationGid;
    }

    public void setDestLocationGid(String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }
}
