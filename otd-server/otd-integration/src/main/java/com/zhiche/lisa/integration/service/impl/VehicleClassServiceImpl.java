package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.VehicleClassMapper;
import com.zhiche.lisa.integration.dao.model.VehicleClass;
import com.zhiche.lisa.integration.service.IVehicleClassService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class VehicleClassServiceImpl extends ServiceImpl<VehicleClassMapper, VehicleClass> implements IVehicleClassService {

    /**
     * 通过车系编码查询车系数据全部返回
     *
     * @param classCode
     */
    @Override
    public List<VehicleClass> queryAllClass(Map<String, Object> classCode) {
        Wrapper<VehicleClass> ew = buildCondition(classCode);
        return this.selectList(ew);
    }

    /**
     * 分页查询
     *
     * @param page
     */
    @Override
    public Page<VehicleClass> queryClassPage(Page<VehicleClass> page) {
        if (page == null) {
            throw new BaseException("分页参数不能为空");
        }
        Map<String, Object> cn = page.getCondition();
        Wrapper<VehicleClass> ew = buildCondition(cn);
        List<VehicleClass> brands = baseMapper.selectPage(page, ew);
        this.selectPage(page, ew);
        page.setRecords(brands);
        return page;
    }

    private Wrapper<VehicleClass> buildCondition(Map<String, Object> condition) {
        Wrapper<VehicleClass> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()) {
            // 车系编码查询
            if (condition.containsKey("code") && StringUtils.isNotBlank(condition.get("code").toString())) {
                ew.eq("code", condition.get("code").toString());
            }
            //车系名称模糊查询
            Object vehicleBrandName = condition.get("name");
            if (vehicleBrandName != null && StringUtils.isNotBlank(vehicleBrandName.toString())) {
                ew.like("name", vehicleBrandName.toString());
            }
            //品牌编码
            Object brandCode = condition.get("brandCode");
            if (brandCode != null && StringUtils.isNotBlank(brandCode.toString())) {
                ew.eq("vehicle_brand_code", brandCode.toString());
            }
        }
        return ew;
    }
}
