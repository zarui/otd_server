package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * OTM车辆标准车型
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-22
 */
@TableName("otm_vehicle_standard_mode")
public class VehicleStandardMode extends Model<VehicleStandardMode> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 编码
     */
	private String code;
    /**
     * 名称
     */
	private String name;
    /**
     * 车辆类型
     */
	@TableField("vehicle_type")
	private String vehicleType;
    /**
     * 品牌编码
     */
	@TableField("vehicle_brand_code")
	private String vehicleBrandCode;
    /**
     * 车系编码
     */
	@TableField("vehicle_class_code")
	private String vehicleClassCode;
    /**
     * 长
     */
	private BigDecimal length;
    /**
     * 宽
     */
	private BigDecimal width;
    /**
     * 高
     */
	private BigDecimal height;
    /**
     * 重
     */
	private BigDecimal weight;
    /**
     * 百公里油耗
     */
	private BigDecimal lpk;
    /**
     * 燃油类型
     */
	@TableField("fuel_type")
	private String fuelType;
    /**
     * 百公里尿素消耗
     */
	private BigDecimal urea;
    /**
     * 邮箱容积
     */
	@TableField("oil_cubage")
	private BigDecimal oilCubage;
    /**
     * 第一箱油比例
     */
	@TableField("oil_ratio")
	private BigDecimal oilRatio;
    /**
     * 准驾驾照类型
     */
	private String license;
    /**
     * 状态(0:失效,1:有效)
     */
	private String status;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleBrandCode() {
		return vehicleBrandCode;
	}

	public void setVehicleBrandCode(String vehicleBrandCode) {
		this.vehicleBrandCode = vehicleBrandCode;
	}

	public String getVehicleClassCode() {
		return vehicleClassCode;
	}

	public void setVehicleClassCode(String vehicleClassCode) {
		this.vehicleClassCode = vehicleClassCode;
	}

	public BigDecimal getLength() {
		return length;
	}

	public void setLength(BigDecimal length) {
		this.length = length;
	}

	public BigDecimal getWidth() {
		return width;
	}

	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	public BigDecimal getHeight() {
		return height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public BigDecimal getLpk() {
		return lpk;
	}

	public void setLpk(BigDecimal lpk) {
		this.lpk = lpk;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public BigDecimal getUrea() {
		return urea;
	}

	public void setUrea(BigDecimal urea) {
		this.urea = urea;
	}

	public BigDecimal getOilCubage() {
		return oilCubage;
	}

	public void setOilCubage(BigDecimal oilCubage) {
		this.oilCubage = oilCubage;
	}

	public BigDecimal getOilRatio() {
		return oilRatio;
	}

	public void setOilRatio(BigDecimal oilRatio) {
		this.oilRatio = oilRatio;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "VehicleStandardMode{" +
			", id=" + id +
			", code=" + code +
			", name=" + name +
			", vehicleType=" + vehicleType +
			", vehicleBrandCode=" + vehicleBrandCode +
			", vehicleClassCode=" + vehicleClassCode +
			", length=" + length +
			", width=" + width +
			", height=" + height +
			", weight=" + weight +
			", lpk=" + lpk +
			", fuelType=" + fuelType +
			", urea=" + urea +
			", oilCubage=" + oilCubage +
			", oilRatio=" + oilRatio +
			", license=" + license +
			", status=" + status +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
