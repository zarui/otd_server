package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 接口导出日志历史
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@TableName("itf_export_log_history")
public class ExportLogHistory extends Model<ExportLogHistory> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 日志ID
     */
	@TableField("log_id")
	private Integer logId;
    /**
     * 来源系统(erp,otm)
     */
	@TableField("targert_sys")
	private String targertSys;
    /**
     * 来源唯一键
     */
	@TableField("export_key")
	private String exportKey;
    /**
     * 类型(10:寻车,11:移车,12:提车,20:入库,21:出库,30:发运,31:在途,32:运抵,40:承运商,41:车辆,42:司机)
     */
	private String type;
    /**
     * 数据存储键
     */
	@TableField("data_storage_key")
	private String dataStorageKey;
    /**
     * 接口地址
     */
	@TableField("interface_url")
	private String interfaceUrl;
    /**
     * 导出状态(1:成功,0:失败)
     */
	@TableField("export_status")
	private String exportStatus;
	/**
	 * 请求ID
	 */
	@TableField("request_id")
	private String requestId;
    /**
     * 导出备注
     */
	@TableField("export_remarks")
	private String exportRemarks;
    /**
     * 导出开始时间
     */
	@TableField("export_start_time")
	private Date exportStartTime;
    /**
     * 导出完成时间
     */
	@TableField("export_end_time")
	private Date exportEndTime;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLogId() {
		return logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public String getTargertSys() {
		return targertSys;
	}

	public void setTargertSys(String targertSys) {
		this.targertSys = targertSys;
	}

	public String getExportKey() {
		return exportKey;
	}

	public void setExportKey(String exportKey) {
		this.exportKey = exportKey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDataStorageKey() {
		return dataStorageKey;
	}

	public void setDataStorageKey(String dataStorageKey) {
		this.dataStorageKey = dataStorageKey;
	}

	public String getInterfaceUrl() {
		return interfaceUrl;
	}

	public void setInterfaceUrl(String interfaceUrl) {
		this.interfaceUrl = interfaceUrl;
	}

	public String getExportStatus() {
		return exportStatus;
	}

	public void setExportStatus(String exportStatus) {
		this.exportStatus = exportStatus;
	}

	public String getExportRemarks() {
		return exportRemarks;
	}

	public void setExportRemarks(String exportRemarks) {
		this.exportRemarks = exportRemarks;
	}

	public Date getExportStartTime() {
		return exportStartTime;
	}

	public void setExportStartTime(Date exportStartTime) {
		this.exportStartTime = exportStartTime;
	}

	public Date getExportEndTime() {
		return exportEndTime;
	}

	public void setExportEndTime(Date exportEndTime) {
		this.exportEndTime = exportEndTime;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ExportLogHistory{" +
			", id=" + id +
			", logId=" + logId +
			", targertSys=" + targertSys +
			", exportKey=" + exportKey +
			", type=" + type +
			", dataStorageKey=" + dataStorageKey +
			", interfaceUrl=" + interfaceUrl +
			", exportStatus=" + exportStatus +
			", exportRemarks=" + exportRemarks +
			", exportStartTime=" + exportStartTime +
			", exportEndTime=" + exportEndTime +
			", gmtCreate=" + gmtCreate +
			"}";
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
}
