package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.BaseUserMapper;
import com.zhiche.lisa.integration.dao.model.BaseUser;
import com.zhiche.lisa.integration.dto.base.BaseUserDTO;
import com.zhiche.lisa.integration.dto.base.UserDTO;
import com.zhiche.lisa.integration.service.IBaseUserService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * OTM用户信息 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-08-24
 */
@Service
public class BaseUserServiceImpl extends ServiceImpl<BaseUserMapper, BaseUser> implements IBaseUserService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 根据条件查询用户信息
     */
    @Override
    public List<UserDTO> queryAllUser(Map<String, String> condition) {
        EntityWrapper<UserDTO> ew = new EntityWrapper<>();
        buildCondition(condition, ew);
        return baseMapper.listUsers(ew);

    }

    /**
     * 根据根据用户中文名称/用户编码分页查询
     *
     */
    @Override
    public Page<BaseUser> queryUserPage(Page<BaseUser> page) {
        if (page == null){
            throw new BaseException("分页参数不能为空");
        }
        Map<String,Object>cn = page.getCondition();
        Wrapper<BaseUser> ew = buildCondition(cn);
        List<BaseUser> standardMode = baseMapper.selectPage(page, ew);
        page.setRecords(standardMode);
        return page;
    }

    @Override
    public List<BaseUserDTO> queryUserOTD(Map<String, String> dto) {
        if (dto == null || dto.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String queryAll = dto.get("queryAll");
        String timeStamp = dto.get("timeStamp");
        EntityWrapper<BaseUser> userEW = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                userEW.gt("gmt_modified", new Date(longTime));
            }
        }
        userEW.orderBy("id", false);
        return baseMapper.selectListWIthDepart(userEW);
    }


    private void buildCondition(Map<String, String> condition, EntityWrapper<UserDTO> ew) {
        if (condition != null && !condition.isEmpty()) {
            String timeAfter = condition.get("timeAfter");
            if (StringUtils.isNotBlank(timeAfter)) {
                //时间戳格式
                if (logger.isInfoEnabled()) {
                    logger.info("   [ queryAllUser --->timestamp:{} ,date:{}",
                            timeAfter, new Date(Long.valueOf(timeAfter)));
                }
                ew.ge("gmt_modified", new Date(Long.valueOf(timeAfter)));
            }
            String code = condition.get("code");
            if (StringUtils.isNotBlank(code)) {
                ew.eq("user_xid", code);
            }
            String email = condition.get("email");
            if (StringUtils.isNotBlank(email)) {
                ew.eq("email", email);
            }
            String name = condition.get("name");
            if (StringUtils.isNotBlank(name)) {
                ew.andNew().like("common_name", name)
                        .or().eq("email", name)
                        .or().eq("user_xid", name);
            }

        }
    }
    private Wrapper<BaseUser> buildCondition(Map<String, Object> condition) {
        Wrapper<BaseUser> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()){
            // 用户编码查询
            Object userCode = condition.get("code");
            if (userCode != null && StringUtils.isNotBlank(userCode.toString())){
                ew.eq("user_xid",userCode.toString());
            }

            // 名称可能是邮箱/编码/中文名
            Object commonName =condition.get("name");
            if (commonName!= null && StringUtils.isNotBlank(commonName.toString())){
                ew.andNew().like("common_name", commonName.toString())
                        .or().eq("email", commonName.toString())
                        .or().eq("user_xid", commonName.toString());
            }

        }
        return ew;
    }
}
