package com.zhiche.lisa.integration.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.Origination;
import com.zhiche.lisa.integration.dto.order.OriginationCity;
import com.zhiche.lisa.integration.service.IOriginationService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhaoguixin on 2018/8/15.
 */
@RestController
@RequestMapping(value = "/origination")
@Api(value = "/origination", description = "OTM起运地", tags = {"OTM起运地"})
public class OriginationController {

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IOriginationService originationService;

    @PostMapping(value = "/queryPageOrigination")
    @ApiOperation(value = "分页查询OTM起运地", notes = "分页查询OTM起运地", response = RestfulResponse.class)
    public RestfulResponse<Page<Origination>> queryPageOrigination(@RequestBody Page page) {
        logger.info("controller:/origination/queryPageOrigination data: {}", page);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            Page<Origination> originationPage = originationService.queryPageOrigination(page);
            result.setData(originationPage);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/queryOriginationName")
    @ApiOperation(value = "模糊查询OTM起运地名称和编码", notes = "模糊查询OTM起运地名称和编码", response = RestfulResponse.class)
    public RestfulResponse<List<Map<String, Object>>> queryOriginationName(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/origination/queryOriginationName data: {}", condition);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            if (condition != null && condition.isEmpty()) {
                condition.put("status", TableStatusEnum.STATUS_Y.getCode());
            }
            List<Origination> originations = originationService.queryListOrigination(condition);
            List<Map> resultList = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(originations)) {
                for (Origination origination : originations) {
                    Map<String, Object> resultMap = new HashMap<>();
                    resultMap.put("id", origination.getCode());
                    resultMap.put("locationName", origination.getName());
                    if (StringUtils.isNotBlank(origination.getCountyCode())) {
                        resultMap.put("relationCode", origination.getCountyCode());
                    } else if (StringUtils.isNotBlank(origination.getCityCode())) {
                        resultMap.put("relationCode", origination.getCityCode());
                    }
                    resultList.add(resultMap);
                }
            }
            result.setData(resultList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/queryOriginationForOms")
    @ApiOperation(value = "查询OMS起运地的", notes = "查询OMS起运地的", response = RestfulResponse.class)
    public RestfulResponse<List<Origination>> queryOriginationForOms(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/origination/queryOriginationForOms data: {}", condition);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            List<Origination> originationList = originationService.queryOriginationForOms(condition);
            result.setData(originationList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/queryListOrigination")
    @ApiOperation(value = "查询OTM全部起运地", notes = "查询OTM全部起运地", response = RestfulResponse.class)
    public RestfulResponse<List<Origination>> queryListOrigination(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/origination/queryListOrigination data: {}", condition);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            List<Origination> originationList = originationService.queryListOrigination(condition);
            result.setData(originationList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @GetMapping(value = "/getOriginationCity")
    @ApiOperation(value = "得到OTM起运城市", notes = "得到OTM起运城市", response = RestfulResponse.class)
    public RestfulResponse<List<OriginationCity>> getOriginationCity(@RequestParam("cityName") String cityName) {
        logger.info("controller:/origination/getOriginationCity data: {}", cityName);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            List<Origination> originationList = originationService.queryOriginationCity(cityName);
            List<OriginationCity> originationCityList = new ArrayList<>();
            for (Origination origination : originationList) {
                OriginationCity originationCity = new OriginationCity();
                originationCity.setProvince(origination.getProvince());
                originationCity.setCity(origination.getCity());
                originationCity.setProvinceCode(origination.getProvinceCode());
                originationCity.setCityCode(origination.getCityCode());
                originationCityList.add(originationCity);
            }

            result.setData(originationCityList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }


    /**
     * fix 报班启运地选择有业务的点
     */
    @GetMapping(value = "/getCheckinCity")
    @ApiOperation(value = "得到报班启运地城市", notes = "得到OTM起运城市", response = RestfulResponse.class)
    public RestfulResponse<List<OriginationCity>> getCheckinCity(@RequestParam("cityName") String cityName) {
        logger.info("controller:/origination/getCheckinCity data: {}", cityName);
        RestfulResponse<List<OriginationCity>> result = new RestfulResponse<>(0, "成功", null);
        try {
            List<Origination> originationList = originationService.queryOriginationCity(cityName);
            List<OriginationCity> originationCityList = new ArrayList<>();
            for (Origination origination : originationList) {
                OriginationCity originationCity = new OriginationCity();
                originationCity.setProvince(origination.getProvince());
                originationCity.setCity(origination.getCity());
                originationCity.setProvinceCode(origination.getProvinceCode());
                originationCity.setCityCode(origination.getCityCode());
                originationCityList.add(originationCity);
            }
            result.setData(originationCityList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }

    @PostMapping(value = "/queryOriginationCode")
    @ApiOperation(value = "根据省市区名查询对应编码", notes = "根据省市区名查询对应编码", response = RestfulResponse.class)
    public RestfulResponse<Origination> queryOriginationCode(@RequestBody Map<String, String> condition) {
        logger.info("controller:/origination/queryOriginationCode data: {}", JSON.toJSON(condition));
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            Origination origination = originationService.queryOriginationCode(condition);
            result.setData(origination);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }
}
