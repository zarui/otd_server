package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.OrderReleaseMidMapper;
import com.zhiche.lisa.integration.dto.sparePart.SPReleaseDTO;
import com.zhiche.lisa.integration.service.ISPReleaseService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description: 零部件实现类
 * @Date: Create in 9:58 2019/7/19
 */
@Service
public class SPReleaseServiceImpl implements ISPReleaseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SPReleaseServiceImpl.class);
@Autowired
private OrderReleaseMidMapper orderReleaseMidMapper;

    @Override
    public List<SPReleaseDTO> queryReleaseData (Map<String, String> param) {
        if(null == param || param.isEmpty()){
            throw new BaseException("入参不能为空");
        }
        String orderNoParam = param.get("ordernNo");
        if(StringUtils.isEmpty(orderNoParam)){
            throw new BaseException("入参【orderNo】不能为空！");
        }
        String[] orderNos = orderNoParam.split(",");
        EntityWrapper<SPReleaseDTO> spew = new EntityWrapper<>();
        spew.in("order_release_name", Arrays.asList(orderNos));
        spew.orderBy("gmt_create",false);
        List<SPReleaseDTO> spReleaseDTOS = orderReleaseMidMapper.queryReleaseData(spew);
        return spReleaseDTOS;
    }
}
