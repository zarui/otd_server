package com.zhiche.lisa.integration.inteface.junma.server;

import com.zhiche.lisa.integration.service.JunMaMesService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class JunMaMesInterface {
    @Autowired
    private Bus bus;

    @Autowired
    private JunMaMesService junMaMesService;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, junMaMesService);
        endpoint.publish("/mesInbound");
        return endpoint;
    }

}
