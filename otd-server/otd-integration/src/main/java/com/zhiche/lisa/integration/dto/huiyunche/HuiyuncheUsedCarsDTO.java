package com.zhiche.lisa.integration.dto.huiyunche;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class HuiyuncheUsedCarsDTO implements Serializable{
     /**
      *服务订单号
      */
     private String orderCode;
     /**
      *客户订单编号
      */
     private String customerOrderCode;
     /**
      *销售订单号
      */
     private String saleOrderCode;
     /**
      *客户ID
      */
     private String customerId;
     /**
      *客户名称
      */
     private String customerName;
     /**
      *下单人
      */
     private String username;
     /**
      *发车省编码
      */
     private String departProvinceCode;
     /**
      *发车省名称
      */
     private String departProvinceName;
     /**
      *发车市编码
      */
     private String departCityCode;
     /**
      *发车市名称
      */
     private String departCityName;
     /**
      *发车县编码
      */
     private String departCountyCode;
     /**
      *发车县名称
      */
     private String departCountyName;
     /**
      *发车详细地址
      */
     private String departAddr;
     /**
      *发车联系人
      */
     private String departContact;
     /**
      *发车联系电话
      */
     private String departPhone;
     /**
      *送达省编码
      */
     private String receiptProvinceCode;
     /**
      *送达省名称
      */
     private String receiptProvinceName;
     /**
      *送达市编码
      */
     private String receiptCityCode;
     /**
      *送达市名称
      */
     private String receiptCityName;
     /**
      *送达县编码
      */
     private String receiptCountyCode;
     /**
      *送达县名称
      */
     private String receiptCountyName;
     /**
      *送达详细地址
      */
     private String receiptAddr;
     /**
      *送达联系人
      */
     private String receiptContact;
     /**
      *送达联系电话
      */
     private String receiptPhone;
     /**
      *品牌ID
      */
     private Integer brandId;
     /**
      *品牌名称
      */
     private String brandName;
     /**
      *车型ID
      */
     private Integer vehicleId;
     /**
      *车型名称
      */
     private String vehicleName;
     /**
      *车架号
      */
     private String Vin;
     /**
      *提车日期
      */
     private Date deliveryDate;
     /**
      *送达日期
      */
     private Date arriveDate;
     /**
      *距离,单位：千米
      */
     private BigDecimal Distance;
     /**
      *金额
      */
     private BigDecimal Cost;

     /**
      *是否现金支付 1 是 0 不是
      */
     private Integer isCash;

     public Integer getIsCash() {
          return isCash;
     }

     public void setIsCash(Integer isCash) {
          this.isCash = isCash;
     }

     public String getOrderCode() {
          return orderCode;
     }

     public void setOrderCode(String orderCode) {
          this.orderCode = orderCode;
     }

     public String getCustomerOrderCode() {
          return customerOrderCode;
     }

     public void setCustomerOrderCode(String customerOrderCode) {
          this.customerOrderCode = customerOrderCode;
     }

     public String getSaleOrderCode() {
          return saleOrderCode;
     }

     public void setSaleOrderCode(String saleOrderCode) {
          this.saleOrderCode = saleOrderCode;
     }

     public String getCustomerId() {
          return customerId;
     }

     public void setCustomerId(String customerId) {
          this.customerId = customerId;
     }

     public String getCustomerName() {
          return customerName;
     }

     public void setCustomerName(String customerName) {
          this.customerName = customerName;
     }

     public String getUsername() {
          return username;
     }

     public void setUsername(String username) {
          this.username = username;
     }

     public String getDepartProvinceCode() {
          return departProvinceCode;
     }

     public void setDepartProvinceCode(String departProvinceCode) {
          this.departProvinceCode = departProvinceCode;
     }

     public String getDepartProvinceName() {
          return departProvinceName;
     }

     public void setDepartProvinceName(String departProvinceName) {
          this.departProvinceName = departProvinceName;
     }

     public String getDepartCityCode() {
          return departCityCode;
     }

     public void setDepartCityCode(String departCityCode) {
          this.departCityCode = departCityCode;
     }

     public String getDepartCityName() {
          return departCityName;
     }

     public void setDepartCityName(String departCityName) {
          this.departCityName = departCityName;
     }

     public String getDepartCountyCode() {
          return departCountyCode;
     }

     public void setDepartCountyCode(String departCountyCode) {
          this.departCountyCode = departCountyCode;
     }

     public String getDepartCountyName() {
          return departCountyName;
     }

     public void setDepartCountyName(String departCountyName) {
          this.departCountyName = departCountyName;
     }

     public String getDepartAddr() {
          return departAddr;
     }

     public void setDepartAddr(String departAddr) {
          this.departAddr = departAddr;
     }

     public String getDepartContact() {
          return departContact;
     }

     public void setDepartContact(String departContact) {
          this.departContact = departContact;
     }

     public String getDepartPhone() {
          return departPhone;
     }

     public void setDepartPhone(String departPhone) {
          this.departPhone = departPhone;
     }

     public String getReceiptProvinceCode() {
          return receiptProvinceCode;
     }

     public void setReceiptProvinceCode(String receiptProvinceCode) {
          this.receiptProvinceCode = receiptProvinceCode;
     }

     public String getReceiptProvinceName() {
          return receiptProvinceName;
     }

     public void setReceiptProvinceName(String receiptProvinceName) {
          this.receiptProvinceName = receiptProvinceName;
     }

     public String getReceiptCityCode() {
          return receiptCityCode;
     }

     public void setReceiptCityCode(String receiptCityCode) {
          this.receiptCityCode = receiptCityCode;
     }

     public String getReceiptCityName() {
          return receiptCityName;
     }

     public void setReceiptCityName(String receiptCityName) {
          this.receiptCityName = receiptCityName;
     }

     public String getReceiptCountyCode() {
          return receiptCountyCode;
     }

     public void setReceiptCountyCode(String receiptCountyCode) {
          this.receiptCountyCode = receiptCountyCode;
     }

     public String getReceiptCountyName() {
          return receiptCountyName;
     }

     public void setReceiptCountyName(String receiptCountyName) {
          this.receiptCountyName = receiptCountyName;
     }

     public String getReceiptAddr() {
          return receiptAddr;
     }

     public void setReceiptAddr(String receiptAddr) {
          this.receiptAddr = receiptAddr;
     }

     public String getReceiptContact() {
          return receiptContact;
     }

     public void setReceiptContact(String receiptContact) {
          this.receiptContact = receiptContact;
     }

     public String getReceiptPhone() {
          return receiptPhone;
     }

     public void setReceiptPhone(String receiptPhone) {
          this.receiptPhone = receiptPhone;
     }

     public Integer getBrandId() {
          return brandId;
     }

     public void setBrandId(Integer brandId) {
          this.brandId = brandId;
     }

     public String getBrandName() {
          return brandName;
     }

     public void setBrandName(String brandName) {
          this.brandName = brandName;
     }

     public Integer getVehicleId() {
          return vehicleId;
     }

     public void setVehicleId(Integer vehicleId) {
          this.vehicleId = vehicleId;
     }

     public String getVehicleName() {
          return vehicleName;
     }

     public void setVehicleName(String vehicleName) {
          this.vehicleName = vehicleName;
     }

     public String getVin() {
          return Vin;
     }

     public void setVin(String vin) {
          Vin = vin;
     }

     public Date getDeliveryDate() {
          return deliveryDate;
     }

     public void setDeliveryDate(Date deliveryDate) {
          this.deliveryDate = deliveryDate;
     }

     public Date getArriveDate() {
          return arriveDate;
     }

     public void setArriveDate(Date arriveDate) {
          this.arriveDate = arriveDate;
     }

     public BigDecimal getDistance() {
          return Distance;
     }

     public void setDistance(BigDecimal distance) {
          Distance = distance;
     }

     public BigDecimal getCost() {
          return Cost;
     }

     public void setCost(BigDecimal cost) {
          Cost = cost;
     }
}
