package com.zhiche.lisa.integration.dto.carrier;

/**
 * Created by zhaoguixin on 2018/8/16.
 */
public class RevokeCheckinDTO {

    private Long checkinId;

    private String revokeTime;

    private String revokePerson;

    private String revokeReason;

    public Long getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(Long checkinId) {
        this.checkinId = checkinId;
    }

    public String getRevokeTime() {
        return revokeTime;
    }

    public void setRevokeTime(String revokeTime) {
        this.revokeTime = revokeTime;
    }

    public String getRevokePerson() {
        return revokePerson;
    }

    public void setRevokePerson(String revokePerson) {
        this.revokePerson = revokePerson;
    }

    public String getRevokeReason() {
        return revokeReason;
    }

    public void setRevokeReason(String revokeReason) {
        this.revokeReason = revokeReason;
    }

    @Override
    public String toString() {
        return "RevokeCheckinDTO{" +
                "checkinId=" + checkinId +
                ", revokeTime=" + revokeTime +
                ", revokePerson='" + revokePerson + '\'' +
                ", revokeReason='" + revokeReason + '\'' +
                '}';
    }
}
