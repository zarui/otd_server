package com.zhiche.lisa.integration.dto.carrier;

import com.zhiche.lisa.integration.anno.ClassAnno;
import com.zhiche.lisa.integration.anno.FiledAnno;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by zhaoguixin on 2018/7/17.
 */
@ClassAnno("classpath:otmxml/truck_register.xml")
public class DriverCheckinDTO {

    private static final long serialVersionUID = 1L;

    /**
     * 报班ID
     */
    @FiledAnno("${id}")
    private Long id;
    /**
     * 司机ID
     */
    @FiledAnno("${driverId}")
    private Long driverId;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 司机手机号
     */
    @FiledAnno("${driverMoble}")
    private String driverMoble;
    /**
     * 司机所属承运商的ID
     */
    @FiledAnno("${lspId}")
    private Long lspId;
    /**
     * 承运商名称
     */
    private String lspName;
    /**
     * 始发区域
     */
    @FiledAnno("${departRegion}")
    private String departRegion;
    /**
     * 目的区域
     */
    @FiledAnno("${destRegion}")
    private String destRegion;
    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 报班时间
     */
    private Date gmtCheckin;
    /**
     * 报班时间
     */
    @FiledAnno("${checkinTime}")
    private String checkinTime;
    /**
     * 期望装车时间
     */
    private Date gmtExpLoad;
    /**
     * 期望装车时间
     */
    @FiledAnno("${expLoadTime}")
    private String expLoadTime;
    /**
     * 车队id
     */
    @FiledAnno("${fleetId}")
    private String fleetId;

    /**
     * 是否长期报班
     */
    @FiledAnno("${isLongTerm}")
    private String isLongTerm;
    /**
     * 是否有效(true:有效，false:失效)
     */
    @FiledAnno("${isActive}")
    private String isActive;

    /**
     * 预计装载台数
     */
    @FiledAnno("${amtExp}")
    private String amtExp;
    /**
     * 报班时的位置详情
     */
    private String checkinAddr;
    /**
     * 报班时的纬度
     */
    private BigDecimal checkinLatitude;
    /**
     * 报班时的经度
     */
    private BigDecimal checkinLongtitude;
    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 牵引车ID
     */
    private Long carrierId;
    /**
     * 牵引车车牌
     */
    @FiledAnno("${carrierPlate}")
    private String carrierPlate;
    /**
     * 挂车ID
     */
    private Long trailerId;
    /**
     * 挂车车牌
     */
    @FiledAnno("${trailerPlate}")
    private String trailerPlate;
    /**
     * 挂车类型ID
     */
    private String trailerTypeId;
    /**
     * 挂车类型名称
     */
    @FiledAnno("${trailerTypeName}")
    private String trailerTypeName;

    @FiledAnno("${transMode}")
    private String transMode;

    private List<DriverCheckinLineDTO> driverCheckinLineList;

    @FiledAnno("${checkinLineStr}")
    private String checkinLineStr;

    @FiledAnno("${fleetType}")
    private String fleetTypeId;

    public String getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMoble() {
        return driverMoble;
    }

    public void setDriverMoble(String driverMoble) {
        this.driverMoble = driverMoble;
    }

    public Long getLspId() {
        return lspId;
    }

    public void setLspId(Long lspId) {
        this.lspId = lspId;
    }

    public String getLspName() {
        return lspName;
    }

    public void setLspName(String lspName) {
        this.lspName = lspName;
    }

    public String getDepartRegion() {
        return departRegion;
    }

    public void setDepartRegion(String departRegion) {
        this.departRegion = departRegion;
    }

    public String getDestRegion() {
        return destRegion;
    }

    public void setDestRegion(String destRegion) {
        this.destRegion = destRegion;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCheckin() {
        return gmtCheckin;
    }

    public void setGmtCheckin(Date gmtCheckin) {
        this.gmtCheckin = gmtCheckin;
    }

    public Date getGmtExpLoad() {
        return gmtExpLoad;
    }

    public void setGmtExpLoad(Date gmtExpLoad) {
        this.gmtExpLoad = gmtExpLoad;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getAmtExp() {
        return amtExp;
    }

    public void setAmtExp(String amtExp) {
        this.amtExp = amtExp;
    }

    public String getCheckinAddr() {
        return checkinAddr;
    }

    public void setCheckinAddr(String checkinAddr) {
        this.checkinAddr = checkinAddr;
    }

    public BigDecimal getCheckinLatitude() {
        return checkinLatitude;
    }

    public void setCheckinLatitude(BigDecimal checkinLatitude) {
        this.checkinLatitude = checkinLatitude;
    }

    public BigDecimal getCheckinLongtitude() {
        return checkinLongtitude;
    }

    public void setCheckinLongtitude(BigDecimal checkinLongtitude) {
        this.checkinLongtitude = checkinLongtitude;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Long carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierPlate() {
        return carrierPlate;
    }

    public void setCarrierPlate(String carrierPlate) {
        this.carrierPlate = carrierPlate;
    }

    public Long getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(Long trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerPlate() {
        return trailerPlate;
    }

    public void setTrailerPlate(String trailerPlate) {
        this.trailerPlate = trailerPlate;
    }

    public String getTrailerTypeId() {
        return trailerTypeId;
    }

    public void setTrailerTypeId(String trailerTypeId) {
        this.trailerTypeId = trailerTypeId;
    }

    public String getTrailerTypeName() {
        return trailerTypeName;
    }

    public void setTrailerTypeName(String trailerTypeName) {
        this.trailerTypeName = trailerTypeName;
    }

    public String getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(String checkinTime) {
        this.checkinTime = checkinTime;
    }

    public String getExpLoadTime() {
        return expLoadTime;
    }

    public void setExpLoadTime(String expLoadTime) {
        this.expLoadTime = expLoadTime;
    }

    public String getIsLongTerm() {
        return isLongTerm;
    }

    public void setIsLongTerm(String isLongTerm) {
        this.isLongTerm = isLongTerm;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public List<DriverCheckinLineDTO> getDriverCheckinLineList() {
        return driverCheckinLineList;
    }

    public void setDriverCheckinLineList(List<DriverCheckinLineDTO> driverCheckinLineList) {
        this.driverCheckinLineList = driverCheckinLineList;
    }

    public String getCheckinLineStr() {
        return checkinLineStr;
    }

    public void setCheckinLineStr(String checkinLineStr) {
        this.checkinLineStr = checkinLineStr;
    }

    public String getTransMode() {
        return transMode;
    }

    public void setTransMode(String transMode) {
        this.transMode = transMode;
    }
}
