package com.zhiche.lisa.integration.dto.order;

import com.zhiche.lisa.integration.anno.FiledAnno;

import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2018/6/13.
 */
public class ErpShipping {

    /**
     * 主键序号
     */
    private Double ilineid;

    /**
     * 出场操作员
     */
    private String vcoutwarehouseuser;

    /**
     * 出场时间
     */
    private Date dtoutwarehouse;

    /**
     * 发运指令号
     */
    @FiledAnno("shipmentId")
    private String vcshipno;

    /**
     * 生成指令时间
     */
    @FiledAnno("gmtCreate")
    private Date dtdate;

    /**
     * 发运指令类型(0:重载、1空载、2待班)
     */
    @FiledAnno("shipmentType")
    private String itype;

    /**
     * 待班天数
     */
    private Double iwaitday;

    /**
     * 待班城市
     */
    private String vcwaitcityname;

    /**
     * 运输车序号
     */
    private Double ivehicleid;

    /**
     * 主驾司机序号
     */
    private Double idriverid;

    /**
     * 副驾司机序号
     */
    private Double idriverid2;

    /**
     * 车牌号
     */
    @FiledAnno("plateNo")
    private String vclicense;

    /**
     * 承运车队
     */
    private Double ifleetid;

    /**
     * 承运车队名称
     */
    @FiledAnno("serviceProviderName")
    private String ifleetname;

    /**
     * 调度员
     */
    private String vcuserno;

    /**
     * 空放审核标志(0:未审核,1.0:已审核,1.1审核通过)
     */
    private Double icheck;

    /**
     * 审核时间
     */
    private Date dtcheckdate;

    /**
     * 审核人
     */
    private String vccheckuserno;

    /**
     * 审核意见
     */
    private String vccheckmemo;

    /**
     * 装车类型(0:单排，1双排)
     */
    private Double iassigntype;

    /**
     * 状态标志(0:未回单,1:已回单)
     */
    @FiledAnno("status")
    private String breturn;

    /**
     * 回单时间
     */
    private Date dtreturn;

    /**
     * 回单人编号
     */
    private String vcreturnuserno;

    /**
     * 核销标志(0:未核销,1:已核销)
     */
    private Double bfinish;

    /**
     * 核销时间
     */
    private Date dtfinish;

    /**
     * 核销人编号
     */
    private String vcfinishuserno;

    /**
     * 核销备注
     */
    private String vcfinishmemo;

    /**
     * 核销单号
     */
    private String vccheckno;

    /**
     * 备注
     */
    private String vcmemo;

    /**
     * 是否短信通知(0:否,1:是)
     */
    private Double bsendinfo;

    /**
     * 最后一次通知时间
     */
    private Date dtsendinfo;

    /**
     * 所属部门
     */
    private Double ideptid;

    /**
     * 待班在驻点(0非驻点,1驻点)
     */
    private Double bfleetcity;

    /**
     * 车辆归属标志(0，内部，1：外协)
     */
    private Double ifleettypeid;

    /**
     * 未核销说明
     */
    private String vcneedcheckmemo;

    /**
     * 需要核销标志(0需要，1：不需要)
     */
    private Double bneedcheck;

    /**
     * 由父级指令生成的待班
     */
    private String vcwaitshipid;

    /**
     * 下个目的地
     */
    private String vcnextdest;

    /**
     * 下个目的地维护人
     */
    private String vcnextuserno;

    /**
     * 前一个指令id
     */
    private Double iplineid;

    /**
     * 打款标志(0:没打,1:已打)
     */
    private Double bsend;

    /**
     * 满载状态(0:未满载;1:已满载)
     */
    private Double imz;

    /**
     * 指令审核状态(0:未审核;1:已审核)
     */
    private Double bcheckem;

    /**
     * 所属车队2
     */
    private Double ifleetid2;

    /**
     * 审核人2
     */
    private String vcchecktempuserno;

    /**
     * 是否打印(1:已打印,0:没打印)
     */
    private String bprint;

    /**
     * 打印时间
     */
    private Date dtprint;

    /**
     * 打印人
     */
    private String vcprintuserno;

    /**
     * TMS指令主表id
     */
    private Double itmsid;

    /**
     * 入场
     */
    private Double iarriveid;

    /**
     * 运力入场确定人
     */
    private String vcarriveuserno;

    /**
     * 抵达时间
     */
    private Date dtarrive;

    /**
     * 油款打款标志(0:没打,1:已打)
     */
    private Double boil;

    /**
     * 无效1
     */
    private Double ietypeid;

    /**
     * 是否导入安联系统(0:否,1:是)
     */
    private Double bimp;

    /**
     * 运输模式
     */
    private Double imodeid;

    /**
     * 司机名称
     */
    @FiledAnno("driverName")
    private String vcdrivername;

    /**
     * 移动电话
     */
    @FiledAnno("driverMoble")
    private String vcmobile;

    /**
     * ${field.comment}
     */
    private Double ivehicleClass;

    /**
     * ${field.comment}
     */
    private Double irateOfVehicle;

    /**
     * 运输工具类型，对应smvehicle.i_yltype和smCapacitytype.ilineid
     */
    private Double icapacity;

    /**
     * 实际配板方案类型，关联smCapacitytype.ilineid
     */
    private Double ischemaType;

    /**
     * 发运指令明细
     */
    private List<ErpShipLine> shipLineInfoBOList;

    public Double getIlineid() {
        return ilineid;
    }

    public void setIlineid(Double ilineid) {
        this.ilineid = ilineid;
    }

    public String getVcoutwarehouseuser() {
        return vcoutwarehouseuser;
    }

    public void setVcoutwarehouseuser(String vcoutwarehouseuser) {
        this.vcoutwarehouseuser = vcoutwarehouseuser;
    }

    public Date getDtoutwarehouse() {
        return dtoutwarehouse;
    }

    public void setDtoutwarehouse(Date dtoutwarehouse) {
        this.dtoutwarehouse = dtoutwarehouse;
    }

    public String getVcshipno() {
        return vcshipno;
    }

    public void setVcshipno(String vcshipno) {
        this.vcshipno = vcshipno;
    }

    public Date getDtdate() {
        return dtdate;
    }

    public void setDtdate(Date dtdate) {
        this.dtdate = dtdate;
    }

    public String getItype() {
        return itype;
    }

    public void setItype(String itype) {
        this.itype = itype;
    }

    public Double getIwaitday() {
        return iwaitday;
    }

    public void setIwaitday(Double iwaitday) {
        this.iwaitday = iwaitday;
    }

    public String getVcwaitcityname() {
        return vcwaitcityname;
    }

    public void setVcwaitcityname(String vcwaitcityname) {
        this.vcwaitcityname = vcwaitcityname;
    }

    public Double getIvehicleid() {
        return ivehicleid;
    }

    public void setIvehicleid(Double ivehicleid) {
        this.ivehicleid = ivehicleid;
    }

    public Double getIdriverid() {
        return idriverid;
    }

    public void setIdriverid(Double idriverid) {
        this.idriverid = idriverid;
    }

    public Double getIdriverid2() {
        return idriverid2;
    }

    public void setIdriverid2(Double idriverid2) {
        this.idriverid2 = idriverid2;
    }

    public String getVclicense() {
        return vclicense;
    }

    public void setVclicense(String vclicense) {
        this.vclicense = vclicense;
    }

    public Double getIfleetid() {
        return ifleetid;
    }

    public void setIfleetid(Double ifleetid) {
        this.ifleetid = ifleetid;
    }

    public String getIfleetname() {
        return ifleetname;
    }

    public void setIfleetname(String ifleetname) {
        this.ifleetname = ifleetname;
    }

    public String getVcuserno() {
        return vcuserno;
    }

    public void setVcuserno(String vcuserno) {
        this.vcuserno = vcuserno;
    }

    public Double getIcheck() {
        return icheck;
    }

    public void setIcheck(Double icheck) {
        this.icheck = icheck;
    }

    public Date getDtcheckdate() {
        return dtcheckdate;
    }

    public void setDtcheckdate(Date dtcheckdate) {
        this.dtcheckdate = dtcheckdate;
    }

    public String getVccheckuserno() {
        return vccheckuserno;
    }

    public void setVccheckuserno(String vccheckuserno) {
        this.vccheckuserno = vccheckuserno;
    }

    public String getVccheckmemo() {
        return vccheckmemo;
    }

    public void setVccheckmemo(String vccheckmemo) {
        this.vccheckmemo = vccheckmemo;
    }

    public Double getIassigntype() {
        return iassigntype;
    }

    public void setIassigntype(Double iassigntype) {
        this.iassigntype = iassigntype;
    }

    public String getBreturn() {
        return breturn;
    }

    public void setBreturn(String breturn) {
        this.breturn = breturn;
    }

    public Date getDtreturn() {
        return dtreturn;
    }

    public void setDtreturn(Date dtreturn) {
        this.dtreturn = dtreturn;
    }

    public String getVcreturnuserno() {
        return vcreturnuserno;
    }

    public void setVcreturnuserno(String vcreturnuserno) {
        this.vcreturnuserno = vcreturnuserno;
    }

    public Double getBfinish() {
        return bfinish;
    }

    public void setBfinish(Double bfinish) {
        this.bfinish = bfinish;
    }

    public Date getDtfinish() {
        return dtfinish;
    }

    public void setDtfinish(Date dtfinish) {
        this.dtfinish = dtfinish;
    }

    public String getVcfinishuserno() {
        return vcfinishuserno;
    }

    public void setVcfinishuserno(String vcfinishuserno) {
        this.vcfinishuserno = vcfinishuserno;
    }

    public String getVcfinishmemo() {
        return vcfinishmemo;
    }

    public void setVcfinishmemo(String vcfinishmemo) {
        this.vcfinishmemo = vcfinishmemo;
    }

    public String getVccheckno() {
        return vccheckno;
    }

    public void setVccheckno(String vccheckno) {
        this.vccheckno = vccheckno;
    }

    public String getVcmemo() {
        return vcmemo;
    }

    public void setVcmemo(String vcmemo) {
        this.vcmemo = vcmemo;
    }

    public Double getBsendinfo() {
        return bsendinfo;
    }

    public void setBsendinfo(Double bsendinfo) {
        this.bsendinfo = bsendinfo;
    }

    public Date getDtsendinfo() {
        return dtsendinfo;
    }

    public void setDtsendinfo(Date dtsendinfo) {
        this.dtsendinfo = dtsendinfo;
    }

    public Double getIdeptid() {
        return ideptid;
    }

    public void setIdeptid(Double ideptid) {
        this.ideptid = ideptid;
    }

    public Double getBfleetcity() {
        return bfleetcity;
    }

    public void setBfleetcity(Double bfleetcity) {
        this.bfleetcity = bfleetcity;
    }

    public Double getIfleettypeid() {
        return ifleettypeid;
    }

    public void setIfleettypeid(Double ifleettypeid) {
        this.ifleettypeid = ifleettypeid;
    }

    public String getVcneedcheckmemo() {
        return vcneedcheckmemo;
    }

    public void setVcneedcheckmemo(String vcneedcheckmemo) {
        this.vcneedcheckmemo = vcneedcheckmemo;
    }

    public Double getBneedcheck() {
        return bneedcheck;
    }

    public void setBneedcheck(Double bneedcheck) {
        this.bneedcheck = bneedcheck;
    }

    public String getVcwaitshipid() {
        return vcwaitshipid;
    }

    public void setVcwaitshipid(String vcwaitshipid) {
        this.vcwaitshipid = vcwaitshipid;
    }

    public String getVcnextdest() {
        return vcnextdest;
    }

    public void setVcnextdest(String vcnextdest) {
        this.vcnextdest = vcnextdest;
    }

    public String getVcnextuserno() {
        return vcnextuserno;
    }

    public void setVcnextuserno(String vcnextuserno) {
        this.vcnextuserno = vcnextuserno;
    }

    public Double getIplineid() {
        return iplineid;
    }

    public void setIplineid(Double iplineid) {
        this.iplineid = iplineid;
    }

    public Double getBsend() {
        return bsend;
    }

    public void setBsend(Double bsend) {
        this.bsend = bsend;
    }

    public Double getImz() {
        return imz;
    }

    public void setImz(Double imz) {
        this.imz = imz;
    }

    public Double getBcheckem() {
        return bcheckem;
    }

    public void setBcheckem(Double bcheckem) {
        this.bcheckem = bcheckem;
    }

    public Double getIfleetid2() {
        return ifleetid2;
    }

    public void setIfleetid2(Double ifleetid2) {
        this.ifleetid2 = ifleetid2;
    }

    public String getVcchecktempuserno() {
        return vcchecktempuserno;
    }

    public void setVcchecktempuserno(String vcchecktempuserno) {
        this.vcchecktempuserno = vcchecktempuserno;
    }

    public String getBprint() {
        return bprint;
    }

    public void setBprint(String bprint) {
        this.bprint = bprint;
    }

    public Date getDtprint() {
        return dtprint;
    }

    public void setDtprint(Date dtprint) {
        this.dtprint = dtprint;
    }

    public String getVcprintuserno() {
        return vcprintuserno;
    }

    public void setVcprintuserno(String vcprintuserno) {
        this.vcprintuserno = vcprintuserno;
    }

    public Double getItmsid() {
        return itmsid;
    }

    public void setItmsid(Double itmsid) {
        this.itmsid = itmsid;
    }

    public Double getIarriveid() {
        return iarriveid;
    }

    public void setIarriveid(Double iarriveid) {
        this.iarriveid = iarriveid;
    }

    public String getVcarriveuserno() {
        return vcarriveuserno;
    }

    public void setVcarriveuserno(String vcarriveuserno) {
        this.vcarriveuserno = vcarriveuserno;
    }

    public Date getDtarrive() {
        return dtarrive;
    }

    public void setDtarrive(Date dtarrive) {
        this.dtarrive = dtarrive;
    }

    public Double getBoil() {
        return boil;
    }

    public void setBoil(Double boil) {
        this.boil = boil;
    }

    public Double getIetypeid() {
        return ietypeid;
    }

    public void setIetypeid(Double ietypeid) {
        this.ietypeid = ietypeid;
    }

    public Double getBimp() {
        return bimp;
    }

    public void setBimp(Double bimp) {
        this.bimp = bimp;
    }

    public Double getImodeid() {
        return imodeid;
    }

    public void setImodeid(Double imodeid) {
        this.imodeid = imodeid;
    }

    public String getVcdrivername() {
        return vcdrivername;
    }

    public void setVcdrivername(String vcdrivername) {
        this.vcdrivername = vcdrivername;
    }

    public String getVcmobile() {
        return vcmobile;
    }

    public void setVcmobile(String vcmobile) {
        this.vcmobile = vcmobile;
    }

    public Double getIvehicleClass() {
        return ivehicleClass;
    }

    public void setIvehicleClass(Double ivehicleClass) {
        this.ivehicleClass = ivehicleClass;
    }

    public Double getIrateOfVehicle() {
        return irateOfVehicle;
    }

    public void setIrateOfVehicle(Double irateOfVehicle) {
        this.irateOfVehicle = irateOfVehicle;
    }

    public Double getIcapacity() {
        return icapacity;
    }

    public void setIcapacity(Double icapacity) {
        this.icapacity = icapacity;
    }

    public Double getIschemaType() {
        return ischemaType;
    }

    public void setIschemaType(Double ischemaType) {
        this.ischemaType = ischemaType;
    }

    public List<ErpShipLine> getShipLineInfoBOList() {
        return shipLineInfoBOList;
    }

    public void setShipLineInfoBOList(List<ErpShipLine> shipLineInfoBOList) {
        this.shipLineInfoBOList = shipLineInfoBOList;
    }

    public List<ErpShipLine> getShipLineList() {
        return shipLineInfoBOList;
    }

    public void setShipLineList(List<ErpShipLine> shipLineInfoBOList) {
        this.shipLineInfoBOList = shipLineInfoBOList;
    }

    @Override
    public String toString() {
        return "ErpShipping{" +
                "ilineid=" + ilineid +
                ", vcoutwarehouseuser='" + vcoutwarehouseuser + '\'' +
                ", dtoutwarehouse=" + dtoutwarehouse +
                ", vcshipno='" + vcshipno + '\'' +
                ", dtdate=" + dtdate +
                ", itype=" + itype +
                ", iwaitday=" + iwaitday +
                ", vcwaitcityname='" + vcwaitcityname + '\'' +
                ", ivehicleid=" + ivehicleid +
                ", idriverid=" + idriverid +
                ", idriverid2=" + idriverid2 +
                ", vclicense='" + vclicense + '\'' +
                ", ifleetid=" + ifleetid +
                ", ifleetname='" + ifleetname + '\'' +
                ", vcuserno='" + vcuserno + '\'' +
                ", icheck=" + icheck +
                ", dtcheckdate=" + dtcheckdate +
                ", vccheckuserno='" + vccheckuserno + '\'' +
                ", vccheckmemo='" + vccheckmemo + '\'' +
                ", iassigntype=" + iassigntype +
                ", breturn=" + breturn +
                ", dtreturn=" + dtreturn +
                ", vcreturnuserno='" + vcreturnuserno + '\'' +
                ", bfinish=" + bfinish +
                ", dtfinish=" + dtfinish +
                ", vcfinishuserno='" + vcfinishuserno + '\'' +
                ", vcfinishmemo='" + vcfinishmemo + '\'' +
                ", vccheckno='" + vccheckno + '\'' +
                ", vcmemo='" + vcmemo + '\'' +
                ", bsendinfo=" + bsendinfo +
                ", dtsendinfo=" + dtsendinfo +
                ", ideptid=" + ideptid +
                ", bfleetcity=" + bfleetcity +
                ", ifleettypeid=" + ifleettypeid +
                ", vcneedcheckmemo='" + vcneedcheckmemo + '\'' +
                ", bneedcheck=" + bneedcheck +
                ", vcwaitshipid='" + vcwaitshipid + '\'' +
                ", vcnextdest='" + vcnextdest + '\'' +
                ", vcnextuserno='" + vcnextuserno + '\'' +
                ", iplineid=" + iplineid +
                ", bsend=" + bsend +
                ", imz=" + imz +
                ", bcheckem=" + bcheckem +
                ", ifleetid2=" + ifleetid2 +
                ", vcchecktempuserno='" + vcchecktempuserno + '\'' +
                ", bprint=" + bprint +
                ", dtprint=" + dtprint +
                ", vcprintuserno='" + vcprintuserno + '\'' +
                ", itmsid=" + itmsid +
                ", iarriveid=" + iarriveid +
                ", vcarriveuserno='" + vcarriveuserno + '\'' +
                ", dtarrive=" + dtarrive +
                ", boil=" + boil +
                ", ietypeid=" + ietypeid +
                ", bimp=" + bimp +
                ", imodeid=" + imodeid +
                ", vcdrivername='" + vcdrivername + '\'' +
                ", vcmobile='" + vcmobile + '\'' +
                ", ivehicleClass=" + ivehicleClass +
                ", irateOfVehicle=" + irateOfVehicle +
                ", icapacity=" + icapacity +
                ", ischemaType=" + ischemaType +
                ", shipLineInfoBOList=" + shipLineInfoBOList +
                '}';
    }
}
