package com.zhiche.lisa.integration.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.*;
import com.zhiche.lisa.integration.dto.base.BaseUserDTO;
import com.zhiche.lisa.integration.dto.otd.*;
import com.zhiche.lisa.integration.service.IOtdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/otdIntegration")
public class OTDController {


    private final static Logger LOGGER = LoggerFactory.getLogger(OTDController.class);

    @Autowired
    private IOtdService otdService;

    /**
     * 查询用户 - otd
     */
    @PostMapping("/queryUser")
    public RestfulResponse<List<BaseUserDTO>> queryUser (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryUser OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<BaseUserDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<BaseUserDTO> users = otdService.queryUserOTD(dto);
            response.setData(users);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryUser OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryUser OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }


    /**
     * 查询启运地 - otd
     */
    @PostMapping("/queryOrigin")
    public RestfulResponse<List<Origination>> queryOrigin (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryOrigin OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<Origination>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<Origination> origins = otdService.queryOriginOTD(dto);
            response.setData(origins);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryOrigin OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryOrigin OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * 查询客户 - otd
     */
    @PostMapping("/queryCorp")
    public RestfulResponse<List<CorporationDTO>> queryCorp (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryCorp OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<CorporationDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<CorporationDTO> data = otdService.queryCorpOTD(dto);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryCorp OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryCorp OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }


    /**
     * 查询业务主体 - otd
     */
    @PostMapping("/queryCiam")
    public RestfulResponse<List<CiamsWithOutIdDTO>> queryCiam (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryCiam OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<CiamsWithOutIdDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<CiamsWithOutIdDTO> ciams = otdService.queryCiamOTD(dto);
            response.setData(ciams);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryCiam OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryCiam OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }


    /**
     * 查询业务主体 客户关系 - otd
     */
    @PostMapping("/queryCorpCiam")
    public RestfulResponse<List<CorpCiamDTO>> queryCorpCiam (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryCorpCiam OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<CorpCiamDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<CorpCiamDTO> data = otdService.queryCorpCiamOTD(dto);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryCorpCiam OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryCorpCiam OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * 查询中铁货运车辆在途
     */
    @PostMapping("/queryCsrcVinDetail")
    public String queryVinDetail (@RequestParam("vinStrs") String vins) {
        LOGGER.info("OTDController.queryVinDetail OTD vins:{}", vins);
        RestfulResponse<List<CorpCiamDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            return otdService.queryCsrcVinDetail(vins);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryVinDetail OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryVinDetail OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return JSONObject.toJSONString(response);
    }

    @PostMapping("/queryOrder")
    public RestfulResponse<Page<OrderMid>> queryOrder (@RequestBody Page<OrderMid> page) {
        LOGGER.info("OTDController.queryOrder OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<OrderMid>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<OrderMid> data = otdService.queryOrder(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryOrder OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryOrder OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    @PostMapping("/queryRouteRelease")
    public RestfulResponse<Page<OrderReleaseMidDTO>> queryRouteRelease (@RequestBody Page<OrderReleaseMidDTO> page) {
        LOGGER.info("OTDController.queryRouteRelease OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<OrderReleaseMidDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<OrderReleaseMidDTO> data = otdService.queryRouteRelease(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryRouteRelease OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryRouteRelease OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }


    /**
     * <p>
     * otd 拉取查询客户时效信息
     * </p>
     */
    @PostMapping("/queryCusLimition")
    public RestfulResponse<List<CusLimitionMid>> queryCusLimition (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryCusLimition OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<CusLimitionMid>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<CusLimitionMid> data = otdService.queryCusLimition(dto);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryCusLimition OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryCusLimition OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }


    /**
     * <p>
     * otd 拉取查询内控时效信息
     * </p>
     */
    @PostMapping("/queryUlcLimition")
    public RestfulResponse<List<UlcLimitionMid>> queryUlcLimition (@RequestBody Map<String, String> dto) {
        LOGGER.info("OTDController.queryUlcLimition OTD dtoJSON:{}", JSONObject.toJSONString(dto));
        RestfulResponse<List<UlcLimitionMid>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            List<UlcLimitionMid> data = otdService.queryUlcLimition(dto);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryUlcLimition OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryUlcLimition OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * <p>
     * otd 拉取查询指令信息
     * </p>
     */
    @PostMapping("/queryShipment")
    public RestfulResponse<Page<OTDShipmentDTO>> queryShipment (@RequestBody Page<OTDShipmentDTO> page) {
        LOGGER.info("OTDController.queryShipment OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<OTDShipmentDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<OTDShipmentDTO> data = otdService.queryShipment(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryShipment OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryShipment OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    @PostMapping("/queryOrderNew")
    public RestfulResponse<Page<NewOrderMid>> queryOrderNew (@RequestBody Page<NewOrderMid> page) {
        LOGGER.info("OTDController.queryOrderNew OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<NewOrderMid>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<NewOrderMid> data = otdService.queryOrderNew(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryOrderNew OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryOrderNew OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * <p>
     * otd 拉取查询指令信息
     * </p>
     */
    @PostMapping("/queryShipmentNew")
    public RestfulResponse<Page<NewOTDShipmentDTO>> queryShipmentNew (@RequestBody Page<NewOTDShipmentDTO> page) {
        LOGGER.info("OTDController.queryShipmentNew OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<NewOTDShipmentDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<NewOTDShipmentDTO> data = otdService.queryShipmentNew(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryShipmentNew OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryShipmentNew OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    @PostMapping("/queryRouteReleaseNew")
    public RestfulResponse<Page<NewOrderReleaseMidDTO>> queryRouteReleaseNew (@RequestBody Page<NewOrderReleaseMidDTO> page) {
        LOGGER.info("OTDController.queryRouteReleaseNew OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<NewOrderReleaseMidDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            Page<NewOrderReleaseMidDTO> data = otdService.queryRouteReleaseNew(page);
            response.setData(data);
        } catch (BaseException be) {
            LOGGER.error("OTDController.queryRouteReleaseNew OTD BaseException:{}", be.getMessage());
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            LOGGER.error("OTDController.queryRouteReleaseNew OTD error:{}", e.getMessage());
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    /**
     * G7 获取中铁，中交，人送的在途信息
     */
    @PostMapping("/queryInTransitInfo")
    public RestfulResponse<Page<InTransitDTO>> queryInTransitInfo (@RequestBody Page<InTransitDTO> page) {
        LOGGER.info("OTDController.queryInTransitInfo OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<InTransitDTO>> response = new RestfulResponse<>(0, "查询成功", null);
        Page<InTransitDTO> data = otdService.queryInTransitInfo(page);
        response.setData(data);
        return response;
    }

    /**
     * G7 查询路由删除记录
     *
     * @param page
     * @return
     */
    @PostMapping("/queryRouteDelInfo")
    public RestfulResponse<Page<RouteDelMid>> queryRouteDelInfo (@RequestBody Page<RouteDelMid> page) {
        LOGGER.info("OTDController.queryRouteReleaseNew OTD dtoJSON:{}", JSONObject.toJSONString(page));
        RestfulResponse<Page<RouteDelMid>> response = new RestfulResponse<>(0, "查询成功", null);
        Page<RouteDelMid> data = otdService.queryRouteDelMid(page);
        response.setData(data);
        return response;
    }

}
