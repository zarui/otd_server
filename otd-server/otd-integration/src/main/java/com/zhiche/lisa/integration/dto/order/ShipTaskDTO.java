package com.zhiche.lisa.integration.dto.order;

import com.zhiche.lisa.integration.anno.FiledAnno;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 运输任务DTO
 * </p>
 *
 * @author qichao
 * @since 2018-06-13
 */
public class ShipTaskDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 运输订单唯一标识
     */
    @FiledAnno("taskId")
    private String taskId;
    /**
     * 调度指令唯一标识
     */
    @FiledAnno("shipmentId")
    private String shipmentId;
    /**
     * 客户唯一标识
     */
    private String customerId;
    /**
     * 客户订单号
     */
    @FiledAnno("cusOrderNo")
    private String cusOrderNo;
    /**
     * 客户运单号
     */
    private String cusWaybillNo;
    /**
     * 客户运输模式分类
     */
    private String cusTransMode;
    /**
     * 是否急发
     */
    private String isUrgent;
    /**
     * 是否超期
     */
    private String pickupIsAppt;
    /**
     * 订单属性
     */
    private String orderAtt;
    /**
     * 要求入库时间
     */
    private Date expectInboundDate;
    /**
     * 要求回单时间
     */
    private Date expectReceiptDate;
    /**
     * 要求发运时间
     */
    private Date expcetShipDate;
    /**
     * 要求抵达时间
     */
    private Date expectArriveDate;
    /**
     * 起运地唯一标识
     */
    @FiledAnno("originLocationId")
    private String originLocationId;
    /**
     * 起运地名称
     */
    private String originLocationName;
    /**
     * 起运地省份
     */
    private String originLocationProvince;
    /**
     * 起运地城市
     */
    @FiledAnno("originLocationCity")
    private String originLocationCity;
    /**
     * 起运地区县
     */
    private String originLocationCounty;
    /**
     * 起运地地址
     */
    private String originLocationAddress;
    /**
     * 目的地唯一标识
     */
    @FiledAnno("destLocationId")
    private String destLocationId;
    /**
     * 目的地名称
     */
    private String destLocationName;
    /**
     * 目的地省份
     */
    private String destLocationProvince;
    /**
     * 目的地城市
     */
    @FiledAnno("destLocationCity")
    private String destLocationCity;
    /**
     * 目的地区县
     */
    private String destLocationCounty;
    /**
     * 目的地地址
     */
    private String destLocationAddress;
    /**
     * 客户车型
     */
    @FiledAnno("cusVehicleType")
    private String cusVehicleType;
    /**
     * 车型描述
     */
    private String vehicleDescribe;
    /**
     * 标准车型
     */
    private String stanVehicleType;
    /**
     * VIN码
     */
    @FiledAnno("vin")
    private String vin;
    /**
     * 是否改装车
     */
    private String isModVehicle;
    /**
     * 改装后的长宽高重
     */
    private String modVehicleSize;
    /**
     * 二维码
     */
    private String qrCode;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 任务状态
     */
    private String status;

    /**
     * 起始地 站点顺序
     */
    private String originLocationSequence;

    /**
     * 目的地 站点顺序
     */
    private String destLocationSequence;

    private String ciamsId;

    private String isTerminal;

    public String getIsTerminal() {
        return isTerminal;
    }

    public void setIsTerminal(String isTerminal) {
        this.isTerminal = isTerminal;
    }
    /**
     * 下单时间
     */
    private Date orderDate;

    /**
     * 订单号
     * @return
     */
    private String orderNo;


    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCiamsId() {
        return ciamsId;
    }

    public void setCiamsId(String ciamsId) {
        this.ciamsId = ciamsId;
    }

    public String getOriginLocationSequence() {
        return originLocationSequence;
    }

    public void setOriginLocationSequence(String originLocationSequence) {
        this.originLocationSequence = originLocationSequence;
    }

    public String getDestLocationSequence() {
        return destLocationSequence;
    }

    public void setDestLocationSequence(String destLocationSequence) {
        this.destLocationSequence = destLocationSequence;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCusOrderNo() {
        return cusOrderNo;
    }

    public void setCusOrderNo(String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybillNo() {
        return cusWaybillNo;
    }

    public void setCusWaybillNo(String cusWaybillNo) {
        this.cusWaybillNo = cusWaybillNo;
    }

    public String getCusTransMode() {
        return cusTransMode;
    }

    public void setCusTransMode(String cusTransMode) {
        this.cusTransMode = cusTransMode;
    }

    public String getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(String isUrgent) {
        this.isUrgent = isUrgent;
    }

    public String getPickupIsAppt() {
        return pickupIsAppt;
    }

    public void setPickupIsAppt(String pickupIsAppt) {
        this.pickupIsAppt = pickupIsAppt;
    }

    public String getOrderAtt() {
        return orderAtt;
    }

    public void setOrderAtt(String orderAtt) {
        this.orderAtt = orderAtt;
    }

    public Date getExpectInboundDate() {
        return expectInboundDate;
    }

    public void setExpectInboundDate(Date expectInboundDate) {
        this.expectInboundDate = expectInboundDate;
    }

    public Date getExpectReceiptDate() {
        return expectReceiptDate;
    }

    public void setExpectReceiptDate(Date expectReceiptDate) {
        this.expectReceiptDate = expectReceiptDate;
    }

    public Date getExpcetShipDate() {
        return expcetShipDate;
    }

    public void setExpcetShipDate(Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate() {
        return expectArriveDate;
    }

    public void setExpectArriveDate(Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public String getOriginLocationId() {
        return originLocationId;
    }

    public void setOriginLocationId(String originLocationId) {
        this.originLocationId = originLocationId;
    }

    public String getOriginLocationName() {
        return originLocationName;
    }

    public void setOriginLocationName(String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getOriginLocationProvince() {
        return originLocationProvince;
    }

    public void setOriginLocationProvince(String originLocationProvince) {
        this.originLocationProvince = originLocationProvince;
    }

    public String getOriginLocationCity() {
        return originLocationCity;
    }

    public void setOriginLocationCity(String originLocationCity) {
        this.originLocationCity = originLocationCity;
    }

    public String getOriginLocationCounty() {
        return originLocationCounty;
    }

    public void setOriginLocationCounty(String originLocationCounty) {
        this.originLocationCounty = originLocationCounty;
    }

    public String getOriginLocationAddress() {
        return originLocationAddress;
    }

    public void setOriginLocationAddress(String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getDestLocationId() {
        return destLocationId;
    }

    public void setDestLocationId(String destLocationId) {
        this.destLocationId = destLocationId;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationAddress() {
        return destLocationAddress;
    }

    public void setDestLocationAddress(String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getCusVehicleType() {
        return cusVehicleType;
    }

    public void setCusVehicleType(String cusVehicleType) {
        this.cusVehicleType = cusVehicleType;
    }

    public String getVehicleDescribe() {
        return vehicleDescribe;
    }

    public void setVehicleDescribe(String vehicleDescribe) {
        this.vehicleDescribe = vehicleDescribe;
    }

    public String getStanVehicleType() {
        return stanVehicleType;
    }

    public void setStanVehicleType(String stanVehicleType) {
        this.stanVehicleType = stanVehicleType;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getIsModVehicle() {
        return isModVehicle;
    }

    public void setIsModVehicle(String isModVehicle) {
        this.isModVehicle = isModVehicle;
    }

    public String getModVehicleSize() {
        return modVehicleSize;
    }

    public void setModVehicleSize(String modVehicleSize) {
        this.modVehicleSize = modVehicleSize;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ShipTaskDTO{" +
                "taskId='" + taskId + '\'' +
                ", shipmentId='" + shipmentId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", cusOrderNo='" + cusOrderNo + '\'' +
                ", cusWaybillNo='" + cusWaybillNo + '\'' +
                ", cusTransMode='" + cusTransMode + '\'' +
                ", isUrgent='" + isUrgent + '\'' +
                ", pickupIsAppt='" + pickupIsAppt + '\'' +
                ", orderAtt='" + orderAtt + '\'' +
                ", expectInboundDate=" + expectInboundDate +
                ", expectReceiptDate=" + expectReceiptDate +
                ", expcetShipDate=" + expcetShipDate +
                ", expectArriveDate=" + expectArriveDate +
                ", originLocationId='" + originLocationId + '\'' +
                ", originLocationName='" + originLocationName + '\'' +
                ", originLocationProvince='" + originLocationProvince + '\'' +
                ", originLocationCity='" + originLocationCity + '\'' +
                ", originLocationCounty='" + originLocationCounty + '\'' +
                ", originLocationAddress='" + originLocationAddress + '\'' +
                ", destLocationId='" + destLocationId + '\'' +
                ", destLocationName='" + destLocationName + '\'' +
                ", destLocationProvince='" + destLocationProvince + '\'' +
                ", destLocationCity='" + destLocationCity + '\'' +
                ", destLocationCounty='" + destLocationCounty + '\'' +
                ", destLocationAddress='" + destLocationAddress + '\'' +
                ", cusVehicleType='" + cusVehicleType + '\'' +
                ", vehicleDescribe='" + vehicleDescribe + '\'' +
                ", stanVehicleType='" + stanVehicleType + '\'' +
                ", vin='" + vin + '\'' +
                ", isModVehicle='" + isModVehicle + '\'' +
                ", modVehicleSize='" + modVehicleSize + '\'' +
                ", qrCode='" + qrCode + '\'' +
                ", remarks='" + remarks + '\'' +
                ", gmtModified=" + gmtModified +
                ", status=" + status +
                '}';
    }


}
