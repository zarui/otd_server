package com.zhiche.lisa.integration.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.VehicleBrand;
import com.zhiche.lisa.integration.service.IVehicleBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/vehicle_brand")
@Api(value = "/VehicleBrand", description = "OTM车辆品牌", tags = {"OTM车辆品牌"})
public class VehicleBrandController {
    public Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IVehicleBrandService vehicleBrandService;

    @PostMapping(value = "queryListVehicleBrand")
    @ApiOperation(value = "通过品牌编码查询OTM全部品牌数据", notes = "通过品牌编码查询OTM全部品牌数据", response = RestfulResponse.class)
    public RestfulResponse<List<VehicleBrand>> queryListVehicleBrand(@RequestBody Map<String, Object> condition) {
        logger.info("controller:/VehicleBrand/queryListVehicleBrand data: {}", condition);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            List<VehicleBrand> vehicleBrandList = vehicleBrandService.queryListVehicleBrand(condition);
            result.setData(vehicleBrandList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }


    @PostMapping(value = "/queryVehicleBrandPage")
    @ApiOperation(value = "通过品牌名称模糊查询/无条件查询OTM全部品牌数据分页返回", notes = "通过品牌名称模糊查询/无条件查询OTM全部品牌数据分页返回", response = RestfulResponse.class)
    public RestfulResponse<Page<VehicleBrand>> queryVehicleBrandPage(@RequestBody Page page) {
        logger.info("controller:/VehicleBrand/queryListVehicleBrandPage data: {}", page);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            Page<VehicleBrand> brandPage = vehicleBrandService.queryVehicleBrandPage(page);
            result.setData(brandPage);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());

        }
        return result;
    }
}




