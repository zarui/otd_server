package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.ExportLogHistory;

/**
 * <p>
 * 接口导出日志历史 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
public interface IExportLogHistoryService extends IService<ExportLogHistory> {

    /**
     *  获取报班推送OTM日志列表
     * @param checkinId
     * @return Object
     */
    Object getCheckinLogList(String checkinId);
}
