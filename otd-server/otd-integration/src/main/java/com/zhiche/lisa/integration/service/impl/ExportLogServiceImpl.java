package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.qiniu.storage.model.DefaultPutRet;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.qiniu.util.QiniuUtils;
import com.zhiche.lisa.integration.dao.mapper.ExportLogMapper;
import com.zhiche.lisa.integration.dao.model.ExportLog;
import com.zhiche.lisa.integration.dao.model.ExportLogHistory;
import com.zhiche.lisa.integration.service.IExportLogHistoryService;
import com.zhiche.lisa.integration.service.IExportLogService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * 接口导出日志 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@Service
public class ExportLogServiceImpl extends ServiceImpl<ExportLogMapper, ExportLog> implements IExportLogService {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private IExportLogHistoryService exportLogHistoryService;

    @Override
    public ExportLog saveExportLog(ExportLogHistory exportLogHistory) {
        Wrapper<ExportLog> ew = new EntityWrapper<>();
        ew.eq("targert_sys", exportLogHistory.getTargertSys());
        ew.eq("export_key", exportLogHistory.getExportKey());
        ew.eq("type", exportLogHistory.getType());
        ExportLog exportLog = this.selectOne(ew);
        if (Objects.nonNull(exportLog)) {
            Integer logId = exportLog.getId();
            BeanUtils.copyProperties(exportLogHistory, exportLog);
            exportLogHistory.setLogId(logId);
            exportLog.setId(logId);
            exportLog.setGmtCreate(null);
            exportLog.setGmtModified(null);
            exportLogHistoryService.insert(exportLogHistory);
            this.updateById(exportLog);
        } else {
            exportLog = new ExportLog();
            BeanUtils.copyProperties(exportLogHistory, exportLog);
            this.insert(exportLog);
            exportLogHistory.setLogId(exportLog.getId());
            exportLogHistoryService.insert(exportLogHistory);
        }
        return exportLog;
    }

    @Override
    public void saveExportLogToQiniu(ExportLogHistory elh,
                                     String dataContent) throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info("saveExportLogToQiniu.start ----> param:{}", dataContent);
        }
        if (StringUtils.isBlank(elh.getTargertSys())) {
            throw new BaseException("saveLog targetSys 不能为空");
        }
        if (StringUtils.isBlank(elh.getExportKey())) {
            throw new BaseException("saveLog exportKey 不能为空");
        }
        if (StringUtils.isBlank(elh.getType())) {
            throw new BaseException("saveLog type 不能为空");
        }
        Wrapper<ExportLog> ew = new EntityWrapper<>();
        ew.eq("targert_sys", elh.getTargertSys());
        ew.eq("export_key", elh.getExportKey());
        ew.eq("type", elh.getType());
        ExportLog exportLog = selectOne(ew);
        if (Objects.nonNull(exportLog)) {
            Integer logId = exportLog.getId();
            BeanUtils.copyProperties(elh, exportLog);
            elh.setLogId(logId);
            exportLog.setId(logId);
            exportLog.setGmtCreate(null);
            exportLog.setGmtModified(null);
            exportLogHistoryService.insert(elh);
            updateById(exportLog);
        } else {
            exportLog = new ExportLog();
            BeanUtils.copyProperties(elh, exportLog);
            insert(exportLog);
            elh.setLogId(exportLog.getId());
            exportLogHistoryService.insert(elh);
        }

        String fileName = elh.getTargertSys() + "_" + elh.getType() + "_" +
                elh.getExportKey() + "_" + elh.getLogId() + "_" + elh.getId();
        DefaultPutRet putRet = QiniuUtils.uploadString(dataContent, fileName);
        if (putRet != null) {
            String qiniuKey = putRet.key;
            elh.setDataStorageKey(qiniuKey);
            exportLog.setDataStorageKey(qiniuKey);
        }
        exportLogHistoryService.updateById(elh);
        updateById(exportLog);
    }
}
