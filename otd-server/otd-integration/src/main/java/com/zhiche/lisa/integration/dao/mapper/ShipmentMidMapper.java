package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.model.ShipmentMid;
import com.zhiche.lisa.integration.dao.model.UlcLimitionMid;
import com.zhiche.lisa.integration.dto.otd.NewOTDShipmentDTO;
import com.zhiche.lisa.integration.dto.otd.OTDShipmentDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * OTM 指令中间表 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface ShipmentMidMapper extends BaseMapper<ShipmentMid> {

    //List<OTDShipmentDTO> queryShipment(@Param("ew") EntityWrapper<UlcLimitionMid> ew);

    List<OTDShipmentDTO> selectShipmentByPage(Page<OTDShipmentDTO> page, @Param("ew") EntityWrapper<OTDShipmentDTO> ew);

    List<NewOTDShipmentDTO> selectShipmentByPageNew(Page<NewOTDShipmentDTO> page, @Param("ew") EntityWrapper<NewOTDShipmentDTO> ew);
}
