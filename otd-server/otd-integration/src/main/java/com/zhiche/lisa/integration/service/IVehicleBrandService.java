package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.VehicleBrand;

import java.util.List;
import java.util.Map;

public interface IVehicleBrandService extends IService<VehicleBrand> {


    /**
     * 根据品牌编码查询品牌数据全部返回
     */
    List<VehicleBrand> queryListVehicleBrand(Map<String, Object> brandCode);

    /**
     * 通过品牌名称模糊分页查询数据
     */
    Page<VehicleBrand> queryVehicleBrandPage(Page<VehicleBrand> page);
}
