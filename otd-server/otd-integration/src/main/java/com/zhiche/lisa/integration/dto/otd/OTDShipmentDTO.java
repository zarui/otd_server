package com.zhiche.lisa.integration.dto.otd;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OTDShipmentDTO implements Serializable {
    private String shipmentId;
    private String shipmentType;
    private String status;
    private Integer totalShipCount;
    private String trailerNo;
    private String transactionCode;
    private String transportModeId;
    private String checkinId;
    private String driverId;
    private Date expcetShipDate;
    private Date expectArriveDate;
    private String plateNo;
    private String serviceProviderId;
    private String serviceProviderName;
    private String originLocationId;
    private String originLocationName;
    private String originLocationProvince;
    private String originLocationCity;
    private String originLocationCounty;
    private String originLocationAddress;
    private String destLocationId;
    private String destLocationName;
    private String destLocationProvince;
    private String destLocationCity;
    private String destLocationCounty;
    private String destLocationAddress;
    private Date insertDate;
    private Date updateDate;
    private String excepSend;
    private String attribute19;//空放说明
    private String tariffOrganizationNumber;//原指令号
    private Date attributeDate9;//司机发运
    private Date attributeDate1;//实际发运
    private Integer attributeNumber2;
    private Integer attributeNumber1;
    private String attribute2;//司机联系方式
    private String attribute7;//司机姓名
    private Date gmtModified;
    private Date gmtCreate;

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getAttributeDate9() {
        return attributeDate9;
    }

    public void setAttributeDate9(Date attributeDate9) {
        this.attributeDate9 = attributeDate9;
    }

    public Date getAttributeDate1() {
        return attributeDate1;
    }

    public void setAttributeDate1(Date attributeDate1) {
        this.attributeDate1 = attributeDate1;
    }

    public Integer getAttributeNumber2() {
        return attributeNumber2;
    }

    public void setAttributeNumber2(Integer attributeNumber2) {
        this.attributeNumber2 = attributeNumber2;
    }

    public Integer getAttributeNumber1() {
        return attributeNumber1;
    }

    public void setAttributeNumber1(Integer attributeNumber1) {
        this.attributeNumber1 = attributeNumber1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public String getTariffOrganizationNumber() {
        return tariffOrganizationNumber;
    }

    public void setTariffOrganizationNumber(String tariffOrganizationNumber) {
        this.tariffOrganizationNumber = tariffOrganizationNumber;
    }

    public String getAttribute19() {
        return attribute19;
    }

    public void setAttribute19(String attribute19) {
        this.attribute19 = attribute19;
    }

    public String getExcepSend() {
        return excepSend;
    }

    public void setExcepSend(String excepSend) {
        this.excepSend = excepSend;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotalShipCount() {
        return totalShipCount;
    }

    public void setTotalShipCount(Integer totalShipCount) {
        this.totalShipCount = totalShipCount;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTransportModeId() {
        return transportModeId;
    }

    public void setTransportModeId(String transportModeId) {
        this.transportModeId = transportModeId;
    }

    public String getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(String checkinId) {
        this.checkinId = checkinId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Date getExpcetShipDate() {
        return expcetShipDate;
    }

    public void setExpcetShipDate(Date expcetShipDate) {
        this.expcetShipDate = expcetShipDate;
    }

    public Date getExpectArriveDate() {
        return expectArriveDate;
    }

    public void setExpectArriveDate(Date expectArriveDate) {
        this.expectArriveDate = expectArriveDate;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getOriginLocationId() {
        return originLocationId;
    }

    public void setOriginLocationId(String originLocationId) {
        this.originLocationId = originLocationId;
    }

    public String getOriginLocationName() {
        return originLocationName;
    }

    public void setOriginLocationName(String originLocationName) {
        this.originLocationName = originLocationName;
    }

    public String getOriginLocationProvince() {
        return originLocationProvince;
    }

    public void setOriginLocationProvince(String originLocationProvince) {
        this.originLocationProvince = originLocationProvince;
    }

    public String getOriginLocationCity() {
        return originLocationCity;
    }

    public void setOriginLocationCity(String originLocationCity) {
        this.originLocationCity = originLocationCity;
    }

    public String getOriginLocationCounty() {
        return originLocationCounty;
    }

    public void setOriginLocationCounty(String originLocationCounty) {
        this.originLocationCounty = originLocationCounty;
    }

    public String getOriginLocationAddress() {
        return originLocationAddress;
    }

    public void setOriginLocationAddress(String originLocationAddress) {
        this.originLocationAddress = originLocationAddress;
    }

    public String getDestLocationId() {
        return destLocationId;
    }

    public void setDestLocationId(String destLocationId) {
        this.destLocationId = destLocationId;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationAddress() {
        return destLocationAddress;
    }

    public void setDestLocationAddress(String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

}
