package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * OTM 客户数据表
 * </p>
 *
 * @author qichao
 * @since 2018-09-01
 */
@TableName("otm_corporation")
public class Corporation extends Model<Corporation> {

    private static final long serialVersionUID = 1L;

    @TableId("corporation_gid")
    private String corporationGid;
    @TableField("corporation_xid")
    private String corporationXid;
    @TableField("corporation_name")
    private String corporationName;
    @TableField("is_domain_master")
    private String isDomainMaster;
    @TableField("is_shipping_agents_active")
    private String isShippingAgentsActive;
    @TableField("is_allow_house_collect")
    private String isAllowHouseCollect;
    @TableField("max_house_collect_amt")
    private BigDecimal maxHouseCollectAmt;
    @TableField("max_house_collect_currency_gid")
    private String maxHouseCollectCurrencyGid;
    @TableField("max_house_collect_amt_base")
    private BigDecimal maxHouseCollectAmtBase;
    @TableField("pickup_routing_sequence_gid")
    private String pickupRoutingSequenceGid;
    @TableField("dropoff_routing_sequence_gid")
    private String dropoffRoutingSequenceGid;
    @TableField("domain_name")
    private String domainName;
    @TableField("insert_user")
    private String insertUser;
    @TableField("insert_date")
    private Date insertDate;
    @TableField("update_user")
    private String updateUser;
    @TableField("update_date")
    private Date updateDate;
    @TableField("gmt_create")
    private Date gmtCreate;
    @TableField("gmt_modified")
    private Date gmtModified;

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getCorporationGid() {
        return corporationGid;
    }

    public void setCorporationGid(String corporationGid) {
        this.corporationGid = corporationGid;
    }

    public String getCorporationXid() {
        return corporationXid;
    }

    public void setCorporationXid(String corporationXid) {
        this.corporationXid = corporationXid;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public String getIsDomainMaster() {
        return isDomainMaster;
    }

    public void setIsDomainMaster(String isDomainMaster) {
        this.isDomainMaster = isDomainMaster;
    }

    public String getIsShippingAgentsActive() {
        return isShippingAgentsActive;
    }

    public void setIsShippingAgentsActive(String isShippingAgentsActive) {
        this.isShippingAgentsActive = isShippingAgentsActive;
    }

    public String getIsAllowHouseCollect() {
        return isAllowHouseCollect;
    }

    public void setIsAllowHouseCollect(String isAllowHouseCollect) {
        this.isAllowHouseCollect = isAllowHouseCollect;
    }

    public BigDecimal getMaxHouseCollectAmt() {
        return maxHouseCollectAmt;
    }

    public void setMaxHouseCollectAmt(BigDecimal maxHouseCollectAmt) {
        this.maxHouseCollectAmt = maxHouseCollectAmt;
    }

    public String getMaxHouseCollectCurrencyGid() {
        return maxHouseCollectCurrencyGid;
    }

    public void setMaxHouseCollectCurrencyGid(String maxHouseCollectCurrencyGid) {
        this.maxHouseCollectCurrencyGid = maxHouseCollectCurrencyGid;
    }

    public BigDecimal getMaxHouseCollectAmtBase() {
        return maxHouseCollectAmtBase;
    }

    public void setMaxHouseCollectAmtBase(BigDecimal maxHouseCollectAmtBase) {
        this.maxHouseCollectAmtBase = maxHouseCollectAmtBase;
    }

    public String getPickupRoutingSequenceGid() {
        return pickupRoutingSequenceGid;
    }

    public void setPickupRoutingSequenceGid(String pickupRoutingSequenceGid) {
        this.pickupRoutingSequenceGid = pickupRoutingSequenceGid;
    }

    public String getDropoffRoutingSequenceGid() {
        return dropoffRoutingSequenceGid;
    }

    public void setDropoffRoutingSequenceGid(String dropoffRoutingSequenceGid) {
        this.dropoffRoutingSequenceGid = dropoffRoutingSequenceGid;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.corporationGid;
    }

    @Override
    public String toString() {
        return "Corporation{" +
                ", corporationGid=" + corporationGid +
                ", corporationXid=" + corporationXid +
                ", corporationName=" + corporationName +
                ", isDomainMaster=" + isDomainMaster +
                ", isShippingAgentsActive=" + isShippingAgentsActive +
                ", isAllowHouseCollect=" + isAllowHouseCollect +
                ", maxHouseCollectAmt=" + maxHouseCollectAmt +
                ", maxHouseCollectCurrencyGid=" + maxHouseCollectCurrencyGid +
                ", maxHouseCollectAmtBase=" + maxHouseCollectAmtBase +
                ", pickupRoutingSequenceGid=" + pickupRoutingSequenceGid +
                ", dropoffRoutingSequenceGid=" + dropoffRoutingSequenceGid +
                ", domainName=" + domainName +
                ", insertUser=" + insertUser +
                ", insertDate=" + insertDate +
                ", updateUser=" + updateUser +
                ", updateDate=" + updateDate +
                "}";
    }
}
