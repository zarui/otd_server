package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.ShipmentMid;
import com.zhiche.lisa.integration.dto.otd.NewOTDShipmentDTO;
import com.zhiche.lisa.integration.dto.otd.OTDShipmentDTO;

/**
 * <p>
 * OTM 指令中间表 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface ShipmentMidService extends IService<ShipmentMid> {

    Page<OTDShipmentDTO> queryShipment(Page<OTDShipmentDTO> page);

    Page<NewOTDShipmentDTO> queryShipmentNew(Page<NewOTDShipmentDTO> page);
}
