package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单信息中间表
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
@TableName("v_order_mid")
public class NewOrderMid extends Model<NewOrderMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统单号
     */
    @TableId("order_gid")
    private String orderGid;
    /**
     * 下单时间
     */
    @TableField("order_time")
    private Date orderTime;
    /**
     * 业务主体
     */
    private String ciams;
    /**
     * 客户
     */
    private String customer;
    /**
     * 客户订单号
     */
    @TableField("cus_order_no")
    private String cusOrderNo;
    /**
     * 客户运单号
     */
    @TableField("cus_ship_no")
    private String cusShipNo;
    /**
     * 打单时间
     */
    @TableField("cus_print_time")
    private Date cusPrintTime;
    /**
     * 订单类型
     */
    @TableField("order_type")
    private String orderType;
    /**
     * 应收里程
     */
    @TableField("ar_miles")
    private BigDecimal totalMileages;
    /**
     * 域
     */
    @TableField("domain_name")
    private String domainName;
    /**
     * 品牌
     */
    private String brand;
    /**
     * 标准车型（订单车辆车型）
     */
    @TableField("std_vechile")
    private String cusVehicle;
    /**
     * VIN码
     */
    private String vin;
    /**
     * 主运输模式(带域名)
     */
    @TableField("m_transport_mode")
    private String mTransportMode;
    /**
     * 目的地经销商
     */
    @TableField("dl_att9")
    private String distributor;
    /**
     * 目的地地址
     */
    @TableField("dl_desc")
    private String destLocationAddress;
    /**
     * 目的地区县
     */
    @TableField("dl_zone3")
    private String destLocationCounty;
    /**
     * 目的地城市
     */
    @TableField("dl_zone2")
    private String destLocationCity;
    /**
     * 目的地省份
     */
    @TableField("dl_zone1")
    private String destLocationProvince;
    /**
     * 目的地名称
     */
    @TableField("dl_loc")
    private String destLocationName;
    /**
     * 目的地编码
     */
    @TableField("dest_location")
    private String destLocationGid;
    /**
     * 起运地地址
     */
    @TableField("sl_desc")
    private String sourceLocationAddress;
    /**
     * 起运地区县
     */
    @TableField("sl_zone3")
    private String sourceLocationCounty;
    /**
     * 起运地城市
     */
    @TableField("sl_zone2")
    private String sourceLocationCity;
    /**
     * 起运地省份
     */
    @TableField("sl_zone1")
    private String sourceLocationProvince;
    /**
     * 起运地名称
     */
    @TableField("sl_loc")
    private String sourceLocationName;
    /**
     * 起运地编码
     */
    @TableField("source_location")
    private String sourceLocationGid;
    /**
     * 要求抵达时间
     */
    private Date planArrivalTime;
    /**
     * 要求发运时间
     */
    private Date planDepartTime;
    /**
     * 当前节点
     */
    @TableField("current_point")
    private String currentPoint;
    /**
     * 订单状态
     */
    @TableField("order_status")
    private String orderStatus;
    /**
     * 急发
     */
    @TableField("urgency_order")
    private Integer urgencyOrder;
    /**
     * 数据-插入时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 数据-更新时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 抵运运输车辆车牌号
     */
    @TableField("attribute12")
    private String arrivalTransportLicensePlateNumber;
    /**
     * 实际交单时间
     */
    @TableField("a_submit_date")
    private Date realSubmitDate;
    /**
     * 实际回单时间
     */
    @TableField("a_pod_date")
    private Date realPodDate;
    /**
     * 路由模式ID
     */
    @TableField("route_mode")
    private String routeModeGid;
    /**
     * 路由模式名称
     */
    @TableField("route_mode_name")
    private String routeModeName;

    private String remark;

    /**
     * 车型名称
     */
    @TableField("vehicle_model_name")
    private String vehicleModelName;

    /**
     * 目的地联系人电话
     */
    @TableField("dl_att8")
    private String dest_phone;

    /**
     * 目的地联系人电话
     */
    @TableField("dl_att7")
    private String dest_name;

    public String getDest_name () {
        return dest_name;
    }

    public void setDest_name (String dest_name) {
        this.dest_name = dest_name;
    }

    public String getOrderGid() {
        return orderGid;
    }

    public void setOrderGid(String orderGid) {
        this.orderGid = orderGid;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getCiams() {
        return ciams;
    }

    public void setCiams(String ciams) {
        this.ciams = ciams;
    }

    public String getDest_phone () {
        return dest_phone;
    }

    public void setDest_phone (String dest_phone) {
        this.dest_phone = dest_phone;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCusOrderNo() {
        return cusOrderNo;
    }

    public void setCusOrderNo(String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusShipNo() {
        return cusShipNo;
    }

    public void setCusShipNo(String cusShipNo) {
        this.cusShipNo = cusShipNo;
    }

    public Date getCusPrintTime() {
        return cusPrintTime;
    }

    public void setCusPrintTime(Date cusPrintTime) {
        this.cusPrintTime = cusPrintTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public BigDecimal getTotalMileages() {
        return totalMileages;
    }

    public void setTotalMileages(BigDecimal totalMileages) {
        this.totalMileages = totalMileages;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCusVehicle() {
        return cusVehicle;
    }

    public void setCusVehicle(String cusVehicle) {
        this.cusVehicle = cusVehicle;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getmTransportMode() {
        return mTransportMode;
    }

    public void setmTransportMode(String mTransportMode) {
        this.mTransportMode = mTransportMode;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getDestLocationAddress() {
        return destLocationAddress;
    }

    public void setDestLocationAddress(String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getDestLocationCounty() {
        return destLocationCounty;
    }

    public void setDestLocationCounty(String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationCity() {
        return destLocationCity;
    }

    public void setDestLocationCity(String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationProvince() {
        return destLocationProvince;
    }

    public void setDestLocationProvince(String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationGid() {
        return destLocationGid;
    }

    public void setDestLocationGid(String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getSourceLocationAddress() {
        return sourceLocationAddress;
    }

    public void setSourceLocationAddress(String sourceLocationAddress) {
        this.sourceLocationAddress = sourceLocationAddress;
    }

    public String getSourceLocationCounty() {
        return sourceLocationCounty;
    }

    public void setSourceLocationCounty(String sourceLocationCounty) {
        this.sourceLocationCounty = sourceLocationCounty;
    }

    public String getSourceLocationCity() {
        return sourceLocationCity;
    }

    public void setSourceLocationCity(String sourceLocationCity) {
        this.sourceLocationCity = sourceLocationCity;
    }

    public String getSourceLocationProvince() {
        return sourceLocationProvince;
    }

    public void setSourceLocationProvince(String sourceLocationProvince) {
        this.sourceLocationProvince = sourceLocationProvince;
    }

    public String getSourceLocationName() {
        return sourceLocationName;
    }

    public void setSourceLocationName(String sourceLocationName) {
        this.sourceLocationName = sourceLocationName;
    }

    public String getSourceLocationGid() {
        return sourceLocationGid;
    }

    public void setSourceLocationGid(String sourceLocationGid) {
        this.sourceLocationGid = sourceLocationGid;
    }

    public Date getPlanArrivalTime() {
        return planArrivalTime;
    }

    public void setPlanArrivalTime(Date planArrivalTime) {
        this.planArrivalTime = planArrivalTime;
    }

    public Date getPlanDepartTime() {
        return planDepartTime;
    }

    public void setPlanDepartTime(Date planDepartTime) {
        this.planDepartTime = planDepartTime;
    }

    public String getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(String currentPoint) {
        this.currentPoint = currentPoint;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getUrgencyOrder() {
        return urgencyOrder;
    }

    public void setUrgencyOrder(Integer urgencyOrder) {
        this.urgencyOrder = urgencyOrder;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getArrivalTransportLicensePlateNumber() {
        return arrivalTransportLicensePlateNumber;
    }

    public void setArrivalTransportLicensePlateNumber(String arrivalTransportLicensePlateNumber) {
        this.arrivalTransportLicensePlateNumber = arrivalTransportLicensePlateNumber;
    }

    public Date getRealSubmitDate() {
        return realSubmitDate;
    }

    public void setRealSubmitDate(Date realSubmitDate) {
        this.realSubmitDate = realSubmitDate;
    }

    public Date getRealPodDate() {
        return realPodDate;
    }

    public void setRealPodDate(Date realPodDate) {
        this.realPodDate = realPodDate;
    }

    public String getRouteModeGid() {
        return routeModeGid;
    }

    public void setRouteModeGid(String routeModeGid) {
        this.routeModeGid = routeModeGid;
    }

    public String getRouteModeName() {
        return routeModeName;
    }

    public void setRouteModeName(String routeModeName) {
        this.routeModeName = routeModeName;
    }

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getVehicleModelName () {
        return vehicleModelName;
    }

    public void setVehicleModelName (String vehicleModelName) {
        this.vehicleModelName = vehicleModelName;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderGid;
    }

}
