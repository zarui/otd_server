package com.zhiche.lisa.integration.dto.order;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.zhiche.lisa.integration.anno.FiledAnno;

import java.util.Date;

/**
 * Created by lenovo on 2018/6/13.
 */
public class ErpShipLine {

    /**
     * 应付价格
     */
    private Double dcpaycost;

    /**
     * 应付金额类型：0、按照ERP流程；1、由慧运车传来；2、慧运车手动修改
     */
    private Double ipaycosttype;

    /**
     * 修改应付金额日期
     */
    private Date dtreceipts;

    /**
     * 主键序号
     */
    @FiledAnno("releaseId")
    private String ilineid;

    /**
     * 发运主表序号
     */
    @FiledAnno("shipmentId")
    private String ishipid;

    /**
     * 起运地ID
     */
    @FiledAnno("originLocationId")
    private String istartcityid;

    /**
     * 起运地城市名称
     */
    @FiledAnno("originLocationCity")
    private String vcstartcityname;

    /**
     * 状态标志(0:安排，5:装车,10:离城,15:运抵)
     */
    private Integer iflag;

    /**
     * 装货时间
     */
    private Date dtstartdate;

    /**
     * 装货确定人
     */
    private String vcstartuserno;

    /**
     * 离城时间
     */
    private Date dtoutdate;

    /**
     * 发运确定人
     */
    private String vcoutuserno;

    /**
     * 目的地城市ID
     */
    @FiledAnno("destLocationGid")
    private String iendcityid;

    /**
     * 目的地城市名称
     */
    @FiledAnno("destLocationCity")
    private String vcendcityname;

    /**
     * 抵达时间
     */
    private Date dtcomedate;

    /**
     * 运抵确定人
     */
    private String vccomeuserno;

    /**
     * 回单备注
     */
    private String vcreturnmemo;

    /**
     * 回单类型(1:原始运单,0:白条）
     */
    private Integer ireturnflag;

    /**
     * 应付公里
     */
    private Double dcapkilometer;

    /**
     * 应收公里(应收公里数)
     */
    private Double dcarkilometer;

    /**
     * 顺序号(顺序号与异地搭载公里有关)
     */
    private Double iflowid;

    /**
     * 装运数量
     */
    private Double dcshipqty;

    /**
     * 应收结算数量
     */
    private Double dcqty;

    /**
     * 订单序号
     */
    private Double iorderid;

    /**
     * 运单mail标志(0:未发，1：已发，2：已收)
     */
    private Double bshipmail;

    /**
     * 运单mail时间
     */
    private Date dtshipmail;

    /**
     * mail 用户
     */
    private String vcshipmailuserno;

    /**
     * 邮寄说明
     */
    private String vcmailmemo;

    /**
     * 快递公司
     */
    private String vcmailcompany;

    /**
     * 快递单号
     */
    private String vcmailno;

    /**
     * 运单mail时间2
     */
    private Date dtgetshipmail;

    /**
     * mail get用户
     */
    private String vcgetshipuserno;

    /**
     * mail get用户2
     */
    private String vcgetshipuserno2;

    /**
     * 说明
     */
    private String vcgetmailmemo;

    /**
     * 代收现金金额
     */
    private Double dcpay;

    /**
     * 已收现金金额
     */
    private Double dcpayed;

    /**
     * 未收现金备注
     */
    private String vcnopaydesc;

    /**
     * 起运地序号
     */
    private Double istartcityid2;

    /**
     * 司机的起运地
     */
    private String vcstartcityname2;

    /**
     * 司机目的地序号
     */
    private Double iendcityid2;

    /**
     * 司机目的地名称
     */
    private String vcendcityname2;

    /**
     * 父类订单序号
     */
    private Double iorderid2;

    /**
     * 生成时间
     */
    private Date dtcreatedate;

    /**
     * 已维护客户订单数量
     */
    private Double dccustqty;

    /**
     * 已收现金备注
     */
    private String vccashmemo;

    /**
     * 订单应付公里
     */
    private Double dcapkilometer2;

    /**
     * 司机支付现金金额
     */
    private Double dcfee;

    /**
     * 司机支付现金方式
     */
    private Double bcash;

    /**
     * 现金订单司机收款
     */
    private Double dccash;

    /**
     * 做抵达的系统时间
     */
    private Date dtcomedate2;

    /**
     * 导入安联系统状态(0:未导入;1:已导入)
     */
    private Double bimp;

    /**
     * 操作抵达时间
     */
    private Date dtoutdate2;

    /**
     * 生成时间2
     */
    private Date dtdate;

    /**
     * 打款邮件发送标志
     */
    private Double bsendemail;

    /**
     * 装车操作时系统时间
     */
    private Date dtstartdate2;

    /**
     * OTD人员记录的认为正确发运时间
     */
    private Date dtoutOtd;

    /**
     * ${field.comment}
     */
    private Double izlshipid;

    /**
     * 1.下一个指令发运时自动运抵上个指令;2.GPS自动运抵;3.指令明细回单自动运抵;6.COMPASS交付接口运抵;7.知车APP中转库入库自动运抵
     */
    private Integer icomeType;

    /**
     * erp 系统订单号
     */
    private String orderno;

    /**
     * 客户订单号
     */
    @FiledAnno("cusOrderNo")
    private String custOrderNo;

    /**
     * 货物型号 id
     */
    private Long istyleid;

    /**
     * 货物型号说明
     */
    @FiledAnno("cusVehicleType")
    private String vcstyleno;

    /**
     * 订单明细 id
     */
    private Long orderItemId;

    /**
     * 车架号
     */
    @FiledAnno("vin")
    private String vcvin;

    public Double getDcpaycost() {
        return dcpaycost;
    }

    public void setDcpaycost(Double dcpaycost) {
        this.dcpaycost = dcpaycost;
    }

    public Double getIpaycosttype() {
        return ipaycosttype;
    }

    public void setIpaycosttype(Double ipaycosttype) {
        this.ipaycosttype = ipaycosttype;
    }

    public Date getDtreceipts() {
        return dtreceipts;
    }

    public void setDtreceipts(Date dtreceipts) {
        this.dtreceipts = dtreceipts;
    }

    public String getIlineid() {
        return ilineid;
    }

    public void setIlineid(String ilineid) {
        this.ilineid = ilineid;
    }

    public String getIshipid() {
        return ishipid;
    }

    public void setIshipid(String ishipid) {
        this.ishipid = ishipid;
    }

    public String getIstartcityid() {
        return istartcityid;
    }

    public void setIstartcityid(String istartcityid) {
        this.istartcityid = istartcityid;
    }

    public String getVcstartcityname() {
        return vcstartcityname;
    }

    public void setVcstartcityname(String vcstartcityname) {
        this.vcstartcityname = vcstartcityname;
    }

    public Integer getIflag() {
        return iflag;
    }

    public void setIflag(Integer iflag) {
        this.iflag = iflag;
    }

    public Date getDtstartdate() {
        return dtstartdate;
    }

    public void setDtstartdate(Date dtstartdate) {
        this.dtstartdate = dtstartdate;
    }

    public String getVcstartuserno() {
        return vcstartuserno;
    }

    public void setVcstartuserno(String vcstartuserno) {
        this.vcstartuserno = vcstartuserno;
    }

    public Date getDtoutdate() {
        return dtoutdate;
    }

    public void setDtoutdate(Date dtoutdate) {
        this.dtoutdate = dtoutdate;
    }

    public String getVcoutuserno() {
        return vcoutuserno;
    }

    public void setVcoutuserno(String vcoutuserno) {
        this.vcoutuserno = vcoutuserno;
    }

    public String getIendcityid() {
        return iendcityid;
    }

    public void setIendcityid(String iendcityid) {
        this.iendcityid = iendcityid;
    }

    public String getVcendcityname() {
        return vcendcityname;
    }

    public void setVcendcityname(String vcendcityname) {
        this.vcendcityname = vcendcityname;
    }

    public Date getDtcomedate() {
        return dtcomedate;
    }

    public void setDtcomedate(Date dtcomedate) {
        this.dtcomedate = dtcomedate;
    }

    public String getVccomeuserno() {
        return vccomeuserno;
    }

    public void setVccomeuserno(String vccomeuserno) {
        this.vccomeuserno = vccomeuserno;
    }

    public String getVcreturnmemo() {
        return vcreturnmemo;
    }

    public void setVcreturnmemo(String vcreturnmemo) {
        this.vcreturnmemo = vcreturnmemo;
    }

    public Integer getIreturnflag() {
        return ireturnflag;
    }

    public void setIreturnflag(Integer ireturnflag) {
        this.ireturnflag = ireturnflag;
    }

    public Double getDcapkilometer() {
        return dcapkilometer;
    }

    public void setDcapkilometer(Double dcapkilometer) {
        this.dcapkilometer = dcapkilometer;
    }

    public Double getDcarkilometer() {
        return dcarkilometer;
    }

    public void setDcarkilometer(Double dcarkilometer) {
        this.dcarkilometer = dcarkilometer;
    }

    public Double getIflowid() {
        return iflowid;
    }

    public void setIflowid(Double iflowid) {
        this.iflowid = iflowid;
    }

    public Double getDcshipqty() {
        return dcshipqty;
    }

    public void setDcshipqty(Double dcshipqty) {
        this.dcshipqty = dcshipqty;
    }

    public Double getDcqty() {
        return dcqty;
    }

    public void setDcqty(Double dcqty) {
        this.dcqty = dcqty;
    }

    public Double getIorderid() {
        return iorderid;
    }

    public void setIorderid(Double iorderid) {
        this.iorderid = iorderid;
    }

    public Double getBshipmail() {
        return bshipmail;
    }

    public void setBshipmail(Double bshipmail) {
        this.bshipmail = bshipmail;
    }

    public Date getDtshipmail() {
        return dtshipmail;
    }

    public void setDtshipmail(Date dtshipmail) {
        this.dtshipmail = dtshipmail;
    }

    public String getVcshipmailuserno() {
        return vcshipmailuserno;
    }

    public void setVcshipmailuserno(String vcshipmailuserno) {
        this.vcshipmailuserno = vcshipmailuserno;
    }

    public String getVcmailmemo() {
        return vcmailmemo;
    }

    public void setVcmailmemo(String vcmailmemo) {
        this.vcmailmemo = vcmailmemo;
    }

    public String getVcmailcompany() {
        return vcmailcompany;
    }

    public void setVcmailcompany(String vcmailcompany) {
        this.vcmailcompany = vcmailcompany;
    }

    public String getVcmailno() {
        return vcmailno;
    }

    public void setVcmailno(String vcmailno) {
        this.vcmailno = vcmailno;
    }

    public Date getDtgetshipmail() {
        return dtgetshipmail;
    }

    public void setDtgetshipmail(Date dtgetshipmail) {
        this.dtgetshipmail = dtgetshipmail;
    }

    public String getVcgetshipuserno() {
        return vcgetshipuserno;
    }

    public void setVcgetshipuserno(String vcgetshipuserno) {
        this.vcgetshipuserno = vcgetshipuserno;
    }

    public String getVcgetshipuserno2() {
        return vcgetshipuserno2;
    }

    public void setVcgetshipuserno2(String vcgetshipuserno2) {
        this.vcgetshipuserno2 = vcgetshipuserno2;
    }

    public String getVcgetmailmemo() {
        return vcgetmailmemo;
    }

    public void setVcgetmailmemo(String vcgetmailmemo) {
        this.vcgetmailmemo = vcgetmailmemo;
    }

    public Double getDcpay() {
        return dcpay;
    }

    public void setDcpay(Double dcpay) {
        this.dcpay = dcpay;
    }

    public Double getDcpayed() {
        return dcpayed;
    }

    public void setDcpayed(Double dcpayed) {
        this.dcpayed = dcpayed;
    }

    public String getVcnopaydesc() {
        return vcnopaydesc;
    }

    public void setVcnopaydesc(String vcnopaydesc) {
        this.vcnopaydesc = vcnopaydesc;
    }

    public Double getIstartcityid2() {
        return istartcityid2;
    }

    public void setIstartcityid2(Double istartcityid2) {
        this.istartcityid2 = istartcityid2;
    }

    public String getVcstartcityname2() {
        return vcstartcityname2;
    }

    public void setVcstartcityname2(String vcstartcityname2) {
        this.vcstartcityname2 = vcstartcityname2;
    }

    public Double getIendcityid2() {
        return iendcityid2;
    }

    public void setIendcityid2(Double iendcityid2) {
        this.iendcityid2 = iendcityid2;
    }

    public String getVcendcityname2() {
        return vcendcityname2;
    }

    public void setVcendcityname2(String vcendcityname2) {
        this.vcendcityname2 = vcendcityname2;
    }

    public Double getIorderid2() {
        return iorderid2;
    }

    public void setIorderid2(Double iorderid2) {
        this.iorderid2 = iorderid2;
    }

    public Date getDtcreatedate() {
        return dtcreatedate;
    }

    public void setDtcreatedate(Date dtcreatedate) {
        this.dtcreatedate = dtcreatedate;
    }

    public Double getDccustqty() {
        return dccustqty;
    }

    public void setDccustqty(Double dccustqty) {
        this.dccustqty = dccustqty;
    }

    public String getVccashmemo() {
        return vccashmemo;
    }

    public void setVccashmemo(String vccashmemo) {
        this.vccashmemo = vccashmemo;
    }

    public Double getDcapkilometer2() {
        return dcapkilometer2;
    }

    public void setDcapkilometer2(Double dcapkilometer2) {
        this.dcapkilometer2 = dcapkilometer2;
    }

    public Double getDcfee() {
        return dcfee;
    }

    public void setDcfee(Double dcfee) {
        this.dcfee = dcfee;
    }

    public Double getBcash() {
        return bcash;
    }

    public void setBcash(Double bcash) {
        this.bcash = bcash;
    }

    public Double getDccash() {
        return dccash;
    }

    public void setDccash(Double dccash) {
        this.dccash = dccash;
    }

    public Date getDtcomedate2() {
        return dtcomedate2;
    }

    public void setDtcomedate2(Date dtcomedate2) {
        this.dtcomedate2 = dtcomedate2;
    }

    public Double getBimp() {
        return bimp;
    }

    public void setBimp(Double bimp) {
        this.bimp = bimp;
    }

    public Date getDtoutdate2() {
        return dtoutdate2;
    }

    public void setDtoutdate2(Date dtoutdate2) {
        this.dtoutdate2 = dtoutdate2;
    }

    public Date getDtdate() {
        return dtdate;
    }

    public void setDtdate(Date dtdate) {
        this.dtdate = dtdate;
    }

    public Double getBsendemail() {
        return bsendemail;
    }

    public void setBsendemail(Double bsendemail) {
        this.bsendemail = bsendemail;
    }

    public Date getDtstartdate2() {
        return dtstartdate2;
    }

    public void setDtstartdate2(Date dtstartdate2) {
        this.dtstartdate2 = dtstartdate2;
    }

    public Date getDtoutOtd() {
        return dtoutOtd;
    }

    public void setDtoutOtd(Date dtoutOtd) {
        this.dtoutOtd = dtoutOtd;
    }

    public Double getIzlshipid() {
        return izlshipid;
    }

    public void setIzlshipid(Double izlshipid) {
        this.izlshipid = izlshipid;
    }

    public Integer getIcomeType() {
        return icomeType;
    }

    public void setIcomeType(Integer icomeType) {
        this.icomeType = icomeType;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCustOrderNo() {
        return custOrderNo;
    }

    public void setCustOrderNo(String custOrderNo) {
        this.custOrderNo = custOrderNo;
    }

    public Long getIstyleid() {
        return istyleid;
    }

    public void setIstyleid(Long istyleid) {
        this.istyleid = istyleid;
    }

    public String getVcstyleno() {
        return vcstyleno;
    }

    public void setVcstyleno(String vcstyleno) {
        this.vcstyleno = vcstyleno;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    @Override
    public String toString() {
        return "ErpShipLine{" +
                "dcpaycost=" + dcpaycost +
                ", ipaycosttype=" + ipaycosttype +
                ", dtreceipts=" + dtreceipts +
                ", ilineid=" + ilineid +
                ", ishipid=" + ishipid +
                ", istartcityid=" + istartcityid +
                ", vcstartcityname='" + vcstartcityname + '\'' +
                ", iflag=" + iflag +
                ", dtstartdate=" + dtstartdate +
                ", vcstartuserno='" + vcstartuserno + '\'' +
                ", dtoutdate=" + dtoutdate +
                ", vcoutuserno='" + vcoutuserno + '\'' +
                ", iendcityid=" + iendcityid +
                ", vcendcityname='" + vcendcityname + '\'' +
                ", dtcomedate=" + dtcomedate +
                ", vccomeuserno='" + vccomeuserno + '\'' +
                ", vcreturnmemo='" + vcreturnmemo + '\'' +
                ", ireturnflag=" + ireturnflag +
                ", dcapkilometer=" + dcapkilometer +
                ", dcarkilometer=" + dcarkilometer +
                ", iflowid=" + iflowid +
                ", dcshipqty=" + dcshipqty +
                ", dcqty=" + dcqty +
                ", iorderid=" + iorderid +
                ", bshipmail=" + bshipmail +
                ", dtshipmail=" + dtshipmail +
                ", vcshipmailuserno='" + vcshipmailuserno + '\'' +
                ", vcmailmemo='" + vcmailmemo + '\'' +
                ", vcmailcompany='" + vcmailcompany + '\'' +
                ", vcmailno='" + vcmailno + '\'' +
                ", dtgetshipmail=" + dtgetshipmail +
                ", vcgetshipuserno='" + vcgetshipuserno + '\'' +
                ", vcgetshipuserno2='" + vcgetshipuserno2 + '\'' +
                ", vcgetmailmemo='" + vcgetmailmemo + '\'' +
                ", dcpay=" + dcpay +
                ", dcpayed=" + dcpayed +
                ", vcnopaydesc='" + vcnopaydesc + '\'' +
                ", istartcityid2=" + istartcityid2 +
                ", vcstartcityname2='" + vcstartcityname2 + '\'' +
                ", iendcityid2=" + iendcityid2 +
                ", vcendcityname2='" + vcendcityname2 + '\'' +
                ", iorderid2=" + iorderid2 +
                ", dtcreatedate=" + dtcreatedate +
                ", dccustqty=" + dccustqty +
                ", vccashmemo='" + vccashmemo + '\'' +
                ", dcapkilometer2=" + dcapkilometer2 +
                ", dcfee=" + dcfee +
                ", bcash=" + bcash +
                ", dccash=" + dccash +
                ", dtcomedate2=" + dtcomedate2 +
                ", bimp=" + bimp +
                ", dtoutdate2=" + dtoutdate2 +
                ", dtdate=" + dtdate +
                ", bsendemail=" + bsendemail +
                ", dtstartdate2=" + dtstartdate2 +
                ", dtoutOtd=" + dtoutOtd +
                ", izlshipid=" + izlshipid +
                ", icomeType=" + icomeType +
                ", orderno='" + orderno + '\'' +
                ", custOrderNo='" + custOrderNo + '\'' +
                ", istyleid=" + istyleid +
                ", vcstyleno='" + vcstyleno + '\'' +
                ", orderItemId=" + orderItemId +
                '}';
    }

    public String getVcvin() {
        return vcvin;
    }

    public void setVcvin(String vcvin) {
        this.vcvin = vcvin;
    }
}
