package com.zhiche.lisa.integration.dto.carrier;

import com.zhiche.lisa.integration.anno.ClassAnno;
import com.zhiche.lisa.integration.anno.FiledAnno;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
@ClassAnno("classpath:otmxml/carrier.xml")
public class VehicleDTO {

    /**
     * 数据处理方式(I:新增,U:更新,D:删除,IU:新增或更新)
     */
    @FiledAnno("${transactionCode}")
    private String transactionCode;
    /**
     * 回调地址
     */
    @FiledAnno("${callBackUrl}")
    private String callBackUrl;

    /**
     * 牵引车ID
     */
    private Long id;
    /**
     * 车牌号
     */
    @FiledAnno("${plate}")
    private String plate;
    /**
     * 车主姓名
     */
    private String ownerName;
    /**
     * 车主电话
     */
    private String ownerMobile;
    /**
     * 业务车牌
     */
    private String plateAnno;
    /**
     * 生产厂商
     */
    private String manufacturer;
    /**
     * 长
     */
    private Float length;
    /**
     * 宽
     */
    private Float width;
    /**
     * 高
     */
    private Float height;
    /**
     * 出厂日期
     */
    private Date gmtManu;
    /**
     * 上次年检日期
     */
    private Date gmtAs;
    /**
     * 品牌型号
     */
    private String model;
    /**
     * 轴数
     */
    private Integer axisNumber;
    /**
     * 已行驶里程
     */
    private BigDecimal travelledDist;
    /**
     * 空载油耗最小值
     */
    private Integer minNl;
    /**
     * 空载油耗最大值
     */
    private Integer maxNl;
    /**
     * 重载油耗最小值
     */
    private Integer minFl;
    /**
     * 重载油耗最大值
     */
    private Integer maxFl;
    /**
     * 自重
     */
    private Integer selfWeight;
    /**
     * 所属承运商
     */
    @FiledAnno("${lspId}")
    private Long lspId;
    /**
     * 所属车队
     */
    private Long fleetId;
    /**
     * 业务状态：0-停用;1-启用
     */
    private Integer status;
    /**
     * 分供方状态(启用:Y，其他:N，回传OTM使用)
     */
    @FiledAnno("${IsActive}")
    private String isActive;
    /**
     * 认证状态：0-未认证;1-已认证
     */
    private Integer authStatus;
    /**
     * 注册人
     */
    private String creator;
    /**
     * 注册时间
     */
    private Date gmtCreate;
    /**
     * 数据来源
     */
    private String createFrom;
    /**
     * 修改时间
     */
    private Date gmtUpdate;
    /**
     * 挂车ID
     */
    private Long trailerId;
    /**
     * 挂车车牌号
     */
    @FiledAnno("${trailerPlate}")
    private String trailerPlate;
    /**
     * 挂车类型ID
     */
    private Long trailerTypeId;
    /**
     * 挂车类型名称
     */
    @FiledAnno("${trailerTypeName}")
    private String trailerTypeName;
    /**
     * 司机ID
     */
    @FiledAnno("${driverId}")
    private Long driverId;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 身份证号
     */
    @FiledAnno("${idCard}")
    private String idCard;
    /**
     * 司机电话
     */
    @FiledAnno("${driverMobile}")
    private String driverMobile;
    /**
     * 承运商名称
     */
    private String lspName;

    @FiledAnno("${vehicleType}")
    private String fleetTypeId;//是否自有车辆标识 y n

    @FiledAnno("${vehicleProper}")
    private String vehicleProper;

    public String getFleetTypeId() {
        return fleetTypeId;
    }

    public String getVehicleProper() {
        return vehicleProper;
    }

    public void setVehicleProper(String vehicleProper) {
        this.vehicleProper = vehicleProper;
    }

    public void setFleetTypeId(String fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerMobile() {
        return ownerMobile;
    }

    public void setOwnerMobile(String ownerMobile) {
        this.ownerMobile = ownerMobile;
    }

    public String getPlateAnno() {
        return plateAnno;
    }

    public void setPlateAnno(String plateAnno) {
        this.plateAnno = plateAnno;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Date getGmtManu() {
        return gmtManu;
    }

    public void setGmtManu(Date gmtManu) {
        this.gmtManu = gmtManu;
    }

    public Date getGmtAs() {
        return gmtAs;
    }

    public void setGmtAs(Date gmtAs) {
        this.gmtAs = gmtAs;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getAxisNumber() {
        return axisNumber;
    }

    public void setAxisNumber(Integer axisNumber) {
        this.axisNumber = axisNumber;
    }

    public BigDecimal getTravelledDist() {
        return travelledDist;
    }

    public void setTravelledDist(BigDecimal travelledDist) {
        this.travelledDist = travelledDist;
    }

    public Integer getMinNl() {
        return minNl;
    }

    public void setMinNl(Integer minNl) {
        this.minNl = minNl;
    }

    public Integer getMaxNl() {
        return maxNl;
    }

    public void setMaxNl(Integer maxNl) {
        this.maxNl = maxNl;
    }

    public Integer getMinFl() {
        return minFl;
    }

    public void setMinFl(Integer minFl) {
        this.minFl = minFl;
    }

    public Integer getMaxFl() {
        return maxFl;
    }

    public void setMaxFl(Integer maxFl) {
        this.maxFl = maxFl;
    }

    public Integer getSelfWeight() {
        return selfWeight;
    }

    public void setSelfWeight(Integer selfWeight) {
        this.selfWeight = selfWeight;
    }

    public Long getLspId() {
        return lspId;
    }

    public void setLspId(Long lspId) {
        this.lspId = lspId;
    }

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(Integer authStatus) {
        this.authStatus = authStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getCreateFrom() {
        return createFrom;
    }

    public void setCreateFrom(String createFrom) {
        this.createFrom = createFrom;
    }

    public Date getGmtUpdate() {
        return gmtUpdate;
    }

    public void setGmtUpdate(Date gmtUpdate) {
        this.gmtUpdate = gmtUpdate;
    }

    public Long getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(Long trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerPlate() {
        return trailerPlate;
    }

    public void setTrailerPlate(String trailerPlate) {
        this.trailerPlate = trailerPlate;
    }

    public Long getTrailerTypeId() {
        return trailerTypeId;
    }

    public void setTrailerTypeId(Long trailerTypeId) {
        this.trailerTypeId = trailerTypeId;
    }

    public String getTrailerTypeName() {
        return trailerTypeName;
    }

    public void setTrailerTypeName(String trailerTypeName) {
        this.trailerTypeName = trailerTypeName;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getLspName() {
        return lspName;
    }

    public void setLspName(String lspName) {
        this.lspName = lspName;
    }

    @Override
    public String toString() {
        return "VehicleDTO{" +
                "id=" + id +
                ", plate='" + plate + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", ownerMobile='" + ownerMobile + '\'' +
                ", plateAnno='" + plateAnno + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", gmtManu=" + gmtManu +
                ", gmtAs=" + gmtAs +
                ", model='" + model + '\'' +
                ", axisNumber=" + axisNumber +
                ", travelledDist=" + travelledDist +
                ", minNl=" + minNl +
                ", maxNl=" + maxNl +
                ", minFl=" + minFl +
                ", maxFl=" + maxFl +
                ", selfWeight=" + selfWeight +
                ", lspId=" + lspId +
                ", fleetId=" + fleetId +
                ", status=" + status +
                ", authStatus=" + authStatus +
                ", creator='" + creator + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", createFrom='" + createFrom + '\'' +
                ", gmtUpdate=" + gmtUpdate +
                ", trailerId=" + trailerId +
                ", trailerPlate='" + trailerPlate + '\'' +
                ", trailerTypeId=" + trailerTypeId +
                ", trailerTypeName='" + trailerTypeName + '\'' +
                ", driverId=" + driverId +
                ", driverName='" + driverName + '\'' +
                ", driverMobile='" + driverMobile + '\'' +
                ", lspName='" + lspName + '\'' +
                '}';
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
}
