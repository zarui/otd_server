package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.integration.dao.mapper.ImportLogHistoryMapper;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import com.zhiche.lisa.integration.service.IImportLogHistoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 接口导入日志历史 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@Service
public class ImportLogHistoryServiceImpl extends ServiceImpl<ImportLogHistoryMapper, ImportLogHistory> implements IImportLogHistoryService {

    @Override
    public Page<ImportLogHistory> queryTmsImportLogs(Page<ImportLogHistory> page) {

        Map<String, Object> cn = page.getCondition();
        Wrapper<ImportLogHistory> wp = new EntityWrapper<>();
        if (cn != null && !cn.isEmpty()) {
            handleCondition(cn, wp);
        }else{
            List<String> types = new ArrayList<>();
            types.add("10");
            types.add("23");
            types.add("30");
            types.add("11");
            types.add("12");
            types.add("31");
            types.add("32");
            types.add("huiyuncheUsedCar");
            List<String> sourceSys = new ArrayList<>();
            sourceSys.add("huiyunche");
            sourceSys.add("otm");
            sourceSys.add("hyc");
            wp.in("source_sys",sourceSys);
            wp.in("type",types);
            wp.isNotNull("target_sys");
        }
        wp.orderBy("gmt_create",false);
        List<ImportLogHistory> list = baseMapper.selectPage(page,wp);
        BeanUtils.copyProperties(page, list);
        page.setRecords(list);
        return page;
    }

    private void handleCondition(Map<String, Object> cn, Wrapper<ImportLogHistory> wp) {

        Object sourceSys = cn.get("sourceSys");
        if (sourceSys != null && StringUtils.isNotBlank(sourceSys.toString())) {
            wp.eq("source_sys", sourceSys);
        }else {
            List<String> sourceSysList = new ArrayList<>();
            sourceSysList.add("huiyunche");
            sourceSysList.add("otm");
            sourceSysList.add("hyc");
            wp.in("source_sys",sourceSysList);
        }
        Object targetSys = cn.get("targetSys");
        if (targetSys != null && StringUtils.isNotBlank(targetSys.toString())) {
            if("tms".equals(targetSys.toString().toLowerCase())){
                List<String> targetSyss = new ArrayList<>();
                targetSyss.add("tms");
                targetSyss.add("oms");
                wp.in("target_sys", targetSyss);
            }else {
                wp.eq("target_sys", targetSys);
            }
        }else{
            wp.isNotNull("target_sys");
        }
        Object importStartTime = cn.get("importStartTime");
        if (importStartTime != null && StringUtils.isNotBlank(importStartTime.toString())) {
            wp.ge("gmt_create", importStartTime);
        }
        Object importEndTime = cn.get("importEndTime");
        if (importEndTime != null && StringUtils.isNotBlank(importEndTime.toString())) {
            wp.le("gmt_create", importEndTime);
        }
        Object sourceKey = cn.get("sourceKey");
        if (sourceKey != null && StringUtils.isNotBlank(sourceKey.toString())) {
            String sourceKeyString = sourceKey.toString().trim().replace("\n", "");
            String[] sourceKeys = sourceKeyString.split(",");
            wp.in("source_key",sourceKeys);
//            wp.like("source_key", sourceKey.toString());
        }
        Object billStatus = cn.get("billStatus");
        if (billStatus != null && StringUtils.isNotBlank(billStatus.toString())) {
            wp.eq("type",billStatus);
        }else {
            List<String> types = new ArrayList<>();
            types.add("10");
            types.add("23");
            types.add("30");
            types.add("11");
            types.add("12");
            types.add("31");
            types.add("32");
            types.add("huiyuncheUsedCar");
            wp.in("type",types);
        }
        Object importStatus = cn.get("importStatus");
        if (importStatus != null && StringUtils.isNotBlank(importStatus.toString())) {
            wp.eq("import_status",importStatus);
        }
    }

    @Override
    public Page<Map<String,Object>> queryOtmEmptyMonitor(Page<ImportLogHistory> page) {
        Map<String, Object> cn = page.getCondition();
        Wrapper<ImportLogHistory> ew = new EntityWrapper<>();
        Object importStartTime = cn.get("startTime");
        if (importStartTime != null && StringUtils.isNotBlank(importStartTime.toString())) {
            ew.ge("gmt_create", importStartTime);
        }
        Object importEndTime = cn.get("endTime");
        if (importEndTime != null && StringUtils.isNotBlank(importEndTime.toString())) {
            ew.le("gmt_create", importEndTime);
        }
        Object sourceKey = cn.get("shipmwnt_xid");
        if (sourceKey != null && StringUtils.isNotBlank(sourceKey.toString())) {
            ew.like("source_key", sourceKey.toString());
        }
        ew.eq("type","23");
        ew.eq("source_sys","otm");
        ew.orderBy("gmt_create",false);
//        String[] properties = {"id","logId","sourceSys","targetSys","type","dataStorageKey","importStatus","importNote","importStartTime","importEndTime"};
        List<ImportLogHistory> list = baseMapper.selectPage(page,ew);
        List<Map<String,Object>> lists = new ArrayList<>();
        for (ImportLogHistory importLogHistory: list){
            Map<String,Object> map = new HashMap<>();
            map.put("gmtCreate",importLogHistory.getGmtCreate());
            map.put("sourceKey",importLogHistory.getSourceKey());
            lists.add(map);
        }
        Page<Map<String,Object>> pageVo = new Page<>();
        BeanUtils.copyProperties(page, pageVo);
        BeanUtils.copyProperties(pageVo, lists);
        pageVo.setRecords(lists);
        return pageVo;
    }

}
