package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.CorporationCiams;
import com.zhiche.lisa.integration.dto.otd.CorpCiamDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 客户业务主体关联表 服务类
 * </p>
 *
 * @author zhangkun
 * @since 2018-12-06
 */
public interface CorporationCiamsService extends IService<CorporationCiams> {

    List<CorpCiamDTO> queryCorpCiamOTD(Map<String, String> dto);
}
