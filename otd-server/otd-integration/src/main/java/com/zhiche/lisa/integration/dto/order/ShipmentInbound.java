package com.zhiche.lisa.integration.dto.order;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:38 2019/3/1
 */
public class ShipmentInbound implements Serializable {
    /**
     * 运单号
     */
    private String order_release_xid;

    /**
     * 指令号
     */
    private String shipmwnt_xid;

    /**
     * 车架号
     */
    private String vin;

    /**
     * 入库状态
     */
    private String status;

    /**
     * 入库时间
     */
    private Date inbound_time;

    public String getOrder_release_xid () {
        return order_release_xid;
    }

    public void setOrder_release_xid (String order_release_xid) {
        this.order_release_xid = order_release_xid;
    }

    public String getShipmwnt_xid () {
        return shipmwnt_xid;
    }

    public void setShipmwnt_xid (String shipmwnt_xid) {
        this.shipmwnt_xid = shipmwnt_xid;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public Date getInbound_time () {
        return inbound_time;
    }

    public void setInbound_time (Date inbound_time) {
        this.inbound_time = inbound_time;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("ShipmentInbound{");
        sb.append("order_release_xid='").append(order_release_xid).append('\'');
        sb.append(", shipmwnt_xid='").append(shipmwnt_xid).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", inbound_time=").append(inbound_time);
        sb.append('}');
        return sb.toString();
    }
}
