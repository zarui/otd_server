package com.zhiche.lisa.integration.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.dao.model.VehicleClass;
import com.zhiche.lisa.integration.service.IVehicleClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/vehicleClass")
@Api(value = "/vehicleClass", description = "OTM车辆车系", tags = "OTM车辆车系")
public class VehicleClassController {
    public Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IVehicleClassService vehicleClassService;

    @PostMapping(value = "queryVehicleClassName")
    @ApiOperation(value = "queryVehicleClassName-API", notes = "根据品牌编码/车系名称/无条件分页查询", response = RestfulResponse.class)
    public RestfulResponse<Page<VehicleClass>> queryVehicleClassName(@RequestBody Page<VehicleClass> page) {
        logger.info("controller:/VehicleClass/queryClassPage data: {}", page);
        RestfulResponse result = new RestfulResponse(0, "成功", null);
        try {
            Page<VehicleClass> brandPage = vehicleClassService.queryClassPage(page);
            result.setData(brandPage);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());

        }
        return result;
    }

    @PostMapping(value = "queryListVehicleClassCode")
    @ApiOperation(value = "queryListVehicleClassCode-API", notes = "可以根据车系编码查询所有", response = RestfulResponse.class)
    public RestfulResponse<List<VehicleClass>> queryListVehicleClassCode(@RequestBody Map<String, Object> brandCode) {
        logger.info("controller:/VehicleClass/queryAllClass data: {}", brandCode);
        RestfulResponse<List<VehicleClass>> result = new RestfulResponse<>(0, "成功", null);
        try {
            List<VehicleClass> vehicleBrandList = vehicleClassService.queryAllClass(brandCode);
            result.setData(vehicleBrandList);
            return result;
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
    }


}
