package com.zhiche.lisa.integration.anno;

import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.XmlUtil;
import org.dom4j.Document;
import org.springframework.util.Assert;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/7/21.
 */
public class AnnoUtil {

    /**
     * 通过字段注解拷贝属性值
     * @param source
     * @param target
     * @throws Exception
     */
    public static void copyPropertiesByAnno(Object source, Object target) throws Exception{
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        Field[] sourceFields = source.getClass().getDeclaredFields();
        Field[] targetFields = target.getClass().getDeclaredFields();

        for(Field targetField : targetFields){
            FiledAnno targetFieldAnno = targetField.getAnnotation(FiledAnno.class);
            if(Objects.nonNull(targetFieldAnno)){
                targetField.setAccessible(true);
                Object sourceValue = getValueByAnnoValue(targetFieldAnno,source,sourceFields);
                targetField.set(target,sourceValue);
            }
        }
    }

    public static String loadXml2String(Object object) throws Exception{
        ClassAnno classAnno = object.getClass().getAnnotation(ClassAnno.class);
        if(Objects.isNull(classAnno)) return null;

        String filePath = classAnno.value();
        Document document = XmlUtil.getDocumentByFile(filePath);
        if (Objects.isNull(document)) {
            throw new BaseException("文档不存在或者转化有误");
        }
        String xmlStr = XmlUtil.xmlToString(document);

        Field[] fields = object.getClass().getDeclaredFields();
        for(Field field : fields){
            FiledAnno fieldAnno = field.getAnnotation(FiledAnno.class);
            if(Objects.nonNull(fieldAnno)){
                field.setAccessible(true);
                String replaceStr = fieldAnno.value();
//                String value = field.get(object).toString();
                String strValue = "";
                Object objValue = field.get(object);
                if(Objects.nonNull(objValue)){
                    strValue = objValue.toString();
                }

                //回调地址
                xmlStr = xmlStr.replace(replaceStr,strValue);
            }
        }

        return xmlStr;
    }


    /**
     * 通过注解值得到属性值
     * @param targetFieldAnno 目标注解
     * @param source 源对象
     * @param sourceFields 源对象字段集合
     * @return
     * @throws Exception
     */
    private static Object getValueByAnnoValue(FiledAnno targetFieldAnno,Object source,Field[] sourceFields)
                                                    throws Exception{
        for(Field field : sourceFields){
            FiledAnno sourceFieldAnno = field.getAnnotation(FiledAnno.class);
            if(Objects.nonNull(sourceFieldAnno)){
                field.setAccessible(true);
                if(targetFieldAnno.value().equals(sourceFieldAnno.value())){
                    return field.get(source);
                }
            }
        }
        return null;
    }

}
