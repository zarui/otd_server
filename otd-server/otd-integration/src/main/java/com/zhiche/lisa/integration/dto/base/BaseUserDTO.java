package com.zhiche.lisa.integration.dto.base;

import com.zhiche.lisa.integration.dao.model.BaseUser;

import java.io.Serializable;

public class BaseUserDTO extends BaseUser implements Serializable {

    private String departmentName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
