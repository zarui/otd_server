package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * OTM 客户业务主体关联表
 * </p>
 *
 * @author hs
 * @since 2018-12-06
 */
@TableName("otm_corporation_ciams")
public class CorporationCiams extends Model<CorporationCiams> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 关联gid
     */
	@TableField("ciams_relation_gid")
	private String ciamsRelationGid;
    /**
     * 关联xid
     */
	@TableField("ciams_relation_xid")
	private String ciamsRelationXid;
    /**
     * 关联业务主体GID
     */
	private String ciams;
    /**
     * 关联业务主体NAME
     */
	@TableField("ciams_name")
	private String ciamsName;
    /**
     * 地址
     */
	private String location;
    /**
     * 客户GID
     */
	private String corporation;
    /**
     * 客户名称
     */
	@TableField("corporation_name")
	private String corporationName;
    /**
     * 状态
     */
	private String status;
    /**
     * 创建时间
     */
	@TableField("insert_date")
	private Date insertDate;
    /**
     * 更新时间
     */
	@TableField("update_date")
	private Date updateDate;
    /**
     * 域名
     */
	@TableField("domain_name")
	private String domainName;
	@TableField("gmt_create")
	private String gmtCreate;
	@TableField("gmt_modified")
	private Date gmtModified;

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCiamsRelationGid() {
		return ciamsRelationGid;
	}

	public void setCiamsRelationGid(String ciamsRelationGid) {
		this.ciamsRelationGid = ciamsRelationGid;
	}

	public String getCiamsRelationXid() {
		return ciamsRelationXid;
	}

	public void setCiamsRelationXid(String ciamsRelationXid) {
		this.ciamsRelationXid = ciamsRelationXid;
	}

	public String getCiams() {
		return ciams;
	}

	public void setCiams(String ciams) {
		this.ciams = ciams;
	}

	public String getCiamsName() {
		return ciamsName;
	}

	public void setCiamsName(String ciamsName) {
		this.ciamsName = ciamsName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCorporation() {
		return corporation;
	}

	public void setCorporation(String corporation) {
		this.corporation = corporation;
	}

	public String getCorporationName() {
		return corporationName;
	}

	public void setCorporationName(String corporationName) {
		this.corporationName = corporationName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CorporationCiams{" +
			", id=" + id +
			", ciamsRelationGid=" + ciamsRelationGid +
			", ciamsRelationXid=" + ciamsRelationXid +
			", ciams=" + ciams +
			", ciamsName=" + ciamsName +
			", location=" + location +
			", corporation=" + corporation +
			", corporationName=" + corporationName +
			", status=" + status +
			", insertDate=" + insertDate +
			", updateDate=" + updateDate +
			", domainName=" + domainName +
			"}";
	}
}
