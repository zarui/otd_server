package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 运单信息中间表
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
@TableName("v_order_release_mid")
public class OrderReleaseMid extends Model<OrderReleaseMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 运输单号
     */
    @TableId("order_release_gid")
	private String orderReleaseGid;
    /**
     * 备注
     */
	private String attribute19;
    /**
     * 车队id
     */
	@TableField("vehi_id")
	private String vehiId;
    /**
     * 二维码信息
     */
	private String attribute17;
    /**
     * 付款方
     */
	private String attribute14;
    /**
     * 应付合同编号
     */
	private String attribute12;
    /**
     * 结算单价
     */
	@TableField("settle_unit_price")
	private BigDecimal settleUnitPrice;
    /**
     * 一口价金额
     */
	@TableField("fixed_cost")
	private BigDecimal fixedCost;
    /**
     * 结算里程
     */
	@TableField("settle_miles")
	private BigDecimal settleMiles;
    /**
     * 应付里程数
     */
	@TableField("attribute_number1")
	private BigDecimal attributeNumber1;
    /**
     * 主运输模式
     */
	private String attribute11;
    /**
     * 扣费应付车型分组
     */
	@TableField("mg5_name")
	private String mg5Name;
    /**
     * 补贴应付车型分组
     */
	@TableField("mg4_name")
	private String mg4Name;
    /**
     * 油价联动应付车型分组
     */
	@TableField("mg3_name")
	private String mg3Name;
    /**
     * 固定费应付车型分组
     */
	@TableField("mg2_name")
	private String mg2Name;
    /**
     * 运输费应收付型分组
     */
	@TableField("mg1_name")
	private String mg1Name;
    /**
     * 运输方式
     */
	@TableField("transport_mode_gid")
	private String transportModeGid;
    /**
     * 车队名称
     */
	private String attribute18;
    /**
     * 分供方类型
     */
	@TableField("serv_type")
	private String servType;
    /**
     * 分供方名称
     */
	@TableField("serv_name")
	private String servName;
    /**
     * 分供方ID
     */
	@TableField("servprov_gid")
	private String servprovGid;
    /**
     * 付款方
     */
	@TableField("ap_party")
	private String apParty;
    /**
     * 标准车型
     */
	private String attribute10;
    /**
     * 合格证车型代码
     */
	private String attribute9;
    /**
     * 改装后的长宽高重
     */
	private String attribute8;
    /**
     * ILS-插入时间
     */
	@TableField("update_date")
	private Date updateDate;
    /**
     * ILS-更新时间
     */
	@TableField("insert_date")
	private Date insertDate;
    /**
     * 是否改装车
     */
	@TableField("delivery_is_appt")
	private String deliveryIsAppt;
    /**
     * 商品车数量
     */
	@TableField("total_ship_unit_count")
	private Integer totalShipUnitCount;
    /**
     * 域
     */
	@TableField("domain_name")
	private String domainName;
    /**
     * VIN码
     */
	private String attribute7;
    /**
     * 车型描述
     */
	private String attribute6;
    /**
     * 客户车型
     */
	private String attribute5;
    /**
     * 客户运输模式分类
     */
	private String attribute4;
    /**
     * 目的地联系电话
     */
	@TableField("dl_attb8")
	private String dlAttb8;
    /**
     * 目的地联系人
     */
	@TableField("dl_attb7")
	private String dlAttb7;
    /**
     * 目的地区县
     */
	@TableField("dl_zone3")
	private String dlZone3;
    /**
     * 目的地城市
     */
	@TableField("dl_zone2")
	private String dlZone2;
    /**
     * 目的地省份
     */
	@TableField("dl_zone1")
	private String dlZone1;
    /**
     * 目的区县（客户）
     */
	@TableField("dl_posc")
	private String dlPosc;
    /**
     * 目的城市（客户）
     */
	@TableField("dl_city")
	private String dlCity;
    /**
     * 目的省份（客户）
     */
	@TableField("dl_prov")
	private String dlProv;
    /**
     * 目的地地址
     */
	@TableField("dl_des")
	private String dlDes;
    /**
     * 目的地名称
     */
	@TableField("dl_loc")
	private String dlLoc;
    /**
     * 目的地编码
     */
	@TableField("dest_location_gid")
	private String destLocationGid;
    /**
     * 起运地区县
     */
	@TableField("sl_zone3")
	private String slZone3;
    /**
     * 起运地城市
     */
	@TableField("sl_zone2")
	private String slZone2;
    /**
     * 起运地省份
     */
	@TableField("sl_zone1")
	private String slZone1;
    /**
     * 起运区县（客户）
     */
	@TableField("sl_posc")
	private String slPosc;
    /**
     * 起运城市（客户）
     */
	@TableField("sl_city")
	private String slCity;
    /**
     * 起运省份（客户）
     */
	@TableField("sl_prov")
	private String slProv;
    /**
     * 起运地地址
     */
	@TableField("sl_des")
	private String slDes;
    /**
     * 起运地名称
     */
	@TableField("sl_loc")
	private String slLoc;
    /**
     * 起运地编码
     */
	@TableField("source_location_gid")
	private String sourceLocationGid;
    /**
     * 分段标识
     */
	@TableField("order_release_type_gid")
	private String orderReleaseTypeGid;
    /**
     * 提车入库时间
     */
	@TableField("attribute_date8")
	private Date attributeDate8;
    /**
     * 要求入库时间
     */
	@TableField("attribute_date7")
	private Date attributeDate7;
    /**
     * 移库运抵时间
     */
	@TableField("attribute_date6")
	private Date attributeDate6;
    /**
     * 要求抵达时间
     */
	@TableField("late_delivery_date")
	private Date lateDeliveryDate;
    /**
     * 实际发运时间
     */
	@TableField("attribute_date5")
	private Date attributeDate5;
    /**
     * 要求发运时间
     */
	@TableField("early_pickup_date")
	private Date earlyPickupDate;
    /**
     * 实际工厂出库时间
     */
	@TableField("attribute_date4")
	private Date attributeDate4;
    /**
     * 工厂出库时间
     */
	@TableField("attribute_date3")
	private Date attributeDate3;
    /**
     * 计费状态
     */
	private String description2;
    /**
     * 计费状态id
     */
	@TableField("user_defined2_icon_gid")
	private String userDefined2IconGid;
    /**
     * 运单状态
     */
	private String description;
    /**
     * 运单状态id
     */
	@TableField("user_defined1_icon_gid")
	private String userDefined1IconGid;
    /**
     * 买断
     */
	@TableField("is_known_shipper")
	private String isKnownShipper;
    /**
     * 超期
     */
	@TableField("pickup_is_appt")
	private String pickupIsAppt;
    /**
     * 急发
     */
	@TableField("duty_paid")
	private String dutyPaid;
    /**
     * 订单属性
     */
	private String attribute3;
    /**
     * 客户
     */
	private String attribute2;
    /**
     * 运输指令号
     */
	private String attribute16;
    /**
     * OMS订单号
     */
	private String attribute15;
    /**
     * 客户运单号
     */
	@TableField("order_release_name")
	private String orderReleaseName;
    /**
     * 客户订单号
     */
	private String attribute1;
    /**
     * 有支付
     */
	@TableField("is_ignore_location_calendar")
	private String isIgnoreLocationCalendar;
    /**
     * 打印时间
     */
	@TableField("attribute_date2")
	private Date attributeDate2;
    /**
     * 下单时间
     */
	@TableField("attribute_date1")
	private Date attributeDate1;
    /**
     * 路由模式名称
     */
	@TableField("route_mode_name")
	private String routeModeName;
    /**
     * 路由模式
     */
	@TableField("route_mode")
	private String routeMode;
    /**
     * 业务主体
     */
	private String attribute20;
    /**
     * Integration 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * Integration 创建时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;


	public String getOrderReleaseGid() {
		return orderReleaseGid;
	}

	public void setOrderReleaseGid(String orderReleaseGid) {
		this.orderReleaseGid = orderReleaseGid;
	}

	public String getAttribute19() {
		return attribute19;
	}

	public void setAttribute19(String attribute19) {
		this.attribute19 = attribute19;
	}

	public String getVehiId() {
		return vehiId;
	}

	public void setVehiId(String vehiId) {
		this.vehiId = vehiId;
	}

	public String getAttribute17() {
		return attribute17;
	}

	public void setAttribute17(String attribute17) {
		this.attribute17 = attribute17;
	}

	public String getAttribute14() {
		return attribute14;
	}

	public void setAttribute14(String attribute14) {
		this.attribute14 = attribute14;
	}

	public String getAttribute12() {
		return attribute12;
	}

	public void setAttribute12(String attribute12) {
		this.attribute12 = attribute12;
	}

	public BigDecimal getSettleUnitPrice() {
		return settleUnitPrice;
	}

	public void setSettleUnitPrice(BigDecimal settleUnitPrice) {
		this.settleUnitPrice = settleUnitPrice;
	}

	public BigDecimal getFixedCost() {
		return fixedCost;
	}

	public void setFixedCost(BigDecimal fixedCost) {
		this.fixedCost = fixedCost;
	}

	public BigDecimal getSettleMiles() {
		return settleMiles;
	}

	public void setSettleMiles(BigDecimal settleMiles) {
		this.settleMiles = settleMiles;
	}

	public BigDecimal getAttributeNumber1() {
		return attributeNumber1;
	}

	public void setAttributeNumber1(BigDecimal attributeNumber1) {
		this.attributeNumber1 = attributeNumber1;
	}

	public String getAttribute11() {
		return attribute11;
	}

	public void setAttribute11(String attribute11) {
		this.attribute11 = attribute11;
	}

	public String getMg5Name() {
		return mg5Name;
	}

	public void setMg5Name(String mg5Name) {
		this.mg5Name = mg5Name;
	}

	public String getMg4Name() {
		return mg4Name;
	}

	public void setMg4Name(String mg4Name) {
		this.mg4Name = mg4Name;
	}

	public String getMg3Name() {
		return mg3Name;
	}

	public void setMg3Name(String mg3Name) {
		this.mg3Name = mg3Name;
	}

	public String getMg2Name() {
		return mg2Name;
	}

	public void setMg2Name(String mg2Name) {
		this.mg2Name = mg2Name;
	}

	public String getMg1Name() {
		return mg1Name;
	}

	public void setMg1Name(String mg1Name) {
		this.mg1Name = mg1Name;
	}

	public String getTransportModeGid() {
		return transportModeGid;
	}

	public void setTransportModeGid(String transportModeGid) {
		this.transportModeGid = transportModeGid;
	}

	public String getAttribute18() {
		return attribute18;
	}

	public void setAttribute18(String attribute18) {
		this.attribute18 = attribute18;
	}

	public String getServType() {
		return servType;
	}

	public void setServType(String servType) {
		this.servType = servType;
	}

	public String getServName() {
		return servName;
	}

	public void setServName(String servName) {
		this.servName = servName;
	}

	public String getServprovGid() {
		return servprovGid;
	}

	public void setServprovGid(String servprovGid) {
		this.servprovGid = servprovGid;
	}

	public String getApParty() {
		return apParty;
	}

	public void setApParty(String apParty) {
		this.apParty = apParty;
	}

	public String getAttribute10() {
		return attribute10;
	}

	public void setAttribute10(String attribute10) {
		this.attribute10 = attribute10;
	}

	public String getAttribute9() {
		return attribute9;
	}

	public void setAttribute9(String attribute9) {
		this.attribute9 = attribute9;
	}

	public String getAttribute8() {
		return attribute8;
	}

	public void setAttribute8(String attribute8) {
		this.attribute8 = attribute8;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getDeliveryIsAppt() {
		return deliveryIsAppt;
	}

	public void setDeliveryIsAppt(String deliveryIsAppt) {
		this.deliveryIsAppt = deliveryIsAppt;
	}

	public Integer getTotalShipUnitCount() {
		return totalShipUnitCount;
	}

	public void setTotalShipUnitCount(Integer totalShipUnitCount) {
		this.totalShipUnitCount = totalShipUnitCount;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getAttribute7() {
		return attribute7;
	}

	public void setAttribute7(String attribute7) {
		this.attribute7 = attribute7;
	}

	public String getAttribute6() {
		return attribute6;
	}

	public void setAttribute6(String attribute6) {
		this.attribute6 = attribute6;
	}

	public String getAttribute5() {
		return attribute5;
	}

	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}

	public String getAttribute4() {
		return attribute4;
	}

	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}

	public String getDlAttb8() {
		return dlAttb8;
	}

	public void setDlAttb8(String dlAttb8) {
		this.dlAttb8 = dlAttb8;
	}

	public String getDlAttb7() {
		return dlAttb7;
	}

	public void setDlAttb7(String dlAttb7) {
		this.dlAttb7 = dlAttb7;
	}

	public String getDlZone3() {
		return dlZone3;
	}

	public void setDlZone3(String dlZone3) {
		this.dlZone3 = dlZone3;
	}

	public String getDlZone2() {
		return dlZone2;
	}

	public void setDlZone2(String dlZone2) {
		this.dlZone2 = dlZone2;
	}

	public String getDlZone1() {
		return dlZone1;
	}

	public void setDlZone1(String dlZone1) {
		this.dlZone1 = dlZone1;
	}

	public String getDlPosc() {
		return dlPosc;
	}

	public void setDlPosc(String dlPosc) {
		this.dlPosc = dlPosc;
	}

	public String getDlCity() {
		return dlCity;
	}

	public void setDlCity(String dlCity) {
		this.dlCity = dlCity;
	}

	public String getDlProv() {
		return dlProv;
	}

	public void setDlProv(String dlProv) {
		this.dlProv = dlProv;
	}

	public String getDlDes() {
		return dlDes;
	}

	public void setDlDes(String dlDes) {
		this.dlDes = dlDes;
	}

	public String getDlLoc() {
		return dlLoc;
	}

	public void setDlLoc(String dlLoc) {
		this.dlLoc = dlLoc;
	}

	public String getDestLocationGid() {
		return destLocationGid;
	}

	public void setDestLocationGid(String destLocationGid) {
		this.destLocationGid = destLocationGid;
	}

	public String getSlZone3() {
		return slZone3;
	}

	public void setSlZone3(String slZone3) {
		this.slZone3 = slZone3;
	}

	public String getSlZone2() {
		return slZone2;
	}

	public void setSlZone2(String slZone2) {
		this.slZone2 = slZone2;
	}

	public String getSlZone1() {
		return slZone1;
	}

	public void setSlZone1(String slZone1) {
		this.slZone1 = slZone1;
	}

	public String getSlPosc() {
		return slPosc;
	}

	public void setSlPosc(String slPosc) {
		this.slPosc = slPosc;
	}

	public String getSlCity() {
		return slCity;
	}

	public void setSlCity(String slCity) {
		this.slCity = slCity;
	}

	public String getSlProv() {
		return slProv;
	}

	public void setSlProv(String slProv) {
		this.slProv = slProv;
	}

	public String getSlDes() {
		return slDes;
	}

	public void setSlDes(String slDes) {
		this.slDes = slDes;
	}

	public String getSlLoc() {
		return slLoc;
	}

	public void setSlLoc(String slLoc) {
		this.slLoc = slLoc;
	}

	public String getSourceLocationGid() {
		return sourceLocationGid;
	}

	public void setSourceLocationGid(String sourceLocationGid) {
		this.sourceLocationGid = sourceLocationGid;
	}

	public String getOrderReleaseTypeGid() {
		return orderReleaseTypeGid;
	}

	public void setOrderReleaseTypeGid(String orderReleaseTypeGid) {
		this.orderReleaseTypeGid = orderReleaseTypeGid;
	}

	public Date getAttributeDate8() {
		return attributeDate8;
	}

	public void setAttributeDate8(Date attributeDate8) {
		this.attributeDate8 = attributeDate8;
	}

	public Date getAttributeDate7() {
		return attributeDate7;
	}

	public void setAttributeDate7(Date attributeDate7) {
		this.attributeDate7 = attributeDate7;
	}

	public Date getAttributeDate6() {
		return attributeDate6;
	}

	public void setAttributeDate6(Date attributeDate6) {
		this.attributeDate6 = attributeDate6;
	}

	public Date getLateDeliveryDate() {
		return lateDeliveryDate;
	}

	public void setLateDeliveryDate(Date lateDeliveryDate) {
		this.lateDeliveryDate = lateDeliveryDate;
	}

	public Date getAttributeDate5() {
		return attributeDate5;
	}

	public void setAttributeDate5(Date attributeDate5) {
		this.attributeDate5 = attributeDate5;
	}

	public Date getEarlyPickupDate() {
		return earlyPickupDate;
	}

	public void setEarlyPickupDate(Date earlyPickupDate) {
		this.earlyPickupDate = earlyPickupDate;
	}

	public Date getAttributeDate4() {
		return attributeDate4;
	}

	public void setAttributeDate4(Date attributeDate4) {
		this.attributeDate4 = attributeDate4;
	}

	public Date getAttributeDate3() {
		return attributeDate3;
	}

	public void setAttributeDate3(Date attributeDate3) {
		this.attributeDate3 = attributeDate3;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getUserDefined2IconGid() {
		return userDefined2IconGid;
	}

	public void setUserDefined2IconGid(String userDefined2IconGid) {
		this.userDefined2IconGid = userDefined2IconGid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserDefined1IconGid() {
		return userDefined1IconGid;
	}

	public void setUserDefined1IconGid(String userDefined1IconGid) {
		this.userDefined1IconGid = userDefined1IconGid;
	}

	public String getIsKnownShipper() {
		return isKnownShipper;
	}

	public void setIsKnownShipper(String isKnownShipper) {
		this.isKnownShipper = isKnownShipper;
	}

	public String getPickupIsAppt() {
		return pickupIsAppt;
	}

	public void setPickupIsAppt(String pickupIsAppt) {
		this.pickupIsAppt = pickupIsAppt;
	}

	public String getDutyPaid() {
		return dutyPaid;
	}

	public void setDutyPaid(String dutyPaid) {
		this.dutyPaid = dutyPaid;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getAttribute16() {
		return attribute16;
	}

	public void setAttribute16(String attribute16) {
		this.attribute16 = attribute16;
	}

	public String getAttribute15() {
		return attribute15;
	}

	public void setAttribute15(String attribute15) {
		this.attribute15 = attribute15;
	}

	public String getOrderReleaseName() {
		return orderReleaseName;
	}

	public void setOrderReleaseName(String orderReleaseName) {
		this.orderReleaseName = orderReleaseName;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getIsIgnoreLocationCalendar() {
		return isIgnoreLocationCalendar;
	}

	public void setIsIgnoreLocationCalendar(String isIgnoreLocationCalendar) {
		this.isIgnoreLocationCalendar = isIgnoreLocationCalendar;
	}

	public Date getAttributeDate2() {
		return attributeDate2;
	}

	public void setAttributeDate2(Date attributeDate2) {
		this.attributeDate2 = attributeDate2;
	}

	public Date getAttributeDate1() {
		return attributeDate1;
	}

	public void setAttributeDate1(Date attributeDate1) {
		this.attributeDate1 = attributeDate1;
	}

	public String getRouteModeName() {
		return routeModeName;
	}

	public void setRouteModeName(String routeModeName) {
		this.routeModeName = routeModeName;
	}

	public String getRouteMode() {
		return routeMode;
	}

	public void setRouteMode(String routeMode) {
		this.routeMode = routeMode;
	}

	public String getAttribute20() {
		return attribute20;
	}

	public void setAttribute20(String attribute20) {
		this.attribute20 = attribute20;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	@Override
	protected Serializable pkVal() {
		return this.orderReleaseGid;
	}

	@Override
	public String toString() {
		return "OrderReleaseMid{" +
			", orderReleaseGid=" + orderReleaseGid +
			", attribute19=" + attribute19 +
			", vehiId=" + vehiId +
			", attribute17=" + attribute17 +
			", attribute14=" + attribute14 +
			", attribute12=" + attribute12 +
			", settleUnitPrice=" + settleUnitPrice +
			", fixedCost=" + fixedCost +
			", settleMiles=" + settleMiles +
			", attributeNumber1=" + attributeNumber1 +
			", attribute11=" + attribute11 +
			", mg5Name=" + mg5Name +
			", mg4Name=" + mg4Name +
			", mg3Name=" + mg3Name +
			", mg2Name=" + mg2Name +
			", mg1Name=" + mg1Name +
			", transportModeGid=" + transportModeGid +
			", attribute18=" + attribute18 +
			", servType=" + servType +
			", servName=" + servName +
			", servprovGid=" + servprovGid +
			", apParty=" + apParty +
			", attribute10=" + attribute10 +
			", attribute9=" + attribute9 +
			", attribute8=" + attribute8 +
			", updateDate=" + updateDate +
			", insertDate=" + insertDate +
			", deliveryIsAppt=" + deliveryIsAppt +
			", totalShipUnitCount=" + totalShipUnitCount +
			", domainName=" + domainName +
			", attribute7=" + attribute7 +
			", attribute6=" + attribute6 +
			", attribute5=" + attribute5 +
			", attribute4=" + attribute4 +
			", dlAttb8=" + dlAttb8 +
			", dlAttb7=" + dlAttb7 +
			", dlZone3=" + dlZone3 +
			", dlZone2=" + dlZone2 +
			", dlZone1=" + dlZone1 +
			", dlPosc=" + dlPosc +
			", dlCity=" + dlCity +
			", dlProv=" + dlProv +
			", dlDes=" + dlDes +
			", dlLoc=" + dlLoc +
			", destLocationGid=" + destLocationGid +
			", slZone3=" + slZone3 +
			", slZone2=" + slZone2 +
			", slZone1=" + slZone1 +
			", slPosc=" + slPosc +
			", slCity=" + slCity +
			", slProv=" + slProv +
			", slDes=" + slDes +
			", slLoc=" + slLoc +
			", sourceLocationGid=" + sourceLocationGid +
			", orderReleaseTypeGid=" + orderReleaseTypeGid +
			", attributeDate8=" + attributeDate8 +
			", attributeDate7=" + attributeDate7 +
			", attributeDate6=" + attributeDate6 +
			", lateDeliveryDate=" + lateDeliveryDate +
			", attributeDate5=" + attributeDate5 +
			", earlyPickupDate=" + earlyPickupDate +
			", attributeDate4=" + attributeDate4 +
			", attributeDate3=" + attributeDate3 +
			", description2=" + description2 +
			", userDefined2IconGid=" + userDefined2IconGid +
			", description=" + description +
			", userDefined1IconGid=" + userDefined1IconGid +
			", isKnownShipper=" + isKnownShipper +
			", pickupIsAppt=" + pickupIsAppt +
			", dutyPaid=" + dutyPaid +
			", attribute3=" + attribute3 +
			", attribute2=" + attribute2 +
			", attribute16=" + attribute16 +
			", attribute15=" + attribute15 +
			", orderReleaseName=" + orderReleaseName +
			", attribute1=" + attribute1 +
			", isIgnoreLocationCalendar=" + isIgnoreLocationCalendar +
			", attributeDate2=" + attributeDate2 +
			", attributeDate1=" + attributeDate1 +
			", routeModeName=" + routeModeName +
			", routeMode=" + routeMode +
			", attribute20=" + attribute20 +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
