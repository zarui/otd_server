package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.model.*;
import com.zhiche.lisa.integration.dto.base.BaseUserDTO;
import com.zhiche.lisa.integration.dto.otd.*;

import java.util.List;
import java.util.Map;

public interface IOtdService {
    /**
     * 查询铁运在途
     */
    String queryCsrcVinDetail(String vins);

    /**
     * 查询订单
     *
     * @param page
     */
    Page<OrderMid> queryOrder(Page<OrderMid> page);

    /**
     * 查询订单（新；供OTD使用）
     *
     * @param page
     */
    Page<NewOrderMid> queryOrderNew(Page<NewOrderMid> page);

    /**
     * 查询用户
     */
    List<BaseUserDTO> queryUserOTD(Map<String, String> dto);

    /**
     * 查询启运地
     */
    List<Origination> queryOriginOTD(Map<String, String> dto);

    /**
     * 查询客户
     */
    List<CorporationDTO> queryCorpOTD(Map<String, String> dto);

    /**
     * 查询业务主体
     */
    List<CiamsWithOutIdDTO> queryCiamOTD(Map<String, String> dto);

    /**
     * 查询客户-业务主体 关联
     */
    List<CorpCiamDTO> queryCorpCiamOTD(Map<String, String> dto);

    /**
     * 客户内控时效
     */
    List<CusLimitionMid> queryCusLimition(Map<String, String> dto);

    /**
     * 查询内控时效
     */
    List<UlcLimitionMid> queryUlcLimition(Map<String, String> dto);

    /**
     * 查询指令
     * @param page
     */
    Page<OTDShipmentDTO> queryShipment(Page<OTDShipmentDTO> page);

    /**
     * 查询指令（新：供OTD使用）
     * @param page
     */
    Page<NewOTDShipmentDTO> queryShipmentNew(Page<NewOTDShipmentDTO> page);

    /**
     * 查询运单+路由
     */
    Page<OrderReleaseMidDTO> queryRouteRelease(Page<OrderReleaseMidDTO> page);

    /**
     * 查询运单+路由（新：供OTD使用）
     */
    Page<NewOrderReleaseMidDTO> queryRouteReleaseNew(Page<NewOrderReleaseMidDTO> page);

    Page<InTransitDTO> queryInTransitInfo (Page<InTransitDTO> page);

    /**
     * 查询路由删除记录
     */
    Page<RouteDelMid> queryRouteDelMid (Page<RouteDelMid> page);
}
