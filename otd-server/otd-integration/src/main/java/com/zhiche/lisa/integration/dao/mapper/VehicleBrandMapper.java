package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.VehicleBrand;

/**
 * <p>
 * OTM车辆品牌 Mapper 接口
 * </p>
 */
public interface VehicleBrandMapper extends BaseMapper<VehicleBrand> {
}
