package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Maps;
import com.zhiche.lisa.core.enums.IntegrationURIEnum;
import com.zhiche.lisa.core.enums.TableStatusEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.HttpClientUtil;
import com.zhiche.lisa.integration.dao.model.*;
import com.zhiche.lisa.integration.dto.base.BaseUserDTO;
import com.zhiche.lisa.integration.dto.otd.*;
import com.zhiche.lisa.integration.service.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class OtdServiceImpl implements IOtdService {

    private final Logger logger = LoggerFactory.getLogger(OtdServiceImpl.class);

    @Autowired
    private IPushSubSystemService pushSubSystemService;
    @Autowired
    private OrderMidService orderMidService;
    @Autowired
    private IBaseUserService baseUserService;
    @Autowired
    private IOriginationService originationService;
    @Autowired
    private CorporationCiamsService corporationCiamsService;
    @Autowired
    private ICiamsService ciamsService;
    @Autowired
    private ICorporationService corporationService;
    @Autowired
    private CusLimitionMidService cusLimitionMidService;
    @Autowired
    private UlcLimitionMidService ulcLimitionMidService;
    @Autowired
    private ShipmentMidService shipmentMidService;
    @Autowired
    private OrderReleaseMidService orderReleaseMidService;

    @Override
    public String queryCsrcVinDetail(String vins) {
        if (StringUtils.isEmpty(vins)) {
            throw new BaseException("查询车架号信息不能为空");
        }
        EntityWrapper<PushSubSystem> urlEW = new EntityWrapper<>();
        urlEW.eq("type", IntegrationURIEnum.OTD_CRSC.getCode())
                .eq("status", TableStatusEnum.STATUS_1.getCode())
                .orderBy("id", false);
        List<PushSubSystem> subSystems = pushSubSystemService.selectList(urlEW);
        if (!CollectionUtils.isEmpty(subSystems)) {
            for (PushSubSystem sub : subSystems) {
                HashMap<String, String> param = Maps.newHashMap();
                param.put("vinStrs", vins);
                if (TableStatusEnum.STATUS_0.getCode().equals(sub.getRemarks())) {
                    try {
                        logger.info("OTD-->铁运查询在途信息 crscurl:{}, 车架号:{}", sub.getUrl(), vins);
                        String res = HttpClientUtil.post(sub.getUrl(), null, param, sub.getSocketTimeOut());
                        logger.info("OTD-->铁运查询在途信息 crscurl:{}, 车架号:{}", sub.getUrl(), vins);
                        if (StringUtils.isBlank(res)) {
                            throw new BaseException("未查询到对应的车辆信息");
                        } else {
                            return res;
                        }
                    } catch (Exception e) {
                        logger.error("OTD-->铁运查询在途信息 crscurl:{},超时异常:{}", sub.getUrl(), e);
                        throw new BaseException("连接铁运Server超时");
                    }
                }
            }
        } else {
            throw new BaseException("未查询到服务server地址信息");
        }
        return null;
    }

    /**
     * 查询订单信息--otd
     * @param page
     */
    @Override
    public Page<OrderMid> queryOrder(Page<OrderMid> page) {
        return orderMidService.queryOrder(page);
    }

    /**
     * 查询订单信息--otd(新)
     * @param page
     */
    @Override
    public Page<NewOrderMid> queryOrderNew(Page<NewOrderMid> page) {
        return orderMidService.queryOrderNew(page);
    }

    /**
     * 查询用户信息--otd
     */
    @Override
    public List<BaseUserDTO> queryUserOTD(Map<String, String> dto) {
        return baseUserService.queryUserOTD(dto);
    }

    /**
     * 启运地 -- otd
     */
    @Override
    public List<Origination> queryOriginOTD(Map<String, String> dto) {
        return originationService.queryOriginOTD(dto);
    }

    /**
     * 客户--otd
     */
    @Override
    public List<CorporationDTO> queryCorpOTD(Map<String, String> dto) {
        return corporationService.queryCorpOTD(dto);
    }

    /**
     * 业务主体  -- otd
     */
    @Override
    public List<CiamsWithOutIdDTO> queryCiamOTD(Map<String, String> dto) {
        return ciamsService.queryCiamOTD(dto);
    }

    /**
     * 客户 业务主体--otd
     */
    @Override
    public List<CorpCiamDTO> queryCorpCiamOTD(Map<String, String> dto) {
        return corporationCiamsService.queryCorpCiamOTD(dto);
    }

    /**
     * 客户 时效规则 otd
     */
    @Override
    public List<CusLimitionMid> queryCusLimition(Map<String, String> dto) {
        return cusLimitionMidService.queryCusLimition(dto);
    }

    /**
     * 中联 时效规则 otd
     */
    @Override
    public List<UlcLimitionMid> queryUlcLimition(Map<String, String> dto) {
        return ulcLimitionMidService.queryUlcLimition(dto);
    }

    /**
     * 查询指令
     */
    @Override
    public Page<OTDShipmentDTO> queryShipment(Page<OTDShipmentDTO> page) {
        return shipmentMidService.queryShipment(page);
    }

    /**
     * 查询指令（新）
     */
    @Override
    public Page<NewOTDShipmentDTO> queryShipmentNew(Page<NewOTDShipmentDTO> page) {
        return shipmentMidService.queryShipmentNew(page);
    }

    /**
     * 查询运单+路由
     *
     */
    @Override
    public Page<OrderReleaseMidDTO> queryRouteRelease(Page<OrderReleaseMidDTO> page) {
        return orderReleaseMidService.queryRouteRelease(page);
    }

    /**
     * 查询运单+路由（新）
     *
     */
    @Override
    public Page<NewOrderReleaseMidDTO> queryRouteReleaseNew(Page<NewOrderReleaseMidDTO> page) {
        return orderReleaseMidService.queryRouteReleaseNew(page);
    }

    @Override
    public Page<InTransitDTO> queryInTransitInfo (Page<InTransitDTO> page) {
        return orderReleaseMidService.queryInTransitInfo(page);
    }

    @Override
    public Page<RouteDelMid> queryRouteDelMid (Page<RouteDelMid> page) {
        return orderMidService.queryRouteDelMid(page);
    }

}
