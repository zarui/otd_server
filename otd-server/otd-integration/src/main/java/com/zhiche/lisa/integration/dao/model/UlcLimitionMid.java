package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * <p>
 * OTM 内控时效规则头信息
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@TableName("v_ulc_limition_mid")
public class UlcLimitionMid extends Model<UlcLimitionMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 内控时效gid
     */
    @TableId("limitation_gid")
    private String limitationGid;
    /**
     * 内控时效xid
     */
    @TableField("limitation_xid")
    private String limitationXid;
    /**
     * 时效规则名称
     */
    @TableField("limitation_name")
    private String limitationName;
    /**
     * 分段名称
     */
    private String stage;
    /**
     * 主运输模式
     */
    @TableField("m_transport_mode")
    private String mTransportMode;
    /**
     * 状态 1 正常 0 禁用
     */
    @TableField("STATUS")
    private Integer status;
    /**
     * otm 数据时间
     */
    @TableField("insert_date")
    private Date insertDate;
    /**
     * otm 数据时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * 数据修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 数据 修改时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;

    private Set<LimitionRuleMid> limitionRuleMids;

    public Set<LimitionRuleMid> getLimitionRuleMids() {
        return limitionRuleMids;
    }

    public void setLimitionRuleMids(Set<LimitionRuleMid> limitionRuleMids) {
        this.limitionRuleMids = limitionRuleMids;
    }

    public String getLimitationGid() {
        return limitationGid;
    }

    public void setLimitationGid(String limitationGid) {
        this.limitationGid = limitationGid;
    }

    public String getLimitationXid() {
        return limitationXid;
    }

    public void setLimitationXid(String limitationXid) {
        this.limitationXid = limitationXid;
    }

    public String getLimitationName() {
        return limitationName;
    }

    public void setLimitationName(String limitationName) {
        this.limitationName = limitationName;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getmTransportMode() {
        return mTransportMode;
    }

    public void setmTransportMode(String mTransportMode) {
        this.mTransportMode = mTransportMode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    protected Serializable pkVal() {
        return this.limitationGid;
    }

    @Override
    public String toString() {
        return "UlcLimitionMid{" +
                ", limitationGid=" + limitationGid +
                ", limitationXid=" + limitationXid +
                ", limitationName=" + limitationName +
                ", stage=" + stage +
                ", mTransportMode=" + mTransportMode +
                ", status=" + status +
                ", insertDate=" + insertDate +
                ", updateDate=" + updateDate +
                ", gmtModified=" + gmtModified +
                ", gmtCreate=" + gmtCreate +
                "}";
    }
}
