package com.zhiche.lisa.integration.dto.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhiche.lisa.integration.dao.model.BaseUser;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL) //过滤null 字段
public class UserDTO extends BaseUser implements Serializable {


}
