package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.RateItem;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM费用名称 服务类
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
public interface IRateItemService extends IService<RateItem> {
    /**
     * 根据费用名称编码查询全部返回数据
     */
    List<RateItem> queryRateItemAll(Map<String, Object> condition);
}
