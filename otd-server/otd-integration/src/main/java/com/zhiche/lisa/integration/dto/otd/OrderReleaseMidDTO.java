package com.zhiche.lisa.integration.dto.otd;

import com.zhiche.lisa.integration.dao.model.OrderReleaseMid;

import java.io.Serializable;
import java.util.Date;


public class OrderReleaseMidDTO extends OrderReleaseMid implements Serializable {
    /**
     * 路由GID
     */
    private String routeOrderRouteGid;
    /**
     * 路由xid
     */
    private String routeOrderRouteXid;
    /**
     * 订单ID
     */
    private String routeOrderGid;
    /**
     * 分段标识
     */
    private String routeSubMark;
    /**
     * 序号
     */
    private Integer routeSeq;
    /**
     * 起运地类型
     */
    private String routeStPointType;
    /**
     * 目的地类型
     */
    private String routeEnPointType;
    /**
     * 起运地
     */
    private String routeSourceLocation;
    /**
     * 目的地
     */
    private String routeDestLocation;
    /**
     * 运输模式
     */
    private String routeTransMode;
    /**
     * 预计发运时间
     */
    private Date routeEtd;
    /**
     * 预计抵达赶时间
     */
    private Date routeEta;
    /**
     * 状态
     */
    private String routeOrStatus;
    /**
     * OTM数据插入时间
     */
    private Date routeInsertDate;
    /**
     * OTM数据更新时间
     */
    private Date routeUpdateDate;
    /**
     * 数据修改时间
     */
    private Date routeGmtModified;
    /**
     * 数据 修改时间
     */
    private Date routeGmtCreate;


    public String getRouteOrderRouteGid() {
        return routeOrderRouteGid;
    }

    public void setRouteOrderRouteGid(String routeOrderRouteGid) {
        this.routeOrderRouteGid = routeOrderRouteGid;
    }

    public String getRouteOrderRouteXid() {
        return routeOrderRouteXid;
    }

    public void setRouteOrderRouteXid(String routeOrderRouteXid) {
        this.routeOrderRouteXid = routeOrderRouteXid;
    }

    public String getRouteOrderGid() {
        return routeOrderGid;
    }

    public void setRouteOrderGid(String routeOrderGid) {
        this.routeOrderGid = routeOrderGid;
    }

    public String getRouteSubMark() {
        return routeSubMark;
    }

    public void setRouteSubMark(String routeSubMark) {
        this.routeSubMark = routeSubMark;
    }

    public Integer getRouteSeq() {
        return routeSeq;
    }

    public void setRouteSeq(Integer routeSeq) {
        this.routeSeq = routeSeq;
    }

    public String getRouteStPointType() {
        return routeStPointType;
    }

    public void setRouteStPointType(String routeStPointType) {
        this.routeStPointType = routeStPointType;
    }

    public String getRouteEnPointType() {
        return routeEnPointType;
    }

    public void setRouteEnPointType(String routeEnPointType) {
        this.routeEnPointType = routeEnPointType;
    }

    public String getRouteSourceLocation() {
        return routeSourceLocation;
    }

    public void setRouteSourceLocation(String routeSourceLocation) {
        this.routeSourceLocation = routeSourceLocation;
    }

    public String getRouteDestLocation() {
        return routeDestLocation;
    }

    public void setRouteDestLocation(String routeDestLocation) {
        this.routeDestLocation = routeDestLocation;
    }

    public String getRouteTransMode() {
        return routeTransMode;
    }

    public void setRouteTransMode(String routeTransMode) {
        this.routeTransMode = routeTransMode;
    }

    public Date getRouteEtd() {
        return routeEtd;
    }

    public void setRouteEtd(Date routeEtd) {
        this.routeEtd = routeEtd;
    }

    public Date getRouteEta() {
        return routeEta;
    }

    public void setRouteEta(Date routeEta) {
        this.routeEta = routeEta;
    }

    public String getRouteOrStatus() {
        return routeOrStatus;
    }

    public void setRouteOrStatus(String routeOrStatus) {
        this.routeOrStatus = routeOrStatus;
    }

    public Date getRouteInsertDate() {
        return routeInsertDate;
    }

    public void setRouteInsertDate(Date routeInsertDate) {
        this.routeInsertDate = routeInsertDate;
    }

    public Date getRouteUpdateDate() {
        return routeUpdateDate;
    }

    public void setRouteUpdateDate(Date routeUpdateDate) {
        this.routeUpdateDate = routeUpdateDate;
    }

    public Date getRouteGmtModified() {
        return routeGmtModified;
    }

    public void setRouteGmtModified(Date routeGmtModified) {
        this.routeGmtModified = routeGmtModified;
    }

    public Date getRouteGmtCreate() {
        return routeGmtCreate;
    }

    public void setRouteGmtCreate(Date routeGmtCreate) {
        this.routeGmtCreate = routeGmtCreate;
    }
}
