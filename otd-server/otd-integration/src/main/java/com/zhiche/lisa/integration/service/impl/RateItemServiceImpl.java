package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysql.jdbc.StringUtils;
import com.zhiche.lisa.integration.dao.mapper.RateItemMapper;
import com.zhiche.lisa.integration.dao.model.RateItem;
import com.zhiche.lisa.integration.service.IRateItemService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * OTM费用名称 服务实现类
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
@Service
public class RateItemServiceImpl extends ServiceImpl<RateItemMapper, RateItem> implements IRateItemService {

    /**
     * 根据费用名称编码查询全部返回数据
     *
     * @param condition
     */
    @Override
    public List<RateItem> queryRateItemAll(Map<String, Object> condition) {
        Wrapper<RateItem> ew = buildCondition(condition);
        return this.selectList(ew);
    }

    private Wrapper<RateItem> buildCondition(Map<String, Object> condition) {
        Wrapper <RateItem> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()){
            //根据费用名称编码查询全返回
            Object rateItemCode = condition.get("code");
            if (rateItemCode != null && !StringUtils.isNullOrEmpty(rateItemCode.toString())){
                ew.eq("code",rateItemCode.toString());
            }
        }
        return ew;
    }
}
