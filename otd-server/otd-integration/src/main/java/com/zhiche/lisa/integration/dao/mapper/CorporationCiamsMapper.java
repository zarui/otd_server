package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.integration.dao.model.CorporationCiams;
import com.zhiche.lisa.integration.dto.otd.CorpCiamDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * OTM 客户业务主体关联表 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-06
 */
public interface CorporationCiamsMapper extends BaseMapper<CorporationCiams> {

    List<CorpCiamDTO> selectListOTD(@Param("ew") EntityWrapper<CorpCiamDTO> ew);
}
