package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.TransportMode;

/**
 * <p>
 * OTM运输方式(运输工具) Mapper 接口
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-23
 */
public interface TransportModeMapper extends BaseMapper<TransportMode> {

}
