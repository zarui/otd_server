package com.zhiche.lisa.integration.scheduled;

import com.zhiche.lisa.integration.service.IHuiyuncheService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * Created by yangzhiyu
 */
@Component
public class HuiyuncheCrawlOrder {
    private static final Logger LOGGER = LoggerFactory.getLogger(HuiyuncheCrawlOrder.class);

    @Autowired
    private IHuiyuncheService huiyuncheService;

//    @Scheduled(cron = "* 0/15 * * * ?")
    public void getOrderCodes() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        LOGGER.info("HuiyuncheCrawlOrder getOrderCodes startTime: {}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));
        try {
            huiyuncheService.getOrderCodes();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("HuiyuncheCrawlOrder 定时任务保存出错 error: {}", e);
        }
        LOGGER.info("HuiyuncheCrawlOrder getOrderCodes endTime: {}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));
    }

//    @Scheduled(cron = "0 0/20 * * * ?")
    public void getNewOrderCodes() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        LOGGER.info("HuiyuncheCrawlOrder getNewOrderCodes startTime: {}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));
        try {
            huiyuncheService.getNewOrderCodes();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("HuiyuncheCrawlOrder 定时任务保存出错 error: {}", e);
        }
        LOGGER.info("HuiyuncheCrawlOrder getNewOrderCodes endTime: {}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));
    }
}
