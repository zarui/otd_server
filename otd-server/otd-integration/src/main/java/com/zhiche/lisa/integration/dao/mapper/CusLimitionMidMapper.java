package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.zhiche.lisa.integration.dao.model.CusLimitionMid;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * OTM 上游时效规则头信息 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface CusLimitionMidMapper extends BaseMapper<CusLimitionMid> {

    List<CusLimitionMid> queryCusLimition(@Param("ew") EntityWrapper<CusLimitionMid> ew);
}
