package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.Corporation;
import com.zhiche.lisa.integration.dto.otd.CorporationDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 客户数据表 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-09-01
 */
public interface ICorporationService extends IService<Corporation> {

    /**
     * 查询OTM客户数据
     */
    List<Corporation> queryCorporationByName(Map<String, Object> condition);

    List<CorporationDTO> queryCorpOTD(Map<String, String> dto);
}
