package com.zhiche.lisa.integration.dto.order;

import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;

/**
 * Created by zhaoguixin on 2018/8/15.
 */
public class OriginationCity implements Serializable{

    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 省编码
     */
    private String provinceCode;
    /**
     * 市编码
     */

    private String cityCode;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
}
