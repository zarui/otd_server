package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.NewOrderMid;
import com.zhiche.lisa.integration.dao.model.OrderMid;
import com.zhiche.lisa.integration.dto.otd.RouteDelMid;

/**
 * <p>
 * 订单信息中间表 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
public interface OrderMidService extends IService<OrderMid> {

    Page<OrderMid> queryOrder(Page<OrderMid> page);

    Page<NewOrderMid> queryOrderNew(Page<NewOrderMid> page);

    Page<RouteDelMid> queryRouteDelMid (Page<RouteDelMid> page);
}
