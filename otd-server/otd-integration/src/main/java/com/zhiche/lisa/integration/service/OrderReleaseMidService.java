package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.OrderReleaseMid;
import com.zhiche.lisa.integration.dto.order.ReleaseDTO;
import com.zhiche.lisa.integration.dto.order.ShipmentInbound;
import com.zhiche.lisa.integration.dto.order.ShipmentInfo;
import com.zhiche.lisa.integration.dto.otd.InTransitDTO;
import com.zhiche.lisa.integration.dto.otd.NewOrderReleaseMidDTO;
import com.zhiche.lisa.integration.dto.otd.OrderReleaseMidDTO;
import com.zhiche.lisa.integration.dto.tms.PodReasonDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 运单信息中间表 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-11
 */
public interface OrderReleaseMidService extends IService<OrderReleaseMid> {

    Page<OrderReleaseMidDTO> queryRouteRelease(Page<OrderReleaseMidDTO> page);

    /**
     * 查询监控数据
     * @param page 入参
     * @return 返回数据
     */
    Page<ShipmentInfo> queryShipmentMonitor (Page<ShipmentInfo> page);

    /**
     * 查询当天指令入库数据
     * @param page 入参
     * @return 返回数据
     */
    Page<ShipmentInbound> queryShipmentInbound (Page<ShipmentInbound> page);

    Page<NewOrderReleaseMidDTO> queryRouteReleaseNew(Page<NewOrderReleaseMidDTO> page);

    Page<InTransitDTO> queryInTransitInfo (Page<InTransitDTO> page);

    void otmUpdateReleaseInfo (List<ReleaseDTO> param);

    String podReasonToOtm(PodReasonDTO podReasonDTO);

    void receiptStatus(Map<String, String> map);
}
