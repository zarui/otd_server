package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.LimitionRuleMid;

/**
 * <p>
 * OTM 时效规则明细表视图 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface LimitionRuleMidMapper extends BaseMapper<LimitionRuleMid> {

}
