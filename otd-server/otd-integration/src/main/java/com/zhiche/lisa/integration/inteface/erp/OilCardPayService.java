package com.zhiche.lisa.integration.inteface.erp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.lisa.core.enums.IntegrationURIEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.HttpClientUtil;
import com.zhiche.lisa.integration.dao.model.ImportLogHistory;
import com.zhiche.lisa.integration.dao.model.PushSubSystem;
import com.zhiche.lisa.integration.dto.oilCardPay.FleetToOliCardDTO;
import com.zhiche.lisa.integration.service.IImportLogService;
import com.zhiche.lisa.integration.service.IPushSubSystemService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by yangzhiyu on 2018/11/25
 */
@Service
public class OilCardPayService {
    public Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private IImportLogService iImportLogService;
    @Autowired
    private IPushSubSystemService pushSubSystemService;
    /**
     * 运力平台设置车队管理员的时候车队信息推送pay
     */
    public void fleetPushPay(FleetToOliCardDTO fleetToOliCardDTO) {
        String paramJson = JSONObject.toJSONString(fleetToOliCardDTO);
        //导入日志
        ImportLogHistory importLogHistory = new ImportLogHistory();
        importLogHistory.setSourceSys("lpsm");
        importLogHistory.setSourceKey(fleetToOliCardDTO.getCode());
        importLogHistory.setType(IntegrationURIEnum.PAY_FLEET.getCode());
        importLogHistory.setImportStartTime(new Date());
        importLogHistory.setImportEndTime(new Date());
        new Thread(() -> {
            LOGGER.info("开始保存导入日志---------------");
            try {
                iImportLogService.saveImportLogToQiniu(importLogHistory, paramJson);
            } catch (Exception e) {
                LOGGER.error("日志保存失败---------------");
            }
            LOGGER.info("完成日志保存---------------");
        }).start();

        Wrapper<PushSubSystem> ew = new EntityWrapper<>();
        ew.eq("type", IntegrationURIEnum.PAY_FLEET.getCode());
        List<PushSubSystem> pushSubSystems = pushSubSystemService.selectList(ew);
        if (CollectionUtils.isEmpty(pushSubSystems) || pushSubSystems.size() <= 0) {
            throw new BaseException("为查询到对应url");
        }
        if (CollectionUtils.isNotEmpty(pushSubSystems)) {
            for (PushSubSystem subSys : pushSubSystems) {
                LOGGER.info("OilCardPayService.fleetPushPay url:{},param:{}", subSys.getUrl(), paramJson);
                String result = HttpClientUtil.postJson(subSys.getUrl(), null, paramJson, subSys.getSocketTimeOut());
                LOGGER.info("OilCardPayService.fleetPushPay result:" + result);
                if (StringUtils.isBlank(result)) {
                    throw new BaseException("pay接口返回为空");
                }
                JSONObject jsonObject = JSON.parseObject(result);
                Integer code = jsonObject.getInteger("code");
                if (code!=0) {
                    throw new BaseException(jsonObject.getString("message"));
                }
            }
        }

    }
}
