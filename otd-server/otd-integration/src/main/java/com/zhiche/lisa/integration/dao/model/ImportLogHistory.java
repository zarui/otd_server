package com.zhiche.lisa.integration.dao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 接口导入日志历史
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-22
 */
@TableName("itf_import_log_history")
public class ImportLogHistory extends Model<ImportLogHistory> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 日志ID
     */
    @TableField("log_id")
    private Integer logId;
    /**
     * 来源系统(erp,otm)
     */
    @TableField("source_sys")
    private String sourceSys;
    /**
     * 下发系统(erp,otm)
     */
    @TableField("target_sys")
    private String targetSys;
    /**
     * 来源唯一键
     */
    @TableField("source_key")
    private String sourceKey;
    /**
     * 类型(10:指令,20:份额)
     */
    private String type;
    /**
     * 数据存储键
     */
    @TableField("data_storage_key")
    private String dataStorageKey;
    /**
     * 下发状态
     */
    @TableField("import_status")
    private String importStatus;
    /**
     * 下发备注
     */
    @TableField("import_note")
    private String importNote;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 导入开始时间
     */
    @TableField("import_start_time")
    private Date importStartTime;
    /**
     * 导入完成时间
     */
    @TableField("import_end_time")
    private Date importEndTime;

    private String remark;
    /**
     * 发送者传输号（用于判断数据下发的先后顺序）
     */
    @TableField("transmission_no")
    private Long transmissionNo;

    public Long getTransmissionNo() {
        return transmissionNo;
    }

    public void setTransmissionNo(Long transmissionNo) {
        this.transmissionNo = transmissionNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTargetSys() {
        return targetSys;
    }

    public void setTargetSys(String targetSys) {
        this.targetSys = targetSys;
    }

    public String getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    public String getImportNote() {
        return importNote;
    }

    public void setImportNote(String importNote) {
        this.importNote = importNote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public String getSourceSys() {
        return sourceSys;
    }

    public void setSourceSys(String sourceSys) {
        this.sourceSys = sourceSys;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDataStorageKey() {
        return dataStorageKey;
    }

    public void setDataStorageKey(String dataStorageKey) {
        this.dataStorageKey = dataStorageKey;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getImportStartTime() {
        return importStartTime;
    }

    public void setImportStartTime(Date importStartTime) {
        this.importStartTime = importStartTime;
    }

    public Date getImportEndTime() {
        return importEndTime;
    }

    public void setImportEndTime(Date importEndTime) {
        this.importEndTime = importEndTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ImportLogHistory{" +
                ", id=" + id +
                ", logId=" + logId +
                ", sourceSys=" + sourceSys +
                ", sourceKey=" + sourceKey +
                ", type=" + type +
                ", dataStorageKey=" + dataStorageKey +
                ", gmtCreate=" + gmtCreate +
                ", importStartTime=" + importStartTime +
                ", importEndTime=" + importEndTime +
                ", remark=" + remark +
                "}";
    }
}
