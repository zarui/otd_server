package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.VehicleStandardMode;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM车辆标准车型 服务类
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-22
 */
public interface IVehicleStandardModeService extends IService<VehicleStandardMode> {
    /**
     * 通过车型编码查询标准车型数据全部返回
     */
    List<VehicleStandardMode> queryAllStandardMode (Map<String, Object> standardModeCode);

    /**
     * 通过品牌编码，车系编码查询标准车型数据分页返回
     * 通过车型名称模糊查询标准车型数据分页返回
     */
    Page<VehicleStandardMode> queryStandardModePage (Page<VehicleStandardMode> page);

    /**
     * 通过品牌编码，车系编码查询标准车型数据不分页返回
     * 通过车型名称模糊查询标准车型数据不分页返回
     */
    List<VehicleStandardMode> queryStandardModeList (Map<String, Object> condition);


    /**
     * 查询车型信息
     *
     * @param vehicleName
     * @return
     */
    Page<VehicleStandardMode> queryVehicleModelInfo (Page<VehicleStandardMode> vehicleName);

    /**
     * 修改后的车型同步OTM
     * @param params
     * @return
     */
    Map<String, Object> modifiedVehicleToOtm (Map<String, String> params);
}
