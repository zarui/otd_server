package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.UlcLimitionMid;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 内控时效规则头信息 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface UlcLimitionMidService extends IService<UlcLimitionMid> {

    List<UlcLimitionMid> queryUlcLimition(Map<String, String> dto);
}
