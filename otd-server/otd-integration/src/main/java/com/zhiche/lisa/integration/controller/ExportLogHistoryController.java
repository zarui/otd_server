package com.zhiche.lisa.integration.controller;

import com.zhiche.lisa.core.enums.CommonEnum;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.service.IExportLogHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 推送OTM日志控制器
 * User: fanguangji
 * Date: 2019/1/22
 */
@RestController
@RequestMapping("/toOtmLog")
@Api(value = "toOtmLog", description = "推送OTM日志控制器", tags = "推送OTM日志控制器")
public class ExportLogHistoryController {

    @Autowired private IExportLogHistoryService exportLogHistoryService;

    @PostMapping(value = "/getCheckinLog")
    @ApiOperation(value = "获取报班推送OTM日志", notes = "获取报班推送OTM日志",
            produces = MediaType.APPLICATION_JSON_VALUE, response = RestfulResponse.class)
    public RestfulResponse<Object> getCheckinLogList(@RequestBody Map<String,String> map) {
        RestfulResponse<Object> response = new RestfulResponse<>(CommonEnum.SUCCESS.getCode(), "成功", null);
        try {
            response.setData(exportLogHistoryService.getCheckinLogList(map.get("id")));
        } catch (BaseException be) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), be.getMessage(), null);
        } catch (Exception ex) {
            return new RestfulResponse<>(CommonEnum.ERROR.getCode(), CommonEnum.ERROR.getText(), null);
        }
        return response;
    }
}
