package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.LimitionRuleMid;

/**
 * <p>
 * OTM 时效规则明细表视图 服务类
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface LimitionRuleMidService extends IService<LimitionRuleMid> {

}
