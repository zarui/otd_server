package com.zhiche.lisa.integration.surpports.enums;

public enum TableStatusEnum {

    STATUS_0("0", "状态0的标识"),
    STATUS_OMS("OMS", "OMS子系统code"),
    STATUS_1("1", "状态1的标识"),
    STATUS_10("10", "状态10的标识"),
    STATUS_11("11", "状态11的标识"),
    STATUS_12("12", "状态12的标识"),
    STATUS_20("20", "状态20的标识"),
    STATUS_30("30", "状态30的标识"),
    STATUS_31("31", "状态31的标识"),
    STATUS_32("32", "状态32的标识"),
    STATUS_40("40", "状态40的标识"),
    STATUS_50("50", "状态50的标识"),
    STATUS_Y("Y", "状态Y的标识"),
    STATUS_N("N", "状态N的标识");

    private final String code;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    TableStatusEnum(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }

}
