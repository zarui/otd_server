package com.zhiche.lisa.integration.dao.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * OTM 订单实际路由中间表
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
@TableName("v_order_route_mid")
public class OrderRouteMid extends Model<OrderRouteMid> {

    private static final long serialVersionUID = 1L;

    /**
     * 路由GID
     */
    @TableId("order_route_gid")
	private String orderRouteGid;
    /**
     * 路由xid
     */
	@TableField("order_route_xid")
	private String orderRouteXid;
    /**
     * 订单ID
     */
	@TableField("order_gid")
	private String orderGid;
    /**
     * 分段标识
     */
	@TableField("sub_mark")
	private String subMark;
    /**
     * 序号
     */
	private Integer seq;
    /**
     * 起运地类型
     */
	@TableField("st_point_type")
	private String stPointType;
    /**
     * 目的地类型
     */
	@TableField("en_point_type")
	private String enPointType;
    /**
     * 起运地
     */
	@TableField("source_location")
	private String sourceLocation;
    /**
     * 目的地
     */
	@TableField("dest_location")
	private String destLocation;
    /**
     * 运输模式
     */
	@TableField("m_trans_mode")
	private String mTransMode;
    /**
     * 预计发运时间
     */
	private Date etd;
    /**
     * 预计抵达赶时间
     */
	private Date eta;
    /**
     * 状态
     */
	@TableField("or_status")
	private String orStatus;
    /**
     * OTM数据插入时间
     */
	@TableField("insert_date")
	private Date insertDate;
    /**
     * OTM数据更新时间
     */
	@TableField("update_date")
	private Date updateDate;
    /**
     * 数据修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;
    /**
     * 数据 修改时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;


	public String getOrderRouteGid() {
		return orderRouteGid;
	}

	public void setOrderRouteGid(String orderRouteGid) {
		this.orderRouteGid = orderRouteGid;
	}

	public String getOrderRouteXid() {
		return orderRouteXid;
	}

	public void setOrderRouteXid(String orderRouteXid) {
		this.orderRouteXid = orderRouteXid;
	}

	public String getOrderGid() {
		return orderGid;
	}

	public void setOrderGid(String orderGid) {
		this.orderGid = orderGid;
	}

	public String getSubMark() {
		return subMark;
	}

	public void setSubMark(String subMark) {
		this.subMark = subMark;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getStPointType() {
		return stPointType;
	}

	public void setStPointType(String stPointType) {
		this.stPointType = stPointType;
	}

	public String getEnPointType() {
		return enPointType;
	}

	public void setEnPointType(String enPointType) {
		this.enPointType = enPointType;
	}

	public String getSourceLocation() {
		return sourceLocation;
	}

	public void setSourceLocation(String sourceLocation) {
		this.sourceLocation = sourceLocation;
	}

	public String getDestLocation() {
		return destLocation;
	}

	public void setDestLocation(String destLocation) {
		this.destLocation = destLocation;
	}

	public String getmTransMode() {
		return mTransMode;
	}

	public void setmTransMode(String mTransMode) {
		this.mTransMode = mTransMode;
	}

	public Date getEtd() {
		return etd;
	}

	public void setEtd(Date etd) {
		this.etd = etd;
	}

	public Date getEta() {
		return eta;
	}

	public void setEta(Date eta) {
		this.eta = eta;
	}

	public String getOrStatus() {
		return orStatus;
	}

	public void setOrStatus(String orStatus) {
		this.orStatus = orStatus;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.orderRouteGid;
	}

	@Override
	public String toString() {
		return "OrderRouteMid{" +
			", orderRouteGid=" + orderRouteGid +
			", orderRouteXid=" + orderRouteXid +
			", orderGid=" + orderGid +
			", subMark=" + subMark +
			", seq=" + seq +
			", stPointType=" + stPointType +
			", enPointType=" + enPointType +
			", sourceLocation=" + sourceLocation +
			", destLocation=" + destLocation +
			", mTransMode=" + mTransMode +
			", etd=" + etd +
			", eta=" + eta +
			", orStatus=" + orStatus +
			", insertDate=" + insertDate +
			", updateDate=" + updateDate +
			", gmtModified=" + gmtModified +
			", gmtCreate=" + gmtCreate +
			"}";
	}
}
