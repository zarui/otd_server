package com.zhiche.lisa.integration.dao.mapper;

import com.zhiche.lisa.integration.dao.model.VehicleStandardMode;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * OTM车辆标准车型 Mapper 接口
 * </p>
 *
 * @author zhangkun
 * @since 2018-08-22
 */
public interface VehicleStandardModeMapper extends BaseMapper<VehicleStandardMode> {

}
