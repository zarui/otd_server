package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.lisa.integration.dao.model.Ciams;
import com.zhiche.lisa.integration.dto.carrier.CiamsResultDto;
import com.zhiche.lisa.integration.dto.otd.CiamsWithOutIdDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * OTM 业务主体表 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-09-04
 */
public interface CiamsMapper extends BaseMapper<Ciams> {

    List<CiamsResultDto> queryCiamsNameList(@Param("ew") Wrapper<CiamsResultDto> ew);

    List<CiamsWithOutIdDTO> selectListWithOutId(@Param("ew") EntityWrapper<CiamsWithOutIdDTO> userEW);
}
