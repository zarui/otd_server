package com.zhiche.lisa.integration.inteface.otm;

import com.google.common.collect.Lists;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.XmlUtil;
import com.zhiche.lisa.integration.dto.order.OTMLocation;
import com.zhiche.lisa.integration.dto.order.OTMShipmentStop;
import com.zhiche.lisa.integration.dto.order.ShipTaskDTO;
import com.zhiche.lisa.integration.dto.order.ShipmentDTO;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/6/13.
 */
public class OtmShipmentParse {

    private static final Logger logger = LoggerFactory.getLogger(OtmShipmentParse.class);

    private static final String OTM_DOMAIN = "ULC/ZC.";

    public static ShipmentDTO parse(String xml) throws Exception {
        String tranMissionHeadPath = "/Transmission/TransmissionHeader";
        String rootPath = "/Transmission/TransmissionBody/GLogXMLElement/PlannedShipment/Shipment";
        String shipmentHeaderPath = "/ShipmentHeader";
        ShipmentDTO otmShipmentDTO = new ShipmentDTO();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar calendar = Calendar.getInstance();
        try {
            Document document = XmlUtil.parseByString(xml);

            Element headerRoot = (Element) document.selectSingleNode(tranMissionHeadPath);
            try {
                //得到事件自增长标识SenderTransmissionNo
                Element senderTransmissionNo = headerRoot.element("SenderTransmissionNo");
                if (!Objects.isNull(senderTransmissionNo)) {
                    Long transmissionNo = Long.valueOf(senderTransmissionNo.getText());
                    otmShipmentDTO.setSenderTransmissionNo(transmissionNo);
                }
            } catch (Exception e) {
//                logger.info("exception totalShipUnitCount:{}",e.getMessage());
            }


            //得到shipHeader节点
            Element root = (Element) document.selectSingleNode(rootPath + shipmentHeaderPath);

            try {
                Element shipmentGid = root.element("ShipmentGid").element("Gid").element("Xid"); //调度指令号
                if (!Objects.isNull(shipmentGid)) otmShipmentDTO.setShipmentId(shipmentGid.getText());
            } catch (Exception e) {
//                logger.info("exception shipmentGid:{}",e.getMessage());
            }
            try {
                Element transactionCode = root.element("TransactionCode");//操作类型
                if (!Objects.isNull(transactionCode)) otmShipmentDTO.setTransactionCode(transactionCode.getText());
            } catch (Exception e) {
//                logger.info("exception transactionCode:{}",e.getMessage());
            }
            try {
                Element serviceProviderGid = root.element("ServiceProviderGid").element("Gid").element("Xid");//承运商
                if (!Objects.isNull(serviceProviderGid))
                    otmShipmentDTO.setServiceProviderId(serviceProviderGid.getText());
            } catch (Exception e) {
//                logger.info("exception serviceProviderGid:{}",e.getMessage());
            }
            try {
                Element transportModeGid = root.element("TransportModeGid").element("Gid").element("Xid");//运输方式
                if (!Objects.isNull(transportModeGid)) otmShipmentDTO.setTransportModeId(transportModeGid.getText());
            } catch (Exception e) {
//                logger.info("exception transportModeGid:{}",e.getMessage());
            }
            try {
                Element totalShipUnitCount = root.element("TotalShipUnitCount"); //总数量
                if (!Objects.isNull(totalShipUnitCount)) {
                    int count = Integer.valueOf(totalShipUnitCount.getText());
                    otmShipmentDTO.setTotalShipCount(count);
                }
            } catch (Exception e) {
//                logger.info("exception totalShipUnitCount:{}",e.getMessage());
            }
            try {
                Element startDt = root.element("StartDt").element("GLogDate"); //发运时间
                otmShipmentDTO.setExpcetShipDate(formatter.parse(startDt.getText()));
            } catch (Exception e) {
//                logger.info("exception startDt:{}",e.getMessage());
            }
            try {
                Element endDt = root.element("EndDt").element("GLogDate"); //抵达时间
                if (!Objects.isNull(endDt)) otmShipmentDTO.setExpectArriveDate(formatter.parse(endDt.getText()));
            } catch (Exception e) {
//                logger.info("exception endDt:{}",e.getMessage());
            }

            try {
                Element termLocationText = root.element("CommercialTerms").element("TermLocationText"); //抵达时间
                if (!Objects.isNull(termLocationText)) otmShipmentDTO.setStatus(termLocationText.getText());
            } catch (Exception e) {
//                logger.info("exception termLocationText:{}",e.getMessage());
            }

            try {
                Element driverGid = root.element("DriverGid").element("Gid").element("Xid"); //司机
                if (!Objects.isNull(driverGid)) otmShipmentDTO.setDriverId(driverGid.getText());
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element shipmentType = root.element("FlexFieldStrings").element("Attribute1"); //指令类型
                if (!Objects.isNull(shipmentType))
                    otmShipmentDTO.setShipmentType(shipmentType.getText().replace(OTM_DOMAIN, ""));
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element trailerNo = root.element("FlexFieldStrings").element("Attribute9"); //挂车号
                if (!Objects.isNull(trailerNo)) otmShipmentDTO.setTrailerNo(trailerNo.getText());
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element providerName = root.element("FlexFieldStrings").element("Attribute10");//分供方名称

                if (!Objects.isNull(providerName)) otmShipmentDTO.setServiceProviderName(providerName.getText());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                Element driverName = root.element("FlexFieldStrings").element("Attribute7");//司机

                if (!Objects.isNull(driverName)) otmShipmentDTO.setDriverName(driverName.getText());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                Element driverPhone = root.element("FlexFieldStrings").element("Attribute2");//联系方式

                if (!Objects.isNull(driverPhone)) otmShipmentDTO.setDriverMoble(driverPhone.getText());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                Element remark = root.element("FlexFieldStrings").element("Attribute11");//备注
                if (!Objects.isNull(remark)) otmShipmentDTO.setRemarks(remark.getText());
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element plateNo = root.element("FlexFieldStrings").element("Attribute12"); //车船号
                if (!Objects.isNull(plateNo)) otmShipmentDTO.setPlateNo(plateNo.getText().replace(OTM_DOMAIN, ""));
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element checkinId = root.element("FlexFieldStrings").element("Attribute14"); //报班ID
                if (!Objects.isNull(checkinId))
                    otmShipmentDTO.setCheckinId(checkinId.getText().replace(OTM_DOMAIN, ""));
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element expectInboundTime = root.element("FlexFieldDates").element("AttributeDate5").element //入库时间
                        ("GLogDate");
                if (!Objects.isNull(expectInboundTime)) otmShipmentDTO.setExpectInboundDate(formatter.parse
                        (expectInboundTime.getText()));
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element expectReceiptTime = root.element("FlexFieldDates").element("AttributeDate7").element //回单时间
                        ("GLogDate");
                if (!Objects.isNull(expectReceiptTime)) otmShipmentDTO.setExpectReceiptDate(formatter.parse
                        (expectReceiptTime.getText()));
            } catch (Exception e) {
//                e.printStackTrace();
            }
            try {
                Element fleetId = root.element("FlexFieldStrings").element("Attribute18"); //车队ID
                if (!Objects.isNull(fleetId)) otmShipmentDTO.setFleetId(fleetId.getText().replace(OTM_DOMAIN, ""));
            } catch (Exception e) {
//                e.printStackTrace();
            }

//            logger.info("otmShipmentDTO:{}", otmShipmentDTO.toString());

            List<Node> locationNodes = document.selectNodes(rootPath + "/Location");
            List<OTMLocation> otmLocationList = new ArrayList<>();
            for (Node node : locationNodes) {
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    OTMLocation otmLocation = new OTMLocation();
                    try {
                        Element locationGid = element.element("LocationGid").element("Gid").element("Xid"); //地址编码
                        if (!Objects.isNull(locationGid)) otmLocation.setLocationId(locationGid.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                    try {
                        Element locationName = element.element("LocationName");  //地址名称
                        if (!Objects.isNull(locationName)) otmLocation.setLocationName(locationName.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                    try {
                        Element description = element.element("Description"); //详细地址
                        if (!Objects.isNull(description)) otmLocation.setDescription(description.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                    try {
                        Element zone1 = element.element("Address").element("Zone1"); //省
                        if (!Objects.isNull(zone1)) otmLocation.setProvince(zone1.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                    try {
                        Element zone2 = element.element("Address").element("Zone2"); //市
                        if (!Objects.isNull(zone2)) otmLocation.setCity(zone2.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                    try {
                        Element zone3 = element.element("Address").element("Zone3"); //县
                        if (!Objects.isNull(zone3)) otmLocation.setCounty(zone3.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    otmLocationList.add(otmLocation);
//                    otmShipmentDTO.addLocation(otmLocation);

//                    logger.info("otmLocation:{}", otmLocation.toString());
                }
            }

            //解析 ShipmentStop
            List<Node> stopNodes = document.selectNodes(rootPath +"/ShipmentStop");
            List<OTMShipmentStop> stops = Lists.newLinkedList();
            for (Node node : stopNodes) {
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    OTMShipmentStop stop = new OTMShipmentStop();
                    if (node instanceof Element) {
                        Element element = (Element) node;
                        Element locationXid = element.element("LocationRef").element("LocationGid").element("Gid").element("Xid");
                        if (null != locationXid) {
                            stop.setLocationId(locationXid.getText());
                        }
                        Element stopSequence = element.element("StopSequence");
                        if (null != stopSequence) {
                            stop.setStopSequence(stopSequence.getText());
                        }
                        Element transactionCode = element.element("TransactionCode");
                        if (null != transactionCode) {
                            stop.setTransactionCode(transactionCode.getText());
                        }
                    }
                    stops.add(stop);
                }
            }

            //解析 release
            List<Node> releaseNodes = document.selectNodes(rootPath + "/Release");
            for (Node node : releaseNodes) {
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    ShipTaskDTO shipTaskDTO = new ShipTaskDTO();

                    try {
                        Element releaseGid = element.element("ReleaseGid").element("Gid").element("Xid");//运输订单号
                        if (!Objects.isNull(releaseGid)) shipTaskDTO.setTaskId(releaseGid.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    Element releaseHeader = element.element("ReleaseHeader");
                    if (!Objects.isNull(releaseHeader)) {
                        try {
                            Element cusWaybillNo = releaseHeader.element("ReleaseName");//客户运单号
                            if (!Objects.isNull(cusWaybillNo)) shipTaskDTO.setCusWaybillNo(cusWaybillNo.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }

                        try {
                            Element termLocationText = releaseHeader.element("CommercialTerms").element("TermLocationText");//执行状态
                            if (!Objects.isNull(termLocationText)) shipTaskDTO.setStatus(termLocationText.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }

                        try {
                            Element isUrgent = releaseHeader.element("DutyPaid");//是否急发
                            if (!Objects.isNull(isUrgent)) shipTaskDTO.setIsUrgent(isUrgent.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element cusOrderNo = releaseHeader.element("FlexFieldStrings").element("Attribute1");//客户订单号
                            if (!Objects.isNull(cusOrderNo)) shipTaskDTO.setCusOrderNo(cusOrderNo.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element customerId = releaseHeader.element("FlexFieldStrings").element("Attribute2");//客户
                            if (!Objects.isNull(customerId)) shipTaskDTO.setCustomerId(customerId.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element orderAtt = releaseHeader.element("FlexFieldStrings").element("Attribute3");//订单属性
                            if (!Objects.isNull(orderAtt)) shipTaskDTO.setOrderAtt(orderAtt.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element cusTransMode = releaseHeader.element("FlexFieldStrings").element("Attribute4");
                            //客户运输模式分类
                            if (!Objects.isNull(cusTransMode)) shipTaskDTO.setCusTransMode(cusTransMode.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element cusVehicleType = releaseHeader.element("FlexFieldStrings").element("Attribute5");
                            //客户车型
                            if (!Objects.isNull(cusVehicleType))
                                shipTaskDTO.setCusVehicleType(cusVehicleType.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element vehicleDescribe = releaseHeader.element("FlexFieldStrings").element("Attribute6");//车型描述
                            if (!Objects.isNull(vehicleDescribe))
                                shipTaskDTO.setVehicleDescribe(vehicleDescribe.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element vin = releaseHeader.element("FlexFieldStrings").element("Attribute7");//VIN码
                            if (!Objects.isNull(vin)) shipTaskDTO.setVin(vin.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element modVehicleSize = releaseHeader.element("FlexFieldStrings").element("Attribute8");
                            //改装后的长宽高重
                            if (!Objects.isNull(modVehicleSize))
                                shipTaskDTO.setModVehicleSize(modVehicleSize.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element stanVehicleType = releaseHeader.element("FlexFieldStrings").element
                                    ("Attribute10");//标准车型
                            if (!Objects.isNull(stanVehicleType))
                                shipTaskDTO.setStanVehicleType(stanVehicleType.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        // fix 新增erp指令推送增加业务主体
                        try {
                            Element ciams = releaseHeader.element("FlexFieldStrings").element
                                    ("Attribute20");//业务主体
                            if (!Objects.isNull(ciams))
                                shipTaskDTO.setCiamsId(ciams.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        //新增下单时间
                        try {
                            Element orderDate = releaseHeader.element("FlexFieldDates").element
                                    ("AttributeDate1").element("GLogDate");
                            if (!Objects.isNull(orderDate)) shipTaskDTO.setOrderDate(formatter.parse
                                    (orderDate.getText()));
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        //新增订单号
                        try {
                            Element orderNo = releaseHeader.element("FlexFieldStrings").element
                                    ("Attribute15");
                            if (!Objects.isNull(orderNo))
                                shipTaskDTO.setOrderNo(orderNo.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element isTerminal = releaseHeader.element("FlexFieldNumbers").element
                                    ("AttributeNumber10");//业务主体
                            if (!Objects.isNull(isTerminal))
                                shipTaskDTO.setIsTerminal(isTerminal.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element eptInboundTime = releaseHeader.element("FlexFieldDates").
                                    element("AttributeDate7").element("GLogDate");//要求入库时间
                            if (!Objects.isNull(eptInboundTime)) shipTaskDTO.setExpectInboundDate(formatter.parse
                                    (eptInboundTime.getText()));
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        try {
                            Element eptReceiptTime = releaseHeader.element("FlexFieldDates").
                                    element("AttributeDate9").element("GLogDate");//要求回单时间
                            if (!Objects.isNull(eptReceiptTime)) shipTaskDTO.setExpectReceiptDate(formatter.parse
                                    (eptReceiptTime.getText()));
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }

                        try {
                            Element qrCode = releaseHeader.element("FlexFieldStrings").
                                    element("Attribute17");//二维码
                            if (!Objects.isNull(qrCode))
                                shipTaskDTO.setQrCode(qrCode.getText());
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }

                    }

                    try {
                        Element shipFromLocationId = element.element("ShipFromLocationRef").element("LocationRef").
                                element("LocationGid").element("Gid").element("Xid");//起运地编码
                        if (!Objects.isNull(shipFromLocationId))
                            shipTaskDTO.setOriginLocationId(shipFromLocationId.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    try {
                        Element shipToLocationId = element.element("ShipToLocationRef").element("LocationRef").
                                element("LocationGid").element("Gid").element("Xid");//目的地编码
                        if (!Objects.isNull(shipToLocationId))
                            shipTaskDTO.setDestLocationId(shipToLocationId.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    try {
                        Element earlyPickupDt = element.element("TimeWindow").element("EarlyPickupDt").
                                element("GLogDate");//要求发运时间
                        if (!Objects.isNull(earlyPickupDt)) shipTaskDTO.setExpcetShipDate(formatter.parse
                                (earlyPickupDt.getText()));
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    try {
                        Element lateDeliveryDt = element.element("TimeWindow").element("LateDeliveryDt").
                                element("GLogDate");//要求抵达时间
                        if (!Objects.isNull(lateDeliveryDt)) shipTaskDTO.setExpectArriveDate(formatter.parse
                                (lateDeliveryDt.getText()));
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    try {
                        Element pickupIsAppt = element.element("TimeWindow").element("PickupIsAppt");//是否超期
                        if (!Objects.isNull(pickupIsAppt)) shipTaskDTO.setPickupIsAppt(pickupIsAppt.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                    try {
                        Element isModVehicle = element.element("TimeWindow").element("DeliveryIsAppt");//是否改装车
                        if (!Objects.isNull(isModVehicle)) shipTaskDTO.setIsModVehicle(isModVehicle.getText());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                    OTMLocation otmOLocation = selectLocationDTO(shipTaskDTO.getOriginLocationId(), otmLocationList);
                    OTMShipmentStop oShipmentStop = geShipmentStop(shipTaskDTO.getOriginLocationId(), stops);
                    if (null != otmOLocation) {
                        shipTaskDTO.setOriginLocationName(otmOLocation.getLocationName());
                        shipTaskDTO.setOriginLocationAddress(otmOLocation.getDescription());
                        shipTaskDTO.setOriginLocationProvince(otmOLocation.getProvince());
                        shipTaskDTO.setOriginLocationCity(otmOLocation.getCity());
                        shipTaskDTO.setOriginLocationCounty(otmOLocation.getCounty());
                    }
                    if (null != oShipmentStop) {
                        shipTaskDTO.setOriginLocationSequence(oShipmentStop.getStopSequence());
                    }


                    OTMShipmentStop dShipmentStop = geShipmentStop(shipTaskDTO.getDestLocationId(), stops);
                    if (null != dShipmentStop) {
                        shipTaskDTO.setDestLocationSequence(dShipmentStop.getStopSequence());
                    }
                    OTMLocation otmDLocation = selectLocationDTO(shipTaskDTO.getDestLocationId(), otmLocationList);
                    if (null != otmDLocation) {
                        shipTaskDTO.setDestLocationName(otmDLocation.getLocationName());
                        shipTaskDTO.setDestLocationAddress(otmDLocation.getDescription());
                        shipTaskDTO.setDestLocationProvince(otmDLocation.getProvince());
                        shipTaskDTO.setDestLocationCity(otmDLocation.getCity());
                        shipTaskDTO.setDestLocationCounty(otmDLocation.getCounty());
                        shipTaskDTO.setShipmentId(otmShipmentDTO.getShipmentId());
                    }


                    otmShipmentDTO.addShipTaskDTO(shipTaskDTO);
                }
            }
            String transactionHeaderPath = "/Transmission/TransmissionBody/GLogXMLElement/TransactionHeader";
            Element transactionHeader = (Element) document.selectSingleNode(transactionHeaderPath);
            try {
                Element schedulingTime = transactionHeader.element("ObjectModInfo").element("InsertDt").element("GLogDate");//调度时间
                if (!Objects.isNull(schedulingTime)){
                    calendar.setTime(formatter.parse(schedulingTime.getText()));
                    calendar.add(Calendar.HOUR, 8);
                    otmShipmentDTO.setInsertDate(calendar.getTime());
                }
            } catch (Exception e) {
               // e.printStackTrace();
            }
            return otmShipmentDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 获取站点顺序
     */
    private static OTMShipmentStop geShipmentStop(String locationId, List<OTMShipmentStop> stops) {
        if (StringUtils.isNotBlank(locationId)) {
            for (OTMShipmentStop stop : stops) {
                if (locationId.equals(stop.getLocationId())) {
                    return stop;
                }
            }
        }
        return null;
    }

    /**
     * 根据地址ID获取地址对象
     *
     * @param locationGid     地址ID
     * @param otmLocationList 地址列表
     */
    private static OTMLocation selectLocationDTO(String locationGid, List<OTMLocation> otmLocationList) {
        if (StringUtils.isEmpty(locationGid)) throw new BaseException("地址ID为空");
        for (OTMLocation otmLocation : otmLocationList) {
            if (locationGid.equals(otmLocation.getLocationId())) {
                return otmLocation;
            }
        }
        return null;
    }

}
