package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.Ciams;
import com.zhiche.lisa.integration.dto.carrier.CiamsResultDto;
import com.zhiche.lisa.integration.dto.otd.CiamsWithOutIdDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * OTM 业务主体表 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-09-04
 */
public interface ICiamsService extends IService<Ciams> {

    /**
     * 通过客户gid查询业务主体全称
     */
    List<CiamsResultDto> queryCiamsNameList(Map<String, Object> condition);

    List<CiamsWithOutIdDTO> queryCiamOTD(Map<String, String> dto);

}
