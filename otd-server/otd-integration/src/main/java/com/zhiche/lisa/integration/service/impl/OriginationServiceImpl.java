package com.zhiche.lisa.integration.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.integration.dao.mapper.OriginationMapper;
import com.zhiche.lisa.integration.dao.model.Origination;
import com.zhiche.lisa.integration.service.IOriginationService;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * OTM起运地 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-15
 */
@Service
public class OriginationServiceImpl extends ServiceImpl<OriginationMapper, Origination> implements IOriginationService {

    @Override
    public Page<Origination> queryPageOrigination(Page page) {
        Wrapper<Origination> ew = buildCondition(page.getCondition());
        page.setCondition(null);
        Page<Origination> originationPage = this.selectPage(page, ew);
        return originationPage;
    }

    @Override
    public List<Origination> queryListOrigination(Map<String, Object> condition) {
        Wrapper<Origination> ew = buildCondition(condition);
        List<Origination> originationList = this.selectList(ew);
        return originationList;
    }

    @Override
    public List<Origination> queryOriginationCity(String cityName) {
        baseMapper.updateSQLMode();
        Wrapper<Origination> ew = new EntityWrapper<>();
        ew.like("city", cityName)
                .in("type", Lists.newArrayList("10", "20"))
                .orderBy("id", false);
        ew.groupBy("province,city");
        return this.selectList(ew);
    }

    @Override
    public List<Origination> queryOriginationForOms(Map<String, Object> condition) {
        Wrapper<Origination> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()) {
            if (condition.containsKey("province") && StringUtils.isNotBlank(condition.get("province").toString())) {
                ew.eq("province", condition.get("province").toString());
            }
            if (condition.containsKey("city") && StringUtils.isNotBlank(condition.get("city").toString())) {
                ew.eq("city", condition.get("city").toString());
            }
            if (condition.containsKey("county") && StringUtils.isNotBlank(condition.get("county").toString())) {
                ew.eq("county", condition.get("county").toString());
            }
            ew.orderBy("id", false);
            Origination origination = this.selectOne(ew);
            if (origination != null) {
                if (!(condition.containsKey("county") && StringUtils.isNotBlank(condition.get("county").toString()))) {
                    origination.setCounty(null);
                    origination.setCountyCode(null);
                }
                ArrayList<Origination> list = Lists.newArrayList();
                list.add(origination);
                return list;
            }

            return null;
        }
        return null;

    }

    @Override
    public List<Origination> queryOriginOTD(Map<String, String> dto) {
        if (dto == null || dto.isEmpty()) {
            throw new BaseException("参数不能为空");
        }
        String queryAll = dto.get("queryAll");
        String timeStamp = dto.get("timeStamp");
        EntityWrapper<Origination> originEW = new EntityWrapper<>();
        if (StringUtils.isBlank(queryAll)
                || !TableStatusEnum.STATUS_Y.getCode().equalsIgnoreCase(queryAll)) {
            if (StringUtils.isBlank(timeStamp)) {
                throw new BaseException("参数不能为空");
            } else {
                Long longTime = null;
                try {
                    longTime = Long.valueOf(timeStamp);
                } catch (NumberFormatException e) {
                    throw new BaseException("时间戳格式不正确");
                }
                originEW.gt("gmt_modified", new Date(longTime));
            }
        }
        return baseMapper.selectList(originEW);
    }

    @Override
    public Origination queryOriginationCode(Map<String, String> condition) {
        EntityWrapper<Origination> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()) {
            if (condition.containsKey("province") && StringUtils.isNotBlank(condition.get("province").toString())) {
                ew.eq("province", condition.get("province").toString());
            }
            if (condition.containsKey("city") && StringUtils.isNotBlank(condition.get("city").toString())) {
                ew.eq("city", condition.get("city").toString());
            }
            if (condition.containsKey("county") && StringUtils.isNotBlank(condition.get("county").toString())) {
                ew.eq("county", condition.get("county").toString());
            }
        }
        Origination origination = this.selectOne(ew);
        if (origination!=null){
            if (origination.getProvinceCode()!=null && !"".equals(origination.getProvinceCode())){
                origination.setProvinceCode(origination.getProvinceCode().concat("0000"));
            }
            if (origination.getCityCode()!=null && !"".equals(origination.getCityCode())){
                origination.setCityCode(origination.getCityCode().concat("00"));
            }
            if (condition.containsKey("county") && !"".equals(condition.get("county"))){
                return origination;
            }else {
                origination.setCountyCode("");
                return origination;
            }
        }
       return null;
    }

    private Wrapper<Origination> buildCondition(Map<String, Object> condition) {
        Wrapper<Origination> ew = new EntityWrapper<>();
        if (!Objects.isNull(condition) && !condition.isEmpty()) {
            if (condition.containsKey("code") && StringUtils.isNotBlank(condition.get("code").toString())) {
                ew.like("code", condition.get("code").toString());
            }
            if (condition.containsKey("name") && StringUtils.isNotBlank(condition.get("name").toString())) {
                ew.like("name", condition.get("name").toString());
            }
            if (condition.containsKey("province") && StringUtils.isNotBlank(condition.get("province").toString())) {
                ew.like("province", condition.get("province").toString());
            }
            if (condition.containsKey("city") && StringUtils.isNotBlank(condition.get("city").toString())) {
                ew.like("city", condition.get("city").toString());
            }
            if (condition.containsKey("county") && StringUtils.isNotBlank(condition.get("county").toString())) {
                ew.like("county", condition.get("county").toString());
            }
            if (condition.containsKey("type") && StringUtils.isNotBlank(condition.get("type").toString())) {
                ew.eq("type", condition.get("type").toString());
            }
            if (condition.containsKey("status") && StringUtils.isNotBlank(condition.get("status").toString())) {
                ew.eq("status", condition.get("status").toString());
            }
            if (condition.containsKey("timeAfter") && StringUtils.isNotBlank(condition.get("timeAfter").toString())) {
                Date timeAfter = new Date(Long.valueOf(condition.get("timeAfter").toString()));
                ew.andNew()
                        .where("(gmt_modified IS NOT NULL and gmt_modified >= {0})", timeAfter)
                        .or()
                        .where("(gmt_modified IS NULL and gmt_create >= {0})", timeAfter);
            }
        }
        return ew;
    }
}
