package com.zhiche.lisa.integration.controller;


import com.alibaba.fastjson.JSONObject;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.service.IBMSService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/bmsIntegration")
@Api(value = "结算系统接口", description = "结算系统接口", tags = {"结算系统接口"})
public class BMSController {

    @Autowired
    private IBMSService ibmsService;

    @RequestMapping("/getPriceAndMiles")
    @ApiOperation(value = "获取结算价格和里程", notes = "获取结算价格和里程", response = RestfulResponse.class)
    public RestfulResponse<JSONObject> getPriceAndMiles(@RequestBody Map<String, String> condition) {
        RestfulResponse<JSONObject> response = new RestfulResponse<>(0, "查询成功", null);
        try {
            JSONObject data = ibmsService.getPriceAndMiles(condition);
            response.setData(data);
        } catch (BaseException be) {
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

    @RequestMapping("/exportPrice")
    @ApiOperation(value = "回写OTM支付结算价格", notes = "回写OTM支付结算价格", response = RestfulResponse.class)
    public RestfulResponse<JSONObject> exportPrice(@RequestBody Map<String, String> condition) {
        RestfulResponse<JSONObject> response = new RestfulResponse<>(0, "回传成功", null);
        try {
            HashMap<String, String> result = ibmsService.exportPrice(condition);
            if (!Boolean.valueOf(result.get("result"))) {
                response.setCode(-1);
                response.setMessage(result.get("message"));
            }
        } catch (BaseException be) {
            response.setCode(-1);
            response.setMessage(be.getMessage());
        } catch (Exception e) {
            response.setCode(-1);
            response.setMessage("系统异常");
        }
        return response;
    }

}
