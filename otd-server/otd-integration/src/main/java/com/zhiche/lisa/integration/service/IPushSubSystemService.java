package com.zhiche.lisa.integration.service;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.lisa.integration.dao.model.PushSubSystem;

import java.util.Date;

/**
 * <p>
 * 接口数据推送子系统 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-30
 */
public interface IPushSubSystemService extends IService<PushSubSystem> {

    /**
     * 推送装车单至子系统
     * @param shipmentDTO
     */
    void pushToSubSystem(Object shipmentDTO, String type);

    /**
     * 推送装车单至子系统
     * @param shipmentDTO
     */
    void pushToSubSystemNew(Object shipmentDTO, String type,Date startDate, String shipmentXml, String shipmentId, Long senderTransmissionNo);

    /**
     * 重推重载指令到TMS
     * @param sourceKey 指令号
     */
    void pushOrderShipmentTms(String sourceKey);

    /**
     * 重推重载指令到WMS
     * @param sourceKey 指令号
     */
    void pushOrderShipmentWms(String sourceKey);

    void queryShipmentToWms ();

    void pushShipmentToTms ();

    /**
     * 保存指令下发日志
     */
    void saveShipmentLog(Object shipmentDTO, String type,Date startDate, String shipmentXml, String shipmentId, Long senderTransmissionNo);
    /**
     * 推送新下发指令到wms和tms
     */
    void pushNewShipment ();

}
