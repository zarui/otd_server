package com.zhiche.lisa.integration.dto.carrier;

import com.zhiche.lisa.integration.anno.ClassAnno;
import com.zhiche.lisa.integration.anno.FiledAnno;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zhaoguixin on 2018/7/17.
 */
@ClassAnno("classpath:otmxml/truck_register_share.xml")
public class DriverCheckinLineDTO {

    @FiledAnno("${id}")
    private Long id;
    /**
     * 报班ID
     */
    private Long checkinId;
    /**
     * 始发省
     */
    private String departProvince;
    /**
     * 始发市
     */
    @FiledAnno("${departCity}")
    private String departCity;
    /**
     * 始发地详细地址
     */
    private String departAddress;
    /**
     * 目的省
     */
    @FiledAnno("${destProvince}")
    private String destProvince;
    /**
     * 目的市
     */
    private String destCity;
    /**
     * 目的地详细地址
     */
    private String destAddress;
    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 计划份额
     */
    private BigDecimal planSharePercent;
    /**
     * 实际执行份额
     */
    private BigDecimal actualSharePercent;
    /**
     * 差异份额
     */
    private BigDecimal differSharePercent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(Long checkinId) {
        this.checkinId = checkinId;
    }

    public String getDepartProvince() {
        return departProvince;
    }

    public void setDepartProvince(String departProvince) {
        this.departProvince = departProvince;
    }

    public String getDepartCity() {
        return departCity;
    }

    public void setDepartCity(String departCity) {
        this.departCity = departCity;
    }

    public String getDepartAddress() {
        return departAddress;
    }

    public void setDepartAddress(String departAddress) {
        this.departAddress = departAddress;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getDestAddress() {
        return destAddress;
    }

    public void setDestAddress(String destAddress) {
        this.destAddress = destAddress;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public BigDecimal getPlanSharePercent() {
        return planSharePercent;
    }

    public void setPlanSharePercent(BigDecimal planSharePercent) {
        this.planSharePercent = planSharePercent;
    }

    public BigDecimal getActualSharePercent() {
        return actualSharePercent;
    }

    public void setActualSharePercent(BigDecimal actualSharePercent) {
        this.actualSharePercent = actualSharePercent;
    }

    public BigDecimal getDifferSharePercent() {
        return differSharePercent;
    }

    public void setDifferSharePercent(BigDecimal differSharePercent) {
        this.differSharePercent = differSharePercent;
    }
}
