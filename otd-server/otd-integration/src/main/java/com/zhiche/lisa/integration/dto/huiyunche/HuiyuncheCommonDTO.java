package com.zhiche.lisa.integration.dto.huiyunche;

import java.io.Serializable;

public class HuiyuncheCommonDTO implements Serializable {
    private String orderCode;
    private String phone;
    private String dtshipdate;

    public String getDtshipdate() {
        return dtshipdate;
    }

    public void setDtshipdate(String dtshipdate) {
        this.dtshipdate = dtshipdate;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
