package com.zhiche.lisa.integration.dao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.integration.dao.model.OrderRouteMid;

/**
 * <p>
 * OTM 订单实际路由中间表 Mapper 接口
 * </p>
 *
 * @author hs
 * @since 2018-12-22
 */
public interface OrderRouteMidMapper extends BaseMapper<OrderRouteMid> {

}
