package com.zhiche.lisa.integration.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.mapper.OrderReleaseMidMapper;
import com.zhiche.lisa.integration.dto.otd.NewOrderReleaseMidDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderReleaseMidReposite {

  @Autowired
  OrderReleaseMidMapper baseMapper;

  public Page<NewOrderReleaseMidDTO> queryOrderReleaseMidNew(long timeStamp) {
    EntityWrapper<NewOrderReleaseMidDTO> ew = new EntityWrapper<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String format = sdf.format(new Date(timeStamp));
    ew.gt("a.gmt_modified", format);
    Page<NewOrderReleaseMidDTO> page = new Page<>();
    page.setCurrent(1);
    page.setSize(2);
    List<NewOrderReleaseMidDTO> list = baseMapper.selectRouteReleaseByPageNew(page, ew);
    page.setRecords(list);
    return page;
  }
}
