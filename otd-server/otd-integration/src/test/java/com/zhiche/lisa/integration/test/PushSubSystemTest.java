package com.zhiche.lisa.integration.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.common.collect.Lists;
import com.zhiche.lisa.core.enums.IntegrationURIEnum;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.core.utils.HttpClientUtil;
import com.zhiche.lisa.core.utils.qiniu.util.QiniuUtils;
import com.zhiche.lisa.integration.IntegrationApp;
import com.zhiche.lisa.integration.config.IntegrationProperties;
import com.zhiche.lisa.integration.dao.mapper.PushSubSystemMapper;
import com.zhiche.lisa.integration.dao.model.PushSubSystem;
import com.zhiche.lisa.integration.dto.carrier.QueryLspInfoDTO;
import com.zhiche.lisa.integration.surpports.enums.TableStatusEnum;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhaoguixin on 2018/6/29.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IntegrationApp.class)
public class PushSubSystemTest {
    @Autowired
    private QiniuUtils qiniuUtils;

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private IntegrationProperties properties;

    @Autowired
    private PushSubSystemMapper pushSubSystemMapper;

    @Test
    public void insertPushSubSystem() {
        PushSubSystem pushSubSystem = new PushSubSystem();
    }

    /**
     * 根据key  取七牛文件
     */
    @Test
    public void getQiniuFile() {
        String downloadUrl = QiniuUtils.getDownloadUrl("otm_10_S19030200020_187082_271249");
        System.out.println(downloadUrl);
    }

    @Test
    public void test1() {
        String json = "{\n" +
                "\t\"code\": 0,\n" +
                "\t\"message\": \"成功\",\n" +
                "\t\"data\": {\n" +
                "\t\t\"id\": 3,\n" +
                "\t\t\"code\": \"CO78322710769303552\",\n" +
                "\t\t\"customerName\": null,\n" +
                "\t\t\"comment\": null,\n" +
                "\t\t\"tos\": \"fleet\",\n" +
                "\t\t\"issuer\": null,\n" +
                "\t\t\"currentStatus\": \"BS_CREATED\",\n" +
                "\t\t\"gmtIssue\": 1537008034000,\n" +
                "\t\t\"gmtCreate\": 1537008034000,\n" +
                "\t\t\"gmtExpectLading\": null,\n" +
                "\t\t\"gmtExpectDelivery\": null,\n" +
                "\t\t\"gmtExpectShipping\": null,\n" +
                "\t\t\"customerId\": \"unlcn\",\n" +
                "\t\t\"customerOrderId\": \"S18091500007\",\n" +
                "\t\t\"orderContact\": null,\n" +
                "\t\t\"orderLocation\": null,\n" +
                "\t\t\"tenantId\": null,\n" +
                "\t\t\"orderItemList\": null,\n" +
                "\t\t\"location\": null,\n" +
                "\t\t\"originLocationSequence\": null,\n" +
                "\t\t\"destLocationSequence\": null,\n" +
                "\t\t\"isDelete\": 0,\n" +
                "\t\t\"transTypeId\": 3\n" +
                "\t}\n" +
                "}";
        JSONObject jo = JSONObject.parseObject(json);
        String shipmentId = jo.getString("customerOrderId");
        String dataStr = jo.getString("data");
        JSONObject data = JSONObject.parseObject(dataStr);
        String shipmentId1 = data.getString("customerOrderId");
    }

    @Test
    public void test2() {
        BigDecimal.valueOf(113.900610000000000354702933691442012786865234375);
        BigDecimal.valueOf(113.900610000000000354702933691442012786865234375D);
        System.out.println(String.format("%.5f", BigDecimal.valueOf(113.900610000000000354702933691442012786865234375)));
    }

    @Test
    public void test3() {
        ArrayList<NameValuePair> headers = Lists.newArrayList();
        BasicNameValuePair pair = new BasicNameValuePair("Authorization", TableStatusEnum.STATUS_1.getCode());
        headers.add(pair);
        //获取tenantId
        EntityWrapper<Object> ew = new EntityWrapper<>();
        ew.eq("id", 26)
                .eq("is_delete", TableStatusEnum.STATUS_0.getCode());
        List<QueryLspInfoDTO> lspInfoDTOS = pushSubSystemMapper.selectLSPinfo(ew);
        if (!CollectionUtils.isEmpty(lspInfoDTOS)) {
            ArrayList<NameValuePair> params = Lists.newArrayList();
            params.add(new BasicNameValuePair("companyName", lspInfoDTOS.get(0).getName()));
            params.add(new BasicNameValuePair("tenantId", String.valueOf(lspInfoDTOS.get(0).getTenantId())));
            String url = properties.getUaaHost() + IntegrationURIEnum.UAA_GET_TOKEN_COMPANY_NAME.getAddress();
            try {
                LOGGER.info("连接uaa获取token url:{},param:{}", url, JSON.toJSONString(params));
                String res = HttpClientUtil.get(url,
                        headers, params, properties.getSocketTimeout());
                if (!StringUtils.isEmpty(res)) {
                    RestfulResponse<String> response = JSON.parseObject(res, new TypeReference<RestfulResponse<String>>() {
                    });
                    if (response != null) {
                       String token = response.getData();
                    }
                }
                LOGGER.info("连接uaa获取token url:{},param:{}:token:{}", url, JSON.toJSONString(params), res);
            } catch (Exception e) {
                LOGGER.error("连接uaa url:{}--->获取token失败,参数:{}", url, JSONObject.toJSONString(params));
            }
        }
    }

}
