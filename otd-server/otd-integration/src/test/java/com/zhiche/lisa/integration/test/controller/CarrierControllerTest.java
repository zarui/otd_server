package com.zhiche.lisa.integration.test.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.IntegrationApp;
import com.zhiche.lisa.integration.dto.carrier.RevokeCheckinDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by zhaoguixin on 2018/8/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IntegrationApp.class)
@ActiveProfiles("local")
public class CarrierControllerTest {


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
    }

    @Test
    public void queryPageOrigination() throws Exception {

        RevokeCheckinDTO revokeCheckinDTO = new RevokeCheckinDTO();
        revokeCheckinDTO.setCheckinId(new Long(1212));
        revokeCheckinDTO.setRevokePerson("李四");
        revokeCheckinDTO.setRevokeReason("车没来");
//        revokeCheckinDTO.setRevokeTime(new Date());

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/carrier/revokeCheckin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON.toJSONString(revokeCheckinDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String strRtn = mvcResult.getResponse().getContentAsString();
        assertThat(strRtn).contains("0");
        RestfulResponse restfulResponse = JSON.parseObject(strRtn,
                new TypeReference<RestfulResponse>() {
                });
        assertThat(restfulResponse.getCode()).isEqualTo(0);
    }

}
