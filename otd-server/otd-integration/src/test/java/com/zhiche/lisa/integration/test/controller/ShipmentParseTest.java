package com.zhiche.lisa.integration.test.controller;

import com.alibaba.fastjson.JSONObject;
import com.zhiche.lisa.core.utils.XmlUtil;
import com.zhiche.lisa.integration.dto.order.ShipmentDTO;
import com.zhiche.lisa.integration.inteface.otm.OtmShipmentService;
import org.dom4j.Document;
import org.dom4j.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ShipmentParseTest {

    @Autowired
    private OtmShipmentService otmShipmentService;

    @Test
    public void testParse() throws Exception {
        String xml = getXml();
        ShipmentDTO shipmentDTO = otmShipmentService.otmShipmentImport(xml);
        System.err.println(JSONObject.toJSONString(shipmentDTO));
        Thread.sleep(50000);
    }

    @Test
    public void testParseEmpty() throws Exception {
        String xml = getEmptyXml();
        Document document = XmlUtil.parseByString(xml);
        Element re = document.getRootElement();
        Element srE = re.element("shipment_returnQuery");
        ShipmentDTO dto = new ShipmentDTO();

        if (srE != null) {
            Element d_loc_zone1 = srE.element("d_loc_zone1");//启运地省
            if (d_loc_zone1 != null) {
                dto.setOriginLocationProvince(d_loc_zone1.getText());
            }
            Element d_loc_zone2 = srE.element("d_loc_zone2");
            if (d_loc_zone2 != null) {
                dto.setOriginLocationCity(d_loc_zone2.getText());
            }
            Element d_loc_zone3 = srE.element("d_loc_zone3");
            if (d_loc_zone3 != null) {
                dto.setOriginLocationCounty(d_loc_zone3.getText());
            }


            Element d_loc_location_gid = srE.element("d_loc_location_gid");//启运地标识
            if (d_loc_location_gid != null) {
                if (d_loc_location_gid.getText().startsWith("ULC/ZC.")) {
                    dto.setOriginLocationId(d_loc_location_gid.getText().replaceAll("ULC/ZC.", ""));
                } else {
                    dto.setOriginLocationId(d_loc_location_gid.getText());
                }
            }
            Element servprov_name = srE.element("servprov_name");//承运商
            if (servprov_name != null) {
                dto.setServiceProviderName(servprov_name.getText());
            }
            //Element attribute10 = srE.element("attribute10");//车队
            //if (attribute10 != null) {
            //    dto.set
            //}
            Element attribute12 = srE.element("attribute12");//车牌
            if (attribute12 != null) {
                dto.setPlateNo(attribute12.getText());
            }
            Element attribute1 = srE.element("attribute1");//指令类型
            if (attribute1 != null) {
                if (attribute1.getText().startsWith("ULC/ZC.")) {
                    dto.setShipmentType(attribute1.getText().replaceAll("ULC/ZC.", ""));
                } else {
                    dto.setShipmentType(attribute1.getText());
                }
            }
            Element transport_type = srE.element("transport_type");//运输方式
            if (transport_type != null) {
                dto.setTransportModeId(transport_type.getText());
            }
            Element attribute7 = srE.element("attribute7");//司机名
            if (attribute7 != null) {
                dto.setDriverName(attribute7.getText());
            }
            Element s_loc_location_gid = srE.element("s_loc_location_gid");
            if (s_loc_location_gid != null) {
                if (s_loc_location_gid.getText().startsWith("ULC/ZC.")) {
                    dto.setDestLocationId(s_loc_location_gid.getText().replaceAll("ULC/ZC.", ""));
                } else {
                    dto.setDestLocationId(s_loc_location_gid.getText());
                }
            }
            Element s_loc_zone1 = srE.element("s_loc_zone1");//目的地-省
            if (s_loc_zone1 != null) {
                dto.setDestLocationProvince(s_loc_zone1.getText());
            }
            Element s_loc_zone2 = srE.element("s_loc_zone2");//目的地-市
            if (s_loc_zone2 != null) {
                dto.setDestLocationCity(s_loc_zone2.getText());
            }
            Element s_loc_zone3 = srE.element("s_loc_zone3");//目的地-区
            if (s_loc_zone3 != null) {
                dto.setDestLocationCounty(s_loc_zone3.getText());
            }
            Element shipment_xid = srE.element("shipment_xid");//指令号
            if (shipment_xid != null) {
                dto.setShipmentId(shipment_xid.getText());
            }
            Element s_loc_location_name = srE.element("s_loc_location_name");
            if (s_loc_location_name != null) {
                dto.setDestLocationName(s_loc_location_name.getText());
            }
            Element servprov_gid = srE.element("servprov_gid");//承运商id
            if (servprov_gid != null) {
                if (servprov_gid.getText().startsWith("ULC/ZC.")) {
                    dto.setServiceProviderId(servprov_gid.getText().replaceAll("ULC/ZC.", ""));
                } else {
                    dto.setServiceProviderId(servprov_gid.getText());
                }
            }
            //Element insert_date = srE.element("insert_date");//调度时间
            //if (insert_date != null) {
            //    dto.set
            //}
        }
    }

    private String getEmptyXml() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<datas>\n" +
                "    <shipment_returnQuery>\n" +
                "        <d_loc_zone1>江西省</d_loc_zone1>\n" +
                "        <d_loc_location_gid>ULC/ZC.南昌发运库1库</d_loc_location_gid>\n" +
                "        <servprov_name>江西路易通达运输有限公司</servprov_name>\n" +
                "        <attribute10>中联重庆队</attribute10>\n" +
                "        <attribute12>赣H86567</attribute12>\n" +
                "        <attribute1>ULC/ZC.空放段</attribute1>\n" +
                "        <transport_type>中置轴</transport_type>\n" +
                "        <attribute7>邓建华</attribute7>\n" +
                "        <s_loc_zone1>广东省</s_loc_zone1>\n" +
                "        <s_loc_zone2>深圳市</s_loc_zone2>\n" +
                "        <d_loc_location_name>南昌发运库1库</d_loc_location_name>\n" +
                "        <s_loc_zone3>龙岗区</s_loc_zone3>\n" +
                "        <shipment_xid>K18121900007</shipment_xid>\n" +
                "        <s_loc_location_name>深圳市湛宝实业发展有限公司</s_loc_location_name>\n" +
                "        <servprov_gid>ULC/ZC.26</servprov_gid>\n" +
                "        <insert_date>2018-12-19</insert_date>\n" +
                "        <d_loc_zone3>南昌县</d_loc_zone3>\n" +
                "        <d_loc_zone2>南昌市</d_loc_zone2>\n" +
                "        <s_loc_location_gid>ULC/ZC.LOC000252</s_loc_location_gid>\n" +
                "    </shipment_returnQuery>\n" +
                "</datas>";
    }


    private String getXml() {

        return "<Transmission\n" +
                "    xmlns=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">\n" +
                "    <otm:TransmissionHeader\n" +
                "        xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\"\n" +
                "        xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">\n" +
                "        <otm:Version>6.4.2</otm:Version>\n" +
                "        <otm:TransmissionCreateDt>\n" +
                "            <otm:GLogDate>20180917181602</otm:GLogDate>\n" +
                "            <otm:TZId>Asia/Shanghai</otm:TZId>\n" +
                "            <otm:TZOffset>+08:00</otm:TZOffset>\n" +
                "        </otm:TransmissionCreateDt>\n" +
                "        <otm:TransactionCount>1</otm:TransactionCount>\n" +
                "        <otm:SenderHostName>http://unlcn-otm-app1:7779</otm:SenderHostName>\n" +
                "        <otm:SenderSystemID>http://unlcn-otm-app1:7779</otm:SenderSystemID>\n" +
                "        <otm:SenderTransmissionNo>2696</otm:SenderTransmissionNo>\n" +
                "        <otm:ReferenceTransmissionNo>0</otm:ReferenceTransmissionNo>\n" +
                "        <otm:GLogXMLElementName>PlannedShipment</otm:GLogXMLElementName>\n" +
                "        <otm:NotifyInfo>\n" +
                "            <otm:ContactGid>\n" +
                "                <otm:Gid>\n" +
                "                    <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                    <otm:Xid>WMS_OUTBOUND_IN</otm:Xid>\n" +
                "                </otm:Gid>\n" +
                "            </otm:ContactGid>\n" +
                "            <otm:ExternalSystemGid>\n" +
                "                <otm:Gid>\n" +
                "                    <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                    <otm:Xid>WMS_OUTBOUND_IN</otm:Xid>\n" +
                "                </otm:Gid>\n" +
                "            </otm:ExternalSystemGid>\n" +
                "        </otm:NotifyInfo>\n" +
                "    </otm:TransmissionHeader>\n" +
                "    <TransmissionBody>\n" +
                "        <GLogXMLElement>\n" +
                "<otm:TransactionHeader xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\" xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"otm:TransactionHeaderType\">\n" +
                "                <otm:SenderTransactionId>2431053</otm:SenderTransactionId>\n" +
                "                <otm:ObjectModInfo>\n" +
                "                    <otm:InsertDt>\n" +
                "                        <otm:GLogDate>20190530090150</otm:GLogDate>\n" +
                "                        <otm:TZId>UTC</otm:TZId>\n" +
                "                        <otm:TZOffset>+00:00</otm:TZOffset>\n" +
                "                    </otm:InsertDt>\n" +
                "                    <otm:UpdateDt>\n" +
                "                        <otm:GLogDate>20190530090150</otm:GLogDate>\n" +
                "                        <otm:TZId>UTC</otm:TZId>\n" +
                "                        <otm:TZOffset>+00:00</otm:TZOffset>\n" +
                "                    </otm:UpdateDt>\n" +
                "                </otm:ObjectModInfo>\n" +
                "                <otm:SendReason>\n" +
                "                    <otm:Remark>\n" +
                "                        <otm:RemarkSequence>1</otm:RemarkSequence>\n" +
                "                        <otm:RemarkQualifierGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:Xid>REM</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:RemarkQualifierGid>\n" +
                "                        <otm:RemarkText></otm:RemarkText>\n" +
                "                    </otm:Remark>\n" +
                "                    <otm:SendReasonGid>\n" +
                "                        <otm:Gid>\n" +
                "                            <otm:Xid>SHIPMENT_EVENT - null</otm:Xid>\n" +
                "                        </otm:Gid>\n" +
                "                    </otm:SendReasonGid>\n" +
                "                    <otm:ObjectType>SHIPMENT</otm:ObjectType>\n" +
                "                </otm:SendReason>\n" +
                "            </otm:TransactionHeader>"+
                "            <otm:TransactionHeader\n" +
                "                xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\"\n" +
                "                xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\"\n" +
                "                xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"otm:TransactionHeaderType\">\n" +
                "                <otm:SenderTransactionId>3978</otm:SenderTransactionId>\n" +
                "                <otm:SendReason>\n" +
                "                    <otm:Remark>\n" +
                "                        <otm:RemarkSequence>1</otm:RemarkSequence>\n" +
                "                        <otm:RemarkQualifierGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:Xid>REM</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:RemarkQualifierGid>\n" +
                "                        <otm:RemarkText></otm:RemarkText>\n" +
                "                    </otm:Remark>\n" +
                "                    <otm:SendReasonGid>\n" +
                "                        <otm:Gid>\n" +
                "                            <otm:Xid>SHIPMENT_EVENT - null</otm:Xid>\n" +
                "                        </otm:Gid>\n" +
                "                    </otm:SendReasonGid>\n" +
                "                    <otm:ObjectType>SHIPMENT</otm:ObjectType>\n" +
                "                </otm:SendReason>\n" +
                "            </otm:TransactionHeader>\n" +
                "            <otm:PlannedShipment\n" +
                "                xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\"\n" +
                "                xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">\n" +
                "                <otm:Shipment>\n" +
                "                    <otm:ShipmentHeader>\n" +
                "                        <otm:ShipmentGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>S18091500005</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:ShipmentGid>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:ServiceProviderGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>85</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:ServiceProviderGid>\n" +
                "                        <otm:TransportModeGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>六位车</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:TransportModeGid>\n" +
                "                        <otm:TotalShipUnitCount>3</otm:TotalShipUnitCount>\n" +
                "                        <otm:StartDt>\n" +
                "                            <otm:GLogDate>20180916140506</otm:GLogDate>\n" +
                "                            <otm:TZId>Asia/Shanghai</otm:TZId>\n" +
                "                            <otm:TZOffset>+08:00</otm:TZOffset>\n" +
                "                        </otm:StartDt>\n" +
                "                        <otm:EndDt>\n" +
                "                            <otm:GLogDate>20180918021441</otm:GLogDate>\n" +
                "                            <otm:TZId>Asia/Shanghai</otm:TZId>\n" +
                "                            <otm:TZOffset>+08:00</otm:TZOffset>\n" +
                "                        </otm:EndDt>\n" +
                "                        <otm:CommercialTerms>\n" +
                "                            <otm:TermLocationText>BS_CREATED</otm:TermLocationText>\n" +
                "                        </otm:CommercialTerms>\n" +
                "                        <otm:FlexFieldStrings>\n" +
                "                            <otm:Attribute1>ULC/ZC.直发段</otm:Attribute1>\n" +
                "                            <otm:Attribute9>赣H8575挂</otm:Attribute9>\n" +
                "                            <otm:Attribute12>赣AQ1717</otm:Attribute12>\n" +
                "                            <otm:Attribute14>ULC/ZC.276</otm:Attribute14>\n" +
                "                        </otm:FlexFieldStrings>\n" +
                "                        <otm:FlexFieldDates>\n" +
                "                            <otm:AttributeDate5>\n" +
                "                                <otm:GLogDate>20180915060506</otm:GLogDate>\n" +
                "                                <otm:TZId>UTC</otm:TZId>\n" +
                "                                <otm:TZOffset>+00:00</otm:TZOffset>\n" +
                "                            </otm:AttributeDate5>\n" +
                "                        </otm:FlexFieldDates>\n" +
                "                    </otm:ShipmentHeader>\n" +
                "                    <otm:ShipmentStop>\n" +
                "                        <otm:StopSequence>1</otm:StopSequence>\n" +
                "                        <otm:LocationRef>\n" +
                "                            <otm:LocationGid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                    <otm:Xid>吉利湘潭发车点</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:LocationGid>\n" +
                "                        </otm:LocationRef>\n" +
                "                    </otm:ShipmentStop>\n" +
                "                    <otm:ShipmentStop>\n" +
                "                        <otm:StopSequence>2</otm:StopSequence>\n" +
                "                        <otm:LocationRef>\n" +
                "                            <otm:LocationGid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                    <otm:Xid>AT007343</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:LocationGid>\n" +
                "                        </otm:LocationRef>\n" +
                "                    </otm:ShipmentStop>\n" +
                "                    <otm:ShipmentStop>\n" +
                "                        <otm:StopSequence>3</otm:StopSequence>\n" +
                "                        <otm:LocationRef>\n" +
                "                            <otm:LocationGid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                    <otm:Xid>南昌发运库1库</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:LocationGid>\n" +
                "                        </otm:LocationRef>\n" +
                "                    </otm:ShipmentStop>\n" +
                "                    <otm:Location>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:LocationGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>85</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:LocationGid>\n" +
                "                        <otm:LocationName>永州市顺远物流有限公司</otm:LocationName>\n" +
                "                        <otm:Address>\n" +
                "                            <otm:CountryCode3Gid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:Xid>CN</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:CountryCode3Gid>\n" +
                "                        </otm:Address>\n" +
                "                    </otm:Location>\n" +
                "                    <otm:Location>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:LocationGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>AT007343</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:LocationGid>\n" +
                "                        <otm:LocationName>赣州德盛汽车贸易有限公司</otm:LocationName>\n" +
                "                        <otm:Description>江西省赣州市赣南大道赣南汽车城内</otm:Description>\n" +
                "                        <otm:Address>\n" +
                "                            <otm:CountryCode3Gid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:Xid>CN</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:CountryCode3Gid>\n" +
                "                            <otm:Zone1>江西省</otm:Zone1>\n" +
                "                            <otm:Zone2>赣州市</otm:Zone2>\n" +
                "                            <otm:Zone3>南康区</otm:Zone3>\n" +
                "                        </otm:Address>\n" +
                "                    </otm:Location>\n" +
                "                    <otm:Location>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:LocationGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>南昌发运库1库</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:LocationGid>\n" +
                "                        <otm:LocationName>南昌发运库1库</otm:LocationName>\n" +
                "                        <otm:Address>\n" +
                "                            <otm:CountryCode3Gid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:Xid>CN</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:CountryCode3Gid>\n" +
                "                            <otm:Zone1>江西省</otm:Zone1>\n" +
                "                            <otm:Zone2>南昌市</otm:Zone2>\n" +
                "                            <otm:Zone3>南昌县</otm:Zone3>\n" +
                "                        </otm:Address>\n" +
                "                    </otm:Location>\n" +
                "                    <otm:Location>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:LocationGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>吉利湘潭发车点</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:LocationGid>\n" +
                "                        <otm:LocationName>吉利湘潭发车点</otm:LocationName>\n" +
                "                        <otm:Description>湖南省湘潭市九华经济技术开发区吉利西路吉利发车现场</otm:Description>\n" +
                "                        <otm:Address>\n" +
                "                            <otm:CountryCode3Gid>\n" +
                "                                <otm:Gid>\n" +
                "                                    <otm:Xid>CN</otm:Xid>\n" +
                "                                </otm:Gid>\n" +
                "                            </otm:CountryCode3Gid>\n" +
                "                            <otm:Zone1>湖南省</otm:Zone1>\n" +
                "                            <otm:Zone2>湘潭市</otm:Zone2>\n" +
                "                            <otm:Zone3>湘潭县</otm:Zone3>\n" +
                "                        </otm:Address>\n" +
                "                    </otm:Location>\n" +
                "                    <otm:Release>\n" +
                "                        <otm:ReleaseGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>OR2018091500043</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:ReleaseGid>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:ReleaseHeader>\n" +
                "                            <otm:ReleaseName>TEST00009</otm:ReleaseName>\n" +
                "                            <otm:CommercialTerms>\n" +
                "                                <otm:TermLocationText>BS_DISPATCH</otm:TermLocationText>\n" +
                "                            </otm:CommercialTerms>\n" +
                "                            <otm:InspectionAndSurveyInfo/>\n" +
                "                            <otm:DutyPaid>NO</otm:DutyPaid>\n" +
                "                            <otm:FlexFieldStrings>\n" +
                "                                <otm:Attribute2>浙江豪情汽车制造有限公司湘潭分公司</otm:Attribute2>\n" +
                "                                <otm:Attribute3>正常订单</otm:Attribute3>\n" +
                "                                <otm:Attribute10>远景</otm:Attribute10>\n" +
                "                            </otm:FlexFieldStrings>\n" +
                "                            <otm:FlexFieldDates>\n" +
                "                                <otm:AttributeDate7>\n" +
                "                                    <otm:GLogDate>20180915060506</otm:GLogDate>\n" +
                "                                    <otm:TZId>UTC</otm:TZId>\n" +
                "                                    <otm:TZOffset>+00:00</otm:TZOffset>\n" +
                "                                </otm:AttributeDate7>\n" +
                "                            </otm:FlexFieldDates>\n" +
                "                        </otm:ReleaseHeader>\n" +
                "                        <otm:ShipFromLocationRef>\n" +
                "                            <otm:LocationRef>\n" +
                "                                <otm:LocationGid>\n" +
                "                                    <otm:Gid>\n" +
                "                                        <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                        <otm:Xid>吉利湘潭发车点</otm:Xid>\n" +
                "                                    </otm:Gid>\n" +
                "                                </otm:LocationGid>\n" +
                "                            </otm:LocationRef>\n" +
                "                        </otm:ShipFromLocationRef>\n" +
                "                        <otm:ShipToLocationRef>\n" +
                "                            <otm:LocationRef>\n" +
                "                                <otm:LocationGid>\n" +
                "                                    <otm:Gid>\n" +
                "                                        <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                        <otm:Xid>南昌发运库1库</otm:Xid>\n" +
                "                                    </otm:Gid>\n" +
                "                                </otm:LocationGid>\n" +
                "                            </otm:LocationRef>\n" +
                "                        </otm:ShipToLocationRef>\n" +
                "                        <otm:TimeWindow>\n" +
                "                            <otm:EarlyPickupDt>\n" +
                "                                <otm:GLogDate>20180916140506</otm:GLogDate>\n" +
                "                                <otm:TZId>Asia/Shanghai</otm:TZId>\n" +
                "                                <otm:TZOffset>+08:00</otm:TZOffset>\n" +
                "                            </otm:EarlyPickupDt>\n" +
                "                            <otm:LateDeliveryDt>\n" +
                "                                <otm:GLogDate>20180916140506</otm:GLogDate>\n" +
                "                                <otm:TZId>Asia/Shanghai</otm:TZId>\n" +
                "                                <otm:TZOffset>+08:00</otm:TZOffset>\n" +
                "                            </otm:LateDeliveryDt>\n" +
                "                            <otm:PickupIsAppt>N</otm:PickupIsAppt>\n" +
                "                            <otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>\n" +
                "                        </otm:TimeWindow>\n" +
                "                    </otm:Release>\n" +
                "                    <otm:Release>\n" +
                "                        <otm:ReleaseGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>OR2018091500044</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:ReleaseGid>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:ReleaseHeader>\n" +
                "                            <otm:ReleaseName>TEST00003</otm:ReleaseName>\n" +
                "                            <otm:CommercialTerms>\n" +
                "                                <otm:TermLocationText>BS_DISPATCH</otm:TermLocationText>\n" +
                "                            </otm:CommercialTerms>\n" +
                "                            <otm:InspectionAndSurveyInfo/>\n" +
                "                            <otm:DutyPaid>NO</otm:DutyPaid>\n" +
                "                            <otm:FlexFieldStrings>\n" +
                "                                <otm:Attribute2>浙江豪情汽车制造有限公司湘潭分公司</otm:Attribute2>\n" +
                "                                <otm:Attribute3>正常订单</otm:Attribute3>\n" +
                "                                <otm:Attribute10>远景</otm:Attribute10>\n" +
                "                            </otm:FlexFieldStrings>\n" +
                "                            <otm:FlexFieldDates>\n" +
                "                                <otm:AttributeDate7>\n" +
                "                                    <otm:GLogDate>20180915053616</otm:GLogDate>\n" +
                "                                    <otm:TZId>UTC</otm:TZId>\n" +
                "                                    <otm:TZOffset>+00:00</otm:TZOffset>\n" +
                "                                </otm:AttributeDate7>\n" +
                "                            </otm:FlexFieldDates>\n" +
                "                        </otm:ReleaseHeader>\n" +
                "                        <otm:ShipFromLocationRef>\n" +
                "                            <otm:LocationRef>\n" +
                "                                <otm:LocationGid>\n" +
                "                                    <otm:Gid>\n" +
                "                                        <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                        <otm:Xid>吉利湘潭发车点</otm:Xid>\n" +
                "                                    </otm:Gid>\n" +
                "                                </otm:LocationGid>\n" +
                "                            </otm:LocationRef>\n" +
                "                        </otm:ShipFromLocationRef>\n" +
                "                        <otm:ShipToLocationRef>\n" +
                "                            <otm:LocationRef>\n" +
                "                                <otm:LocationGid>\n" +
                "                                    <otm:Gid>\n" +
                "                                        <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                        <otm:Xid>AT007343</otm:Xid>\n" +
                "                                    </otm:Gid>\n" +
                "                                </otm:LocationGid>\n" +
                "                            </otm:LocationRef>\n" +
                "                        </otm:ShipToLocationRef>\n" +
                "                        <otm:TimeWindow>\n" +
                "                            <otm:PickupIsAppt>N</otm:PickupIsAppt>\n" +
                "                            <otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>\n" +
                "                        </otm:TimeWindow>\n" +
                "                    </otm:Release>\n" +
                "                    <otm:Release>\n" +
                "                        <otm:ReleaseGid>\n" +
                "                            <otm:Gid>\n" +
                "                                <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                <otm:Xid>OR2018091500045</otm:Xid>\n" +
                "                            </otm:Gid>\n" +
                "                        </otm:ReleaseGid>\n" +
                "                        <otm:TransactionCode>R</otm:TransactionCode>\n" +
                "                        <otm:ReleaseHeader>\n" +
                "                            <otm:ReleaseName>TEST00003</otm:ReleaseName>\n" +
                "                            <otm:CommercialTerms>\n" +
                "                                <otm:TermLocationText>BS_DISPATCH</otm:TermLocationText>\n" +
                "                            </otm:CommercialTerms>\n" +
                "                            <otm:InspectionAndSurveyInfo/>\n" +
                "                            <otm:DutyPaid>NO</otm:DutyPaid>\n" +
                "                            <otm:FlexFieldStrings>\n" +
                "                                <otm:Attribute2>浙江豪情汽车制造有限公司湘潭分公司</otm:Attribute2>\n" +
                "                                <otm:Attribute3>正常订单</otm:Attribute3>\n" +
                "                                <otm:Attribute7>VIN_TEST003</otm:Attribute7>\n" +
                "                                <otm:Attribute10>远景</otm:Attribute10>\n" +
                "                                <otm:Attribute17>QRCODE_XXX</otm:Attribute17>\n" +
                "                            </otm:FlexFieldStrings>\n" +
                "                            <otm:FlexFieldDates>\n" +
                "                                <otm:AttributeDate7>\n" +
                "                                    <otm:GLogDate>20180915053616</otm:GLogDate>\n" +
                "                                    <otm:TZId>UTC</otm:TZId>\n" +
                "                                    <otm:TZOffset>+00:00</otm:TZOffset>\n" +
                "                                </otm:AttributeDate7>\n" +
                "                            </otm:FlexFieldDates>\n" +
                "                        </otm:ReleaseHeader>\n" +
                "                        <otm:ShipFromLocationRef>\n" +
                "                            <otm:LocationRef>\n" +
                "                                <otm:LocationGid>\n" +
                "                                    <otm:Gid>\n" +
                "                                        <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                        <otm:Xid>吉利湘潭发车点</otm:Xid>\n" +
                "                                    </otm:Gid>\n" +
                "                                </otm:LocationGid>\n" +
                "                            </otm:LocationRef>\n" +
                "                        </otm:ShipFromLocationRef>\n" +
                "                        <otm:ShipToLocationRef>\n" +
                "                            <otm:LocationRef>\n" +
                "                                <otm:LocationGid>\n" +
                "                                    <otm:Gid>\n" +
                "                                        <otm:DomainName>ULC/ZC</otm:DomainName>\n" +
                "                                        <otm:Xid>AT007343</otm:Xid>\n" +
                "                                    </otm:Gid>\n" +
                "                                </otm:LocationGid>\n" +
                "                            </otm:LocationRef>\n" +
                "                        </otm:ShipToLocationRef>\n" +
                "                        <otm:TimeWindow>\n" +
                "                            <otm:PickupIsAppt>N</otm:PickupIsAppt>\n" +
                "                            <otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>\n" +
                "                        </otm:TimeWindow>\n" +
                "                    </otm:Release>\n" +
                "                </otm:Shipment>\n" +
                "            </otm:PlannedShipment>\n" +
                "        </GLogXMLElement>\n" +
                "    </TransmissionBody>\n" +
                "</Transmission>";

    }


}
