package com.zhiche.lisa.integration.test.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.IntegrationApp;
import com.zhiche.lisa.integration.dao.model.Origination;
import com.zhiche.lisa.integration.dto.order.OriginationCity;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by zhaoguixin on 2018/8/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IntegrationApp.class)
@ActiveProfiles("dev")
public class OriginationControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
    }

    @Test
    public void queryPageOrigination() throws Exception {

        Page page = new Page();
        Map<String, Object> condition = new HashedMap();
        condition.put("name", "南");
        page.setCondition(condition);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/origination/queryPageOrigination")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON.toJSONString(page))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String strRtn = mvcResult.getResponse().getContentAsString();
        assertThat(strRtn).contains("0");
        RestfulResponse<Page<Origination>> restfulResponse = JSON.parseObject(strRtn,
                new TypeReference<RestfulResponse<Page<Origination>>>() {
                });
        assertThat(restfulResponse.getCode()).isEqualTo(0);
    }

    @Test
    public void queryListOrigination() throws Exception {

        Map<String, Object> condition = new HashedMap();
        condition.put("name", "南");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/origination/queryListOrigination")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON.toJSONString(condition))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String strRtn = mvcResult.getResponse().getContentAsString();
        assertThat(strRtn).contains("0");
        RestfulResponse<List<Origination>> restfulResponse = JSON.parseObject(strRtn,
                new TypeReference<RestfulResponse<List<Origination>>>() {
                });
        assertThat(restfulResponse.getCode()).isEqualTo(0);
    }

    @Test
    public void getOriginationCity() throws Exception {

        Page page = new Page();
        Map<String, Object> condition = new HashedMap();
        condition.put("name", "南");
        page.setCondition(condition);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/origination/getOriginationCity")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("cityName", "南")
//                .content(JSON.toJSONString(page))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String strRtn = mvcResult.getResponse().getContentAsString();
        assertThat(strRtn).contains("0");
        RestfulResponse<List<OriginationCity>> restfulResponse = JSON.parseObject(strRtn,
                new TypeReference<RestfulResponse<List<OriginationCity>>>() {
                });
        assertThat(restfulResponse.getCode()).isEqualTo(0);
//        assertThat(strRtn).contains("0");
//        RestfulResponse<Boolean> restfulResponse = JSON.parseObject(strRtn, new TypeReference<RestfulResponse<Boolean>>(){});
//        assertThat(restfulResponse.getData()).isEqualTo(true);
    }
}
