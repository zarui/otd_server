package com.zhiche.lisa.integration.test.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.common.collect.Maps;
import com.zhiche.lisa.core.supports.RestfulResponse;
import com.zhiche.lisa.integration.IntegrationApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IntegrationApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BMSControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private Filter springSecurityFilterChain;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
    }


    @Test
    public void getPriceAndMiles() throws Exception {
        HashMap<String, String> params = Maps.newHashMap();
        params.put("orderno", "OR2018082400324");
        String res = mockMvc.perform(MockMvcRequestBuilders.post("/bmsIntegration/getPriceAndMiles")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnbXRfY3JlYXRlIjoiMTUzNTk3ODQzODg1NCIsImFjY291bnRJZCI6MTAyLCJ1c2VyX25hbWUiOiIxNTExMDI4ODM4OUAxNjMuY29tIiwic2NvcGUiOlsiYWxsIl0sInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX05PUk1BTCJ9XSwidGVuYW50SWQiOjEsImNvcnBOYW1lIjoi5Lit6IGU54mp5rWB77yI5Lit5Zu977yJ5pyJ6ZmQ5YWs5Y-4IiwiZXhwIjoxNTM1OTg1NjM4LCJhdXRob3JpdGllcyI6WyJST0xFX05PUk1BTCJdLCJqdGkiOiJjN2JjYjM3ZS04ZWViLTRkODctOGRhMy1hZDdmODRmMDNmNTMiLCJjbGllbnRfaWQiOiJkZXZvcHMiLCJ1c2VybmFtZSI6IjE1MTEwMjg4Mzg5QDE2My5jb20ifQ.pzUqj5zKDVcHpKtys6Q-GJ3-VU9D5o23_i6n46FkCEo")
                .content(JSON.toJSONString(params))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString();
        RestfulResponse restfulResponse = JSON.parseObject(res,  new TypeReference<RestfulResponse>() {});
        assertThat(restfulResponse.getCode()).isEqualTo(0);
    }


    @Test
    public void exportPrice() throws Exception {
        HashMap<String, String> params = Maps.newHashMap();
        params.put("orderlineId", "OR2018082800123");
        params.put("oilCost", "150");
        String res = mockMvc.perform(MockMvcRequestBuilders.post("/bmsIntegration/exportPrice")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnbXRfY3JlYXRlIjoiMTUzNTk3ODQzODg1NCIsImFjY291bnRJZCI6MTAyLCJ1c2VyX25hbWUiOiIxNTExMDI4ODM4OUAxNjMuY29tIiwic2NvcGUiOlsiYWxsIl0sInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX05PUk1BTCJ9XSwidGVuYW50SWQiOjEsImNvcnBOYW1lIjoi5Lit6IGU54mp5rWB77yI5Lit5Zu977yJ5pyJ6ZmQ5YWs5Y-4IiwiZXhwIjoxNTM1OTg1NjM4LCJhdXRob3JpdGllcyI6WyJST0xFX05PUk1BTCJdLCJqdGkiOiJjN2JjYjM3ZS04ZWViLTRkODctOGRhMy1hZDdmODRmMDNmNTMiLCJjbGllbnRfaWQiOiJkZXZvcHMiLCJ1c2VybmFtZSI6IjE1MTEwMjg4Mzg5QDE2My5jb20ifQ.pzUqj5zKDVcHpKtys6Q-GJ3-VU9D5o23_i6n46FkCEo")
                .content(JSON.toJSONString(params))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString();
        RestfulResponse restfulResponse = JSON.parseObject(res,  new TypeReference<RestfulResponse>() {});
        assertThat(restfulResponse.getCode()).isEqualTo(0);
    }

}
