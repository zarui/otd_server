package com.zhiche.lisa.integration.test.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Maps;
import com.zhiche.lisa.integration.IntegrationApp;
import com.zhiche.lisa.integration.dao.model.VehicleClass;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IntegrationApp.class)
public class UserControllerTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private String token;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
        //token= uaaUtil.getTenantToken("tech-11");
        token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnbXRfY3JlYXRlIjoiMTUzNjEzODU1MjQxOSIsImFjY291bnRJZCI6MTAyLCJ1c2VyX25hbWUiOiIxNTExMDI4ODM4OUAxNjMuY29tIiwic2NvcGUiOlsiYWxsIl0sInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX05PUk1BTCJ9XSwidGVuYW50SWQiOjEsImNvcnBOYW1lIjoi5Lit6IGU54mp5rWB77yI5Lit5Zu977yJ5pyJ6ZmQ5YWs5Y-4IiwiZXhwIjoxNTM2MTQ1NzUyLCJhdXRob3JpdGllcyI6WyJST0xFX05PUk1BTCJdLCJqdGkiOiIzMWMyNjNjYy0wYmY4LTRkZGQtYjk0Mi05ZWE1OGY3NmRhNjMiLCJjbGllbnRfaWQiOiJkZXZvcHMiLCJ1c2VybmFtZSI6IjE1MTEwMjg4Mzg5QDE2My5jb20ifQ.WSiJn-6QspSFtnvJWWgQ8O_eAeLKiRckyN9mktyOgLw";
    }

    @Test
    public void testQueryVehicleBrandPage() throws Exception {
        Page<VehicleClass> page = new Page<>();
        page.setSize(10);
        page.setCurrent(1);
        Map<String, Object> condition = new HashMap<>();
        condition.put("commonName", "高亮");
        page.setCondition(condition);
        mockMvc.perform(MockMvcRequestBuilders.post("/user/queryUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", token).content(JSONArray.toJSONString(page))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("0")));
    }

    @Test
    public void testQueryVehicleClassName() throws Exception {

        HashMap<String, Object> params = Maps.newHashMap();
        params.put("timeAfter", "1535092905000");
        params.put("name", "sunbw@unlcn.com");
        mockMvc.perform(MockMvcRequestBuilders.post("/user/queryAllUser")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", token).content(JSONArray.toJSONString(params))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("0")));
    }

}
