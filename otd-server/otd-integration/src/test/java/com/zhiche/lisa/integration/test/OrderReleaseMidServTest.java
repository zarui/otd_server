package com.zhiche.lisa.integration.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.config.MybatisPlusConfig;
import com.zhiche.lisa.integration.dto.otd.NewOrderReleaseMidDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@JdbcTest
@Transactional
@Import(OrderReleaseMidReposite.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(locations = { "classpath:mybatisPlus-test.xml" }, classes = { MybatisPlusConfig.class })
public class OrderReleaseMidServTest {
  private final static Logger LOGGER = LoggerFactory.getLogger(OrderReleaseMidReposite.class);

  @Autowired
  OrderReleaseMidReposite repo;

  @Test
  public void shouldFetchSomeRecord() {
    Page<NewOrderReleaseMidDTO> queryOrderReleaseMidNew = repo.queryOrderReleaseMidNew(1638177176000l);
    LOGGER.info(".method: {}", queryOrderReleaseMidNew);
    assertNotNull(queryOrderReleaseMidNew);
  }

}
