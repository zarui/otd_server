package com.zhiche.lisa.integration.test.controller;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Maps;
import com.zhiche.lisa.integration.IntegrationApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.HashMap;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IntegrationApp.class)
public class TransportModeControllerTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private String token;

    @Autowired
    private Filter springSecurityFilterChain;
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();
        //token= uaaUtil.getTenantToken("tech-11");
        token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnbXRfY3JlYXRlIjoiMTUzNTM1MzE4ODgzNyIsImFjY291bnRJZCI6MTAyLCJ1c2VyX25hbWUiOiIxNTExMDI4ODM4OUAxNjMuY29tIiwic2NvcGUiOlsiYWxsIl0sInJvbGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX05PUk1BTCJ9XSwidGVuYW50SWQiOjEsImNvcnBOYW1lIjoi5Lit6IGU54mp5rWB77yI5Lit5Zu977yJ5pyJ6ZmQ5YWs5Y-4IiwiZXhwIjoxNTM1MzYwMzg4LCJhdXRob3JpdGllcyI6WyJST0xFX05PUk1BTCJdLCJqdGkiOiJhNDhjMzJlOC1lNmZhLTQ5YzYtOWQyOS05OTBjYWM2MzI1NWQiLCJjbGllbnRfaWQiOiJkZXZvcHMiLCJ1c2VybmFtZSI6IjE1MTEwMjg4Mzg5QDE2My5jb20ifQ.q-gS5YcYWIZh8nPqaHrGYV-uNCcVTIEbmqGoWL0hWZc";
    }

    @Test
    public void queryListVehicleStandardModeCode() throws Exception {

        HashMap<String, Object> params = Maps.newHashMap();
        params.put("code", "救援车");
        mockMvc.perform(MockMvcRequestBuilders.post("/transportMode/queryTransportModeAll")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", token).content(JSONArray.toJSONString(params))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
