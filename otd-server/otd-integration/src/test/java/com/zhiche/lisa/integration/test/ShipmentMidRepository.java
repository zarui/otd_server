package com.zhiche.lisa.integration.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.lisa.integration.dao.mapper.ShipmentMidMapper;
import com.zhiche.lisa.integration.dto.otd.NewOTDShipmentDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ShipmentMidRepository {
  @Autowired
  ShipmentMidMapper baseMapper;

  public List<NewOTDShipmentDTO> queryPageShipmentMid(long timeStamp) {
    Page<NewOTDShipmentDTO> page = new Page<>();
    page.setCurrent(1);
    page.setSize(10);

    EntityWrapper<NewOTDShipmentDTO> ew = new EntityWrapper<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String format = sdf.format(new Date(timeStamp));
    ew.gt("gmt_modified", format);
    List<NewOTDShipmentDTO> selectShipmentByPageNew = baseMapper.selectShipmentByPageNew(page, ew);
    return selectShipmentByPageNew;
  }
}
