package com.zhiche.lisa.integration.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import com.zhiche.lisa.integration.config.MybatisPlusConfig;
import com.zhiche.lisa.integration.dto.otd.NewOTDShipmentDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JdbcTest
@Import(ShipmentMidRepository.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(locations = { "classpath:mybatisPlus-test.xml" }, classes = { MybatisPlusConfig.class })
public class ShipmentMidTest {

  @Autowired
  ShipmentMidRepository repo;

  @Test
  public void shouldFetchSomething() {
    List<NewOTDShipmentDTO> queryPage = repo.queryPageShipmentMid(1638177176000l);
    assertNotNull(queryPage);
  }
}
