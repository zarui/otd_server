package com.zhiche.lisa.core.test;


import com.zhiche.lisa.core.utils.geo.util.GeoUtil;
import com.zhiche.lisa.core.utils.geo.util.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class GeoUtilTest {

    @Test
    public void testConvertMars2Earth(){
        Point point = new Point(113.923462, 22.521500);
        System.out.println(point);
        Point src_ = GeoUtil.convertMars2Earth(point);
        System.out.println(src_);
    }
}
