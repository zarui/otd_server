package com.zhiche.lisa.core.test;

import com.zhiche.lisa.core.utils.SnowFlakeId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author qichao
 * @create 2018-05-23
 **/
@RunWith(SpringJUnit4ClassRunner.class)
public class SnowFlakeIdTest {

	public Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void genIdTest(){
		SnowFlakeId snowFlakeId = new SnowFlakeId(0,0);
		logger.info("generated id is {}:",snowFlakeId.nextId());
	}

}
