package com.zhiche.lisa.core.enums;

public enum IntegrationEnum {
    EXPORT("y","进行同步"),
    NOEXPORT("n", "不进行同步"),
    ISACTIVE("Y", "有效"),
    NOACTIVE("N", "失效");
    private final String code;
    private final String text;

    IntegrationEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
