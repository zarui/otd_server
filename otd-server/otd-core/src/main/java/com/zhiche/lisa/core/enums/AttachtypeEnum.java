package com.zhiche.lisa.core.enums;

/**
 * @auther lsj
 * @create 2018-05-30
 */
public enum AttachtypeEnum {
    POSITIVE("01","正面"),
    REVERSE("02","反面"),
    HANDHELD("03","手持");

    private final String code;
    private final String text;

    AttachtypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
