package com.zhiche.lisa.core.utils.geo.BaiDuMap.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "baiduMap.routeApi")
public class BaiDuMapRouteConfig {

    private String driveRouterUrl;

    public String getDriveRouterUrl() {
        return driveRouterUrl;
    }

    public void setDriveRouterUrl(String driveRouterUrl) {
        this.driveRouterUrl = driveRouterUrl;
    }

}
