package com.zhiche.lisa.core.enums;

/**
 * 司机证件类型
 * @auther lsj
 * @create 2018-05-24
 */
public enum LicenseTypeEnum {

    DRIVER_ID_CARD("lic01", "身份证"),
    DRIVER_DRIVER_LICENSE("lic02","驾驶证"),
    DRIVER_OPERATING_LICENSE("lic03","从业许可证"),
    VEHICLE_OPERATING_LICENSE("lic09","车辆道路运输许可证"),
    VEHICLE_VEHICLE_LICENSE("lic10","行驶证"),
    CARRIER_LICENCE("lic11","牵引车照"),
    TRAILER_LICENCE("lic12","挂车照"),
    LSP_TAX_ID("lic04","营业执照");

    private final String code;
    private final String text;

    LicenseTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
