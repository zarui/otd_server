package com.zhiche.lisa.core.utils.geo.BaiDuMap.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "baiduMap.placeApi")
public class BaiDuMapPlaceConfig {

    private String regionSearch;

    private String radiusSearch;

    private String boundSearch;

    private String addressSearch;

    private String locationIPUrl;

    public String getRegionSearch() {
        return regionSearch;
    }

    public void setRegionSearch(String regionSearch) {
        this.regionSearch = regionSearch;
    }

    public String getRadiusSearch() {
        return radiusSearch;
    }

    public void setRadiusSearch(String radiusSearch) {
        this.radiusSearch = radiusSearch;
    }

    public String getBoundSearch() {
        return boundSearch;
    }

    public void setBoundSearch(String boundSearch) {
        this.boundSearch = boundSearch;
    }

    public String getAddressSearch() {
        return addressSearch;
    }

    public void setAddressSearch(String addressSearch) {
        this.addressSearch = addressSearch;
    }

    public String getLocationIPUrl() {
        return locationIPUrl;
    }

    public void setLocationIPUrl(String locationIPUrl) {
        this.locationIPUrl = locationIPUrl;
    }
}
