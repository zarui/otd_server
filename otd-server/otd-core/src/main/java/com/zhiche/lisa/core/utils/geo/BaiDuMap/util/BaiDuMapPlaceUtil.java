package com.zhiche.lisa.core.utils.geo.BaiDuMap.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.HttpClientUtil;
import com.zhiche.lisa.core.utils.geo.BaiDuMap.config.BaiDuMapCoreConfig;
import com.zhiche.lisa.core.utils.geo.BaiDuMap.config.BaiDuMapPlaceConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaiDuMapPlaceUtil {

    private static final Logger logger = LoggerFactory.getLogger(BaiDuMapPlaceUtil.class);

    private static int socketTimeout = 10000;

    //静态注入的方式
    private static BaiDuMapCoreConfig baiDuMapCoreConfig;

    private static BaiDuMapPlaceConfig baiDuMapPlaceConfig;


    @Autowired
    public void setBaiDuMapCoreConfig(BaiDuMapCoreConfig baiDuMapCoreConfig) {
        BaiDuMapPlaceUtil.baiDuMapCoreConfig = baiDuMapCoreConfig;
    }

    @Autowired
    public void setBaiDuMapGeoConfig(BaiDuMapPlaceConfig baiDuMapPlaceConfig) {
        BaiDuMapPlaceUtil.baiDuMapPlaceConfig = baiDuMapPlaceConfig;
    }



    /**
     * 行政区划区域检索
     * @param query
     * @param tag
     * @param region
     * @param scope
     * @return 检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息
     * @throws BaseException
     * regionSearch: "http://api.map.baidu.com/place/v2/search?query=%s&tag=%s&region=%s&scope=%s&output=json&ak=%s"
     */
    public static JSONObject searchPlaceByRegion(String query, String tag, String region, String scope) throws BaseException {
        JSONObject jsonObject = null;
        try{
            tag = StringUtils.isEmpty(tag)?"":tag;
            scope = StringUtils.isEmpty(scope)?"":scope;
            String regionSearchUrl = String.format(baiDuMapPlaceConfig.getRegionSearch(),query,tag,region,scope,baiDuMapCoreConfig.getApiKey());
            String strReturn = HttpClientUtil.get(regionSearchUrl,null,socketTimeout);
            jsonObject = JSON.parseObject(strReturn);
        }
        catch (Exception ex){
            logger.error("searchPlaceByRegion ERROR:{}",ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        return jsonObject;
    }

    /**
     * 周边检索
     * @param query
     * @param tag
     * @param location
     * @param redius
     * @param scope
     * @return
     * @throws BaseException
     * radiusSearch: "http://api.map.baidu.com/place/v2/search?query=%s&tag=%s&location=%s&radius=%s&scope=%s&output=json&ak=%s"
     */
    public static JSONObject searchPlaceByRadius(String query, String tag, String location, String redius, String scope) throws BaseException{
        JSONObject jsonObject = null;
        try{
            tag = StringUtils.isEmpty(tag)?"":tag;
            scope = StringUtils.isEmpty(scope)?"":scope;
            String regionSearchUrl = String.format(baiDuMapPlaceConfig.getRadiusSearch(),query,tag,location,redius,scope,baiDuMapCoreConfig.getApiKey());
            String strReturn = HttpClientUtil.get(regionSearchUrl,null,socketTimeout);
            jsonObject = JSON.parseObject(strReturn);
        }
        catch (Exception ex){
            logger.error("searchPlaceByRadius ERROR:{}",ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        return jsonObject;
    }

    /**
     * 矩形区域检索
     * @param query
     * @param tag
     * @param bounds
     * @param scope
     * @return
     * @throws BaseException
     * boundSearch:  "https://api.map.baidu.com/place/v2/search?query=%s&bounds=%s&scope=%s&output=json&ak=%s"
     */
    public static JSONObject searchPlaceByBound(String query, String tag, String bounds, String scope) throws BaseException{
        JSONObject jsonObject = null;
        try{
            tag = StringUtils.isEmpty(tag)?"":tag;
            scope = StringUtils.isEmpty(scope)?"":scope;
            String regionSearchUrl = String.format(baiDuMapPlaceConfig.getBoundSearch(),query,tag,bounds,scope,baiDuMapCoreConfig.getApiKey());
            String strReturn = HttpClientUtil.get(regionSearchUrl,null,socketTimeout);
            jsonObject = JSON.parseObject(strReturn);
        }
        catch (Exception ex){
            logger.error("searchPlaceByBound ERROR:{}",ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        return jsonObject;
    }

    /**
     * 地点详情检索服务
     * @param uids uid的集合，最多可以传入10个uid，多个uid之间用英文逗号分隔。
     * @param scope
     * @return
     * @throws BaseException
     */
    public static JSONObject searchPlaceByAddress(String uids, String scope) throws BaseException {
        JSONObject jsonObject = null;
        try{
            scope = StringUtils.isEmpty(scope)?"":scope;
            String regionSearchUrl = String.format(baiDuMapPlaceConfig.getAddressSearch(),uids,scope,baiDuMapCoreConfig.getApiKey());
            String strReturn = HttpClientUtil.get(regionSearchUrl,null,socketTimeout);
            jsonObject = JSON.parseObject(strReturn);
        }
        catch (Exception ex){
            logger.error("searchPlaceByAddress ERROR:{}",ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        return jsonObject;
    }

    /**
     * 根据IP获取位置
     * @param ip
     * @return
     * @throws BaseException
     */
    public static String getLocationByIP(String ip) throws BaseException {
        String  address = null;
        try{
            String locationIP = String.format(baiDuMapPlaceConfig.getLocationIPUrl(),ip,baiDuMapCoreConfig.getApiKey());
            String strReturn = HttpClientUtil.get(locationIP,null,socketTimeout);
            JSONObject jsonObject = JSON.parseObject(strReturn);
            int status = jsonObject.getInteger("status");
            String msg = BaiDuMapStatusEnum.getTest(status);
            if(status == 0){
                address = jsonObject.getJSONObject("content").getString("address");
            }
            else{
                throw new BaseException(msg);
            }
        }
        catch (Exception ex){
            logger.error("getLocationByIP ERROR:{}",ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        return address;
    }

}
