package com.zhiche.lisa.core.enums;

/**
 * 证件类型-主体类型
 * @auther lsj
 * @create 2018-05-30
 */
public enum SubjectTypeEnum {
    DRIVER("01", "司机"),
    VEHICLE("04","车辆"),
    LSP("03","承运商");

    private final String code;
    private final String text;

    SubjectTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
