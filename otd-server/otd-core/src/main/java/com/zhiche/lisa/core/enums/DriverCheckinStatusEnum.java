package com.zhiche.lisa.core.enums;

/**
 * 司机报班状态
 * @auther lsj
 * @create 2018-06-08
 */
public enum DriverCheckinStatusEnum {

    CANCEL_APPLY("10","申请取消"),
    CANCEL("20","已取消"),
    ARRANGED("30","已安排"),
    NOT_ARRANGED("40","未安排"),
    FAILURE("50","已失效");

    private final String code;
    private final String text;

    DriverCheckinStatusEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
