package com.zhiche.lisa.core.enums.exception;

/**
 * 承运商异常枚举（1000-2000）
 * @since 2018-05-23
 * @author lbl
 */
public enum LspEnum {

    LSP_NAME_EXIST(1000, "承运商名称已经存在"),
    LSP_NAMEABBR_EXIST(1001, "承运商简称已经存在"),
    LSP_HAVE_CARRIER(1002, "承运商已经绑定有牵引车");

    private final Integer code;
    private final String text;

    LspEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
