package com.zhiche.lisa.core.utils;

import org.joda.time.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lenovo on 2018/5/30.
 */
public class JodaUtil {

    /**
     * 计算两时间差（秒）
     * @param startDate
     * @param endDate
     * @return
     */
    public static Integer differSecond(Date startDate, Date endDate) {
        DateTime dt1 = new DateTime(startDate);
        DateTime dt2 = new DateTime(endDate);
        return Seconds.secondsBetween(dt1, dt2).getSeconds();
    }
}
