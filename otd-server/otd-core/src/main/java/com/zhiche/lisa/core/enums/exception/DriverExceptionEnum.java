package com.zhiche.lisa.core.enums.exception;

/**
 * 司机exception
 * @auther lsj
 * @create 2018-05-25
 */
public enum DriverExceptionEnum {

    DRIVER_MOBLIE_IS_NULL(2000, "司机手机号不能为空"),
    WEIXINID_IS_NULL(2001,"微信信息不能为空"),
    MOBILE_FORMAT_ERROR(2002,"手机号码格式不正确"),
    DRIVER_MOBLIE_EXIST(2003,"该手机号已注册司机"),
    DRIVER_INFO_NOT_EXIST(2004,"司机信息不存在"),
    DRIVER_CHEKIN_INFO_NOT_EXIST(2005,"司机报班信息不存在"),
    DRIVER_CHEKIN_CANCEL_NO_PERMISSIONS(2006,"你没有权限取消该报班"),
    DRIVER_ID_IS_NULL(2007,"司机id不能为空"),
    DRIVER_CANCEL_CHECKIN_ERROR(2008,"只能取消未安排的报班"),
    DRIVER_CHECKIN_STATUS_ERROR(209,"报班状态数据不正确"),
    DRIVER_CHECKIN_ONLY_ONE(2010,"已存在报班信息");

    private final Integer code;
    private final String text;

    DriverExceptionEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
