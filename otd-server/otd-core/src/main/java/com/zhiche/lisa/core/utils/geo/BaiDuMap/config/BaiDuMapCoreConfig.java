package com.zhiche.lisa.core.utils.geo.BaiDuMap.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author liangpeng
 *
 */
@Configuration
@ConfigurationProperties(prefix = "baiduMap")
@EnableConfigurationProperties({BaiDuMapGeoConfig.class,BaiDuMapPlaceConfig.class,BaiDuMapRouteConfig.class})
public class BaiDuMapCoreConfig {

    private String apiKey;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
