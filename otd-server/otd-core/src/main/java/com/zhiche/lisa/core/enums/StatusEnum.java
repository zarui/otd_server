package com.zhiche.lisa.core.enums;

/**
 * 状态
 * @since 2018-05-30
 * @author lbl
 */
public enum StatusEnum {

    DISCONTINUAT(0, "停用"),
    ENABLE(1, "启用");

    private final Integer code;
    private final String text;

    StatusEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
