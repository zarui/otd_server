package com.zhiche.lisa.core.enums;

/**
 * 常用枚举（0-1000）
 * @since 2018-05-23
 * @author lbl
 */
public enum CommonEnum {

    SUCCESS(0, "操作成功"),
    ERROR(-1, "操作失败"),
    ERRORLIST(-2, "操作失败");

    private final Integer code;
    private final String text;

    CommonEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
