package com.zhiche.lisa.core.enums;

/**
 * 状态
 * @since 2018-05-30
 * @author lbl
 */
public enum SourceEnum {

    WEIXIN("WEIXIN", "运力微信公众号"),
    LSPM("LSPM", "运力管理");

    private final String code;
    private final String text;

    SourceEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
