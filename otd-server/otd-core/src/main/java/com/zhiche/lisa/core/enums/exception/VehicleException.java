package com.zhiche.lisa.core.enums.exception;

/**
 * 车辆 exception
 * @auther lsj
 * @create 2018-05-28
 */
public enum VehicleException {

    CARRIER_PLATE_IS_NULL(3000, "牵引车车牌号不能为空"),
    TRAILER_PLATE_IS_NULL(3001, "挂车车牌号不能为空"),
    CARRIER_PLATE_EXISTS(3002,"牵引车车牌号已存在"),
    TRAILER_PLATE_EXISTS(3003,"挂车车牌号已存在"),
    DRIVER_PLATE_EXISTS(3004,"司机已绑定其他车牌号");

    private final Integer code;
    private final String text;

    VehicleException(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
