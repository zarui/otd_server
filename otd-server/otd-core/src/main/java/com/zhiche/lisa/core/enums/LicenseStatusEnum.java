package com.zhiche.lisa.core.enums;

/**
 * 状态
 * @since 2018-05-30
 * @author lbl
 */
public enum LicenseStatusEnum {

    INVALID(0, "无效"),
    VALID(1, "有效");

    private final Integer code;
    private final String text;

    LicenseStatusEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
