package com.zhiche.lisa.core.enums;

public enum FleetResourcesEnum {
	CARRIER("01","牵引车"),
	TRAILER("02", "挂车"),
	DRIVER("03", "司机"),
    PEOPLE_SEND_DRIVER("04","人送司机");

    private final String code;
    private final String text;

    FleetResourcesEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
