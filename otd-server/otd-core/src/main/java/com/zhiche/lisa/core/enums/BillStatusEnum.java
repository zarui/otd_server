package com.zhiche.lisa.core.enums;

/**
 * @Author: liuanshun
 * @Description:
 * @Date: Create in 9:20 2018/9/7
 */

public enum BillStatusEnum {
    UNCHECKBILL("10","未对账"),
    ALREADYEXPENDITURE("20","已出账"),
    ACKNOWLEDGED("30","已确认"),
    ALREADYSETTLED("40","已结算"),
    CLEAN("50","已取消");

    private final String code;
    private final String text;

    BillStatusEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
