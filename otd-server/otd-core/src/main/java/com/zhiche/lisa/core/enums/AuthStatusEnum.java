package com.zhiche.lisa.core.enums;

/**
 * 认证状态
 * @auther lsj
 * @create 2018-06-12
 */
public enum AuthStatusEnum {

    UNAUTHORIZED(0,"未认证"),
    CERTIFICATION(1,"认证");

    private final Integer code;
    private final String text;

    AuthStatusEnum(Integer code, String text){
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
