package com.zhiche.lisa.core.enums;

public enum TractorTypeEnum {
    TRACTORVEHICLE(10,"牵引车挂车"),
    RECOVERYVEHICLE(20, "救援车");

    private final Integer code;
    private final String text;

    TractorTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
