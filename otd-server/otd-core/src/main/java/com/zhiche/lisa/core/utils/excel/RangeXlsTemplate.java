package com.zhiche.lisa.core.utils.excel;

import java.io.Serializable;

/**
 * 距离excel模版数据实体
 *
 * @author qichao
 * @create 2018-06-28
 **/
public class RangeXlsTemplate implements Serializable {

	@IsNeeded
	private String originProvince;

	@IsNeeded
	private String originCity;

	@IsNeeded
	private String originCounty;

	@IsNeeded
	private String originAddress;


	@IsNeeded
	private String destProvince;

	@IsNeeded
	private String destCity;

	@IsNeeded
	private String destCounty;

	@IsNeeded
	private String destAddress;

	@IsNeeded
	private String baiduRange;

	public String getOriginProvince() {
		return originProvince;
	}

	public void setOriginProvince(String originProvince) {
		this.originProvince = originProvince;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginCounty() {
		return originCounty;
	}

	public void setOriginCounty(String originCounty) {
		this.originCounty = originCounty;
	}

	public String getOriginAddress() {
		return originAddress;
	}

	public void setOriginAddress(String originAddress) {
		this.originAddress = originAddress;
	}

	public String getDestProvince() {
		return destProvince;
	}

	public void setDestProvince(String destProvince) {
		this.destProvince = destProvince;
	}

	public String getDestCity() {
		return destCity;
	}

	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}

	public String getDestCounty() {
		return destCounty;
	}

	public void setDestCounty(String destCounty) {
		this.destCounty = destCounty;
	}

	public String getDestAddress() {
		return destAddress;
	}

	public void setDestAddress(String destAddress) {
		this.destAddress = destAddress;
	}

	public String getBaiduRange() {
		return baiduRange;
	}

	public void setBaiduRange(String baiduRange) {
		this.baiduRange = baiduRange;
	}
}
