package com.zhiche.lisa.core.enums;

/**
 * 四大家
 */
public enum FourPeopleEnum {

    LYTD("江西路易通达运输有限公司"),
    JST("景德镇久顺途运输有限公司"),
    YJ("鹰潭运捷运输有限公司"),
    JX("江西佳迅运输有限公司");

    private final String text;

    FourPeopleEnum(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
