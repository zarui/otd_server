package com.zhiche.lisa.core.utils.Account;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
public class JwtAccountVO {
    private String accountId;
    private String username;
    private String gmtCreate;
    private String gmtExp;
    private String tenantId;
    private String corpName;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtExp() {
        return gmtExp;
    }

    public void setGmtExp(String gmtExp) {
        this.gmtExp = gmtExp;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    @Override
    public String toString() {
        return "JwtAccountVO{" +
                "accountId='" + accountId + '\'' +
                ", username='" + username + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", gmtExp='" + gmtExp + '\'' +
                ", tenantId='" + tenantId + '\'' +
                ", corpName='" + corpName + '\'' +
                '}';
    }
}
