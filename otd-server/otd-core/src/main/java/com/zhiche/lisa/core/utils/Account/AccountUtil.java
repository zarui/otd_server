package com.zhiche.lisa.core.utils.Account;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
public class AccountUtil {
    public static JwtAccountVO getAccountInfoFromSecurityContext() {
        JwtAccountVO jwtAccountVO =null;
        Authentication principal = SecurityContextHolder.getContext().getAuthentication();
        if (principal instanceof OAuth2Authentication) {
            OAuth2Authentication authentication = (OAuth2Authentication) principal;
            Object details = authentication.getDetails();
            if (details instanceof OAuth2AuthenticationDetails) {
                OAuth2AuthenticationDetails oauthsDetails = (OAuth2AuthenticationDetails) details;
                String token = oauthsDetails.getTokenValue();
                jwtAccountVO = JwtTokenUtil.decodeJwt(token);
            }
        }
        return jwtAccountVO;
    }
}
