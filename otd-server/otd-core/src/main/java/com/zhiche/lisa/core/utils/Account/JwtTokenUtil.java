package com.zhiche.lisa.core.utils.Account;

import com.alibaba.fastjson.JSON;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

import java.util.Map;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/7/23.
 */
public class JwtTokenUtil {

    public static JwtAccountVO decodeJwt(String token) {
        JwtAccountVO jwtAccountVO = new JwtAccountVO();
        Jwt jwt = JwtHelper.decode(token);
        String claimsStr = jwt.getClaims();
        Map<String, Object> claims = JSON.parseObject(claimsStr, Map.class);
        jwtAccountVO.setGmtCreate(claims.get("gmt_create").toString()); //创建时间(unix时间戳)
        jwtAccountVO.setUsername(claims.get("user_name").toString());//用户名
        jwtAccountVO.setGmtExp(claims.get("exp").toString()); //本token何时过期
        jwtAccountVO.setAccountId(claims.get("accountId").toString()); // 账号ID
        jwtAccountVO.setTenantId(claims.get("tenantId").toString()); //租户ID
        jwtAccountVO.setCorpName(Objects.isNull(claims.get("corpName")) ? "" : claims.get("corpName").toString());//公司名称
        return jwtAccountVO;

    }
}
