package com.zhiche.lisa.core.enums;

public enum InterfaceEventEnum {
    BS_WMS_IN("01", "BS_WMS_IN", "已入库"),
    BS_CREATED("01", "BS_CREATED", "已调度"),
    BS_OP_DELIVERY("02", "BS_OP_DELIVERY", "已发运"),
    BS_TRANS_ARRIVED("03", "BS_TRANS_ARRIVED", "已运抵"),
    OR_FIND("04", "OR_FIND", "已寻车"),
    BS_ENROUTED("05", "BS_ENROUTED", "在途"),
    BINDING_CODE("06", "BINDING_CODE", "绑码"),
    ABNORMAL("07", "ABNORMAL", "异常");

    private final String index;
    private final String code;
    private final String desc;

    InterfaceEventEnum(String index, String code, String desc) {
        this.index = index;
        this.code = code;
        this.desc = desc;
    }

    public String getIndex() {
        return index;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
