package com.zhiche.lisa.core.utils.geo.BaiDuMap.util;

public enum BaiDuMapStatusEnum {

    OK(0, "ok"),
    INNER_ERROR(1, "服务器内部错误"),
    PARAMETER_ERROR(2, "请求参数非法,必要参数拼写错误或漏传"),
    AUTHORITY_ERROR(3, "权限校验失败"),
    CALLTIMES_ERROR(4, "配额校验失败,服务当日调用次数已超限"),
    AK_ERROR(5, "ak不存在或者非法"),
    SERVER_ERROR(101, "服务禁用"),
    SN_ERROR(102, "不通过白名单或者安全码不对"),
    NOAUTH_ERROR(200, "无权限"),
    CALL_ERROR(300, "配额错误");

    private final String text;
    private final int value;

    BaiDuMapStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param value
     * @return
     */
    public static BaiDuMapStatusEnum getByValue(int value) {
        for (BaiDuMapStatusEnum temp : BaiDuMapStatusEnum.values()) {
            if (temp.getValue() == value) {
                return temp;
            }
        }
        return null;
    }


    /**
     * 得到错误信息
     * @param value
     * @return
     */
    public static String getTest(int value){
        String msg = null;
        if(value >=200 && value < 300) {
            msg = BaiDuMapStatusEnum.NOAUTH_ERROR.getText();
        }
        else if(value >= 300 && value <400 ){
            msg = BaiDuMapStatusEnum.CALL_ERROR.getText();
        }
        else{
            msg = BaiDuMapStatusEnum.getByValue(value).getText();
        }
        return msg;
    }
}
