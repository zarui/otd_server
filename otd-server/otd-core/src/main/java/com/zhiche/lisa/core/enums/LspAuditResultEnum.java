package com.zhiche.lisa.core.enums;

/**
 * 承运商审核结果
 * @since 2018-05-29
 * @author lbl
 */
public enum LspAuditResultEnum {

    NO_PASS(0, "取消认证"),
    PASS(1, "认证通过");

    private final Integer code;
    private final String text;

    LspAuditResultEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
