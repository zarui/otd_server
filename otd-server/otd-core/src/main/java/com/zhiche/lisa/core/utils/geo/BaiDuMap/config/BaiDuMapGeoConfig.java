package com.zhiche.lisa.core.utils.geo.BaiDuMap.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "baiduMap.geocoderApi")
public class BaiDuMapGeoConfig {

    private String gpsToAddressUrl;

    private String geoconvUrl;

    private String addressToGPSUrl;

    public String getGpsToAddressUrl() {
        return gpsToAddressUrl;
    }

    public void setGpsToAddressUrl(String gpsToAddressUrl) {
        this.gpsToAddressUrl = gpsToAddressUrl;
    }

    public String getGeoconvUrl() {
        return geoconvUrl;
    }

    public void setGeoconvUrl(String geoconvUrl) {
        this.geoconvUrl = geoconvUrl;
    }

    public String getAddressToGPSUrl() {
        return addressToGPSUrl;
    }

    public void setAddressToGPSUrl(String addressToGPSUrl) {
        this.addressToGPSUrl = addressToGPSUrl;
    }
}
