package com.zhiche.lisa.core.enums;

public enum TableStatusEnum {

    STATUS_0("0", "状态0的标识"),
    STATUS_01("01", "状态01的标识"),
    STATUS_02("02", "状态02的标识"),
    STATUS_03("03", "状态03的标识"),
    STATUS_04("04", "状态04的标识"),
    STATUS_05("05", "状态05的标识"),
    STATUS_1("1", "状态1的标识"),
    STATUS_2("2", "状态2的标识"),
    STATUS_3("3", "状态3的标识"),
    STATUS_4("4", "状态4的标识"),
    STATUS_10("10", "状态10的标识"),
    STATUS_20("20", "状态20的标识"),
    STATUS_30("30", "状态30的标识"),
    STATUS_40("40", "状态40的标识"),
    STATUS_50("50", "状态50的标识"),
    STATUS_FALSE("false", "状态50的标识"),
    STATUS_TRUE("true", "状态50的标识"),
    STATUS_Y("y", "状态y的标识"),
    STATUS_N("n", "状态n的标识"),
    STATUS_CHECK_IN_IU("IU", "状态IU的标识"),
    STATUS_CHECK_IN_RC("RC", "状态RC的标识"),
    STATUS_CARD_100("100", "油卡code"),
    STATUS_CARD_200("200", "路桥卡code"),
    STATUS_HUIYUNCHE_SYS("huiyunche", "慧运车人送系统customerid"),
    STATUS_HUIYUNCHE_USEDCAR("huiyuncheUsedCar", "慧运车二手车"),
    STATUS_HUIYUNCHE_51("51", "慧运车标识"),
    STATUS_FLEET("119978066775638016", "中联提车队");
    private final String code;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    TableStatusEnum(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }

}
