package com.zhiche.lisa.core.enums;

public enum IntegrationURIEnum {
    SHIPMENT_PUSH("10", "10", "OTM指令推送子系统"),
    EMPTY_SHIPMENT_PUSH("23", "23", "OTM空放指令推送子系统"),
    UAA_GET_TOKEN_COMPANY_NAME("GET_TOKEN_COMPANY_NAME", "/tenant/getTokenByTenatIdOrCompanyName", "UAA 根据公司名换取TOKEN"),
    UAA_GET_TOKEN_DRIVER_MOBILE("GET_TOKEN_COMPANY_NAME", "/tenant/getTokenByMobile", "UAA 根据司机手机号换取TOKEN"),
    CANCEL_TRUCK_PUSH("30", "30", "取消报班推送运力平台"),
    PROCESS_RESULT_URI("51", "/event/getProcessResult", "integration得到异步处理结果"),
    EVENT_URI("201", "/event/export", "提车/寻车/移车/入库/出库/发运/运抵任务回传OTM接口地址"),
    TMS_SHIP_URI("205", "/tmsAPI/shipDeliver", "公众号司机发运"),
    ERP_CARRIER("801", "801", "ERP自有车队-牵引车定义"),
    ERP_TRAILER("802", "802", "ERP自有车队-挂车定义"),
    ERP_DRIVER("803", "803", "ERP自有车队-司机定义"),
    ERP_SUPPLIER("804", "804", "ERP自有车队-供方(车队)定义"),
    TMS_DELIVER("205", "205", "TMS公众号发运对接OTM"),
    OTM_PRICE_MILES("202", "202", "OTM获取价格和里程(BMS)"),
    EXPORT_PRICE("203", "203", "回写结算价格(BMS)"),
    SHIP_TMS("204", "204", "发运WMS->TMS接口"),
    OTD_CRSC("207", "207", "OTD系统获取铁运在途信息"),
    SHIP_TO_PROV("301", "301", "运抵-->无车承运人上报省平台"),
    SYS_JUNMA_MES("701", "JUNMA_MES", "君马MES系统"),
    SYS_JUNMA_DCS("702", "JUNMA_DCS", "君马DCS系统"),
    SYS_JUNMA_SAP("703", "JUNMA_SAP", "君马SAP系统"),
    PAY_FLEET("501", "501", "车队信息推送PAY"),
    USED_CARS("206", "206", "二手车推送OTM"),
    UPDATE_VEHICEL("208", "208", "修改车型推送OTM");
    private final String code;
    private final String address;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    public String getAddress() {
        return address;
    }

    IntegrationURIEnum(String code, String address, String detail) {
        this.code = code;
        this.address = address;
        this.detail = detail;
    }

}
