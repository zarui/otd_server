package com.zhiche.lisa.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 常见正则表达式
 *
 * @author qichao
 * @create 2018-05-23
 **/
public class RegexUtil {
	/**
	 * 正则表达式：验证邮箱
	 */
	public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
	/**
	 * 正则表达式：验证汉字
	 */
	public static final String REGEX_CHINESE = "^[\u4e00-\u9fa5],{0,}$";

	public static final String REGEX_UNICODECHAR="^[\\w\\u4e00-\\u9fa5（）()]*$";
	/**
	 * 正则表达式：验证身份证
	 */
	public static final String REGEX_ID_CARD = "(^\\d{18}$)|(^\\d{15}$)";

	/**
	 * 正则表达式：验证URL
	 */
	public static final String REGEX_URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";

	/**
	 * 正则表达式：验证IP地址
	 */
	public static final String REGEX_IP_ADDR = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";

	/**
	 * 正则表达式：验证手机号
	 */
	public static final String REGEX_MOBILE = "^(1(([345879][0-9])|(4[57])|(77)))\\d{8}$";

	// 6-14位数字和字母组合密码
	public static final String reg = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,14}$";

	public static final String NUMBER = "^[0-9]+$";

	// 商户版账号规则
	public static final String ACCOUNT_RULES = "^[A-Z|a-z|0-9|_]+$";

	// 字母数字
	public static final String letter_number = "^[0-9A-Za-z]*$";

	// 两位小数的正数（非0开头）
	public static final String number_decimals = "^([0-9]*)+(.[0-9]{1,2})?$";

	// 牵引车车牌号
	public static final String tractorNumber = "^[\u4E00-\u9FA5]{1}[A-Za-z]{1}[\\dA-Za-z]{5}$";

	// 挂车车牌号
	public static final String trailerNumber = "^[\u4E00-\u9FA5|WJ][A-Za-z][\\dA-Za-z]{4}[\u4e00-\u9fa5]{1}$";

	// 校验表达式中不能存在特殊字符
	public static final String special = "^[\u4E00-\u9FA5|A-Z|a-z|0-9|#|.]+$";

	public static final String specialTag = "^[\u4E00-\u9FA5|A-Z|a-z|0-9|#|.|-]+$";

	public static boolean check(String rule, String data) {
		Pattern regex = Pattern.compile(rule);
		Matcher matcher = regex.matcher(data);
		return matcher.matches();
	}
}
