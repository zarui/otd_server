package com.zhiche.lisa.core.enums.exception;

/**
 * 承运商异常枚举（2000-3000）
 * @since 2018-06-06
 * @author lbl
 */
public enum CarrierEnum {

    PLATE_EXIST(2000, "车牌号已经存在"),
    DRIVER_EXIST(2001, "司机已经绑定牵引车");

    private final Integer code;
    private final String text;

    CarrierEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }
}
