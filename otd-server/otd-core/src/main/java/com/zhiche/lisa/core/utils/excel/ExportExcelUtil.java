package com.zhiche.lisa.core.utils.excel;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

/**
 * @author qichao
 * @create 2018-06-30
 **/
public class ExportExcelUtil {

	//标题
	private String title;
	//列
	private String[] columns;

	private List<RangeXlsTemplate> dataList;

	private String path;

	//构造方法，传入要导出的数据
	public ExportExcelUtil(String title, String[] columns, List<RangeXlsTemplate> dataList, String
			path) {
		this.dataList = dataList;
		this.columns = columns;
		this.title = title;
		this.path = path;
	}

	/*
	 * 导出数据
	 * */
	public void export() throws Exception {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();                        // 创建工作簿对象
			HSSFSheet sheet = workbook.createSheet(title);                     // 创建工作表

			// 产生表格标题行
			HSSFRow titleRow = sheet.createRow(0);
			HSSFCell cellTiltle = titleRow.createCell(0);

			titleRow.setHeight((short) (25 * 30)); //设置高度

			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, (columns.length - 1)));
			cellTiltle.setCellValue(title);

			// 定义所需列数
			int columnNum = columns.length;
			HSSFRow headerRow = sheet.createRow(2);

			headerRow.setHeight((short) (25 * 30)); //设置高度

			// 将列头设置到sheet的单元格中
			for (int n = 0; n < columnNum; n++) {
				HSSFCell headerCell = headerRow.createCell(n);
				HSSFRichTextString text = new HSSFRichTextString(columns[n]);
				headerCell.setCellValue(text);
			}

			//将查询出的数据设置到sheet对应的单元格中
			for (int i = 0; i < dataList.size(); i++) {
				Field[] fields = dataList.get(i).getClass().getDeclaredFields();
				HSSFRow dataRow = sheet.createRow(i + 3);//创建所需的行数
				dataRow.setHeight((short) (25 * 20)); //设置高度
				RangeXlsTemplate rangeXlsTemplate = dataList.get(i);
				for (int j = 0; j < fields.length; j++) {
					if (!"".equals(fields[j]) && fields[j] != null) {
						Cell cell = dataRow.createCell(j);
						fields[j].setAccessible(true);
						String fieldName = fields[j].getName();
						String fieldValue = (String) PropertyUtils.getSimpleProperty(rangeXlsTemplate, fieldName);
						cell.setCellValue(fieldValue);
					}
				}
			}
			//让列宽随着导出的列长自动适应
			for (int colNum = 0; colNum < columnNum; colNum++) {
				int columnWidth = sheet.getColumnWidth(colNum) / 256;
				for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
					HSSFRow currentRow;
					//当前行未被使用过
					if (sheet.getRow(rowNum) == null) {
						currentRow = sheet.createRow(rowNum);
					} else {
						currentRow = sheet.getRow(rowNum);
					}
					if (currentRow.getCell(colNum) != null) {
						HSSFCell currentCell = currentRow.getCell(colNum);
						if (currentCell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							int length = currentCell.getStringCellValue().getBytes().length;
							if (columnWidth < length) {
								columnWidth = length;
							}
						}
					}
				}
				if (colNum == 0) {
					sheet.setColumnWidth(colNum, (columnWidth - 2) * 128);
				} else {
					sheet.setColumnWidth(colNum, (columnWidth + 4) * 256);
				}


			}

			if (workbook != null) {
				try {
					FileOutputStream out = new FileOutputStream(path);
					workbook.write(out);
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
